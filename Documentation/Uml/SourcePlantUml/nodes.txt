@startuml


class Node {
    + Node(interface)
    .. Called twice in parallel (first reading and after partitioning) ..
    + SetIndex()
    + AddDofGroupForUnknown(dof_group_per_unknown)
    + SetProcessor()
    - index_
    - processor_
    - dof_group_list_
    - interface_
}

note right of Node : Node objects are for instance stored within FiniteElement \nor DofManager (see UML about operators)


class DofGroup {
    + DofGroup(unknown_component, shape_function_label, Ndof)
    - unknown_component_
    - shape_function_label_
    - dof_list_
    
}


class Dof {
    + Dof()
    + SetProgramWiseIndex(index)
    + SetProcessorWiseOrGhostIndex(index)
    - processor_wise_or_ghost_index_
    - program_wise_index_


}


class UnknownManager{
    + UnknownManager(unknown_parameter_data, mesh_dimension)
    .. Register a new unknown by giving its name and few properties ..
    - RegisterUnknown(unknown)
    - unknown_list_
    - unknown_component_list_per_unknown_
}


class Unknown {
    + Unknown(name, shape_function_label_, degree_of_exactness, Ncomponent)
    - name_
    .. This label is there due to current structure of input file; it will disappear shortly! ..
    - shape_function_label_
    - degree_of_exactness_
    - Ncomponent_
}


class UnknownComponent {
    + UnknownComponent(unknown, index)
    - unknown_
    - index_
}


'Extract dof_group_list_ from Node and link it to both Node and DofGroup.'
class dof_group_list_ << (A,orchid) >>
class Node o.. dof_group_list_
dof_group_list_ "1" *-- "few" DofGroup : contains

'Extract dof_list_ from DofGroup and link it to both DofGroup and Dof.'
class dof_list_ << (A,orchid) >>
class DofGroup o.. dof_list_
dof_list_ "1" *-- "many" Dof : contains

'Extract unknown_list_ from UnknownManager and link it to both UnknownManager and Unknown'
class unknown_list_ << (A,orchid) >>
class UnknownManager o.. unknown_list_
unknown_list_ "1" *-- "few" Unknown : contains


'Extract unknown_component_list_per_unknown_ from UnknownManager and link it to both UnknownManager and UnknownComponent'
class unknown_component_list_per_unknown_ << (A,orchid) >>
class UnknownManager o.. unknown_component_list_per_unknown_
unknown_component_list_per_unknown_ "1" *-- "few" UnknownComponent : contains

'Extract unknown_ from UnknownComponent and link it to both UnknownComponent and Unknown'
class unknown_ << (A,orchid) >>
class UnknownComponent o.. unknown_
unknown_  --  Unknown : is

DofGroup o-- UnknownComponent : contains

@enduml