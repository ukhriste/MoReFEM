//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM
{


    namespace ___VARIABLE_problemName:identifier___NS
    {


        template<class InputParameterDataT>
        void VariationalFormulation::SupplInit(const InputParameterDataT& input_parameter_data)
        {
            // TODO: Put here the initialization operations specific to the variational formulation at hand.
            // This encompass the reading of the input file: if for instance your problem needs the volumic mass,
            // you need to set it here by consulting its value in the input parameter file.
            // The definition of the operators in particular is expected here.
        }
        
        
        inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation::ImplementSnesFunction() const
        {
            // TODO: The three ImplementSnesXXX() methods are there to tell Petsc SNES (Newton) algorithm
            // how to proceed. They must be meaningfully filled if SolveNonLinear() is called.
            // If you do a direct solve and don't want to bother with Newton solve, simply return nullptr. 
        }
        

        inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation::ImplementSnesJacobian() const
        {
            // TODO: Same comment as in ImplementSnesFunction().
        }
        

        inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation::ImplementSnesViewer() const
        {
            // TODO: Same comment as in ImplementSnesFunction().
        }
        
        
        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            // TODO: Same comment as in ImplementSnesFunction().
            // However, this one might be nullptr even if others above are not.
        }
        
        
    } // namespace ___VARIABLE_problemName:identifier___NS
    

} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
