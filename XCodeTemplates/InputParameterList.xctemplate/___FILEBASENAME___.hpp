//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"

# include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp" // TODO Remove it if not relevant!


namespace MoReFEM
{
    
    
    namespace ___VARIABLE_problemName:identifier___NS
    {
        
        
        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            /* TODO Complete with something like:
            highest_dimension = 1,
            neumann = 2
            */
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            /* TODO Complete with something like:
            highest_dimension = 1,
            neumann = 2,
            numbering_subset_test = 3
            */
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            /* TODO Complete with something like:
            velocity = 1,
            pressure = 2
            */
        };
        
        
        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            /* TODO Complete with something like:
             velocity = 1,
             pressure = 2
             */
        };



        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };
        
        
        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            /* TODO Complete with something like:
            monolithic = 
            */
        };
        


        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,
            
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::/* one such line for each item defined earlier */)>,
            
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::/* one such line for each item defined earlier */)>,
        
            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::/* one such line for each item defined earlier */)>,
        
            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::/* one such line for each item defined earlier */)>,
            
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::/* one such line for each item defined earlier */)>,
        
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::/* one such line for each item defined earlier */)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::/* one such line for each item defined earlier */)>,

        
            InputParameter::Result::OutputDirectory
            // TODO: Add the problem-specific input parameters here!
        >;
        

        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;
    
    
    } // namespace ___VARIABLE_problemName:identifier___NS


} // namespace MoReFEM


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP) */
