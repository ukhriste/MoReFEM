import os

# MUST BE RUN IN THE SCRIPTS FOLDER!!!

# Check all the includes are given related to the Sources folder.

if __name__ == '__main__':
    
    src_folder = '../Sources'
    ignore_dir_list = ['ThirdParty/Source', ]
    
    # Run through all C++ files in src folder.
    for root, dirs, files in os.walk(src_folder):
        
        root_without_src_folder = root[len(src_folder)+1:]
        
        do_ignore_current_folder = False
        
        for dir_to_ignore in ignore_dir_list:
            if root_without_src_folder.startswith(dir_to_ignore):
                do_ignore_current_folder = True
                
        if do_ignore_current_folder:
            continue
            
    
        for file in files:
            if file.endswith(".cpp") or file.endswith(".hpp") or file.endswith(".hxx"):
                file_path = "{0}/{1}".format(root, file)
                
                # Open each source file and look for includes with quotes.
                current_stream = open(file_path)
                
                for line in current_stream:
                    if line.startswith('#') and "include" in line and '<' not in line:
                        # Strip the line.
                        line = line.rstrip("\n")
                        
                        # Include with quotes should include a /: there are no files directly in src folder!
                        splitted = line.split("\"")
                        assert(len(splitted) > 1)
                        
                        path_in_include = splitted[1]
                        
                        if '/' not in path_in_include:
                            
                            file_path_without_src_folder = file_path[len(src_folder)+1:]
                            
                            if "hxx" not in path_in_include:
                                
                                # Special case of third party: some includes are within quotes without full path.
                                if root_without_src_folder.startswith('ThirdParty') and path_in_include.endswith(".h"):
                                    continue
                                
                                print("Incorrect include in {0}: {1}".format(file_path_without_src_folder, line))
                            else:
                                
                                # In case of an hxx file included, a suggestion can be computed, as it is typically a file that is in the same folder as the hpp file that includes it.
                                suggestion = "{0}/{1}".format(root_without_src_folder,path_in_include)
                                
                                suggestion = "/".join(suggestion.split('/')[:-1])
                                
                                suggestion = "{0}/{1}".format(suggestion,path_in_include)

                                print("Incorrect include in {0}: {1}; proposal is {2}".format(file_path_without_src_folder, line, suggestion))
                                
                                
        
        
        #print root, dirs, files