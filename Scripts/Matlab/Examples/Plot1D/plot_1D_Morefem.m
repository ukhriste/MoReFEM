clc; close all;

addpath('~/Codes/MoReFEM/Scripts/Matlab/');

load_ressults = 1;
plot_results = 1;
do_pause = 0;
skip_time_steps = 1; % n plot every n time steps otherwise 1 every time step.

% Mesh
x = 0:0.5:10;

if (load_ressults)
        
    dir_hh = '~/Codes/MoReFEM/Scripts/Matlab/Examples/Plot1D/Results/Mesh_1/NumberingSubset_1/';
    
    name = 'solution';
    extension = 'hhdata';

    % Last number enables to load only x files. If 0 all are loaded.
    results_hh = load_files(dir_hh, name, extension, 0);
    
    maximum = max(max(results_hh));
    minimum = min(min(results_hh));
    
end

if (plot_results)
    figure;
    for i=1:(size(results_hh,2))
        if (mod(i, skip_time_steps) == 0)
            plot(x,results_hh(:,i),'-b');
            hold on;
            ylim([minimum-0.1,maximum+0.1]);
            if (do_pause)
                disp(i)
                pause
            else
                pause(0.1)
            end
            hold off;
        end
    end
end
   