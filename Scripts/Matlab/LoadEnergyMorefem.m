function [energy, time] = LoadEnergyMoReFEM(path, name)

energy_filename = [path name '.dat'];
energy_file = fopen(energy_filename, 'rt' );

% header
str = fgetl( energy_file );

field = fscanf(energy_file,'%e %e',[2,inf])';
time = field(:,1);
energy = field(:,2);

fclose(energy_file);

end