def WarningFlags():
    '''
    List of warning flags to consider when compiling with gcc.

    This list is adapted from http://stackoverflow.com/questions/5088460/flags-to-enable-thorough-and-verbose-g-warnings.
    '''

    return ('-Wall',
            '-Wextra',
             #'-Wpedantic', Can't be activated because of third-party error; pragma doesn't work to disable it only for third party.
            '-Wcast-align',
            '-Wcast-qual',
            '-Wconversion',
            '-Wdisabled-optimization',
            '-Wfloat-equal',
            '-Wformat=2',
            '-Wformat-nonliteral',
            '-Wformat-security',
            '-Wformat-y2k',
            '-Wimport',
            '-Winit-self',
            # '-Winline', Tag destructor defined with = default as inline...
            '-Winvalid-pch',
            '-Wmissing-field-initializers',
            '-Wmissing-format-attribute',
            '-Wmissing-include-dirs',
            '-Wmissing-noreturn',
            '-Wpacked',
            '-Wpointer-arith',
            '-Wredundant-decls',
             #'-Wshadow', g++ shadow is too cumbersome: can't name function argument same as a method for instance...
            '-Wstack-protector',
            '-Wstrict-aliasing=2',  
            '-Wswitch-enum',
            '-Wunreachable-code',
            '-Wunused',
            '-Wunused-parameter',
            '-Wvariadic-macros',
            '-Wwrite-strings'
        )
        
def DebugMacroFlags():
    '''Macro flags that are present only in debug mode.
    
    Only macros specific to compiler are given here; NDEBUG for instance is not and is handled elsewhere.
    
    For instance here we use '_LIBCPP_DEBUG2=0' which is a libc++ (STL used by clang) macro to tells when there 
    are overflows in array indexes.
    '''
    return ('_LIBCPP_DEBUG2=0', )        
        
def MiscellaneousFlags(env):
    '''Any flag not yet covered in above categories.
    
    \param[in] env Scons environment.
    '''
    return ('-fdiagnostics-color', '-fPIC', )
