///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 10:40:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "Utilities/Numeric/Numeric.hpp"
#include "FiniteElement/QuadratureRules/Internal/GaussQuadratureFormula.hpp"



namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace GaussQuadratureNS
        {
            
            
            using GaussFormula = QuadratureNS::GaussFormula;
            
            
            void ComputeExactFormula<GaussFormula::Gauss_Lobatto, 1u>
            ::Perform(std::vector<double>& points, std::vector<double>& weights)
            {
                // [1-,1] with weights 1, 1
                points[0] = -1.;
                points[1] = 1.;
                
                weights[0] = 1.;
                weights[1] = 1.;
            }
            
            
            void ComputeExactFormula<GaussFormula::Gauss_Lobatto, 2u>
            ::Perform(std::vector<double>& points, std::vector<double>& weights)
            {
                // [-1, 0, 1] with weights 1/3, 4/3, 1/3
                points[0] = -1.;
                points[1] = 0.;
                points[2] = 1.;
                
                weights[0] = 1./3.;
                weights[1] = 4./3.;
                weights[2] = weights[0];
            }
            
            
            void ComputeExactFormula<GaussFormula::Gauss_Lobatto, 3u>
            ::Perform(std::vector<double>& points, std::vector<double>& weights)
            {
                points[0] = -1.;
                points[1] = ((5. - std::sqrt(5.)) * .2) - 1.;
                points[2] = - points[1];
                points[3] = 1.;
                
                weights[0] = 1. / 6.;
                weights[1] = 10. * weights[0];
                weights[2] = weights[1];
                weights[3] = weights[0];
            }
            
            void ComputeExactFormula<GaussFormula::Gauss, 1u>
            ::Perform(std::vector<double>& points, std::vector<double>& weights)
            {
                points[0] = 0.;
                weights[0] = 2.;
            }
            
            
            void ComputeExactFormula<GaussFormula::Gauss, 2u>
            ::Perform(std::vector<double>& points, std::vector<double>& weights)
            {
                points[0] = (3. - sqrt(3.)) / 3. - 1.;
                points[1] = - points[0];
                
                weights[0] = 1.;
                weights[1] = 1.;
            }
            
            
            void ComputeExactFormula<GaussFormula::Gauss, 3u>
            ::Perform(std::vector<double>& points, std::vector<double>& weights)
            {
                points[0] = - sqrt(15.) / 5.; // Check formula! See #240...
                points[1] = 0.;
                points[2] = - points[0];
                
                weights[0] = 5. / 9.;
                weights[1] = 8. / 9.;
                weights[2] = weights[0];
            }
            
            
            void ComputeFourierCoef(double& cz,
                                    std::vector<double>& cp,
                                    std::vector<double>& dcp,
                                    std::vector<double>& ddcp)
            {
                const auto n = static_cast<unsigned int>(cp.size());
                
                const unsigned int ncp = (n + 1u) / 2u;
                double t1 = -1.;
                double t2 = n + 1.;
                double t3 = 0.;
                double t4 = 2. * n + 1.;
                
                assert(dcp.size() == cp.size()); // Number of quadrature points.
                assert(ddcp.size() == cp.size());
                
                if(n % 2 == 0)
                {
                    assert(ncp < cp.size());
                    cp[ncp] = 1.;
                    for (unsigned int j = ncp; j >= 2; --j)
                    {
                        t1 += 2.;
                        t2 += -1.;
                        t3 += 1.;
                        t4 += -2.;
                        assert(!NumericNS::IsZero(t3));
                        assert(!NumericNS::IsZero(t4));
                        cp[j - 1] = (t1 * t2) / (t3 * t4) * cp[j];
                    }
                    
                    t1 = t1 + 2.;
                    t2 = t2 - 1.;
                    t3 = t3 + 1.;
                    t4 = t4 - 2.;
                    
                    assert(!NumericNS::IsZero(t3));
                    assert(!NumericNS::IsZero(t4));
                    cz = (t1 * t2) / (t3 * t4) * cp[1];
                    
                    for (unsigned int j = 1; j <= ncp; j++)
                    {
                        dcp[j] = (2u * j) * cp[j];
                        ddcp[j] = (2 * j) * dcp[j];
                    }
                }
                else
                {
                    cp[ncp] = 1.;
                    for (unsigned int j = ncp-1; j >= 1; j--)
                    {
                        t1 = t1 + 2.;
                        t2 = t2 - 1.;
                        t3 = t3 + 1.;
                        t4 = t4 - 2.;
                        
                        assert(!NumericNS::IsZero(t3));
                        assert(!NumericNS::IsZero(t4));
                        cp[j] = (t1 * t2) / (t3 * t4) * cp[j+1];
                    }
                    for (unsigned int j = 1; j <= ncp; j++)
                    {
                        dcp[j] = (2 * j - 1) * cp[j];
                        ddcp[j] = (2 * j - 1) * dcp[j];
                    }
                }
            }
            
            
            //! intermediary function used for the computation of Gauss points
            void ComputeLegendrePolAndDerivative(unsigned int n, double theta, double cz,
                                                 const std::vector<double>& cp,
                                                 const std::vector<double>& dcp,
                                                 const std::vector<double>& ddcp,
                                                 double& pb, double& dpb, double& ddpb)
            {
                double cdt = std::cos(2 * theta);
                double sdt = std::sin(2 * theta);
                double cth(0.), sth(0.), chh(0.);
                
                assert(cp.size() == dcp.size());
                assert(cp.size() == ddcp.size());

                if (n % 2 == 0)
                {
                    // n even
                    unsigned int kdo = n / 2;
                    
                    assert(kdo < cp.size());
                    
                    pb = cz / 2;
                    dpb = double(0.);
                    ddpb = double(0.);
                    if (n > 0)
                    {
                        cth = cdt;
                        sth = sdt;
                        for (unsigned int k = 1; k <= kdo; ++k)
                        {
                            //      pb = pb+cp(k)*std::cos(2*k*theta)
                            pb += cp[k] * cth;
                            //      dpb = dpb-(k+k)*cp(k)*std::sin(2*k*theta)
                            dpb -= dcp[k] * sth;
                            ddpb -= ddcp[k] * cth;
                            chh = cdt * cth - sdt * sth;
                            sth = sdt * cth + cdt * sth;
                            cth = chh;
                        }
                    }
                }
                else
                {
                    
                    //  n odd
                    unsigned int kdo = (n + 1) / 2;
                    
                    assert(kdo < cp.size());
                    
                    pb = 0.;
                    dpb = 0.;
                    ddpb = 0.;
                    cth = std::cos(theta);
                    sth = std::sin(theta);
                    
                    for (unsigned int k = 1; k <= kdo; k++)
                    {
                        // 	pb = pb+cp(k)*std::cos((2*k-1)*theta)
                        pb += cp[k] * cth;
                        // dpb = dpb-(k+k-1)*cp(k)*std::sin((2*k-1)*theta)
                        dpb -= dcp[k] * sth;
                        ddpb -= ddcp[k] * cth;
                        chh = cdt * cth-sdt * sth;
                        sth = sdt * cth + cdt * sth;
                        cth = chh;
                    }
                }
            }
            
            
        } // namespace GaussQuadratureNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
