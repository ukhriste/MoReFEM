///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 11:55:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HPP_

# include <memory>
# include <map>

# include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"

# include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_quadrature_rule_per_topology_arg
     *
     * \param[in] quadrature_rule_per_topology Quadrature rule for each relevant topology (if one useful in the Model
     * is not specified there an exception will be thrown when usage is attempted). Do not deallocate this raw pointer:
     * it is assumed the actual object is stored in an object such as \a VariationalFormulation or a \a Model,
     * probably under a const_unique_ptr to avoid bothering with manual deallocation.
     */


    /*!
     * \class doxygen_hide_quadrature_rule_per_topology_nullptr_arg
     *
     * \copydetails doxygen_hide_quadrature_rule_per_topology_arg
     * If nullptr, the rules defined in the \a FEltSpace are used.
     */


    /*!
     * \brief This class list the quadrature rule to use fo each topology.
     *
     * It is intended to be used either in FEltSpace level or in GlobalVariationalOperator level (the latter supersedes
     * the former if specified; if not the operator takes the set of rules defines within its FEltSpace).
     */
    class QuadratureRulePerTopology
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = QuadratureRulePerTopology;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Convenient alias.
        using storage_type = std::map<RefGeomEltNS::TopologyNS::Type, QuadratureRule::const_shared_ptr>;

    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] quadrature_rule_per_topology Quadrature rule to use for each topology.
         */
        explicit QuadratureRulePerTopology(storage_type quadrature_rule_per_topology);


        /*!
         * \brief Constructor to produce default choices for each topology.
         *
         * \param[in] degree_of_exactness Parameter used to determine rule to use for topologies that are built upon
         * PerDegreeOfExactness process.
         * \param[in] shape_function_order Parameter used to determine rule to use for topologies that are built upon
         * PerShapeFunctionOrder process.
         */
        explicit QuadratureRulePerTopology(unsigned int degree_of_exactness,
                                           unsigned int shape_function_order);


        //! Destructor.
        ~QuadratureRulePerTopology() = default;

        //! Copy constructor.
        QuadratureRulePerTopology(const QuadratureRulePerTopology&) = delete;

        //! Move constructor.
        QuadratureRulePerTopology(QuadratureRulePerTopology&&) = delete;

        //! Copy affectation.
        QuadratureRulePerTopology& operator=(const QuadratureRulePerTopology&) = delete;

        //! Move affectation.
        QuadratureRulePerTopology& operator=(QuadratureRulePerTopology&&) = delete;

        ///@}


        /*!
         * \brief Print the list of quadrature rules in storage.
         *
         * This method is mostly there for dev purposes.
         *
         * \param[in,out] out Stream to which object must be written.
         */
        void Print(std::ostream& out) const noexcept;


        //! Access to the list of quadrature rule to use for each topology.
        const storage_type& GetRulePerTopology() const noexcept;

        //! Return the rule to use with a given topology.
        const QuadratureRule& GetRule(RefGeomEltNS::TopologyNS::Type topology) const;


    private:

        /*!
         * \brief Quadrature rule to use for each topology.
         */
        const storage_type quadrature_rule_per_topology_;

    };



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HPP_
