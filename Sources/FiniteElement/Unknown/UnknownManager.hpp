///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 08:44:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HPP_

# include <memory>
# include <vector>
# include <map>

# include "Utilities/Singleton/Singleton.hpp"

# include "Core/InputParameter/FElt/Unknown.hpp"

# include "Geometry/Mesh/GeometricMeshRegion.hpp"

# include "FiniteElement/Unknown/Unknown.hpp"
# include "FiniteElement/Nodes_and_dofs/Exceptions/Dof.hpp"


namespace MoReFEM
{


    /*!
     * \brief Singleton that is aware of all considered unknown, regardless of GodOfDof.
     *
     * Each of the unknowns must be defined in the input parameter file.
     */
    class UnknownManager final : public Utilities::Singleton<UnknownManager>
    {

    public:

        //! Return the name of the class.
        static const std::string& ClassName();

        //! Base type of Domain as input parameter (requested to identify domains in the input parameter data).
        using input_parameter_type = InputParameter::BaseNS::Unknown;


    public:

        /// \name Special members.
        ///@{
        //! Destructor.
        ~UnknownManager() = default;

        //! Copy constructor.
        UnknownManager(const UnknownManager&) = delete;

        //! Move constructor.
        UnknownManager(UnknownManager&&) = default;

        //! Affectation.
        UnknownManager& operator=(const UnknownManager&) = delete;

        //! Affectation.
        UnknownManager& operator=(UnknownManager&&) = delete;

        ///@}


        /*!
         * \brief Create a new Unknown object from the data of the input parameter file.
         *
         */
        template<class UnknownSectionT>
        void Create(const UnknownSectionT& section);


        //! Returns the number of unknowns.
        unsigned int Nunknown() const noexcept;

        //! Get the unknown associated with \a unique_id.
        const Unknown& GetUnknown(unsigned int unique_id) const noexcept;

        //! Get the unknown associated with \a unique_id as a smart pointer.
        Unknown::const_shared_ptr GetUnknownPtr(unsigned int unique_id) const;

        /*!
         * \brief Get the unknown which name is given as argument.
         */
        const Unknown& GetUnknown(const std::string& unknown_name) const;

        /*!
         * \brief Get the unknown which name is given as argument.
         */
        Unknown::const_shared_ptr GetUnknownPtr(const std::string& unknown_name) const;

        //! Access to the list of unknowns.
        const Unknown::vector_const_shared_ptr& GetList() const noexcept;


    private:

        /*!
         * \brief Create a new Unknown, which is a unique id, a name and a nature (scalar or vectorial).
         *
         * \copydetails doxygen_hide_unknown_constructor_args
         */
        void Create(unsigned int unique_id,
                    const std::string& name,
                    const std::string& nature);

    private:

        //! \name Singleton requirements.
        ///@{
        //! Constructor.
        UnknownManager() = default;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<UnknownManager>;
        ///@}


    private:

        //! Register properly unknown in use.
        void RegisterUnknown(const Unknown::const_shared_ptr& unknown);


    private:

        //! List of unknowns.
        Unknown::vector_const_shared_ptr unknown_list_;

    };


    /*!
     * \brief Write in output directory a file that lists all the unknowns.
     *
     * Should be called only on root processor.
     *
     * \param[in] output_directory Output directory in which a file named 'unknowns.hhdata'
     * will be written.
     */
    void WriteUnknownList(const std::string& output_directory);


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/Unknown/UnknownManager.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HPP_
