///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Jun 2015 16:50:41 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_EXTENDED_UNKNOWN_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_EXTENDED_UNKNOWN_HXX_


namespace MoReFEM
{


    inline const Unknown& ExtendedUnknown::GetUnknown() const noexcept
    {
        assert(!(!unknown_));
        return *unknown_;
    }


    inline const Unknown::const_shared_ptr& ExtendedUnknown::GetUnknownPtr() const noexcept
    {
        assert(!(!unknown_));
        return unknown_;
    }


    inline const NumberingSubset& ExtendedUnknown::GetNumberingSubset() const noexcept
    {
        assert(!(!numbering_subset_));
        return *numbering_subset_;
    }


    inline bool operator==(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs)
    {
        return lhs.GetUnknown() == rhs.GetUnknown()
        && lhs.GetNumberingSubset() == rhs.GetNumberingSubset()
        && lhs.GetShapeFunctionLabel() == rhs.GetShapeFunctionLabel();
    }


    inline bool operator<(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs)
    {
        if (lhs.GetUnknown() != rhs.GetUnknown())
            return lhs.GetUnknown() < rhs.GetUnknown();

        if (lhs.GetNumberingSubset() != rhs.GetNumberingSubset())
            return lhs.GetNumberingSubset() < rhs.GetNumberingSubset();

        return lhs.GetShapeFunctionLabel() < rhs.GetShapeFunctionLabel();
    }


    inline const NumberingSubset::const_shared_ptr& ExtendedUnknown::GetNumberingSubsetPtr() const noexcept
    {
        assert(!(!numbering_subset_));
        return numbering_subset_;
    }


    inline const std::string& ExtendedUnknown::GetShapeFunctionLabel() const noexcept
    {
        return shape_function_label_;
    }


    inline UnknownNS::Nature ExtendedUnknown::GetNature() const noexcept
    {
        return GetUnknown().GetNature();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_EXTENDED_UNKNOWN_HXX_
