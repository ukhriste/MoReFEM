///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Nov 2014 09:54:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_IMPL_x_BASIC_REF_F_ELT_HOLDER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_IMPL_x_BASIC_REF_F_ELT_HOLDER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            namespace Impl
            {

                template<class BasicRefFEltT>
                BasicRefFEltHolder<BasicRefFEltT>::BasicRefFEltHolder()
                : basic_ref_felt_(nullptr)
                { }


                template<class BasicRefFEltT>
                const BasicRefFElt& BasicRefFEltHolder<BasicRefFEltT>::GetBasicRefFElt()
                {
                    // If not already existing, built it now!
                    if (basic_ref_felt_ == nullptr)
                        basic_ref_felt_ = std::make_unique<BasicRefFEltT>();

                    return *basic_ref_felt_;
                }


            } // namespace Impl


        } // namepace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup




#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_IMPL_x_BASIC_REF_F_ELT_HOLDER_HXX_
