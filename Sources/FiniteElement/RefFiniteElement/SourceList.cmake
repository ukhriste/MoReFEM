target_sources(${MOREFEM_FELT}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instantiation/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
