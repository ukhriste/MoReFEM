///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_POINT_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_POINT_HPP_

# include <memory>
# include <vector>

# include "Geometry/RefGeometricElt/Instances/Point/Topology/Point.hpp"
# include "Geometry/RefGeometricElt/Instances/Point/ShapeFunction/Point1.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        //! Enum class to "distinguish" P0 and P1.
        enum class PointShapeEnum
        {
            P0,
            P1
        };


        /*!
         * \brief Reference finite element for Point.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Point class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         *
         * \tparam PointShapeEnumT Enum value to "distinguish" P0 and P1. The behaviour of the generated class is
         * exactly the same; the reason to this parameter is that we want a \a Point object to be generated for
         * both "P0" and "P1" shape function labels given in the input file.
         */
        template<PointShapeEnum PointShapeEnumT>
        class Point
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Point,
            Internal::ShapeFunctionNS::Order0,
            InterfaceNS::Nature::none,
            1u
        >
        {

        public:


            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();

        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit Point() = default;

            //! Destructor.
            ~Point() override;

            //! Copy constructor.
            Point(const Point&) = default;

            //! Move constructor.
            Point(Point&&) = default;

            //! Copy affectation.
            Point& operator=(const Point&) = default;

            //! Move affectation.
            Point& operator=(Point&&) = default;

            ///@}

        };


    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_POINT_HPP_
