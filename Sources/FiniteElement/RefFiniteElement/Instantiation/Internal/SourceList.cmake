target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/SpectralHelper.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/GeometryBasedBasicRefFElt.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GeometryBasedBasicRefFElt.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/SpectralHelper.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SpectralHelper.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/ShapeFunction/SourceList.cmake)
