///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "FiniteElement/RefFiniteElement/Instantiation/TriangleP0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM
{
    
    
    namespace RefFEltNS
    {
        
        
        namespace // anonymous
        {
            
            
            __attribute__((unused)) const bool registered =
                Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<TriangleP0>();
            
            
        } // namespace // anonymous
        

        const std::string& TriangleP0::ShapeFunctionLabel()
        {
            static std::string ret("P0");
            return ret;
        }
        
        
        TriangleP0::~TriangleP0() = default;        
        
        
    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
