///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P2_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P2_HPP_

# include <memory>
# include <vector>

# include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"
# include "Geometry/RefGeometricElt/Instances/Triangle/ShapeFunction/Triangle6.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TriangleP1.
         *
         * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
         * the one on edges.
         * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Triangle class.
         * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in RefGeomEltNS::TopologyNS::Triangle traits
         * class is now 'Nvertex + i'.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class TriangleP2 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Triangle,
            RefGeomEltNS::ShapeFunctionNS::Triangle6,
            InterfaceNS::Nature::edge,
            0u
        >
        {

        public:

            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TriangleP2() = default;

            //! Destructor.
            virtual ~TriangleP2() override;

            //! Copy constructor.
            TriangleP2(const TriangleP2&) = default;

            //! Move constructor.
            TriangleP2(TriangleP2&&) = default;

            //! Affectation.
            TriangleP2& operator=(const TriangleP2&) = default;

            //! Affectation.
            TriangleP2& operator=(TriangleP2&&) = default;

            ///@}



        };



    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TRIANGLE_P2_HPP_
