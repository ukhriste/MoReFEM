///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 14:47:48 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        inline unsigned int RefFEltInLocalOperator::Nnode() const noexcept
        {
            return GetUnderlyingRefFElt().Nnode();
        }


        inline unsigned int RefFEltInLocalOperator::Ndof() const noexcept
        {
            return GetUnderlyingRefFElt().Ndof();
        }


        inline const Internal::RefFEltNS::BasicRefFElt& RefFEltInLocalOperator::GetBasicRefFElt() const noexcept
        {
            return GetUnderlyingRefFElt().GetBasicRefFElt();
        }


        inline const Seldon::Vector<int>& RefFEltInLocalOperator::GetLocalNodeIndexList() const noexcept
        {
            return local_node_indexes_;
        }


        inline const Seldon::Vector<int>& RefFEltInLocalOperator::GetLocalDofIndexList() const noexcept
        {
            return local_dof_indexes_;
        }


        inline const Seldon::Vector<int>& RefFEltInLocalOperator::GetLocalDofIndexList(unsigned int component_index) const noexcept
        {
            assert(component_index < local_dof_indexes_per_component_.size());
            return local_dof_indexes_per_component_[component_index];
        }


        inline const Internal::RefFEltNS::RefFEltInFEltSpace& RefFEltInLocalOperator::GetUnderlyingRefFElt() const noexcept
        {
            return ref_felt_;
        }


        inline const ExtendedUnknown& RefFEltInLocalOperator::GetExtendedUnknown() const noexcept
        {
            return GetUnderlyingRefFElt().GetExtendedUnknown();
        }


        inline unsigned int RefFEltInLocalOperator::Ncomponent() const noexcept
        {
            return GetUnderlyingRefFElt().Ncomponent();
        }


        inline unsigned int RefFEltInLocalOperator::GetIndexFirstDofInElementaryData() const noexcept
        {
            assert(static_cast<int>(index_first_dof_) == GetLocalDofIndexList()(0));
            return index_first_dof_;
        }


        inline unsigned int RefFEltInLocalOperator::GetIndexFirstDofInElementaryData(unsigned int component) const noexcept
        {
            assert(component < GetUnderlyingRefFElt().Ncomponent());
            return GetIndexFirstDofInElementaryData() + NdofPerComponent() * component;
        }


        inline unsigned int RefFEltInLocalOperator::NdofPerComponent() const noexcept
        {
            return Ndof_per_component_;
        }


        inline unsigned int RefFEltInLocalOperator::GetMeshDimension() const noexcept
        {
            return GetUnderlyingRefFElt().GetMeshDimension();
        }


        inline unsigned int RefFEltInLocalOperator::GetFEltSpaceDimension() const noexcept
        {
            return GetUnderlyingRefFElt().GetFEltSpaceDimension();
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_ADVANCED_x_REF_F_ELT_IN_LOCAL_OPERATOR_HXX_
