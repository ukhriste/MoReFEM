///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"
#include "FiniteElement/Nodes_and_dofs/Node.hpp"


namespace MoReFEM
{
    
    
    Node::Node(const std::shared_ptr<const NodeBearer>& node_bearer_ptr,
               const ExtendedUnknown& extended_unknown,
               unsigned int Ndof)
    : unknown_(extended_unknown.GetUnknown()),
    shape_function_label_(extended_unknown.GetShapeFunctionLabel())
    {
        assert(Ndof > 0);
        for (unsigned int i = 0u; i < Ndof; ++i)
            dof_list_.push_back(std::make_shared<Dof>(node_bearer_ptr));
        
        const auto& numbering_subset_ptr = extended_unknown.GetNumberingSubsetPtr();
        assert(!(!numbering_subset_ptr));
        RegisterNumberingSubset(numbering_subset_ptr);
    }
    
    
    bool operator<(const Node& lhs, const Node& rhs)
    {
        const auto& lhs_unknown = lhs.GetUnknown();
        const auto& rhs_unknown = rhs.GetUnknown();
        
        if (lhs_unknown != rhs_unknown)
            return lhs_unknown < rhs_unknown;
        
        return lhs.GetShapeFunctionLabel() < rhs.GetShapeFunctionLabel();
    }
    
    
    bool operator==(const Node& lhs, const Node& rhs)
    {
        return lhs.GetUnknown() == rhs.GetUnknown()
            && lhs.GetShapeFunctionLabel() == rhs.GetShapeFunctionLabel();
        
    }
    
    
    void Node::RegisterNumberingSubset(NumberingSubset::const_shared_ptr numbering_subset)
    {
        assert(!(!numbering_subset));
        
        if (!DoBelongToNumberingSubset(*numbering_subset))
            numbering_subset_list_.emplace_back(std::move(numbering_subset));
    }
    

    bool Node::DoBelongToNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto end = numbering_subset_list_.cend();
        
        return std::find_if(numbering_subset_list_.cbegin(),
                            end,
                            [&numbering_subset](const auto& current_ptr)
                            {
                                assert(!(!current_ptr));
                                return *current_ptr == numbering_subset;
                            }) != end;
    }



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
