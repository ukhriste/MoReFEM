///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 18 Sep 2013 11:05:16 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <algorithm>

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        #ifndef NDEBUG
        void AssertNewNumberingSubset(const std::vector<std::pair<unsigned int, unsigned int>>& storage,
                                      const NumberingSubset& numbering_subset);
        #endif // NDEBUG
        
        
        //! Return the const iterator matching \a numbering_subset in \a storage.
        std::vector<std::pair<unsigned int, unsigned int>>::const_iterator
        Iterator(const std::vector<std::pair<unsigned int, unsigned int>>& storage,
                 const NumberingSubset& numbering_subset);

        
        
        //! Returns the index stored in \a storage that matches the input \a numbering_subset.
        unsigned int FindInStorage(const std::vector<std::pair<unsigned int, unsigned int>>& storage,
                                   const NumberingSubset& numbering_subset);
        
        
    } // namespace anonymous
    
    
    Dof::Dof(const std::shared_ptr<const NodeBearer>& node_bearer_ptr)
    : node_bearer_(node_bearer_ptr)
    { }

    
    Dof::~Dof() = default;
    
    
    void Dof::SetProgramWiseIndex(const NumberingSubset& numbering_subset, unsigned int index)
    {
        #ifndef NDEBUG
        AssertNewNumberingSubset(program_wise_index_per_numbering_subset_, numbering_subset);
        #endif // NDEBUG
        
        program_wise_index_per_numbering_subset_.push_back({ numbering_subset.GetUniqueId(), index });
    }
    
    
    void Dof::SetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset, unsigned int index)
    {
        #ifndef NDEBUG
        AssertNewNumberingSubset(processor_wise_or_ghost_index_per_numbering_subset_, numbering_subset);
        #endif // NDEBUG
        
        processor_wise_or_ghost_index_per_numbering_subset_.push_back({ numbering_subset.GetUniqueId(), index });
    }
    
    
    void Dof::SetInternalProcessorWiseOrGhostIndex(unsigned int index)
    {
        assert(internal_processor_wise_or_ghost_index_ == NumericNS::UninitializedIndex<unsigned int>()
               && "Should be allocated only once!");
        internal_processor_wise_or_ghost_index_ = index;
    }
    
    
    unsigned int Dof::GetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset) const
    {
        return FindInStorage(GetProcessorWiseOrGhostIndexPerNumberingSubset(), numbering_subset);
    }
    
    
    unsigned int Dof::GetProgramWiseIndex(const NumberingSubset& numbering_subset) const
    {
        return FindInStorage(GetProgramWiseIndexPerNumberingSubset(), numbering_subset);
    }
    
    
    bool Dof::IsInNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto& program_wise_index_per_numbering_subset = GetProgramWiseIndexPerNumberingSubset();
        return Iterator(program_wise_index_per_numbering_subset, numbering_subset)
        != program_wise_index_per_numbering_subset.cend();
    }
    
    
    namespace // anonymous
    {
        
        
        #ifndef NDEBUG
        void AssertNewNumberingSubset(const std::vector<std::pair<unsigned int, unsigned int>>& storage,
                                      const NumberingSubset& numbering_subset)
        {
            const auto id = numbering_subset.GetUniqueId();
            
            assert(std::find_if(storage.cbegin(),
                                storage.cend(),
                                [id](const auto& pair)
                                {
                                    return pair.first == id;
                                }
                                
                                ) == storage.cend() && "A given numbering subset should appear only once!");
            
            
        }
        #endif // NDEBUG
        
        
        std::vector<std::pair<unsigned int, unsigned int>>::const_iterator
        Iterator(const std::vector<std::pair<unsigned int, unsigned int>>& storage,
                 const NumberingSubset& numbering_subset)
        {
            assert(!storage.empty());
            const auto id = numbering_subset.GetUniqueId();
        
            return std::find_if(storage.cbegin(),
                                storage.cend(),
                                [id](const auto& pair)
                                {
                                    return pair.first == id;
                                });
        }

        
        

        unsigned int FindInStorage(const std::vector<std::pair<unsigned int, unsigned int>>& storage,
                                   const NumberingSubset& numbering_subset)
        {
            auto it = Iterator(storage, numbering_subset);
            assert(it != storage.cend());
            return it->second;
        }
        
        
    } // namespace anonymous



    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
