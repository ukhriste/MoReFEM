///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Sep 2015 15:25:21 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalDiagonalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{
    
    
    void AllocateGlobalMatrix(const GodOfDof& god_of_dof,
                              GlobalMatrix& matrix)
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();
        const auto& col_numbering_subset = matrix.GetColNumberingSubset();

        const auto& matrix_pattern = god_of_dof.GetMatrixPattern(row_numbering_subset, col_numbering_subset);
        
        const auto& mpi = god_of_dof.GetMpi();
        
        const unsigned int Nrow_processor_wise_dof = god_of_dof.NprocessorWiseDof(row_numbering_subset);
        const unsigned int Ncolumn_processor_wise_dof = god_of_dof.NprocessorWiseDof(col_numbering_subset);
        
        namespace Petsc = Wrappers::Petsc;
        
        const unsigned int Nproc = mpi.template Nprocessor<unsigned int>();
        
        if (Nproc == 1)
        {
            matrix.InitSequentialMatrix(Nrow_processor_wise_dof, Ncolumn_processor_wise_dof,
                                        matrix_pattern, mpi,
                                        __FILE__, __LINE__);
        }
        else
        {
            const unsigned int Nrow_program_wise_dof = god_of_dof.NprogramWiseDof(row_numbering_subset);
            const unsigned int Ncolumn_program_wise_dof = god_of_dof.NprogramWiseDof(col_numbering_subset);
            
            matrix.InitParallelMatrix(Nrow_processor_wise_dof, Ncolumn_processor_wise_dof,
                                      Nrow_program_wise_dof, Ncolumn_program_wise_dof,
                                      matrix_pattern,
                                      mpi,
                                      __FILE__, __LINE__);
        }
        
        matrix.ZeroEntries(__FILE__, __LINE__);
    }
    
    
    void AllocateGlobalMatrix(const GodOfDof& god_of_dof,
                              GlobalDiagonalMatrix& matrix)
    {
        const auto& numbering_subset = matrix.GetRowNumberingSubset();
        assert(numbering_subset == matrix.GetColNumberingSubset() && "Square matrix expected!");
        
        // ===================================================
        // Determine the (simplistic) matrix pattern of the diagonal matrix.
        // ===================================================
        const auto Nrow_proc_wise = god_of_dof.NprocessorWiseDof(numbering_subset);
        std::vector<std::vector<PetscInt>> non_zero_slots_per_local_row(static_cast<std::size_t>(Nrow_proc_wise));

        const auto& dof_list = god_of_dof.GetProcessorWiseDofList();
        
        for (const auto& dof_ptr : dof_list)
        {
            assert(!(!dof_ptr));
            const auto& dof = *dof_ptr;
            
            if (!dof.IsInNumberingSubset(numbering_subset))
                continue;
            
            const auto proc_wise_index = static_cast<std::size_t>(dof.GetProcessorWiseOrGhostIndex(numbering_subset));
            assert(proc_wise_index < static_cast<std::size_t>(Nrow_proc_wise));
            assert(non_zero_slots_per_local_row[proc_wise_index].empty() && "Each should be filled once!");
            const auto program_wise_index = static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset));
            non_zero_slots_per_local_row[proc_wise_index] = { program_wise_index };
        }
        
        Wrappers::Petsc::MatrixPattern diagonal_pattern(non_zero_slots_per_local_row);
        
        // ===================================================
        // Now fill the structure of the matrix as in AllocateGlobalMatrix().
        // ===================================================

        const auto& mpi = god_of_dof.GetMpi();
        
        namespace Petsc = Wrappers::Petsc;
        
        const unsigned int Nproc = mpi.template Nprocessor<unsigned int>();
        
        if (Nproc == 1)
        {
            matrix.InitSequentialMatrix(Nrow_proc_wise, Nrow_proc_wise,
                                        diagonal_pattern, mpi,
                                        __FILE__, __LINE__);
        }
        else
        {
            const unsigned int Nrow_program_wise = god_of_dof.NprogramWiseDof(numbering_subset);
            
            matrix.InitParallelMatrix(Nrow_proc_wise, Nrow_proc_wise,
                                      Nrow_program_wise, Nrow_program_wise,
                                      diagonal_pattern,
                                      mpi,
                                      __FILE__, __LINE__);
        }
        
        matrix.ZeroEntries(__FILE__, __LINE__);
    }

    
    
    void AllocateGlobalVector(const GodOfDof& god_of_dof,
                              GlobalVector& vector)
    {        
        const auto& mpi = god_of_dof.GetMpi();
        
        const auto& numbering_subset = vector.GetNumberingSubset();
        
        const unsigned int Nprocessor_wise_dof = god_of_dof.NprocessorWiseDof(numbering_subset);
        
        namespace Petsc = Wrappers::Petsc;
        
        const unsigned int Nproc = mpi.template Nprocessor<unsigned int>();
        
        if (Nproc == 1)
            vector.InitSequentialVector(Nprocessor_wise_dof, mpi, __FILE__, __LINE__);
        else
        {
            std::vector<PetscInt> ghosted_dof_index_list;
            
            const unsigned int Nprogram_wise_dof = god_of_dof.NprogramWiseDof(numbering_subset);
            
            {
                const auto& ghosted_dof_list = god_of_dof.GetGhostedDofList();
                ghosted_dof_index_list.reserve(ghosted_dof_list.size());
                
                for (const auto& dof_ptr : ghosted_dof_list)
                {
                    assert((!(!dof_ptr)));
                    const auto& dof = *dof_ptr;
                    
                    if (dof.IsInNumberingSubset(numbering_subset))
                        ghosted_dof_index_list.push_back(static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset)));
                }
            }
            
            assert(std::is_sorted(ghosted_dof_index_list.cbegin(), ghosted_dof_index_list.cend()));
            
            vector.InitMpiVectorWithGhost(Nprocessor_wise_dof,
                                          Nprogram_wise_dof,
                                          ghosted_dof_index_list,
                                          mpi,
                                          __FILE__, __LINE__);
            
        }
        
        vector.ZeroEntries(__FILE__, __LINE__);
    }



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
