///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"



namespace MoReFEM
{
    
    
    const std::string& GodOfDofManager::ClassName()
    {
        static std::string ret("GodOfDofManager");
        return ret;
    }
    
    
    GodOfDofManager::GodOfDofManager()
    {
        list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }
    
    
    GodOfDof::shared_ptr GodOfDofManager::GetGodOfDofPtr(unsigned int unique_id) const
    {
        auto it = list_.find(unique_id);
        
        assert(it != list_.cend());
        assert(!(!(it->second)));
        
        return it->second;
    }
    
    
    void ClearGodOfDofTemporaryData()
    {
        const auto& storage = GodOfDofManager::GetInstance().GetStorage();
        
        for (const auto& pair : storage)
        {
            auto& god_of_dof_ptr = pair.second;
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;
            god_of_dof.ClearTemporaryData();
        }
    }
  

} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
