///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 3 Apr 2015 17:09:56 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CONNECTIVITY_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CONNECTIVITY_HPP_


# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            //! Enum class to make the call to ComputeNodeConnectivity() more readable than with a mere boolean.
            enum class KeepSelfConnexion { yes, no };


            /*!
             * \brief Convenient alias.
             *
             * \internal <b><tt>[internal]</tt></b> Unordered map use the address and not the NodeBearer object in its hash table.
             */
            using connectivity_type = std::unordered_map<NodeBearer::shared_ptr, NodeBearer::vector_shared_ptr>;



            /*!
             * \brief Compute the connectivity between \a NodeBearer.
             *
             * In MoReFEM, parallelism ensures that all nodes and dofs located on a same \a Interface are
             * managed by the same processor; that's why we need first to define a connectivity between
             * \a NodeBearer prior to the processor reduction.
             *
             * \param[in] felt_space_list List of all finite element spaces within the god of dof (current
             * function is called once per \a GodOfDof.
             * \param[in] mpi_rank Mpi rank, as given by Wrappers::Mpi::GetRank<int>().
             * \param[in] Nnode_bearer Number of node bearers.
             * \param[in] KeepSelfConnexion Whether we keep auto-association or not for each node bearer.
             *
             * \return For each node bearer, the list of connected node bearers.
             */
            connectivity_type
            ComputeNodeBearerConnectivity(const FEltSpace::vector_unique_ptr& felt_space_list,
                                          const unsigned int mpi_rank,
                                          const std::size_t Nnode_bearer,
                                          KeepSelfConnexion KeepSelfConnexion);

        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_CONNECTIVITY_HPP_
