///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Impl/FEltSpaceInternalStorage.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {

            
            namespace Impl
            {
                
                
                InternalStorage
                ::InternalStorage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                  LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_ref_felt_space)
                : ::MoReFEM::Crtp::CrtpMpi<InternalStorage>(mpi),
                felt_list_per_ref_felt_space_(std::move(felt_list_per_ref_felt_space))
                { }
                
                
                bool InternalStorage::IsEmpty() const noexcept
                {
                    return GetLocalFEltSpacePerRefLocalFEltSpace().empty();
                }
                
                          
            } // namespace Impl
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
