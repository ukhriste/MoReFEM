///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 May 2014 13:48:55 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_COMPUTATIONS_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_COMPUTATIONS_HXX_



namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            template<MpiScale MpiScaleT>
            inline void ComputeDofIndexesForNumberingSubset(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                            DofNumberingScheme dof_numbering_scheme,
                                                            const NumberingSubset::const_shared_ptr& numbering_subset,
                                                            unsigned int& current_dof_index)
            {

                Dof::vector_shared_ptr buf; // to comply with Impl::ComputeDofIndexesHelper() prototype, but will remain
                // empty!


                switch(MpiScaleT)
                {
                    case MpiScale::program_wise:
                    {
                        Impl::ComputeDofIndexesHelper::Perform
                        <
                            TypeDofIndex::program_wise_per_numbering_subset,
                            Impl::DoProduceDofList::no
                        >(node_bearer_list,
                          dof_numbering_scheme,
                          numbering_subset,
                          current_dof_index,
                          buf);
                        break;
                    }
                    case MpiScale::processor_wise:
                    {

                        Impl::ComputeDofIndexesHelper::Perform
                        <
                            TypeDofIndex::processor_wise_per_numbering_subset,
                            Impl::DoProduceDofList::no
                        >(node_bearer_list,
                          dof_numbering_scheme,
                          numbering_subset,
                          current_dof_index,
                          buf);
                        break;

                    }
                }

                assert(buf.empty());
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_COMPUTATIONS_HXX_
