///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 14:51:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            inline unsigned int NdofHolder::NprogramWiseDof() const noexcept
            {
                return Nprogram_wise_dof_;
            }


            inline unsigned int NdofHolder::NprocessorWiseDof() const noexcept
            {
                return Nprocessor_wise_dof_;
            }


            inline const std::map<unsigned int, unsigned int>& NdofHolder::NprocessorWiseDofPerNumberingSubset() const noexcept
            {
                assert(!Nprocessor_wise_dof_per_numbering_subset_.empty());
                return Nprocessor_wise_dof_per_numbering_subset_;
            }


            inline  const std::map<unsigned int, unsigned int>& NdofHolder::NprogramWiseDofPerNumberingSubset() const noexcept
            {
                assert(!Nprogram_wise_dof_per_numbering_subset_.empty());
                return Nprogram_wise_dof_per_numbering_subset_;
            }


            inline unsigned int NdofHolder::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
            {
                const auto& Nprocessor_wise_dof_per_numbering_subset = NprocessorWiseDofPerNumberingSubset();

                const auto it = Nprocessor_wise_dof_per_numbering_subset.find(numbering_subset.GetUniqueId());

                assert(it != Nprocessor_wise_dof_per_numbering_subset.cend());
                return it->second;
            }


            inline unsigned int NdofHolder::NprogramWiseDof(const NumberingSubset& numbering_subset) const
            {
                const auto& Nprogram_wise_dof_per_numbering_subset = NprogramWiseDofPerNumberingSubset();

                const auto it = Nprogram_wise_dof_per_numbering_subset.find(numbering_subset.GetUniqueId());

                assert(it != Nprogram_wise_dof_per_numbering_subset.cend());
                return it->second;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HXX_
