///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 14:51:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HPP_

# include <map>

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
# include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Structure that holds the number of dofs for different configurations.
             *
             * This structure is an helper structure to be used only by GodOfDof.
             */
            class NdofHolder final
            {
            public:

                //! Alias for unique_ptr.
                using const_unique_ptr = std::unique_ptr<const NdofHolder>;


                /*!
                 * \brief Compute the number of dofs (both program- and processor-wise).
                 *
                 * \param[in] program_wise_node_bearer_list Node bearer list before processor-wise reduction.
                 * \param[in] mpi_rank Rank of the current processor.
                 * \param[in] numbering_subset_list List of \a NumberingSubset.
                 *
                 */
                NdofHolder(const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                           const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                           const unsigned int mpi_rank);

                //! Destructor.
                ~NdofHolder() = default;

                //! Copy constructor.
                NdofHolder(const NdofHolder&) = delete;

                //! Move constructor.
                NdofHolder(NdofHolder&&) = delete;

                //! Copy affectation.
                NdofHolder& operator=(const NdofHolder&) = delete;

                //! Move affectation.
                NdofHolder& operator=(NdofHolder&&) = delete;

                //! Get the number of program-wise dof.
                unsigned int NprogramWiseDof() const noexcept;

                //! Get the number of processor-wise dof (ghost excluded).
                unsigned int NprocessorWiseDof() const noexcept;

                //! Get the number of processor-wise dof (ghost excluded) within a \a numbering_subset.
                unsigned int NprocessorWiseDof(const NumberingSubset& numbering_subset) const;

                //! Get the number of program-wise dof within a \a numbering_subset.
                unsigned int NprogramWiseDof(const NumberingSubset& numbering_subset) const;

            private:

                //! Accessor to the number of processor-wise dof per numbering subset.
                const std::map<unsigned int, unsigned int>& NprocessorWiseDofPerNumberingSubset() const noexcept;

                //! Accessor to the number of program-wise dof per numbering subset.
                const std::map<unsigned int, unsigned int>& NprogramWiseDofPerNumberingSubset() const noexcept;

            private:

                //! Number of program-wise dof.
                unsigned int Nprogram_wise_dof_ = 0u;

                //! Number of processor-wise dof (ghost excluded).
                unsigned int Nprocessor_wise_dof_ = 0u;

                /*!
                 * \brief Number of processor-wise dof for each numbering subset.
                 *
                 * . Key is numbering subset id.
                 * . Value is the number of dofs.
                 */
                std::map<unsigned int, unsigned int> Nprocessor_wise_dof_per_numbering_subset_;


                /*!
                 * \brief Number of program-wise dof for each numbering subset.
                 *
                 * . Key is numbering subset id.
                 * . Value is the number of dofs.
                 */
                std::map<unsigned int, unsigned int> Nprogram_wise_dof_per_numbering_subset_;

            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_NDOF_HOLDER_HPP_
