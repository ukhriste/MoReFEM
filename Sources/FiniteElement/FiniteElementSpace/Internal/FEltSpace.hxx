///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            template<unsigned int UniqueIdT, class InputParameterDataT>
            Domain& ExtractDomain(const InputParameterDataT& input_parameter_data)
            {
                namespace IPL = Utilities::InputParameterListNS;
                using FEltSpaceNS = InputParameter::FEltSpace<UniqueIdT>;

                const auto domain_index = IPL::Extract<typename FEltSpaceNS::DomainIndex>::Value(input_parameter_data);

                return DomainManager::GetInstance().GetNonCstDomain(domain_index);
            }



            template<class SectionT>
            ExtendedUnknown::vector_const_shared_ptr
            ExtractExtendedUnknownList(const SectionT& section)
            {
                namespace ipl = Internal::InputParameterListNS;

                decltype(auto) unknown_name_list = ipl::ExtractParameter<typename SectionT::UnknownList>(section);
                decltype(auto) shape_function_list = ipl::ExtractParameter<typename SectionT::ShapeFunctionList>(section);
                decltype(auto) numbering_subset_list = ipl::ExtractParameter<typename SectionT::NumberingSubsetList>(section);

                const auto unique_id = section.GetUniqueId();

                if (unknown_name_list.size() != numbering_subset_list.size())
                    throw Exception("Error in input parameter file: the number of numbering subsets and the number "
                                    "of unknowns in finite element space " + std::to_string(unique_id) + " should be "
                                    "the same.", __FILE__, __LINE__);

                if (unknown_name_list.size() != shape_function_list.size())
                    throw Exception("Error in input parameter file: the number of numbering subsets and the number "
                                    "of shape functions in finite element space " + std::to_string(unique_id) + " should be "
                                    "the same.", __FILE__, __LINE__);

                const auto& unknown_manager = UnknownManager::GetInstance();
                auto& numbering_subset_manager =
                    Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();

                const auto size = unknown_name_list.size();

                ExtendedUnknown::vector_const_shared_ptr ret;

                for (std::size_t i = 0; i < size; ++i)
                {
                    auto&& ptr = std::make_shared<ExtendedUnknown>(unknown_manager.GetUnknownPtr(unknown_name_list[i]),
                                                                   numbering_subset_manager.GetNumberingSubsetPtr(numbering_subset_list[i]),
                                                                   shape_function_list[i]);

                    ret.emplace_back(std::move(ptr));
                }

                return ret;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_HXX_
