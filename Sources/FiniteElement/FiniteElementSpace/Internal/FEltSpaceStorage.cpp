///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <cassert>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpaceStorage.hpp"



namespace MoReFEM
{
    
    
    namespace Internal
    {

        
        namespace FEltSpaceNS
        {
            
            
            namespace // anonymous
            {
                
                
                
  
                /*!
                 * \brief Return the internal storage matching the given \a domain. If the domain doesn't exist yet,
                 * compute it on the fly.
                 *
                 * \param[in] full_content The complete storage data of the finite element space.
                 * \param[in] domain We actually want the subset of the finite element space that is included in this
                 * domain.
                 * \param[in,out] mutex Mutex of InternalStorage() that controls access to the internal storage per
                 * domain.
                 * \param[in,out] storage_per_domain List of all known storages per domain. If \a domain is not known,
                 * it is added in the output.
                 *
                 * \return The storage associated to \a domain.
                 */
                Impl::InternalStorage& AddOrFetchDomainStorage(const LocalFEltSpacePerRefLocalFEltSpace& full_content,
                                                               const Domain& domain,
                                                               const ::MoReFEM::Wrappers::Mpi& mpi,
                                                               std::mutex& mutex,
                                                               std::unordered_map<unsigned int, Impl::InternalStorage>& storage_per_domain);
                
                /*!
                 * \brief An helper function of AddOrFetchDomainStorage() in charge of computing a new storage for a
                 * domain that wasn't stored yet.
                 */
                LocalFEltSpacePerRefLocalFEltSpace ComputeFEltListForDomain(const LocalFEltSpacePerRefLocalFEltSpace& full_content,
                                                                           const Domain& domain);
                

                
                
                
            } // namespace anonymous


            Storage::Storage(const ::MoReFEM::Wrappers::Mpi& mpi,
                             LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_type)
            :  ::MoReFEM::Crtp::CrtpMpi<Storage>(mpi),
            internal_storage_(Impl::InternalStorage(mpi, std::move(felt_list_per_type)))
            {
                GetNonCstFEltStoragePerDomain().max_load_factor(Utilities::DefaultMaxLoadFactor());
            }
            
            
            const LocalFEltSpacePerRefLocalFEltSpace& Storage::GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const
            {
                // Check whether the domain is already known in the class.
                auto& felt_storage_for_domain = AddOrFetchDomainStorage(GetLocalFEltSpacePerRefLocalFEltSpace(),
                                                                        domain,
                                                                        GetMpi(),
                                                                        GetMutex(),
                                                                        GetNonCstFEltStoragePerDomain());
                
                return felt_storage_for_domain.GetLocalFEltSpacePerRefLocalFEltSpace();
            }
            
            
            bool Storage::IsEmpty() const noexcept
            {
                return GetStorage().IsEmpty();
            }
            
            
            namespace // anonymous
            {
                
                
                Impl::InternalStorage& AddOrFetchDomainStorage(const LocalFEltSpacePerRefLocalFEltSpace& full_content,
                                                               const Domain& domain,
                                                               const ::MoReFEM::Wrappers::Mpi& mpi,
                                                               std::mutex& mutex,
                                                               std::unordered_map<unsigned int, Impl::InternalStorage>& storage_per_domain)
                {
                    const auto domain_unique_id = domain.GetUniqueId();
                    auto it = storage_per_domain.find(domain_unique_id);
                    
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        
                        if (it == storage_per_domain.cend())
                        {
                            auto&& pair = std::make_pair(domain_unique_id,
                                                         Impl::InternalStorage(mpi,
                                                                               ComputeFEltListForDomain(full_content,
                                                                                                             domain)));
                            
                            auto check = storage_per_domain.emplace(std::move(pair));
                            
                            assert(check.second && "The id was supposed not to exist, otherwise the test above would have fail "
                                   "and we wouldn't be in this block.");
                            
                            it = check.first;
                        }
                    }

                    assert(it != storage_per_domain.cend());
                    return it->second;
                }

                
                
                LocalFEltSpacePerRefLocalFEltSpace ComputeFEltListForDomain(const LocalFEltSpacePerRefLocalFEltSpace& full_content,
                                                                       const Domain& domain)
                {
                    LocalFEltSpacePerRefLocalFEltSpace ret;
                    
                    for (const auto& pair : full_content)
                    {
                        auto& ref_felt_space_ptr = pair.first;
                        const auto& complete_local_felt_space_list = pair.second;
                        
                        LocalFEltSpace::per_geom_elt_index local_felt_space_list;
                        local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                        
                        for (const auto& local_felt_space_pair : complete_local_felt_space_list)
                        {
                            const auto& local_felt_space_ptr = local_felt_space_pair.second;
                            assert(!(!local_felt_space_ptr));
                            
                            const auto& local_felt_space = *local_felt_space_ptr;
                            
                            if (IsLocalFEltSpaceInDomain(local_felt_space, domain))
                                local_felt_space_list.insert({local_felt_space.GetGeometricElt().GetIndex(), local_felt_space_ptr});
                        }
                        
                        if (!local_felt_space_list.empty())
                        {
                            auto&& copy_ref_felt_space_ptr
                                = std::make_unique<Internal::RefFEltNS::RefLocalFEltSpace>(*ref_felt_space_ptr);
                            ret.push_back({ std::move(copy_ref_felt_space_ptr), local_felt_space_list});
                        }
                    }
                    
                    return ret;
                }
                
                
                
            } // namespace anonymous

            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
