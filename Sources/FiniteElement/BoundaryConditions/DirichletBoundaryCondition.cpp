///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Jan 2014 13:56:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <iostream>
#include <string>

#include "Utilities/Containers/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{
    
    
    const std::string& DirichletBoundaryCondition::ClassName()
    {
        static std::string ret("DirichletBoundaryCondition");
        return ret;
    }
    
    
    DirichletBoundaryCondition::DirichletBoundaryCondition(const unsigned int unique_id,
                                                           const std::string& name,
                                                           const Domain& domain,
                                                           const Unknown& unknown,
                                                           const std::vector<double>& value_per_component,
                                                           const std::string& component,
                                                           bool is_mutable,
                                                           bool may_overlap)
    : unique_id_parent(unique_id),
    name_(name),
    domain_(domain),
    unknown_(unknown),
    component_manager_(Internal::BoundaryConditionNS::ComponentFactory::GetInstance().CreateFromName(component)),
    value_per_component_(value_per_component),
    processor_wise_method_(may_overlap ? processor_wise_method::brute_force : processor_wise_method::fast)
    # ifndef NDEBUG
    ,
    is_mutable_(is_mutable)
    # endif // NDEBUG
    {
        static_cast<void>(is_mutable);
    }
    
      
    double DirichletBoundaryCondition::GetValueForComponent(unsigned int component) const
    {
        assert(component < value_per_component_.size());
        return value_per_component_[component];
    }
    
    
    namespace // anonymous
    {
        
        
        bool BruteForceIsProcessorWiseOrGhostDof(const GodOfDof& god_of_dof,
                                                 const Dof::shared_ptr& dof_ptr)
        {
            const auto& god_of_dof_processor_dof_list = god_of_dof.GetProcessorWiseDofList();
            const auto& god_of_dof_ghost_dof_list = god_of_dof.GetGhostedDofList();
            
            const auto begin_god_of_dof_processor_dof_list = god_of_dof_processor_dof_list.cbegin();
            const auto begin_god_of_dof_ghost_dof_list = god_of_dof_ghost_dof_list.cbegin();
            
            const auto end_god_of_dof_processor_dof_list = god_of_dof_processor_dof_list.cend();
            const auto end_god_of_dof_ghost_dof_list = god_of_dof_ghost_dof_list.cend();
            
            
            // Search current dof in the list of processor-wise ones.
            bool ret = (std::find(begin_god_of_dof_processor_dof_list,
                                    end_god_of_dof_processor_dof_list,
                                    dof_ptr) != end_god_of_dof_processor_dof_list);
            
            // Search current dof in the list of ghosted ones.
            if (!ret)
                ret = (std::find(begin_god_of_dof_ghost_dof_list,
                                   end_god_of_dof_ghost_dof_list,
                                   dof_ptr) != end_god_of_dof_ghost_dof_list);

            return ret;
        }
        
        
        
    } // namespace anonymous

    
    
    void DirichletBoundaryCondition::ShrinkToProcessorWise(const GodOfDof& god_of_dof)
    {
        assert(GetDomain().GetGeometricMeshRegionIdentifier() == god_of_dof.GetUniqueId());
        
        // We can drop here the Dof which count is 1 (it means another processor is in charge of it - the Dof is
        // then expected to have been removed everywhere except in present class).
        std::vector<std::pair<Dof::shared_ptr, double>> reduced_initial_dof_value_list;
        
        auto& initial_dof_value_list = GetNonCstInitialDofValueList();
        
        reduced_initial_dof_value_list.reserve(initial_dof_value_list.size());
        
        auto& dof_list = GetNonCstDofList();
        Dof::vector_shared_ptr reduced_dof_list;
        
        reduced_dof_list.reserve(dof_list.size());
        
        const auto chosen_processor_wise_method = processor_wise_method_;
        
        auto is_dof_relevant_for_processor = [chosen_processor_wise_method, &god_of_dof](const auto& dof_ptr)
        {
            assert(!(!dof_ptr));
            
            switch(chosen_processor_wise_method)
            {
                case processor_wise_method::fast:
                {
                    // If the dof is not relevant on the processor, only Dirichlet boundary conditions still hold a handle on it.
                    // As this might be changed in a refactoring of GodOfDof initialization, a more secure (but less efficient)
                    // computation is performed in debug mode.
                    const bool ret = dof_ptr.use_count() > 2; // because Dofs are in initial_dof_value_list and dof_list_.
                    // \todo #619 When initial_dof_value_list is removed, replace this
                    // by dof_ptr.unique().
                    
                    #ifndef NDEBUG
                    assert(ret == BruteForceIsProcessorWiseOrGhostDof(god_of_dof, dof_ptr)
                           && "If not, it likely means the assumption under which current lambda works is now "
                           "incorrect, probably following a refactoring of MoReFEM that let Dofs not "
                           "relevant for current processor live longer that previously (currently it is assumed "
                           "non relevant Dofs no longer exist except in current class).");
                    #endif // NDEBUG
                    
                    return ret;
                }
                case processor_wise_method::brute_force:
                {
                    return BruteForceIsProcessorWiseOrGhostDof(god_of_dof, dof_ptr);
                }                    
            }
            
            assert(false && "Should not be reached!");
            exit(EXIT_FAILURE);
        };
        
        for (const auto& pair : initial_dof_value_list)
        {
            const auto& dof_ptr = pair.first;
            
            if (is_dof_relevant_for_processor(dof_ptr))
                reduced_initial_dof_value_list.push_back(pair);
        }
        
        std::copy_if(dof_list.cbegin(),
                     dof_list.cend(),
                     std::back_inserter(reduced_dof_list),
                     is_dof_relevant_for_processor);
        
        std::swap(reduced_initial_dof_value_list, initial_dof_value_list);
        std::swap(reduced_dof_list, dof_list_);

    }

    
    void DirichletBoundaryCondition::ComputeDofList(NodeBearer::vector_shared_ptr&& node_bearer_list)
    {
        // First determine the components upon which a condition is applied and the associated values.
        std::map<unsigned int, double> value_per_component;
        
        {
            const auto& component_manager = GetComponentManager();
            const auto Nactive_component = component_manager.Nactive();
            
            for (unsigned int i = 0u; i < Nactive_component; ++i)
                value_per_component.insert({ component_manager.ActiveComponent(i), GetValueForComponent(i) });
        }        
        
        // Then iterate to identify the dofs concerned by the boundary condition and affect the values determined
        // above.
        const auto& unknown = GetUnknown();
        
        auto& dof_list = GetNonCstDofList();
        auto& initial_dof_value_list = initial_dof_value_list_; // do not use the accessor as it is empty and would
                                                                // actually trigger an assert.
        
        
        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            const auto& node_bearer = *node_bearer_ptr;
            
            if (!node_bearer.IsUnknown(unknown))
                continue;
            
            // In this very specific case, we want all \a Node related to the \a Unknown regardless of the shape
            // function label. So we fetch all nodes and filter a bit later to keep only those related to the unknown
            // (providing an accessor in \a NodeBearer would be error prone as it most cases shape function label
            // is to be strictly enforced).
            auto node_list = node_bearer.GetNodeList();
            
            for (const auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;
                
                if (node.GetUnknown() != unknown)
                    continue;
                
                for (const auto& pair : value_per_component)
                {
                    const auto component_index = pair.first;
                    const auto value = pair.second;
                    
                    initial_dof_value_list.push_back({node.GetDofPtr(component_index), value});
                    dof_list.push_back(node.GetDofPtr(component_index));
                }
            }
        }        
    }
    
    
    void DirichletBoundaryCondition::SetDofListForNumberingSubset(const NumberingSubset& numbering_subset,
                                                                  Dof::vector_shared_ptr&& dof_list)
    {
        auto& storage = GetNonCstDofStorage();
        
        const auto numbering_subset_id = numbering_subset.GetUniqueId();
        
        assert(std::find_if(storage.cbegin(),
                            storage.cend(),
                            [numbering_subset_id](const auto& pair)
                            {
                                return pair.first == numbering_subset_id;
                            }) == storage.cend() && "A numbering subset should not be present twice!");
        
        Utilities::PointerComparison::Map<Dof::shared_ptr, PetscScalar> value_per_dof_in_numbering_subset;
        
        const auto& value_per_dof = GetInitialDofValueList();
        const auto begin_value_per_dof = value_per_dof.cbegin();
        const auto end_value_per_dof = value_per_dof.cend();
        
        for (const auto& dof_ptr : dof_list)
        {
            assert((!(!dof_ptr)));
            
            auto it = std::find_if(begin_value_per_dof,
                                   end_value_per_dof,
                                   [&dof_ptr](const auto& pair)
                                   {
                                       return pair.first == dof_ptr;
                                   });
            
            assert(it != end_value_per_dof);
            
            auto check = value_per_dof_in_numbering_subset.insert(*it);
            
            assert(check.second && "A given dof should be present only once!");
            static_cast<void>(check);
        }
        
        storage.push_back(std::make_pair(numbering_subset_id,
                                         Internal::BoundaryConditionNS::DofStorage(numbering_subset,
                                                                              std::move(value_per_dof_in_numbering_subset))));
    }
    
    
    const Internal::BoundaryConditionNS::DofStorage& DirichletBoundaryCondition
    ::GetDofStorage(const NumberingSubset& numbering_subset) const noexcept
    {
        const auto& dof_storage = GetDofStorage();
        
        const auto numbering_subset_id = numbering_subset.GetUniqueId();
        
        const auto it = std::find_if(dof_storage.cbegin(),
                                     dof_storage.cend(),
                                     [numbering_subset_id](const auto& pair)
                                     {
                                         return pair.first == numbering_subset_id;
                                     });
        assert(it != dof_storage.cend());
        return  it->second;
    }
        
    
    void DirichletBoundaryCondition::ConsiderNumberingSubset(const NumberingSubset& numbering_subset)
    {
        if (!IsNumberingSubset(numbering_subset))
        {
            auto& dof_storage = GetNonCstDofStorage();
            dof_storage.push_back({numbering_subset.GetUniqueId(), Internal::BoundaryConditionNS::DofStorage()});
        }
    }
    
    
    bool DirichletBoundaryCondition::IsNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto& dof_storage = GetDofStorage();
        
        const auto numbering_subset_id = numbering_subset.GetUniqueId();
     
        const auto end = dof_storage.cend();
        
        const auto it = std::find_if(dof_storage.cbegin(),
                                     end,
                                     [numbering_subset_id](const auto& pair)
                                     {
                                         return pair.first == numbering_subset_id;
                                     });
        
        return it != end;
    }
    
    
    void DirichletBoundaryCondition::ClearInitialDofValueList()
    {
        #ifndef NDEBUG
        is_cleared_ = true;
        #endif // NDEBUG
        
        initial_dof_value_list_.clear();
    }
    
    
    void DirichletBoundaryCondition::UpdateValues(const GlobalVector& new_values)
    {
        // Read the values from the GlobalVector.
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(new_values, __FILE__, __LINE__);
        
        const auto Nvalues = content.GetSize(__FILE__, __LINE__);
        
        std::vector<PetscScalar> new_values_as_std_vector(static_cast<std::size_t>(Nvalues));
        
        for (unsigned int i = 0u; i < Nvalues; ++i)
            new_values_as_std_vector[static_cast<std::size_t>(i)] = content.GetValue(i);
        
        // Read the numbering subset from \a new_values and call namesake function.
        const auto& numbering_subset = new_values.GetNumberingSubset();
        
        UpdateValues(numbering_subset, std::move(new_values_as_std_vector));
    }
    
    
    void DirichletBoundaryCondition::UpdateValues(const NumberingSubset& numbering_subset,
                                                  std::vector<PetscScalar>&& a_new_values)
    {
        assert(is_mutable_ && "Should not be called for a fixed-values boundary conditions!");
        
        auto& storage = GetNonCstDofStorage(numbering_subset);
        storage.UpdateValueList(std::move(a_new_values));
        
        // Update the values in all other nunbering subsets.
        const auto& dof_list = storage.GetDofList();
        const auto begin = dof_list.cbegin();
        const auto end = dof_list.cend();
        
        auto& numbering_subset_storage = GetNonCstDofStorage();
        const auto& new_values = storage.GetDofValueList();
        
        for (auto& pair : numbering_subset_storage)
        {
            if (pair.first == numbering_subset.GetUniqueId())
                // Current numbering subset is already up-to-date!
                continue;
            
            auto& boundary_condition_storage = pair.second;
            
            const auto& current_numbering_subset_dof_list = boundary_condition_storage.GetDofList();
            auto& current_numbering_subset_value_list = boundary_condition_storage.GetNonCstDofValueList();
            
            assert(current_numbering_subset_dof_list.size() == current_numbering_subset_value_list.size());
            
            const auto size = current_numbering_subset_dof_list.size();
            
            for (auto i = 0ul; i < size; ++i)
            {
                auto& current_dof_ptr = current_numbering_subset_dof_list[i];
                assert(!(!current_dof_ptr));
                
                auto it = std::find(begin, end, current_dof_ptr);
                
                if (it != end)
                {
                    const auto pos = static_cast<std::size_t>(it - begin);
                    
                    assert(pos < new_values.size());
                    current_numbering_subset_value_list[i] = new_values[pos];
                    
                }
            }
        }
        
    }

    
    void DirichletBoundaryCondition::SetBruteForceShrinking() noexcept
    {
        processor_wise_method_ = processor_wise_method::brute_force;
    }
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
