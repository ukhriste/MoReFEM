///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Jan 2014 13:56:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_T_COMPONENT_MANAGER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_T_COMPONENT_MANAGER_HPP_


# include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            /*!
             * \brief Each Component to be added to ComponentManager should derive from this class.
             *
             * This class is to be used as a CRTP:
             *
             * \code
             *  struct Comp12 final : public TComponentManager<Comp12>
             * \endcode
             *
             */
            template<class ComponentT>
            struct TComponentManager: public ComponentManager
            {

                //! Constructor.
                explicit TComponentManager();

                //! Destructor
                ~TComponentManager() = default;

                //! Copy constructor.
                TComponentManager(const TComponentManager&) = delete;

                //! Move constructor.
                TComponentManager(TComponentManager&&) = delete;

                //! Copy affectation.
                TComponentManager& operator=(const TComponentManager&) = delete;

                //! Move affectation.
                TComponentManager& operator=(TComponentManager&&) = delete;

            };


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_T_COMPONENT_MANAGER_HPP_
