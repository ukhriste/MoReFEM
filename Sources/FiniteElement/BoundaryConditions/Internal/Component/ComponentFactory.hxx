///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 15:19:05 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_FACTORY_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_FACTORY_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace BoundaryConditionNS
        {


            template<class ComponentT>
            bool ComponentFactory::Register(FunctionPrototype callback)
            {
                if (!callbacks_.insert(make_pair(ComponentT::Name(), callback)).second)
                {
                    ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(ComponentT::Name(),
                                                                           "component",
                                                                           __FILE__, __LINE__));
                }

                return true;
            }



            inline ComponentFactory::CallBack::size_type ComponentFactory::Nvariable() const
            {
                return callbacks_.size();
            }


        } // namespace BoundaryConditionNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPONENT_x_COMPONENT_FACTORY_HXX_
