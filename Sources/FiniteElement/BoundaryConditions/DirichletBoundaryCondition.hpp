///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Jan 2014 13:56:11 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_HPP_


# include <string>
# include <map>
# include <unordered_map>
# include <utility>
# include <memory>

# include "Utilities/UniqueId/UniqueId.hpp"

# include "Geometry/Domain/MeshLabel.hpp"

# include "FiniteElement/Unknown/Unknown.hpp"
# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
# include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"
# include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================

    class Domain;
    class GodOfDof;
    class GlobalVector;
    class DirichletBoundaryConditionManager;
    void ClearAllBoundaryConditionInitialValueList();


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    /// \addtogroup FiniteElementGroup
    ///@{


    /*!
     * \brief Choice of the method used to apply Dirichlet boundary conditions.
     *
     */
    enum class BoundaryConditionMethod
    {
        pseudo_elimination,
        penalization
    };




    /*!
     * \brief Class in charge of Dirichlet boundary conditions.
     *
     * This class is still relatively close to its counterpart in Felisce; a move heavy refactoring is
     * not excluded at all.
     *
     */
    class DirichletBoundaryCondition final
    : public Crtp::UniqueId<DirichletBoundaryCondition, UniqueIdNS::AssignationMode::manual>
    {
    public:

        //! Shared smart pointer.
        using shared_ptr = std::shared_ptr<DirichletBoundaryCondition>;

        //! Vector of smart pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to GodOfDof, which is the only allowed to call some of the private methods.
        friend GodOfDof;

        //! Class name.
        static const std::string& ClassName();

        //! Frienship to the manager, that will erase some temporary data.
        friend void ClearAllBoundaryConditionInitialValueList();

        //! Friendship to Manager class.
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend DirichletBoundaryConditionManager;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    private:

        //! Alias to parent.
        using unique_id_parent = Crtp::UniqueId<DirichletBoundaryCondition, UniqueIdNS::AssignationMode::manual>;


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_boundary_condition_constructor_args
         */
        explicit DirichletBoundaryCondition(unsigned int unique_id,
                                            const std::string& name,
                                            const Domain& domain,
                                            const Unknown& unknown,
                                            const std::vector<double>& value_per_component,
                                            const std::string& component,
                                            bool is_mutable,
                                            bool may_overlap);

    public:

        //! Destructor.
        ~DirichletBoundaryCondition() = default;

        //! Copy constructor.
        DirichletBoundaryCondition(const DirichletBoundaryCondition&) = delete;

        //! Move constructor.
        DirichletBoundaryCondition(DirichletBoundaryCondition&&) = delete;

        //! Copy affectation.
        DirichletBoundaryCondition& operator=(const DirichletBoundaryCondition&) = delete;

        //! Move affectation.
        DirichletBoundaryCondition& operator=(DirichletBoundaryCondition&&) = delete;

        ///@}


        //! Unknown considered in the boundary condition.
        const Unknown& GetUnknown() const noexcept;

        //! Domain upon which the condition is applied.
        const Domain& GetDomain() const noexcept;

        //! Access to the list of Dofs encompassed by the Dirichlet boundary condition.
        const Dof::vector_shared_ptr& GetDofList() const noexcept;

        /*!
         * \brief Whether a given numbering subset is relevant for the boundary condition.
         *
         * The relevancy is here program-wise: it might return true even if no dofs are managed by the current processor.
         *
         * \param[in] numbering_subset Numbering subset considered.
         *
         * \return Whether boundary condition is related to \a numbering_subset.
         */
        bool IsNumberingSubset(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Replace the values to apply on dofs by the ones given in \a new_values.
         *
         * \param[in] new_values Global vector from which new boundaruy condition values should be extracted.
         *
         * \attention All numbering subsets are updated if they share some of the dofs involved.
         */
        void UpdateValues(const GlobalVector& new_values);

        /*!
         * \brief Replace the values to apply on dofs by the ones given in \a new_values for \a numbering subset.
         *
         * \copydetails UpdateValues()
         *
         * \param[in] numbering_subset Numbering subset considered.
         *
         */
        void UpdateValues(const NumberingSubset& numbering_subset, std::vector<PetscScalar>&& new_values);

        //! Access to the name of the boundary condition, as defined in the input parameter file.
        const std::string& GetName() const noexcept;


    private:

        //! \name Methods to be called by friend GodOfDof.
        ///@{

        /*!
         * \brief Keep only the dofs that are related to current processor (i.e. either processor-wise or ghost).
         *
         * Should be called only by GodOfDof.
         * \param[in] god_of_dof God of dof that possess the dofs considered in the boundary condition.
         */
        void ShrinkToProcessorWise(const GodOfDof& god_of_dof);


        /*!
         * \brief Fill properly the storage for a given \a numbering_subset.
         *
         * Should be called only by GodOfDof.
         *
         * \param[in] numbering_subset NumberingSubset considered.
         * \param[in] dof_list Dof list to associate to \a numbering_subset.
         */
        void SetDofListForNumberingSubset(const NumberingSubset& numbering_subset,
                                          Dof::vector_shared_ptr&& dof_list);

        /*!
         * \brief Tell a numbering subset is relevant for the boundary condition, even if not present processor-wisely.
         *
         * Should be called only by GodOfDof.
         * \param[in] numbering_subset NumberingSubset to consider.
         */
        void ConsiderNumberingSubset(const NumberingSubset& numbering_subset);

        /*!
         * \brief Compute the list of dofs and their associated values.
         *
         * \param[in] node_bearer_list List of node bearers that are encompassed by the labels on which
         * the boundary condition must be applied.
         *
         * Should be called only by GodOfDof.
         */
        void ComputeDofList(NodeBearer::vector_shared_ptr&& node_bearer_list);


        /*!
         * \brief Access to component manager.
         *
         * Should be called only by GodOfDof.
         *
         * \return Component manager.
         */
        const Internal::BoundaryConditionNS::ComponentManager& GetComponentManager() const noexcept;


        /*!
         * \brief Return the value of the requested boundary condition.
         *
         * \param[in] component  Index of the active component considered. If for instance Comp13, component = 0
         * will yield x and component = 1 will yield z.
         *
         * \return Value of the boundary condition for \a component -th component.
         */
        double GetValueForComponent(unsigned int component) const;


        //! Specify that brute-force method should be used for shrinking to processor-wise current boundary condition.
        void SetBruteForceShrinking() noexcept;


        ///@}

    private:

        //! Non constant access to the list of Dofs encompassed by the Dirichlet boundary condition.
        Dof::vector_shared_ptr& GetNonCstDofList() noexcept;

        //! \accessor{Storage of dof for all numbering subsets}.
        const std::vector<std::pair<unsigned int, Internal::BoundaryConditionNS::DofStorage>>& GetDofStorage() const noexcept;

        //! \non_cst_accessor{Storage of dof for all numbering subsets}.
        std::vector<std::pair<unsigned int, Internal::BoundaryConditionNS::DofStorage>>& GetNonCstDofStorage() noexcept;

        //! Access to the storage of dof for a given numbering subset.
        const Internal::BoundaryConditionNS::DofStorage& GetDofStorage(const NumberingSubset& numbering_subset) const noexcept;

        //! Non constant access to the storage of dof for a given numbering subset.
        Internal::BoundaryConditionNS::DofStorage& GetNonCstDofStorage(const NumberingSubset& numbering_subset) noexcept;

        //! Accessor to the dof and their associated values.
        const std::vector<std::pair<Dof::shared_ptr, double>>& GetInitialDofValueList() const noexcept;

        //! Non constant accessor to the dof and their associated values.
        std::vector<std::pair<Dof::shared_ptr, double>>& GetNonCstInitialDofValueList() noexcept;

        //! Clear the initial values storage.
        void ClearInitialDofValueList();

    private:

        //! Name of the boundary condition (as defined in the input parameter file).
        const std::string& name_;

        //! Domain upon which the condition is applied.
        const Domain& domain_;

        //! variable concerned by this DirichletBoundaryCondition
        const Unknown& unknown_;

        //! Object which can tell which components are encompassed by the boundary condition.
        Internal::BoundaryConditionNS::ComponentManager::const_shared_ptr component_manager_ = nullptr;

        /*!
         * \brief Initial values associated to each dof.
         *
         * \internal <b><tt>[internal]</tt></b> When this data is created, dofs aren't yet given their program-wise index, so both std::map and
         * std::unordered_map would be very unreliable here.
         *
         * All dofs are considered here, regardless of their numbering subset.
         * \todo #619 This container should disappear: if dofs are generated after the reduction to processor-wise
         * the initial values can be retrieved in the same time and current container could hence be avoided.
         *
         * \attention This container is cleared once it has been used!
         */
        std::vector<std::pair<Dof::shared_ptr, double>> initial_dof_value_list_;

        //! List of all dofs encompassed by the boundary condition.
        Dof::vector_shared_ptr dof_list_;

        /*!
         * \brief Helpful vector when penalization is to be used, which stores the penalization value for each dof.
         *
         * This vector may be given directly to Petsc::Matrix::SetValues().
         */
        std::vector<double> penalization_array_;

        /*!
         * \brief For each numbering subset, store the relevant information about the dofs involved in the boundary
         * condition.
         *
         * Key is unique id of the numbering subset; value is the object that stored the informations about the dofs.
         *
         * \attention The value might not include any dofs: the convention is that a boundary condition must get
         * a numbering subset key as soon as it is relevant. So if for instance a boundary condition encompasses n dofs
         * all stored on processor i, all processors will nonetheless get a pair in the present list, even if there
         * are no associated dofs.
         */
        std::vector<std::pair<unsigned int, Internal::BoundaryConditionNS::DofStorage>> dof_storage_per_numbering_subset_;


        //! Initial value per component (should not be stored past #619).
        std::vector<double> value_per_component_;

        # ifndef NDEBUG
        //! Whether ClearInitialDofValueList() has already been called or not.
        bool is_cleared_ = false;
        # endif // NDEBUG




        /*!
         * \brief Two internal possible methods methods to reduce to processor-wise.
         *
         * The 'fast' one is the default choice: it relies of a clever use of shared_pointers properties.
         * However, it doesn't work in parallel for ill-defined boundary conditions (when a dof is shared
         * by several boundary conditions). When this appears, a warning is displayed on screen and all
         * boundary conditions concerned by the issue are automatically handled through a
         * 'brute_force' method, which painstainkingly look for each dof whether they belong to a
         * processor-wise or a ghost list or not.
         *
         */
        enum class processor_wise_method
        {
            fast,
            brute_force
        };

        //! Determine which method should be used to reduce the processor-wise.
        processor_wise_method processor_wise_method_ = processor_wise_method::fast;

        # ifndef NDEBUG
        //! Whether the values may vary over time.
        bool is_mutable_ = false;
        # endif // NDEBUG

    };


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_HPP_
