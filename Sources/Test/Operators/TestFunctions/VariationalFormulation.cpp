/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/TestFunctions/VariationalFormulation.hpp"
#include "Test/Operators/TestFunctions/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestFunctionsNS
    {
        
      
        void VariationalFormulation::AssembleStaticOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& mesh = god_of_dof.GetGeometricMeshRegion();
            
            const auto dimension = mesh.GetDimension();
            
            const auto& potential_1_potential_2_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2));
            const auto& potential_1_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1));
            const auto& potential_2_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_2));
            const auto& potential_3_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_3));
            const auto& displacement_potential_1_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1));
            const auto& potential_1_potential_2_potential_4_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4));
            const auto& displacement_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacement));
            const auto& P1_potential_1_P2_potential_2_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2));
            
            auto& potential_1_potential_2_system_matrix = GetNonCstSystemMatrix(potential_1_potential_2_numbering_subset, potential_1_potential_2_numbering_subset);
            auto& potential_1_system_matrix = GetNonCstSystemMatrix(potential_1_numbering_subset, potential_1_numbering_subset);
            auto& potential_2_system_matrix = GetNonCstSystemMatrix(potential_2_numbering_subset, potential_2_numbering_subset);
            auto& potential_3_system_matrix = GetNonCstSystemMatrix(potential_3_numbering_subset, potential_3_numbering_subset);
            auto& potential_1_potential_3_system_matrix = GetNonCstSystemMatrix(potential_1_numbering_subset, potential_3_numbering_subset);
            auto& displacement_potential_1_system_matrix = GetNonCstSystemMatrix(displacement_potential_1_numbering_subset, displacement_potential_1_numbering_subset);
            auto& potential_1_potential_2_potential_4_system_matrix = GetNonCstSystemMatrix(potential_1_potential_2_potential_4_numbering_subset, potential_1_potential_2_potential_4_numbering_subset);
            auto& displacement_displacement_system_matrix = GetNonCstSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
            auto& P1_potential_1_P2_potential_2_system_matrix = GetNonCstSystemMatrix(P1_potential_1_P2_potential_2_numbering_subset, P1_potential_1_P2_potential_2_numbering_subset);
            
            auto& monolithic_system_rhs = GetNonCstSystemRhs(potential_1_potential_2_numbering_subset);
            auto& potential_1_system_rhs = GetNonCstSystemRhs(potential_1_numbering_subset);
            auto& potential_1_system_solution = GetNonCstSystemSolution(potential_1_numbering_subset);
            auto& potential_3_system_rhs = GetNonCstSystemRhs(potential_3_numbering_subset);
            auto& displacement_system_rhs = GetNonCstSystemRhs(displacement_numbering_subset);
            auto& displacement_system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_system_matrix, 1.);
                mass_operator_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            const bool is_parallel = parent::GetMpi().Nprocessor<int>() > 1;
            
            decltype(auto) matricial_expected_results = GetExpectedMatricialResults(dimension, is_parallel);
            decltype(auto) vectorial_expected_results = GetExpectedVectorialResults(dimension, is_parallel);

            CheckMatrix(god_of_dof,
                        potential_1_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_1",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_2_system_matrix, 1.);
                mass_operator_potential_2_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_1_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_2",
                        __FILE__, __LINE__);

            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.); 
                mass_operator_potential_1_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }

            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_1_potential_1",
                        __FILE__, __LINE__);
            

            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                mass_operator_potential_1_potential_2_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_1_potential_2",
                        __FILE__, __LINE__);
            
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                mass_operator_potential_2_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }
   
            
            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_2_potential_1",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                mass_operator_potential_2_potential_2_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
      
            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_2_potential_2",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_3_system_matrix, 1.);
                mass_operator_potential_3_->Assemble(std::make_tuple(std::ref(matrix)));
            }

            CheckMatrix(god_of_dof,
                        potential_3_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_3",
                        __FILE__, __LINE__,
                        1.e-8);
            
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_3_system_matrix, 1.);
                mass_operator_potential_1_potential_3_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_1_potential_3_system_matrix,
                        matricial_expected_results,
                        "mass_operator_potential_1_potential_3",
                        __FILE__, __LINE__);
            
            {
                potential_1_system_matrix.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(potential_1_system_matrix, 1.);
                stiffness_operator_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            

            CheckMatrix(god_of_dof,
                        potential_1_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_1",
                        __FILE__, __LINE__);
            
            {
                potential_1_potential_2_system_matrix.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                stiffness_operator_potential_1_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            

            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_1_potential_1",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                stiffness_operator_potential_1_potential_2_->Assemble(std::make_tuple(std::ref(matrix)));
            }

            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_1_potential_2",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                stiffness_operator_potential_2_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }

            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_2_potential_1",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_system_matrix, 1.);
                stiffness_operator_potential_2_potential_2_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_1_potential_2_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_2_potential_2",
                        __FILE__, __LINE__);

            
            {
                potential_3_system_matrix.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(potential_3_system_matrix, 1.);
                stiffness_operator_potential_3_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_3_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_3",
                        __FILE__, __LINE__,
                        1.e-8);

            
            {
                potential_1_potential_3_system_matrix.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(potential_1_potential_3_system_matrix, 1.);
                stiffness_operator_potential_1_potential_3_->Assemble(std::make_tuple(std::ref(matrix)));
            }

            CheckMatrix(god_of_dof,
                        potential_1_potential_3_system_matrix,
                        matricial_expected_results,
                        "stiffness_operator_potential_1_potential_3",
                        __FILE__, __LINE__);

            
            
            {
                GlobalVectorWithCoefficient vector(potential_1_system_rhs, 1.);
                source_operator_potential_1_->Assemble(std::make_tuple(std::ref(vector)), 0.);
            }

            CheckVector(god_of_dof,
                        potential_1_system_rhs,
                        vectorial_expected_results,
                        "source_operator_potential_1",
                        __FILE__, __LINE__);


            
            {
                GlobalVectorWithCoefficient vector(monolithic_system_rhs, 1.);
                source_operator_potential_1_potential_1_->Assemble(std::make_tuple(std::ref(vector)), 0.);
            }
      
            CheckVector(god_of_dof,
                        monolithic_system_rhs,
                        vectorial_expected_results,
                        "source_operator_potential_1_potential_1",
                        __FILE__, __LINE__);

            
            {
                GlobalVectorWithCoefficient vector(monolithic_system_rhs, 1.);
                source_operator_potential_1_potential_2_->Assemble(std::make_tuple(std::ref(vector)), 0.);
            }
   
            CheckVector(god_of_dof,
                        monolithic_system_rhs,
                        vectorial_expected_results,
                        "source_operator_potential_1_potential_2",
                        __FILE__, __LINE__);

            
            {
                GlobalVectorWithCoefficient vector(potential_3_system_rhs, 1.);
                source_operator_potential_3_->Assemble(std::make_tuple(std::ref(vector)), 0.);
            }
            
            CheckVector(god_of_dof,
                        potential_3_system_rhs,
                        vectorial_expected_results,
                        "source_operator_potential_3",
                        __FILE__, __LINE__);

            
            {
                potential_1_system_matrix.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(potential_1_system_matrix, 1.);
                variable_mass_operator_potential_1_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_1_system_matrix,
                        matricial_expected_results,
                        "variable_mass_operator_potential_1",
                        __FILE__, __LINE__);

            
            {
                GlobalMatrixWithCoefficient matrix(GetNonCstMatrixBidomain(), 1.);
                bidomain_operator_->Assemble(std::make_tuple(std::ref(matrix)));
            }
        
            CheckMatrix(god_of_dof,
                        GetMatrixBidomain(),
                        matricial_expected_results,
                        "bidomain_operator",
                        __FILE__, __LINE__);

            
            {
                GlobalMatrixWithCoefficient matrix(potential_1_potential_2_potential_4_system_matrix, 1.);
                bidomain_potential_124_operator_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        potential_1_potential_2_potential_4_system_matrix,
                        matricial_expected_results,
                        "bidomain_potential_124_operator",
                        __FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient matrix(GetNonCstMatrixGradPhiTauTauGradPhi(), 1.);
                grad_phi_tau_tau_grad_phi_operator_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        GetMatrixGradPhiTauTauGradPhi(),
                        matricial_expected_results,
                        "grad_phi_tau_tau_grad_phi_operator",
                        __FILE__, __LINE__);

            std::cout << std::scientific <<std::setprecision(15);

            if (dimension == 3)
            {
                {
                    GetNonCstMatrixGradPhiTauTauGradPhi().ZeroEntries(__FILE__, __LINE__);
                    
                    GlobalMatrixWithCoefficient matrix(GetNonCstMatrixGradPhiTauTauGradPhi(), 1.);
                    grad_phi_tau_ortho_tau_grad_phi_operator_->Assemble(std::make_tuple(std::ref(matrix)));
                    
                    CheckMatrix(god_of_dof,
                                GetMatrixGradPhiTauTauGradPhi(),
                                matricial_expected_results,
                                "grad_phi_tau_ortho_tau_grad_phi_operator",
                                __FILE__, __LINE__,
                                1.e-5);
                }
                
                {
                    P1_potential_1_P2_potential_2_system_matrix.ZeroEntries(__FILE__, __LINE__);
                    
                    GlobalMatrixWithCoefficient matrix(P1_potential_1_P2_potential_2_system_matrix, 1.);
                    surfacic_bidomain_operator_->Assemble(std::make_tuple(std::ref(matrix)));
                                        
                    CheckMatrix(god_of_dof,
                                P1_potential_1_P2_potential_2_system_matrix,
                                matricial_expected_results,
                                "surfacic_bidomain_operator",
                                __FILE__, __LINE__,
                                1.e-4);
                }
            }
            
            {
                GlobalMatrixWithCoefficient matrix(displacement_potential_1_system_matrix, 1.);
                scalar_div_vectorial_operator_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        displacement_potential_1_system_matrix,
                        matricial_expected_results,
                        "scalar_div_vectorial_operator",
                        __FILE__, __LINE__);
            
            {
                displacement_potential_1_system_matrix.ZeroEntries(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(displacement_potential_1_system_matrix, 1.);
                stokes_operator_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        displacement_potential_1_system_matrix,
                        matricial_expected_results,
                        "stokes_operator",
                        __FILE__, __LINE__);

            
            {
                GlobalMatrixWithCoefficient matrix(displacement_displacement_system_matrix, 1.);
                elasticity_operator_->Assemble(std::make_tuple(std::ref(matrix)));
            }
            
            CheckMatrix(god_of_dof,
                        displacement_displacement_system_matrix,
                        matricial_expected_results,
                        "elasticity_operator",
                        __FILE__, __LINE__);
            
            {
                displacement_displacement_system_matrix.ZeroEntries(__FILE__, __LINE__);
                displacement_system_rhs.SetUniformValue(1., __FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(displacement_displacement_system_matrix, 1.);
                ale_operator_->Assemble(std::make_tuple(std::ref(matrix)), displacement_system_rhs);
            }

            CheckMatrix(god_of_dof,
                        displacement_displacement_system_matrix,
                        matricial_expected_results,
                        "ale_operator_1",
                        __FILE__, __LINE__);
            
            
            {
                displacement_displacement_system_matrix.ZeroEntries(__FILE__, __LINE__);
                displacement_system_rhs.ZeroEntries(__FILE__, __LINE__);
                
                if (dimension == 1)
                {
                    displacement_system_rhs.SetValue(0., 1., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(1., 2., INSERT_VALUES, __FILE__, __LINE__);
                }
                else if (dimension == 2)
                {
                    displacement_system_rhs.SetValue(0., 1., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(1., 2., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(2., 2., INSERT_VALUES, __FILE__, __LINE__);
                }
                else
                {
                    displacement_system_rhs.SetValue(0., 1., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(1., 2., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(2., 2., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(3., 2., INSERT_VALUES, __FILE__, __LINE__);
                }
                
                displacement_system_rhs.Assembly(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(displacement_displacement_system_matrix, 1.);
                ale_operator_->Assemble(std::make_tuple(std::ref(matrix)), displacement_system_rhs);
            }

            CheckMatrix(god_of_dof,
                        displacement_displacement_system_matrix,
                        matricial_expected_results,
                        "ale_operator_2",
                        __FILE__, __LINE__);
            
            
            
            if (dimension == 3)
            {
                displacement_displacement_system_matrix.ZeroEntries(__FILE__, __LINE__);
                displacement_system_rhs.ZeroEntries(__FILE__, __LINE__);
                displacement_system_solution.SetUniformValue(1., __FILE__, __LINE__);
                
                displacement_system_solution.Assembly(__FILE__, __LINE__);
            
                GlobalMatrixWithCoefficient matrix(displacement_displacement_system_matrix, 1.);
                GlobalVectorWithCoefficient vec(displacement_system_rhs, 1.);
                following_pressure_operator_->Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)), displacement_system_solution);

                CheckMatrix(god_of_dof,
                            displacement_displacement_system_matrix,
                            matricial_expected_results,
                            "following_pressure_operator",
                            __FILE__, __LINE__);
                
                CheckVector(god_of_dof,
                            displacement_system_rhs,
                            vectorial_expected_results,
                            "following_pressure_operator",
                            __FILE__, __LINE__);
            }
            
            {
                potential_1_system_rhs.ZeroEntries(__FILE__, __LINE__);
                potential_1_system_solution.SetUniformValue(1., __FILE__, __LINE__);
                
                potential_1_system_solution.Assembly(__FILE__, __LINE__);
                
                GlobalVectorWithCoefficient vector(potential_1_system_rhs, 1.);
                non_linear_source_operator_->Assemble(std::make_tuple(std::ref(vector)), potential_1_system_solution);
            }
            
            CheckVector(god_of_dof,
                        potential_1_system_rhs,
                        vectorial_expected_results,
                        "non_linear_source_operator",
                        __FILE__, __LINE__);
            
            
            
            {
                displacement_displacement_system_matrix.ZeroEntries(__FILE__, __LINE__);
                displacement_system_rhs.ZeroEntries(__FILE__, __LINE__);
                displacement_system_solution.ZeroEntries(__FILE__, __LINE__);
                if (dimension == 1)
                {
                    displacement_system_solution.SetValue(0., 1., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_solution.SetValue(1., 2., INSERT_VALUES, __FILE__, __LINE__);
                }
                else if (dimension == 2)
                {
                    displacement_system_rhs.SetValue(0., 1., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(1., 2., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(2., 2., INSERT_VALUES, __FILE__, __LINE__);
                }
                else
                {
                    displacement_system_rhs.SetValue(0., 1., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(1., 2., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(2., 2., INSERT_VALUES, __FILE__, __LINE__);
                    displacement_system_rhs.SetValue(3., 2., INSERT_VALUES, __FILE__, __LINE__);
                }
                
                displacement_system_rhs.Assembly(__FILE__, __LINE__);
                
                GlobalMatrixWithCoefficient matrix(displacement_displacement_system_matrix, 1.);
                GlobalVectorWithCoefficient vec(displacement_system_rhs, 1.);
                pk2_operator_->Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)), displacement_system_solution);
            }
            
            CheckMatrix(god_of_dof,
                        displacement_displacement_system_matrix,
                        matricial_expected_results,
                        "pk2_operator",
                        __FILE__, __LINE__,
                        1.e-5);
            
            CheckVector(god_of_dof,
                        displacement_system_rhs,
                        vectorial_expected_results,
                        "pk2_operator",
                        __FILE__, __LINE__,
                        1.e-5);
            
        }
        

    } // namespace TestFunctionsNS


} // namespace MoReFEM
