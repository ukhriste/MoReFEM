/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Fiber/Fiber.hpp"
# include "Core/InputParameter/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputParameter/Parameter/Solid/Solid.hpp"
# include "Core/InputParameter/Parameter/Fluid/Fluid.hpp"
# include "Core/InputParameter/Reaction/MitchellSchaeffer.hpp"
# include "Core/InputParameter/InitialConditionGate.hpp"

namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            volume = 1,
            surface = 2,
            full_mesh = 3
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            volume_potential_1_potential_2 = 1,
            volume_potential_1 = 2,
            volume_potential_2 = 3,
            volume_potential_3 = 4,
            volume_displacement_potential_1 = 5,
            volume_displacement = 6,
            volume_potential_1_potential_2_potential_4 = 7,
            surface_potential_1 = 8,
            surface_displacement = 9,
            surface_potential_1_potential_2 = 10
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            potential_1 = 1,
            potential_2 = 2,
            potential_3 = 3,
            potential_4 = 4,
            displacement = 5
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            potential_1_potential_2 = 1,
            potential_1 = 2,
            potential_2 = 3,
            potential_3 = 4,
            displacement_potential_1 = 5,
            displacement = 6,
            potential_1_potential_2_potential_4 = 7,
            P1_potential_1_P2_potential_2 = 8
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_fiber_enum
        enum class FiberIndex
        {
            fibers_in_volume = 1,
            fibers_on_surface = 2,
            angles = 3
        };

        
        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : unsigned int
        {
            source = 1
        };

            
        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_2)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_3)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_potential_1)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_1_potential_2_potential_4)>,
            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::P1_potential_1_P2_potential_2)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::potential_1)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::potential_2)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::potential_3)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::potential_4)>,
            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::surface)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_2)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_3)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_displacement_potential_1)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_displacement)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume_potential_1_potential_2_potential_4)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_potential_1)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_displacement)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_potential_1_potential_2)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::Fiber<EnumUnderlyingType(FiberIndex::fibers_in_volume), ParameterNS::Type::vector>,
            InputParameter::Fiber<EnumUnderlyingType(FiberIndex::fibers_on_surface), ParameterNS::Type::vector>,
            InputParameter::Fiber<EnumUnderlyingType(FiberIndex::angles), ParameterNS::Type::scalar>,

            InputParameter::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::source)>,

            InputParameter::Solid::VolumicMass,
            InputParameter::Solid::HyperelasticBulk,
            InputParameter::Solid::Kappa1,
            InputParameter::Solid::Kappa2,
            InputParameter::Solid::PoissonRatio,
            InputParameter::Solid::YoungModulus,
            InputParameter::Solid::LameLambda,
            InputParameter::Solid::LameMu,
            InputParameter::Solid::Viscosity,
            InputParameter::Solid::PlaneStressStrain,

            InputParameter::Fluid::Viscosity,

            InputParameter::ReactionNS::MitchellSchaeffer,
            InputParameter::InitialConditionGate,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace TestFunctionsNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_INPUT_PARAMETER_LIST_HPP_
