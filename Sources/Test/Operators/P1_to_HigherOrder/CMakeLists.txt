add_executable(MoReFEMTestP1_to_P1b
               ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
               ${CMAKE_CURRENT_LIST_DIR}/main_interpolator_P1_P1b.cpp
              )
          
target_link_libraries(MoReFEMTestP1_to_P1b
                      ${MOREFEM_MODEL})
                      
                      
add_executable(MoReFEMTestP1_to_P2
             ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
             ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
             ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
             ${CMAKE_CURRENT_LIST_DIR}/main_interpolator_P1_P2.cpp
            )

target_link_libraries(MoReFEMTestP1_to_P2
                    ${MOREFEM_MODEL})                  
                    
morefem_install(MoReFEMTestP1_to_P2 MoReFEMTestP1_to_P1b)