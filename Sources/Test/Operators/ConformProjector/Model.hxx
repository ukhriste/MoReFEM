/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_CONFORM_PROJECTOR_x_MODEL_HXX_
# define MOREFEM_x_TEST_x_OPERATORS_x_CONFORM_PROJECTOR_x_MODEL_HXX_


namespace MoReFEM
{


    namespace ConformProjectorNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("ConformProjector");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        {
        }


        inline const std::string& Model::GetOutputDirectory() const noexcept
        {
            return output_directory_;
        }


    } // namespace ConformProjectorNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_CONFORM_PROJECTOR_x_MODEL_HXX_
