add_library(MoReFEM_test_tools ${LIBRARY_TYPE} "")
include(${CMAKE_CURRENT_LIST_DIR}/Tools/CMakeLists.txt)

include(${CMAKE_CURRENT_LIST_DIR}/Utilities/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Core/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Geometry/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Ondomatic/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Operators/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Parameter/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/ThirdParty/CMakeLists.txt)

