/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 26 Jul 2017 14:46:48 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_MODEL_HXX_
# define MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_MODEL_HXX_


namespace MoReFEM
{


    namespace TestVariableTimeStepNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("VariableTimeStep");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


    } // namespace TestVariableTimeStepNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_CORE_x_VARIABLE_TIME_STEP_x_MODEL_HXX_
