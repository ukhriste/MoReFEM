/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 26 Jul 2017 14:46:48 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Filesystem/File.hpp"

# include "Test/Core/VariableTimeStep/Model.hpp"

namespace MoReFEM
{
    
    
    namespace TestVariableTimeStepNS
    {
        
        
        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }

        
        void Model::SupplInitialize(const morefem_data_type& morefem_data)
        {
            static_cast<void>(morefem_data);
        }
    

        void Model::Forward()
        {
            auto& time_manager = GetNonCstTimeManager();
            const auto& mpi = GetMpi();
            const double time = time_manager.GetTime();
            
            if (NumericNS::AreEqual(time, 1., 1.e-12))
            {
                time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
            }
            
            static unsigned int i = 0;
            
            if (NumericNS::AreEqual(time, 1.4, 1.e-12) && i == 0)
            {
                time_manager.ResetTimeAtPreviousTimeStep();
                ++i;
            }
            
            if (NumericNS::AreEqual(time, 1.5, 1.e-12))
            {
                time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
            }
            
            if (NumericNS::AreEqual(time, 2., 1.e-12))
            {
                time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
            }
            
            if (NumericNS::AreEqual(time, 2.5, 1.e-12))
            {
                time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
            }
                        
            if (NumericNS::AreEqual(time, 3., 1.e-12))
            {
                time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
            }
            
            if (time > 4.)
            {
                time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
            }

        }

        
        void Model::SupplInitializeStep()
        { }
        

        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }


    } // namespace TestVariableTimeStepNS


} // namespace MoReFEM
