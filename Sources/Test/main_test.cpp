/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Jan 2013 11:46:59 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>
#include <iostream>


#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Petsc.hpp"

#include "Utilities/Containers/Tuple.hpp"



using namespace MoReFEM;

double f(int a, double b);


double f(int a, double b)
{
    return static_cast<double>(a) * 10. + b;
}


template<class DerivedT>
struct Foo
{

    double g(int a, double b)
    {
        return static_cast<double>(a) * 10. + b;
    }


    template<int I>
    double h(int a, double b)
    {
        return static_cast<double>(I) + static_cast<double>(a) * 10. + b;
    }


    template<class T>
    double i(int a, double b)
    {
        return static_cast<double>(a) * 10. + b;
    }

};


template<class T>
struct Bar : public Foo<Bar<T>>
{



};


template<class BarT>
void Test()
{
    std::cout << "f (normal) = " << f(4, 7.) << std::endl;;

    auto tuple = std::make_tuple(4, 7.);

    std::cout << "f (call) = " << Utilities::Tuple::CallFunctionWithArgumentsFromTuple<double>(f, tuple) << std::endl;;


    using f_type = std::add_pointer_t<double(int, double)>;

    f_type ptr = &f;
    std::cout << "f (call ptr) = " << Utilities::Tuple::CallFunctionWithArgumentsFromTuple<double>(ptr, tuple) << std::endl;;


    BarT object;

    std::cout << "g (normal) = " << object.g(4, 7.) << std::endl;;




//    std::cout << "g (call) = " <<
//    Utilities::Tuple::CallMethodWithArgumentsFromTuple<double>(object, &BarT::g, tuple) << std::endl;;

    std::cout << "g (call template) = " <<
    Utilities::Tuple::CallMethodWithArgumentsFromTuple<double>(object, &BarT::template h<0>, tuple) << std::endl;;

    std::cout << "g (call template 2) = " <<
    Utilities::Tuple::CallMethodWithArgumentsFromTuple<double>(object, &BarT::template i<int>, tuple) << std::endl;;
}


int main(int argc, char** argv)
{
    Wrappers::Mpi::InitEnvironment(argc, argv);



    Test<Bar<int>>();
//
//    Wrappers::Mpi mpi(0, Wrappers::MpiNS::Comm::World);
//    Wrappers::Petsc::Petsc petsc(__FILE__, __LINE__);
//    
//    std::vector<std::vector<PetscInt>> nnz
//    {
//        { 0 },
//        { 1, 2 },
//        { 3 },
//        { 2 }
//    };
//    
//    
//    Wrappers::Petsc::MatrixPattern pattern(nnz);
//    
//    Wrappers::Petsc::Matrix matrix;
//    
//    matrix.InitSequentialMatrix(4, 4,
//                                pattern,
//                                mpi,
//                                __FILE__, __LINE__);
//    
//    matrix.View(mpi, __FILE__, __LINE__);
//    
//    std::vector<PetscInt> row { 1, 3 };
//    std::vector<PetscInt> col { 1, 2 };
//    
//    std::vector<PetscScalar> val { 1., 1., 0., 1. };
//    
//    matrix.SetValues(row,
//                     col,
//                     val.data(),
//                     INSERT_VALUES,
//                     __FILE__, __LINE__,
//                     Wrappers::Petsc::ignore_zero_entries::yes);
//    
//    matrix.Assembly(__FILE__, __LINE__);
//    
//    matrix.View(mpi, __FILE__, __LINE__);

    
    return EXIT_SUCCESS;
}






