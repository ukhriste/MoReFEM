-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

-- Tells which policy is used to describe time evolution.
-- Expected format: "VALUE"
-- Constraint: ops_in(v, {'constant_time_step', 'variable_time_step'})
time_evolution_policy = "constant_time_step",

-- Time at the beginning of the code (in seconds).
-- Expected format: VALUE
-- Constraint: v >= 0.
init_time = 0.,

-- Time step between two iterations, in seconds.
-- Expected format: VALUE
-- Constraint: v > 0.
timeStep = 0.1,

-- Minimum time step between two iterations, in seconds.
-- Expected format: VALUE
-- Constraint: v > 0.
minimum_time_step = 1.e-6,

-- Maximum time, if set to zero run a static case.
-- Expected format: VALUE
-- Constraint: v >= 0.
timeMax = 1.

} -- transient

Result = {

	-- Directory in which all the results will be written. This path may use the environment variable
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. 
	-- Expected format: "VALUE"
    output_directory = '/Volumes/Data/${USER}/MoReFEM/Results/Test/MpiErrorHandling',

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1

} -- Result

