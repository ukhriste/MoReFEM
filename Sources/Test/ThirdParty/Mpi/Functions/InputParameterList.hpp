/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Jul 2017 15:16:12 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_MPI_x_FUNCTIONS_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_THIRD_PARTY_x_MPI_x_FUNCTIONS_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace InputParameterListNS
        {


            //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
            <
                InputParameter::TimeManager,
                InputParameter::Result
            >;


            //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


        } // namespace TestNS


    } // namespace InputParameterListNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_MPI_x_FUNCTIONS_x_INPUT_PARAMETER_LIST_HPP_
