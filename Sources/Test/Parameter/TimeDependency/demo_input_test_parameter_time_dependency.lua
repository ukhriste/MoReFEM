-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

-- transient
transient = {
	-- Tells which policy is used to describe time evolution.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'constant_time_step', 'variable_time_step'}})
	time_evolution_policy = "constant_time_step",

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
    -- Minimum time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    minimum_time_step = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 5.
}

-- NumberingSubset1
NumberingSubset1 = {
	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
    name = 'scalar',
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

}

-- NumberingSubset2
NumberingSubset2 = {
	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
    name = 'vectorial',
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false

}



-- Unknown1
Unknown1 = {
	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = 'scalar',

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'scalar', 'vectorial'})
	nature = 'scalar'
}

-- Unknown2
Unknown2 = {
	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = 'vectorial',

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'scalar', 'vectorial'})
	nature = 'vectorial'
}

-- Mesh1
Mesh1 = {
	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
    mesh = '${HOME}/Codes/MoReFEM/Data/Mesh/one_triangle.mesh',

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: ops_in(v, {'Ensight', 'Medit'})
	format = "Medit",

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 2,
    
    space_unit = 1.
}

-- Domain1
Domain1 = {
	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: ops_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: {VALUE1, VALUE2, ...}
	mesh_label_list = { },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at Ops level, as some geometric element types could be added after 
	-- generation of current input parameter file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built.
 	-- The known types when this file was generated are: 
 	-- . Point1
 	-- . Segment2, Segment3
 	-- . Triangle3, Triangle6
 	-- . Quadrangle4, Quadrangle8, Quadrangle9
 	-- . Tetrahedron4, Tetrahedron10
 	-- . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {}
}


-- Domain2
Domain2 = {
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: ops_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at Ops level, as some geometric element types could be added after
    -- generation of current input parameter file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = {}
}

-- FiniteElementSpace1
FiniteElementSpace1 = {
	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,

	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 1,

	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = { 'scalar', 'vectorial' },

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1', 'P1' },

	-- List of the numbering subset to use for each unknown;
	-- Expected format: {VALUE1, VALUE2, ...}
	numbering_subset_list = { 1, 2 }
}






Petsc1 = {
    
    -- Absolute tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    absoluteTolerance = 1e-10,
    
    -- gmresStart
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v >= 0
    gmresRestart = 200,
    
    -- Maximum iteration
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0
    maxIteration = 1000,
    
    -- List of preconditioner: { none jacobi sor lu bjacobi ilu asm cholesky }.
    -- To use mumps:
    -- preconditioner = lu
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, 'lu')
    preconditioner = 'lu',
    
    -- Relative tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    relativeTolerance = 1e-6,
    
    
    -- Step size tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    stepSizeTolerance = 1e-8,
    
    -- List of solver: { chebychev cg gmres preonly bicg python };
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, {'Mumps', 'Umfpack'})
    solver = 'Mumps',
    
} -- Petsc

-- Result
Result = {
	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step.
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_RESULT_DIR}/TestParameterTimeDependency',
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1
}



Solid = {
    

    PoissonRatio = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function', 'at_quadrature_point', 'piecewise_constant_by_domain'})
        nature = 'constant',
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 10.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by Ops until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {}
        
    }, -- PoissonRatio
    
    YoungModulus = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function', 'at_quadrature_point', 'piecewise_constant_by_domain'})
        nature = 'constant',
        
        -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: VALUE
        scalar_value = 20.,
        
        -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
        -- 'at_quadrature_point'; irrelevant otherwise).
        -- Expected format: Function in Lua language, for instance:
        -- 		-- function(arg1, arg2, arg3)
        -- return arg1 + arg2 -
        -- arg3
        -- end
        -- sin, cos and tan require a 'math.'
        -- preffix.
        -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
        -- content is not interpreted by Ops until an actual use of the underlying function.
        lua_function = none,
        
        -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
        -- domains given here must not intersect.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_id = {},
        
        -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
        -- Expected format: {VALUE1, VALUE2, ...}
        piecewise_constant_domain_value = {}
        
    }, -- YoungModulus
    

    
    
} -- Solid


StaticPressure = {
    
    -- How is given the volumic mass (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: ops_in(v, {'constant', 'lua_function', 'at_quadrature_point', 'piecewise_constant_by_domain'})
    nature = 'constant',
    
    -- Value of the volumic mass in the case nature is 'constant'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: VALUE
    scalar_value = 1.,
    
    -- Value of the volumic mass in the case nature is 'lua_function'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function = none,
    
    -- Domain indices of the parameter in the case nature is 'piecewise_constant_by_domain'. The various
    -- domains given here must not intersect.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_id = { },
    
    -- Value of the parameter in the case nature is 'piecewise_constant_by_domain'.
    -- Expected format: {VALUE1, VALUE2, ...}
    piecewise_constant_domain_value = { }
    
} -- StaticPressure


PressureFromFile1 = {
    
    -- Path of the file to use.
    -- Expected format: "VALUE"
    -- Constraint: Format:time pressurevalue1 value1value2 value2...
    FilePath = "${HOME}/Codes/MoReFEM/Sources/Test/Parameter/TimeDependency/test.dat"
    
} -- PressureFromFile



TransientSource1 = {
    
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: ops_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant"},
    
    -- Value of the transient source in the case nature is 'constant'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Expected format: {VALUE1, VALUE2, ...}
    scalar_value = { 10. , 20., 30.},
    
    -- Value of the transient source  in the case nature is 'lua_function'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_x = none,
    
    -- Value of the transient source  in the case nature is 'lua_function'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_y = none,
    
    -- Value of the transient source  in the case nature is 'lua_function'(and also initial value if nature is
    -- 'at_quadrature_point'; irrelevant otherwise).
    -- Function expects as arguments global coordinates (coordinates in the mesh).
    -- Expected format: Function in Lua language, for instance:
    -- 	-- function(arg1, arg2, arg3)
    -- return arg1 + arg2 -
    -- arg3
    -- end
    -- sin, cos and tan require a 'math.'
    -- preffix.
    -- If you do not wish to provide one, put anything you want (e.g. 'none'): the
    -- content is not interpreted by Ops until an actual use of the underlying function.
    lua_function_z = none
    
} -- TransientSource1

