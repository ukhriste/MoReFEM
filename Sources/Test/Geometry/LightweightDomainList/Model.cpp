/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Geometry/LightweightDomainList/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace LightweightDomainListNS
        {


            Model::Model(const morefem_data_type& morefem_data)
            : parent(morefem_data),
            output_directory_(morefem_data.GetResultDirectory())
            { }
            
            
            namespace // anonymous
            {
            
                
                void ThrowIfNotExpected(const std::map<unsigned int, unsigned int>& expected,
                                        const std::map<unsigned int, unsigned int>& obtained)
                {
                    if (obtained != expected)
                    {
                        std::ostringstream oconv;
                        
                        oconv << "Domains do not contain the expected number of elements: prevision was ";
                        Utilities::PrintAssociativeContainer(expected, oconv);
                        oconv << " and what was obtained was: ";
                        Utilities::PrintAssociativeContainer(obtained, oconv);
                        oconv << " (key is domain id, value the number of GeometricElt inside).";
                        
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }
                }
                
                
            } // namespace anonymous

            
            void Model::SupplInitialize(const morefem_data_type& morefem_data)
            {
                static_cast<void>(morefem_data);
                
                decltype(auto) lightweigth_domain_manager = Advanced::LightweightDomainListManager::GetInstance();
                
                decltype(auto) domain_list = lightweigth_domain_manager.GetLightweightDomainList(1).GetList();
                
                decltype(auto) mesh = parent::GetGeometricMeshRegion(1);
                
                decltype(auto) geom_elt_list = mesh.GetGeometricEltList();
                
                std::map<unsigned int, unsigned int> Nper_domain;
                decltype(auto) mpi = parent::GetMpi();
                
                for (const auto& domain_ptr : domain_list)
                {
                    assert(!(!domain_ptr));
                    const auto& domain = *domain_ptr;
                    
                    unsigned int counter = 0u;
                    
                    for (const auto& geom_elt_ptr : geom_elt_list)
                    {
                        assert(!(!geom_elt_ptr));
                        const auto& geom_elt = *geom_elt_ptr;
                        
                        if (domain.IsGeometricEltInside(geom_elt))
                            ++counter;
                    }
                    
                    const auto check = Nper_domain.insert({domain.GetUniqueId(), counter});
                    assert(check.second && "Each domain should be read only once!");
                    static_cast<void>(check);
                }
                
                const auto Nprocessor = mpi.Nprocessor<int>();
                
                if (Nprocessor == 1)
                {
                    std::map<unsigned int, unsigned int> expected_result
                    { { { 3, 16}, { 4, 29 }, { 5, 8 }, { 6, 10 } } };
                    
                    ThrowIfNotExpected(expected_result, Nper_domain);

                }
                else if (Nprocessor == 4)
                {
                    // We can't really compare processor by processor, as parallel repartition may differ from one
                    // compiler to another. So we gather them all and check all the requested geometric elements are
                    // there.
                    // The expected sum is not the same as in sequential as I have only defined one \a FEltSpace over
                    // the highest dimensions; all segments have been dropped in the reduction as they were
                    // irrelevant.
                    std::map<unsigned int, unsigned int> expected_result
                    { { { 3, 2}, { 4, 20 }, { 5, 8 }, { 6, 10 } } };
                    
                    std::vector<unsigned int> Nper_domain_vector;
                    Nper_domain_vector.reserve(4ul);
                    
                    for (const auto& pair : Nper_domain)
                        Nper_domain_vector.push_back(pair.second);
                    
                    auto sum = mpi.ReduceOnRootProcessor(Nper_domain_vector, Wrappers::MpiNS::Op::Sum);
                    
                    // Put the sum over all processors in Nper_domain
                    if (mpi.IsRootProcessor())
                    {
                        auto index = 0ul;
                        for (auto& pair : Nper_domain)
                            pair.second = sum[index++];
                    
                        ThrowIfNotExpected(expected_result, Nper_domain);
                    }
                }
                else
                    throw Exception("This test was written to be checked against 1 or 4 processors.",
                                    __FILE__, __LINE__);
            }
            
        
            void Model::Forward()
            { }


            void Model::SupplFinalizeStep()
            { }


            void Model::SupplFinalize()
            { }


    
        } // namespace LightweightDomainListNS


    } // namespace TestNS


} // namespace MoReFEM
