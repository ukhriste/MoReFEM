add_executable(MoReFEMTestCoordsInParallel
               ${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
               ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              )
          
target_link_libraries(MoReFEMTestCoordsInParallel
                      ${MOREFEM_MODEL})
                      
morefem_install(MoReFEMTestCoordsInParallel)                       