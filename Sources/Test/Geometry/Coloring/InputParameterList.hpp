/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 22:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"

# include "Core/InputParameter/TimeManager/TimeManager.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS
    {
        
        
        namespace ColoringNS
        {
            
            
            //! Default value for some input parameter that are required by a MoReFEM model but are actually unused for current test.
            enum class Unused
            {
                value = 1
            };
            
            
            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };
            
            
            //! Enum used to index numbering subsets, domains and finite element spaces.
            enum class DimensionIndex
            {
                Dim1 = 1,
                Dim2 = 2
            };
            

            //! \copydoc doxygen_hide_input_parameter_tuple
            using InputParameterTuple = std::tuple
            <
                InputParameter::TimeManager,
            
                InputParameter::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim1)>,
                InputParameter::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim2)>,
            
                InputParameter::Unknown<EnumUnderlyingType(Unused::value)>,
            
                InputParameter::Domain<EnumUnderlyingType(DimensionIndex::Dim1)>,
                InputParameter::Domain<EnumUnderlyingType(DimensionIndex::Dim2)>,
            
                InputParameter::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim1)>,
                InputParameter::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim2)>,
            
                InputParameter::Petsc<EnumUnderlyingType(Unused::value)>,
            
                InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,
            
                InputParameter::Result
            >;
            
            
            //! \copydoc doxygen_hide_model_specific_input_parameter_list
            using InputParameterList = InputParameterList<InputParameterTuple>;
            
            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputParameterList>;
            
            
        } // namespace P1_to_P_HigherOrder_NS
        
        
    } // namespace TestNS
    
    
} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COLORING_x_INPUT_PARAMETER_LIST_HPP_
