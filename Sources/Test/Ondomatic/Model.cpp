/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Ondomatic/Model.hpp"


namespace MoReFEM
{
    
    
    namespace OndomaticNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }


        void Model::SupplInitialize(const morefem_data_type& morefem_data)
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            
            const auto& numbering_subset = god_of_dof.GetNumberingSubset(1);
            
            {
                const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance();
                
                auto&& bc_list =
                { bc_manager.GetDirichletBoundaryConditionPtr("face123"),
                };
                
                variational_formulation_ =
                    std::make_unique<VariationalFormulation>(morefem_data,
                                                             numbering_subset,
                                                             GetNonCstTimeManager(),
                                                             god_of_dof,
                                                             std::move(bc_list));
            }
            
            auto& variational_formulation = GetNonCstVariationalFormulation();
            const auto& mpi = GetMpi();
            
            variational_formulation.Init(morefem_data.GetInputParameterList());
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n",
                                                          mpi, __FILE__, __LINE__);
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n",
                                                          mpi, __FILE__, __LINE__);
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n",
                                                          mpi, __FILE__, __LINE__);
            
            
            variational_formulation.SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset,
                                                                  __FILE__, __LINE__);
            variational_formulation.DebugPrintSolution(numbering_subset);
            variational_formulation.DebugPrintNorm(numbering_subset);
            variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
        }


        void Model::Forward()
        {
        }


        void Model::SupplFinalizeStep()
        {
        }
        


        void Model::SupplFinalize()
        {
        }
        
            


    } // namespace OndomaticNS


} // namespace MoReFEM
