/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 13 Sep 2017 17:32:09 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS
    {
        
        
        void CheckMatrix(const GodOfDof& god_of_dof,
                         const GlobalMatrix& obtained,
                         const std::vector<std::vector<PetscScalar>>& expected,
                         const char* invoking_file, int invoking_line,
                         const double epsilon)
        {
            PetscInt Nrow, Ncol;
            std::tie(Nrow, Ncol) = obtained.GetProgramWiseSize(invoking_file, invoking_line);
            
            const auto Nexpected_program_wise_row = expected.size();
            
            assert(Nexpected_program_wise_row > 0);
            
            const auto Ncol_first_row = expected.front().size();
            
            # ifndef NDEBUG
            {
                for (auto i = 1ul; i < Nexpected_program_wise_row; ++i)
                {
                    assert("The expected matrix is assumed to be dense in this function."
                           && expected[i].size() == Ncol_first_row);
                }
            }
            # endif // NDEBUG
            
            if (Ncol_first_row != static_cast<std::size_t>(Ncol))
            {
                std::ostringstream oconv;
                oconv << "Mismatch between expected and obtained matrix format: a matrix with " << Ncol_first_row
                << " columns was expected but the obtained matrix got " << Ncol << '.';
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }
            
            
            if (Nexpected_program_wise_row != static_cast<std::size_t>(Nrow))
            {
                std::ostringstream oconv;
                oconv << "Mismatch between expected and obtained matrix format: a matrix with " << Nexpected_program_wise_row
                << " program-wise rows was expected but the obtained matrix got " << Nrow << '.';
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }
            
            decltype(auto) row_numbering_subset = obtained.GetRowNumberingSubset();
            
            decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();
            
            for (const auto& dof_ptr : processor_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;
                
                if (!dof.IsInNumberingSubset(row_numbering_subset))
                    continue;
                
                const auto program_wise_index = static_cast<PetscInt>(dof.GetProgramWiseIndex(row_numbering_subset));
                
                assert(program_wise_index < static_cast<PetscInt>(expected.size())
                       && "Should be guaranteed by previous asserts!");
                decltype(auto) expected_row = expected[static_cast<std::size_t>(program_wise_index)];
                
                for (auto col = 0; col < Ncol; ++col)
                {
                    // Remember: Petsc convention is that 0 is returned if location is sparse.
                    const auto obtained_value =
                    obtained.GetValue(program_wise_index, col, invoking_file, invoking_line);
                    
                    const auto expected_value = expected_row[static_cast<std::size_t>(col)];
                    
                    if (!NumericNS::AreEqual(obtained_value, expected_value, epsilon))
                    {
                        std::ostringstream oconv;
                        oconv << "Mismatch between expected and obtained matrix: value obtained for program-wise ("
                        << program_wise_index << ", " << col << ") was " << obtained_value << " whereas "
                        << expected_value << " was expected.";
                        throw Exception(oconv.str(), invoking_file, invoking_line);
                    }
                }
            }
        }
        
        
        void CheckVector(const GodOfDof& god_of_dof,
                         const GlobalVector& obtained,
                         const std::vector<PetscScalar>& expected,
                         const char* invoking_file, int invoking_line,
                         const double epsilon)
        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(obtained,
                                                                                       invoking_file, invoking_line);
            
            const auto size = obtained.GetProgramWiseSize(invoking_file, invoking_line);
            
            if (size != static_cast<PetscInt>(expected.size()))
            {
                std::ostringstream oconv;
                oconv << "Mismatch between expected and obtained vector format: a vector with " << expected.size()
                << " rows was expected but the obtained vector got " << size << '.';
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }
            
            decltype(auto) numbering_subset = obtained.GetNumberingSubset();
            decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();
            
            for (const auto& dof_ptr : processor_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;
                
                if (!dof.IsInNumberingSubset(numbering_subset))
                    continue;
                
                const auto processor_wise_index =
                    dof.GetProcessorWiseOrGhostIndex(numbering_subset);
                
                const auto program_wise_index =
                    dof.GetProgramWiseIndex(numbering_subset);
                
                const auto obtained_value = content.GetValue(processor_wise_index);
                
                assert(static_cast<std::size_t>(processor_wise_index) < expected.size());
                const auto expected_value = expected[static_cast<std::size_t>(program_wise_index)];
                
                if (!NumericNS::AreEqual(obtained_value, expected_value, epsilon))
                {
                    std::ostringstream oconv;
                    oconv << "Mismatch between expected and obtained vector: value obtained for program-wise ("
                    << dof.GetProgramWiseIndex(numbering_subset) << ") was " << obtained_value << " whereas "
                    << expected_value << " was expected.";
                    throw Exception(oconv.str(), invoking_file, invoking_line);
                }
                
            }
            
            
            
        }
        

        
     
        
    } // namespace TestNS


} // namespace MoReFEM
