///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Dec 2014 16:05:16 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_EXCEPTIONS_x_EXCEPTION_HPP_
# define MOREFEM_x_POST_PROCESSING_x_EXCEPTIONS_x_EXCEPTION_HPP_

# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace ExceptionNS
        {


            //! Exception when file is ill-formatted.
            class InvalidFormatInFile final : public ::MoReFEM::Exception
            {
            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor with simple message.
                 *
                 * \param[in] file File for which the problem occured.
                 * \param[in] description The issue might be explained more explicitly there as a string.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                explicit InvalidFormatInFile(const std::string& file, const std::string& description,
                                             const char* invoking_file, int invoking_line);

                //! Destructor.
                virtual ~InvalidFormatInFile();

                //! Copy constructor.
                InvalidFormatInFile(const InvalidFormatInFile&) = default;

                //! Move constructor.
                InvalidFormatInFile(InvalidFormatInFile&&) = default;

                //! Affectation.
                InvalidFormatInFile& operator=(const InvalidFormatInFile&) = default;

                //! Affectation.
                InvalidFormatInFile& operator=(InvalidFormatInFile&&) = default;

                ///@}

            };


            //! Exception when format line is invalid.
            class InvalidFormatInLine final : public ::MoReFEM::Exception
            {
            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor with simple message.
                 *
                 * \param[in] line Line in which the problem occurred.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit InvalidFormatInLine(const std::string& line, const char* invoking_file, int invoking_line);

                //! Same with a description of the issue.
                explicit InvalidFormatInLine(const std::string& line, const std::string&  description,
                                             const char* invoking_file, int invoking_line);

                //! Destructor.
                virtual ~InvalidFormatInLine() ;

                //! Copy constructor.
                InvalidFormatInLine(const InvalidFormatInLine&) = default;

                //! Move constructor.
                InvalidFormatInLine(InvalidFormatInLine&&) = default;

                //! Affectation.
                InvalidFormatInLine& operator=(const InvalidFormatInLine&) = default;

                //! Affectation.
                InvalidFormatInLine& operator=(InvalidFormatInLine&&) = default;

                ///@}

            };



        } // namespace ExceptionNS


    } // namespace PostProcessingNS



} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_EXCEPTIONS_x_EXCEPTION_HPP_
