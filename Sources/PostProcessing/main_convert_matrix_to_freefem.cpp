/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"


#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/InputParameterData/Advanced/SetFromInputParameterData.hpp"
#include "Core/InputParameterData/InputParameterList.hpp"
#include "Core/InputParameter/Geometry/Mesh.hpp"
#include "Core/InputParameter/Result.hpp"
#include "Core/InputParameter/FElt/NumberingSubset.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"

#include "PostProcessing/ConvertLinearAlgebra/Freefem/MatrixConversion.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    constexpr const auto mesh_id = 1;
    constexpr const auto numbering_subset_id = 15;
    
    
    try
    {
        using Mesh = InputParameter::Mesh<1>;
        
        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::Mesh<mesh_id>,
        
            InputParameter::NumberingSubset<numbering_subset_id>,
        
            InputParameter::Result::OutputDirectory
        >;

        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;
        
        MoReFEMData<InputParameterList, Utilities::InputParameterListNS::DoTrackUnusedFields::no> morefem_data(argc, argv);
                
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        // Path to the matrix to convert, hardcoded at the moment.
        const std::string matrix_path =
            Utilities::EnvironmentNS::SubstituteValues("/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Mesh_1/NumberingSubset_15/full_matrix_1.binary.hhdata");
        const std::string freefem_dof_numbering =
            Utilities::EnvironmentNS::SubstituteValues("/Users/${USER}/Desktop/freefem_numbering.txt");
        const std::string output_dir =
            Utilities::EnvironmentNS::SubstituteValues("/Volumes/Data/${USER}/Sandbox/Poromechanics");
        
        try
        {
            namespace IPL = Utilities::InputParameterListNS;
            
            decltype(auto) mesh_file = IPL::Extract<Mesh::Path>::Path(input_parameter_data);
            using Result = InputParameter::Result;
            decltype(auto) result_directory = IPL::Extract<Result::OutputDirectory>::Folder(input_parameter_data);
            
            if (!FilesystemNS::Folder::DoExist(result_directory))
                throw Exception("The specified directory doesn't exist!", __FILE__, __LINE__);
            
            auto& mesh_manager = Internal::MeshNS::GeometricMeshRegionManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, mesh_manager);
        
            const GeometricMeshRegion& mesh = mesh_manager.GetMesh(mesh_id);
        
            auto& numbering_subset_manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<Internal::NumberingSubsetNS::NumberingSubsetManager>(input_parameter_data,
                                                                                                     numbering_subset_manager);
            
            const auto& numbering_subset =
                Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance().GetNumberingSubset(numbering_subset_id);
            
            PostProcessingNS::FreefemNS::MatrixConversion conversion(mpi,
                                                                     freefem_dof_numbering,
                                                                     matrix_path,
                                                                     result_directory,
                                                                     { "fluid_mass", "fluid_pressure", "fluid_velocity"},
                                                                     //{ "fluid_velocity"},
                                                                     //{ "fluid_pressure"},
                                                                     output_dir,
                                                                     numbering_subset,
                                                                     mesh,
                                                                     __FILE__, __LINE__);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

