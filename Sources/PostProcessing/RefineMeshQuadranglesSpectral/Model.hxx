///
////// \file
///
///
/// Created by Federica Caforio <federica.caforio@inria.fr> on the Thu, 12 May 2016 16:34:28 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_MODEL_HXX_
# define MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_MODEL_HXX_


namespace MoReFEM
{


    namespace RefineMeshNS
    {


        template<class MoReFEMDataT>
        Model::Model(const MoReFEMDataT& morefem_data)
        : parent(morefem_data)
        { }


        template<class InputParameterDataT>
        void Model::SupplInitialize(const InputParameterDataT& input_parameter_data)
        {
            static_cast<void>(input_parameter_data);

            decltype(auto) god_of_dof = GodOfDofManager::GetInstance().GetGodOfDof(1);
            decltype(auto) felt_space = god_of_dof.GetFEltSpace(1);
            decltype(auto) unknown = UnknownManager::GetInstance().GetUnknown(1);
            decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

            felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr),
                                           DoComputeProcessorWiseLocal2Global::yes);

            RefineMeshNS::RefineMeshSpectral(felt_space,
                                             god_of_dof.GetGeometricMeshRegion(),
                                             parent::GetOutputDirectory());
        }



        inline const std::string& Model::ClassName()
        {
            static std::string name("RefineMesh");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return true; // No time iteration!
        }


        inline void Model::SupplInitializeStep()
        { }


    } // namespace RefineMeshNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_MODEL_HXX_
