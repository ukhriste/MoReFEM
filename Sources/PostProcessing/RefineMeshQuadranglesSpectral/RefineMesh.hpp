///
////// \file
///
///
/// Created by Federica Caforio <federica.caforio@inria.fr> on the Mon, 6 Jun 2016 16:19:29 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HPP_
# define MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HPP_

# include <memory>
# include <vector>

# include "Geometry/Mesh/GeometricMeshRegion.hpp"
# include "Geometry/RefGeometricElt/RefGeomElt.hpp"

# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
# include "Geometry/Coords/LocalCoords.hpp"
# include "Geometry/Coords/Coords.hpp"

# include "FiniteElement/RefFiniteElement/Instantiation/Spectral.hpp"

# include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"

# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"

# include "Geometry/GeometricElt/Instances/Quadrangle/Quadrangle4.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"

# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM
{


    namespace RefineMeshNS
    {


        /*!
         * \brief Create a new refined mesh, whose vertices are the coordinates of the DOFs.
         *
         *  Useful for higher order shape functions. Remark: the
         *  numbering is specific for spectral elements and the algorithm is only valid for quadrangles.
         * \param[in] felt_space finite element space instance,
         * \param[in] mesh initial mesh to be refined,
         * \param[in] output_directory output directory in which the refined mesh will be saved.
         */
        void RefineMeshSpectral(const FEltSpace& felt_space,
                                const GeometricMeshRegion& mesh,
                                const std::string& output_directory);


    } // namespace RefineMesh


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/RefineMeshQuadranglesSpectral/RefineMesh.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_REFINE_MESH_QUADRANGLES_SPECTRAL_x_REFINE_MESH_HPP_
