///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 09:45:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#include <set>

#include "Utilities/String/String.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {
        
        
        PostProcessing::PostProcessing(const std::string& data_directory,
                                       const std::vector<unsigned int>& numbering_subset_id_list,
                                       const GeometricMeshRegion& geometric_mesh_region)
        : data_directory_(data_directory),
        geometric_mesh_region_(geometric_mesh_region),
        Nprocessor_(NumericNS::UninitializedIndex<std::size_t>()),
        interface_file_(nullptr),
        unknown_file_(nullptr),
        time_iteration_file_(nullptr),
        numbering_subset_id_list_(numbering_subset_id_list)
        {
            
            // Read the different files.
            ReadInterfaceFile();
            
            //! Read the unknown file and store its content into the class.
            ReadUnknownFile();
            
            //! Read the time iteration file and store its content into the class.
            ReadTimeIterationFile();
            
            //! Read how many processors were involved in the simulation.
            ReadNprocessor();
            
            //! Read the dof files (one per processor) and store its content into the class.
            ReadDofFiles();
        }
        
        
        std::vector<double> PostProcessing::LoadVector(const std::string& file) const
        {
            std::ifstream stream;

            FilesystemNS::File::Read(stream, file, __FILE__, __LINE__);
            
            std::string line;
            
            std::vector<double> ret;
                
            while(getline(stream, line))
                ret.push_back(std::stod(line));
            
            return ret;
        }
        
                
        void PostProcessing::ReadInterfaceFile()
        {
            std::string input_file(GetDataDirectory() + "/Mesh_" + std::to_string(GetGeometricMeshRegion().GetUniqueId()));
            input_file += "/interfaces.hhdata";
            
            assert(!interface_file_);
            interface_file_ = std::make_unique<InterfaceFile>(input_file);
        }
        
        
        void PostProcessing::ReadUnknownFile()
        {
            std::string input_file(GetDataDirectory());
            input_file += "/unknowns.hhdata";
     
            assert(!unknown_file_);
            unknown_file_ = std::make_unique<UnknownInformationFile>(input_file);
        }
        
        
        void PostProcessing::ReadTimeIterationFile()
        {
            std::string input_file(GetDataDirectory() + "/Mesh_" + std::to_string(GetGeometricMeshRegion().GetUniqueId()));
            input_file += "/time_iteration.hhdata";
            
            assert(!time_iteration_file_);
            time_iteration_file_ = std::make_unique<TimeIterationFile>(input_file);
        }
        
        
        void PostProcessing::ReadNprocessor()
        {
            assert(Nprocessor_ == NumericNS::UninitializedIndex<std::size_t>());
            std::ifstream in;
            
            FilesystemNS::File::Read(in, GetDataDirectory() + "/mpi.hhdata", __FILE__, __LINE__);
            
            std::string line;
            getline(in, line);
            
            const std::string sequence("Nprocessor:");
            
            if (!Utilities::String::StartsWith(line, sequence))
                throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
            
            line.erase(0, sequence.size());
            
            Nprocessor_ = std::stoul(line);
        }
        
        
        void PostProcessing::ReadDofFiles()
        {
            DofInformationFile::vector_unique_ptr dof_information_file_list_for_numbering_subset;
                
            const std::size_t Nproc = Nprocessor();
            dof_information_file_list_for_numbering_subset.reserve(Nproc);
            
            assert(Ndof_ == 0u);
            
            const auto& interface_file = GetInterfaceData();
            
            decltype(auto) numbering_subset_id_list = GetNumberingSubsetIdList();
            
            auto set_numbering_subset_id_list = std::set<unsigned int>(numbering_subset_id_list.cbegin(),
                                                                       numbering_subset_id_list.cend());
            
            for (const auto& numbering_subset_id : set_numbering_subset_id_list)
            {
                std::ostringstream oconv;
                
                decltype(auto) numbering_subset_directory = GetNumberingSubsetDirectory(numbering_subset_id);
                
                for (std::size_t i = 0; i < Nproc; ++i)
                {
                    oconv.str("");
                    oconv << numbering_subset_directory << "/dof_information_proc_" << i
                    << ".hhdata";
                    auto&& dof_list_ptr= std::make_unique<DofInformationFile>(i, oconv.str(), interface_file);
                    Ndof_ += dof_list_ptr->Ndof();
                    
                    dof_information_file_list_for_numbering_subset.emplace_back(std::move(dof_list_ptr));
                }

                assert(dof_information_file_list_for_numbering_subset.size() == Nproc);
                
                using pair_type = std::pair<unsigned int, DofInformationFile::vector_unique_ptr>;
                
                pair_type&& pair = { numbering_subset_id, std::move(dof_information_file_list_for_numbering_subset) };
                
                dof_file_per_processor_list_per_numbering_subset_.insert(std::move(pair));
            }
            
        }


        const DofInformationFile& PostProcessing::GetDofFile(const unsigned int numbering_subset_id,
                                                                      const std::size_t processor) const
        {
            assert(processor < Nprocessor());
            
            auto it = dof_file_per_processor_list_per_numbering_subset_.find(numbering_subset_id);
            assert(it != dof_file_per_processor_list_per_numbering_subset_.cend());
            
            const auto& dof_file_per_processor_list = it->second;
            
            assert(Nprocessor() == dof_file_per_processor_list.size());
            return *dof_file_per_processor_list[processor];
        }
        
        
        std::string PostProcessing::GetNumberingSubsetDirectory(const unsigned int numbering_subset_id) const
        {
            auto ret = GetDataDirectory() + "/Mesh_" +  std::to_string(GetGeometricMeshRegion().GetUniqueId())
            + "/NumberingSubset_"
            + std::to_string(numbering_subset_id) + "/";
            return ret;
        }
       
        
    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
