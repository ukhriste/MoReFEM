///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_DOF_INFORMATION_FILE_HPP_
# define MOREFEM_x_POST_PROCESSING_x_FILE_x_DOF_INFORMATION_FILE_HPP_

# include <memory>
# include <vector>

# include "PostProcessing/Data/DofInformation.hpp"


namespace MoReFEM
{


    // ============================
    // Forward declarations.
    // ============================


    namespace PostProcessingNS
    {


        class InterfaceFile;


    } // namespace PostProcessingNS


    // ============================
    // End of forward declarations.
    // ============================


    namespace PostProcessingNS
    {


        /*!
         * \brief Class which holds the informations obtained from dof_information_proc_*rank*.hhdata output files.
         *
         * There is one such set of files per \a NumberingSubset.
         *
         * Format of one such file is:
         *
         * \verbatim
         Ndof (processor_wise) = 291
         # First column: program-wise index.
         # Second column: processor-wise index.
         # Third column: the interface upon which the dof is located. Look at the interface file in same directory to relate it to the initial mesh.
         # Fourth column: unknown and component involved.
         # Fifth column: shape function label.
         0;0;Vertex 0;fluid_velocity 0;P1b
         1;1;Vertex 0;fluid_velocity 1;P1b
         2;2;Vertex 0;fluid_mass 0;P1
         3;3;Vertex 1;fluid_velocity 0;P1b
         4;4;Vertex 1;fluid_velocity 1;P1b
         ...
         \endverbatim
         */
        class DofInformationFile final
        {
        public:

            //! Alias for unique_ptr;
            using const_unique_ptr = std::unique_ptr<const DofInformationFile>;

            //! Alias for vector of unique_ptr;
            using vector_unique_ptr = std::vector<const_unique_ptr>;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit DofInformationFile(unsigned int processor,
                                        const std::string& input_file,
                                        const InterfaceFile& interface_file);

            //! Destructor.
            ~DofInformationFile() = default;

            //! Copy constructor.
            DofInformationFile(const DofInformationFile&) = default;

            //! Move constructor.
            DofInformationFile(DofInformationFile&&) = default;

            //! Affectation.
            DofInformationFile& operator=(const DofInformationFile&) = default;

            //! Affectation.
            DofInformationFile& operator=(DofInformationFile&&) = default;

            ///@}

        public:

            //! Return the entire list of dof.
            const Data::DofInformation::vector_const_shared_ptr& GetDofList() const noexcept;

            /*!
             * \brief Return the list of dof information that match the interface for the given unknown.
             */
            Data::DofInformation::vector_const_shared_ptr GetDof(InterfaceNS::Nature nature,
                                                                 unsigned int interface_index,
                                                                 const std::string& unknown) const;

            /*!
             * \brief Return the dof information that match the interface for the given unknown/component.
             */
            const Data::DofInformation& GetDof(InterfaceNS::Nature nature,
                                               unsigned int interface_index,
                                               const std::string& unknown,
                                               unsigned int component) const;

            //! Return the number of dofs.
            unsigned int Ndof() const noexcept;

            //! Return the processor.
            unsigned int GetProcessor() const noexcept;


        private:

            //! Processor the data are related to.
            const unsigned int processor_;

            //! List of dofs with their related informations.
            Data::DofInformation::vector_const_shared_ptr dof_information_list_;


        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/File/DofInformationFile.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_DOF_INFORMATION_FILE_HPP_
