///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_
# define MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_

# include <string>

# include "PostProcessing/Data/TimeIteration.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        /*!
         * \brief Class which holds the informations obtained from time_iteration.hhdata output file.
         *
         * There is one such file per mesh.
         *
         * Example:
         * \verbatim
         # Time iteration; time; numbering subset id; filename
         1;0.001;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Mesh_1/NumberingSubset_10/fluid_velocity_time_00001_proc_*.hhdata
         2;0.002;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Mesh_1/NumberingSubset_10/fluid_velocity_time_00002_proc_*.hhdata
         ...
         \endverbatim
         *
         */
        class TimeIterationFile final
        {
        public:

            //! Alias to most relevant smart pointer.
            using const_unique_ptr = std::unique_ptr<const TimeIterationFile>;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TimeIterationFile(const std::string& input_file);

            //! Destructor.
            ~TimeIterationFile() = default;

            //! Copy constructor.
            TimeIterationFile(const TimeIterationFile&) = delete;

            //! Move constructor.
            TimeIterationFile(TimeIterationFile&&) = delete;

            //! Affectation.
            TimeIterationFile& operator=(const TimeIterationFile&) = delete;

            //! Affectation.
            TimeIterationFile& operator=(TimeIterationFile&&) = delete;

            ///@}

        public:

            //! Accessor to the list of time iterations.
            const Data::TimeIteration::vector_const_unique_ptr& GetTimeIterationList() const;

            //! Number of steps.
            unsigned int Nstep() const;

        private:

            //! List of all time iterations considered.
            Data::TimeIteration::vector_const_unique_ptr time_iteration_list_;

        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/File/TimeIterationFile.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_
