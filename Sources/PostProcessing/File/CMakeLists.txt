target_sources(${MOREFEM_POST_PROCESSING}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DofInformationFile.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceFile.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeIterationFile.cpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnknownInformationFile.cpp" / 

#	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/DofInformationFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/DofInformationFile.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/InterfaceFile.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeIterationFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/TimeIterationFile.hxx" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnknownInformationFile.hpp" / 
		"${CMAKE_CURRENT_LIST_DIR}/UnknownInformationFile.hxx" / 
)

