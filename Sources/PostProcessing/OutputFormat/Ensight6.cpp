///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 16:13:29 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#include <fstream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Mesh/GeometricMeshRegion.hpp"

#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/OutputFormat/Ensight6.hpp"


namespace MoReFEM
{
    
    
    namespace PostProcessingNS
    {
        
        
        namespace OutputFormat
        {
            
            
            namespace // anonymous
            {
                
                
                
                void CreateCaseFile(const std::string& output_directory,
                                    const Data::UnknownInformation::vector_const_shared_ptr& unknown_list,
                                    const Data::TimeIteration::vector_const_unique_ptr& time_iteration_list);
                
                void CreateUnknownFile(const Data::UnknownInformation& unknown,
                                       const unsigned int numbering_subset_id,
                                       const PostProcessingNS::Data::TimeIteration& time_iteration,
                                       const std::string& data_directory,
                                       const PostProcessing& post_processing,
                                       RefinedMesh is_mesh_refined);
                
                
                
            } // namespace anonymous
            
                   
            Ensight6::Ensight6(const std::string& data_directory,
                               const std::vector<std::string>& unknown_list,
                               const std::vector<unsigned int>& numbering_subset_id_list,
                               const GeometricMeshRegion& geometric_mesh_region,
                               RefinedMesh is_mesh_refined)
            {
                assert(unknown_list.size() == numbering_subset_id_list.size());
                const auto Nunknown = unknown_list.size();

                if (!FilesystemNS::Folder::DoExist(data_directory))
                    throw Exception("Invalid data directory: '" + data_directory + "' doesn't exist!",
                                    __FILE__, __LINE__);
                
                const std::string mesh_directory = data_directory + "/Mesh_"
                                                 + std::to_string(geometric_mesh_region.GetUniqueId()) + "/";
                
                if (!FilesystemNS::Folder::DoExist(mesh_directory))
                    throw Exception("Invalid mesh directory: '" + mesh_directory + "' doesn't exist!",
                                    __FILE__, __LINE__);
                
                const std::string ensight_directory = mesh_directory + "Ensight6/";
                
                if (FilesystemNS::Folder::DoExist(ensight_directory))
                    FilesystemNS::Folder::Remove(ensight_directory, __FILE__, __LINE__);
                
                FilesystemNS::Folder::Create(ensight_directory, __FILE__, __LINE__);
                
                PostProcessing post_processing(data_directory,
                                               numbering_subset_id_list,
                                               geometric_mesh_region);
                
                geometric_mesh_region.Write<MeshNS::Format::Ensight>(ensight_directory + "mesh.geo");
                
                const auto& time_iteration_list = post_processing.GetTimeIterationList();
                
                // Create the case file.
                decltype(auto) complete_unknown_list = post_processing.GetExtendedUnknownList();
                Data::UnknownInformation::vector_const_shared_ptr selected_unknown_list;
                
                
                // Associate the more complete unknown object from the name given in input.
                {
                    for (const auto& unknown_name : unknown_list)
                    {
                        const auto it = std::find_if(complete_unknown_list.cbegin(),
                                                     complete_unknown_list.cend(),
                                                     [&unknown_name](const auto& unknown_ptr)
                                                     {
                                                         assert(!(!unknown_ptr));
                                                         return unknown_ptr->GetName() == unknown_name;
                                                     });
                        
                        if (it == complete_unknown_list.cend())
                            throw Exception("Unknown '" + unknown_name + "' required in the constructor was not one of "
                                            "those used in the model.",
                                            __FILE__, __LINE__);
                        
                        selected_unknown_list.push_back(*it);
                    }
                }
                
                assert(selected_unknown_list.size() == Nunknown);
                CreateCaseFile(ensight_directory,
                               selected_unknown_list,
                               time_iteration_list);
                
                // Sort the unknowns by associated numbering subset.
                std::multimap<unsigned int, Data::UnknownInformation::const_shared_ptr> unknown_list_per_numbering_subset;
                
                for (auto i = 0ul; i < Nunknown; ++i)
                    unknown_list_per_numbering_subset.insert({numbering_subset_id_list[i], selected_unknown_list[i]});
                
                
                // Iterate over time:
                for (const auto& time_iteration_ptr : time_iteration_list)
                {
                    assert(!(!time_iteration_ptr));
                    const auto& time_iteration = *time_iteration_ptr;
                    
                    const auto range = unknown_list_per_numbering_subset.equal_range(time_iteration.GetNumberingSubsetId());
                    
                    
                    for (auto it = range.first; it != range.second; ++it)
                    {
                        const auto& unknown_list_for_numbering_subset = *it;
                        
                        decltype(auto) unknown_ptr = unknown_list_for_numbering_subset.second;
                        assert(!(!unknown_ptr));
                        
                        CreateUnknownFile(*unknown_ptr,
                                          unknown_list_for_numbering_subset.first,
                                          time_iteration,
                                          ensight_directory,
                                          post_processing,
                                          is_mesh_refined);
                    }
                }
                
            }
            
            
            namespace // anonymous
            {
                
                
                
                void CreateCaseFile(const std::string& output_directory,
                                    const Data::UnknownInformation::vector_const_shared_ptr& unknown_list,
                                    const Data::TimeIteration::vector_const_unique_ptr& time_iteration_list)
                {
                    std::string case_file(output_directory);
                    case_file += "/problem.case";
                    
                    std::ofstream stream;
                    
                    FilesystemNS::File::Create(stream, case_file, __FILE__, __LINE__);
                    
                    stream << "FORMAT" << std::endl;
                    stream << "type: ensight" << std::endl;
                    stream << "GEOMETRY" << std::endl;
                    stream << "model: 1 mesh.geo" << std::endl;
                    stream << "VARIABLE" << std::endl;
                    
                    for (const auto& unknown_ptr : unknown_list)
                    {
                        assert(!(!unknown_ptr));
                        
                        switch (unknown_ptr->GetNature())
                        {
                            case Data::UnknownNature::scalar:
                                stream << "scalar";
                                break;
                            case Data::UnknownNature::vectorial:
                                stream << "vectorial";
                                break;
                        }
                        
                        const auto& name = unknown_ptr->GetName();
                        
                        stream << " per node: 1 " << name << ' ' << name << ".*****.scl" << std::endl;
                    }
                    
                    stream << "TIME" << std::endl;
                    stream << "time set: 1" << std::endl;
                
                    unsigned int count = 0u;
                    
                    unsigned int previous_index = static_cast<unsigned int>(-1);
                    
                    std::ostringstream time_values_stream;
                    std::ostringstream time_index_stream;
                    
                    auto Ntime_iteration = 0u;
                    
                    for (const auto& time_iteration_ptr : time_iteration_list)
                    {
                        assert(!(!time_iteration_ptr));
                        const auto& time_iteration = *time_iteration_ptr;
                        
                        const auto current_iteration = time_iteration.GetIteration();
                        
                        if (current_iteration == previous_index) // May happen if several unknowns for same time step.
                            continue;
                        
                        ++Ntime_iteration;
                        
                        time_index_stream << current_iteration;
                        time_values_stream << time_iteration.GetTime();
                        
                        if (++count % 5u == 0u) // lines must not exceed 79 characters; I'm truly on the very safe side here!
                        {
                            time_index_stream << std::endl;
                            time_values_stream << std::endl;
                        }
                        else
                        {
                            time_index_stream << ' ';
                            time_values_stream << ' ';
                        }
                        
                        previous_index = current_iteration;
                    }
                    
                    stream << "number of steps: " << Ntime_iteration << std::endl;
                    stream << "filename numbers: ";
                    stream << time_index_stream.str();
                    
                    stream << std::endl;
                    
                    stream << "time values: ";
                    stream << time_values_stream.str();
                    
                    stream << std::endl;                    
                }
                
                
                void CreateUnknownFile(const Data::UnknownInformation& unknown,
                                       const unsigned int numbering_subset_id,
                                       const PostProcessingNS::Data::TimeIteration& time_iteration,
                                       const std::string& output_directory,
                                       const PostProcessing& post_processing,
                                       RefinedMesh is_mesh_refined)
                {
                    const auto& unknown_name = unknown.GetName();
                    
                    const auto& geometric_mesh_region = post_processing.GetGeometricMeshRegion();
                    
                    std::ostringstream oconv;
                    oconv << output_directory;
                    oconv << '/' << unknown_name << '.' << std::setfill('0') << std::setw(5)
                    << time_iteration.GetIteration() << ".scl";
                    
                    std::string ensight_output_file(oconv.str());
                    
                    std::ofstream ensight_output_stream;
                    
                    FilesystemNS::File::Create(ensight_output_stream, ensight_output_file, __FILE__, __LINE__);
                    ensight_output_stream << "First line which content must be chosen (but later)" << std::endl;
                    
                    
                    const unsigned int dimension = geometric_mesh_region.GetDimension();
                    
                    const auto unknown_nature = unknown.GetNature();
                    
                    const std::size_t Nprocessor = post_processing.Nprocessor();
                    
                    using DofWithValuesType = std::pair<Data::DofInformation::const_shared_ptr, double>;
                    
                    std::vector<DofWithValuesType> dof_with_values_type;
                    
                    #ifndef NDEBUG
                    if (is_mesh_refined == RefinedMesh::yes)
                    {
                        assert(Nprocessor == 1ul
                               && "At the moment, restricted to model run sequentially.");
                    }
                    #endif // NDEBUG
                    
                    decltype(auto) filename_prototype = time_iteration.GetSolutionFilename();
                    
                    for (std::size_t processor = 0u; processor < Nprocessor; ++processor)
                    {
                        const auto& dof_list = post_processing.GetDofInformationList(numbering_subset_id, processor);
                        
                        std::string filename = filename_prototype;
                        
                        const auto check = Utilities::String::Replace("*", std::to_string(processor), filename);
                        
                        assert(check == 1);
                        static_cast<void>(check);
                        
                        const auto& solution = post_processing.LoadVector(filename);
                        
                        for (const auto& dof_ptr : dof_list)
                        {
                            assert(!(!dof_ptr));
                            const auto& dof = *dof_ptr;
                            
                            if (dof.GetUnknown() != unknown_name)
                                continue;
                            
                            if (is_mesh_refined == RefinedMesh::no)
                            {
                                // Very crude: dofs not on vertex are simply ignored.
                                if (dof.GetInterfaceNature() != InterfaceNS::Nature::vertex)
                                {
                                    static bool first = true;
                                    
                                    if (first)
                                    {
                                        std::cout << "[WARNING] As ensight handles only P1, dofs not on vertices are "
                                        "blatantly ignored." << std::endl;
                                        first = false;
                                    }
                                    
                                    continue;
                                }
                                
                            }
                            
                            const unsigned int processor_wise_index = dof.GetProcessorWiseIndex();
                            
                            assert(processor_wise_index < solution.size());
                            
                            dof_with_values_type.push_back({dof_ptr, solution[processor_wise_index]});
                        }
                    }

                    
                    
                    // The sorting below is required to handle the parallel case; it assumes P1 or Q1.
                    // That's the reason refined mesh does not work with model run in parallel.
                    if (is_mesh_refined == RefinedMesh::no)
                    {
                        std::sort(dof_with_values_type.begin(), dof_with_values_type.end(),
                                  [](const DofWithValuesType& lhs, const DofWithValuesType& rhs)
                                  {
                                      if (lhs == rhs)
                                          return false;
                                      
                                      const auto& lhs_dof = lhs.first;
                                      const auto& rhs_dof = rhs.first;
                                      
                                      assert(!(!lhs_dof));
                                      assert(!(!rhs_dof));
                                      
                                      decltype(auto) lhs_vertex_coords_list = lhs_dof->GetInterface().GetVertexCoordsIndexList();
                                      decltype(auto) rhs_vertex_coords_list = rhs_dof->GetInterface().GetVertexCoordsIndexList();
                                      
                                      assert(lhs_vertex_coords_list.size() == 1ul && "At the moment EnsightOutput deals only with P1 (from both geometric and felt point of view)!");
                                      assert(rhs_vertex_coords_list.size() == 1ul && "At the moment EnsightOutput deals only with P1 (from both geometric and felt point of view)!");
                                      
                                      const unsigned int lhs_index = lhs_vertex_coords_list.back();
                                      const unsigned int rhs_index = rhs_vertex_coords_list.back();
                                      
                                      assert(lhs_dof->GetUnknown() == rhs_dof->GetUnknown()
                                             && "Filter upon unknowns should have occurred earlier");
                                      
                                      if (lhs_index != rhs_index)
                                          return lhs_index < rhs_index;
                                      
                                      assert(lhs_dof->GetUnknownComponent() != rhs_dof->GetUnknownComponent());
                                      
                                      return lhs_dof->GetUnknownComponent() < rhs_dof->GetUnknownComponent();
                                  });
                    }
                    
                    unsigned int counter = 0u;
                    
                    for (const auto& pair : dof_with_values_type)
                    {
                        ensight_output_stream << std::setw(12) << std::scientific << std::setprecision(5)
                        << pair.second;
                        
                        assert(!(!pair.first));
                        const auto& dof = *(pair.first);
                        
                        if (unknown_nature == Data::UnknownNature::vectorial
                            && dimension == 2u && dof.GetUnknownComponent() == 1u)
                        {
                            ensight_output_stream << std::setw(12) << std::scientific << std::setprecision(5) << 0.;
                            if (++counter % 6u == 0u) // Ensight expects 6 values per line
                                ensight_output_stream << '\n';
                            
                        }
                        
                        if (++counter % 6u == 0u) // Ensight expects 6 values per line
                            ensight_output_stream << '\n';
                    }
                    
                    if (counter % 6u != 0u)
                        ensight_output_stream << '\n';
                }
                
                
            } // namespace anonymous
            
            
        } // namespace OutputFormat
        
        
    } // namespace PostProcessingNS
    
    
} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
