///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Oct 2013 09:09:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_OPS_x_OPS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_OPS_x_OPS_HPP_

# include "Utilities/Pragma/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

# ifdef __clang__
    PRAGMA_DIAGNOSTIC(ignored "-Wdangling-else")
    PRAGMA_DIAGNOSTIC(ignored "-Wdocumentation")
# endif // __clang__

#  ifdef MOREFEM_GCC
    PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
    PRAGMA_DIAGNOSTIC(ignored "-Wdangling-else")
#  endif // MOREFEM_GCC

PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")


# include "Ops.hxx"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_OPS_x_OPS_HPP_
