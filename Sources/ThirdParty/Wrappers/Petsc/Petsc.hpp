///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Sep 2013 14:51:15 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PETSC_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PETSC_HPP_

# include <memory>


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief RAII class to initialize and close PETSc properly.
             */
            struct Petsc final
            {


                //! Convenient alias against the relevant smart pointer to use with this RAII class.
                using const_unique_ptr = std::unique_ptr<const Petsc>;

                /// \name Special members.
                ///@{

                //! Constructor (call to PetscInitialize)
                explicit Petsc(const char* invoking_file, int invoking_line);

                //! Destructor (call to PetscFinalize).
                ~Petsc();

                //! Copy constructor.
                Petsc(const Petsc&) = delete;

                //! Move constructor.
                Petsc(Petsc&&) = delete;

                //! Copy affectation.
                Petsc& operator=(const Petsc&) = delete;

                //! Move affectation.
                Petsc& operator=(Petsc&&) = delete;

                ///@}


            };



        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PETSC_HPP_
