///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 12 Nov 2013 11:55:34 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            template<typename... Args>
            void PrintMessageOnFirstProcessor(const std::string& message, const Mpi& mpi,
                                              const char* invoking_file, int invoking_line, Args&&... arguments)
            {
                int error_code = PetscPrintf(mpi.GetCommunicator(), message.c_str(), std::forward<Args>(arguments)...);

                if (error_code)
                     throw ExceptionNS::Exception(error_code, "PetscPrintf", invoking_file, invoking_line);
            }


            template<typename... Args>
            void PrintSynchronizedMessage(const std::string& message, const Mpi& mpi,
                                          FILE* C_file,
                                          const char* invoking_file, int invoking_line, Args&&... arguments)
            {
                int error_code = PetscSynchronizedFPrintf(mpi.GetCommunicator(),
                                                          C_file,
                                                          message.c_str(),
                                                          std::forward<Args>(arguments)...);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PetscSynchronizedFPrintf", invoking_file, invoking_line);
            }


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HXX_
