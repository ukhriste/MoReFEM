///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#include <cassert>
#include <cmath>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMatPrivate.h"

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            Matrix::Matrix()
            : parent(),
            petsc_matrix_(PETSC_NULL),
            do_petsc_destroy_(false)
            { }
            
            
            Matrix::Matrix(const Mpi& mpi,
                           const std::string& binary_file,
                           const char* invoking_file, int invoking_line)
            : parent(),
            petsc_matrix_(PETSC_NULL),
            do_petsc_destroy_(true)
            {
                const auto communicator = mpi.GetCommunicator();
                
                Viewer viewer(mpi,
                              binary_file,
                              FILE_MODE_READ,
                              invoking_file, invoking_line);
                
                int error_code = MatCreate(communicator, &petsc_matrix_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreate", invoking_file, invoking_line);
                
                error_code = MatLoad(petsc_matrix_, viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatLoad", invoking_file, invoking_line);
            }
            
            
            Matrix::Matrix(const Matrix& rhs)
            : parent(rhs),
            petsc_matrix_(PETSC_NULL),
            do_petsc_destroy_(rhs.do_petsc_destroy_)
            {
                CompleteCopy(rhs, __FILE__, __LINE__);
            }
            
            
            Matrix::~Matrix()
            {
                if (do_petsc_destroy_)
                {
                    assert(petsc_matrix_ != PETSC_NULL);
                    int error_code = MatDestroy(&petsc_matrix_);
                    assert(!error_code && "Error in Mat destruction."); // no exception in destructors!
                    static_cast<void>(error_code); // to avoid arning in release compilation.
                }
            }
            
            
            void Swap(Matrix& A, Matrix& B)
            {
                using std::swap;
                swap(A.do_petsc_destroy_, B.do_petsc_destroy_);
                swap(A.petsc_matrix_, B.petsc_matrix_);
            }
            
                    
            
            void Matrix::DuplicateLayout(const Matrix& original, MatDuplicateOption option, const char* invoking_file, int invoking_line)
            {
                assert(petsc_matrix_ == PETSC_NULL && "Should not be initialized when this method is called!");

                int error_code = MatDuplicate(original.Internal(), option, &petsc_matrix_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatDuplicate", invoking_file, invoking_line);

		        do_petsc_destroy_ = original.do_petsc_destroy_;                
            }
            
            
            void Matrix::InitSequentialMatrix(unsigned int Nrow, unsigned int Ncolumn,
                                              const MatrixPattern& matrix_pattern,
                                              const Mpi& mpi, const char* invoking_file, int invoking_line)
            {
                assert(matrix_pattern.Nrow() == Nrow);
                const auto& communicator = mpi.GetCommunicator();
                
                assert(petsc_matrix_ == PETSC_NULL && "Should not be initialized when this method is called!");
                
                // Follow Petsc's advice and build the matrix step by step (see MatCreateSeqAIJ for this advice).
                int error_code = MatCreate(communicator, &petsc_matrix_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreate", invoking_file, invoking_line);
                
                const Mat& internal = Internal(); // const is indeed ignored below as Mat is truly a pointer...
                                
                error_code = MatSetType(internal, MATSEQAIJ);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetType", invoking_file, invoking_line);
                
                
                const PetscInt Nrow_int = static_cast<PetscInt>(Nrow);
                const PetscInt Ncol_int = static_cast<PetscInt>(Ncolumn);
                
                error_code = MatSetSizes(internal, Nrow_int, Ncol_int, Nrow_int, Ncol_int);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetSizes", invoking_file, invoking_line);
                
                
                // Preallocate the sparse positions. Petsc indicates it speeds up tremendously the creation
                // process. This must be called AFTER size of the matrix is set.
                error_code = MatSeqAIJSetPreallocationCSR(Internal(),
                                                          matrix_pattern.GetICsr().data(),
                                                          matrix_pattern.GetJCsr().data(),
                                                          PETSC_NULL);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSeqAIJSetPreallocation", invoking_file, invoking_line);
                
                
                error_code = MatSetOption(internal, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetOption", invoking_file, invoking_line);
                
                do_petsc_destroy_ = true;
            }
            
            
            
            void Matrix::InitParallelMatrix(unsigned int Nlocal_row, unsigned int Nlocal_column,
                                            unsigned int Nglobal_row, unsigned int Nglobal_column,
                                            const MatrixPattern& matrix_pattern,
                                            const Mpi& mpi, const char* invoking_file, int invoking_line)
            {
                assert(matrix_pattern.Nrow() == Nlocal_row);

                assert(petsc_matrix_ == PETSC_NULL && "Should not be initialized when this method is called!");
                
                const auto& communicator = mpi.GetCommunicator();
                
                // Follow Petsc's advice and build the matrix step by step (see MatCreateSeqAIJ for this advice).
                int error_code = MatCreate(communicator, &petsc_matrix_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreate", invoking_file, invoking_line);
                
                const Mat& internal = Internal(); // const is indeed ignored below as Mat is truly a pointer...
                
                error_code = MatSetType(internal, MATAIJ);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetType", invoking_file, invoking_line);
                
                const PetscInt Nlocal_row_int = static_cast<PetscInt>(Nlocal_row);
                const PetscInt Nlocal_col_int = static_cast<PetscInt>(Nlocal_column);
                const PetscInt Nglobal_row_int = static_cast<PetscInt>(Nglobal_row);
                const PetscInt Nglobal_col_int = static_cast<PetscInt>(Nglobal_column);
                                
                error_code = MatSetSizes(internal, Nlocal_row_int, Nlocal_col_int, Nglobal_row_int, Nglobal_col_int);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetSizes", invoking_file, invoking_line);
                
                
                // Preallocate the sparse positions. Petsc indicates it speeds up tremendously the creation
                // process. This must be called AFTER size of the matrix is set.               
                error_code = MatMPIAIJSetPreallocationCSR(internal,
                                                          matrix_pattern.GetICsr().data(),
                                                          matrix_pattern.GetJCsr().data(),
                                                          PETSC_NULL);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMPIAIJSetPreallocation", invoking_file, invoking_line);                
                
                error_code = MatSetOption(internal, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetOption", invoking_file, invoking_line);
                
                error_code = MatSetOption(internal, MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_TRUE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetOption", invoking_file, invoking_line);
                
                do_petsc_destroy_ = true;
            }
            
            
            void Matrix::InitSequentialDenseMatrix(unsigned int Nrow, unsigned int Ncolumn,
                                                   const Mpi& mpi, const char* invoking_file, int invoking_line)
            {
                const auto& communicator = mpi.GetCommunicator();
                
                assert(petsc_matrix_ == PETSC_NULL && "Should not be initialized when this method is called!");
                
                // Follow Petsc's advice and build the matrix step by step (see MatCreateSeqAIJ for this advice).
                int error_code = MatCreate(communicator, &petsc_matrix_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreate", invoking_file, invoking_line);
                
                const Mat& internal = Internal(); // const is indeed ignored below as Mat is truly a pointer...
                
                error_code = MatSetType(internal, MATSEQDENSE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetType", invoking_file, invoking_line);
                
                
                const PetscInt Nrow_int = static_cast<PetscInt>(Nrow);
                const PetscInt Ncol_int = static_cast<PetscInt>(Ncolumn);
                
                error_code = MatSetSizes(internal, Nrow_int, Ncol_int, Nrow_int, Ncol_int);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetSizes", invoking_file, invoking_line);
                
                error_code = MatSetUp(internal);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetUp", invoking_file, invoking_line);
                
                do_petsc_destroy_ = true;
            }
            

            void Matrix::InitParallelDenseMatrix(unsigned int Nlocal_row, unsigned int Nlocal_column,
                                                 unsigned int Nglobal_row, unsigned int Nglobal_column,
                                                 const Mpi& mpi, const char* invoking_file, int invoking_line)
            {
                const auto& communicator = mpi.GetCommunicator();
                
                assert(petsc_matrix_ == PETSC_NULL && "Should not be initialized when this method is called!");
                
                // Follow Petsc's advice and build the matrix step by step (see MatCreateSeqAIJ for this advice).
                int error_code = MatCreate(communicator, &petsc_matrix_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreate", invoking_file, invoking_line);
                
                const Mat& internal = Internal(); // const is indeed ignored below as Mat is truly a pointer...
                
                error_code = MatSetType(internal, MATMPIDENSE);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetType", invoking_file, invoking_line);
                
                const PetscInt Nlocal_row_int = static_cast<PetscInt>(Nlocal_row);
                const PetscInt Nlocal_col_int = static_cast<PetscInt>(Nlocal_column);
                const PetscInt Nglobal_row_int = static_cast<PetscInt>(Nglobal_row);
                const PetscInt Nglobal_col_int = static_cast<PetscInt>(Nglobal_column);
                
                error_code = MatSetSizes(internal, Nlocal_row_int, Nlocal_col_int, Nglobal_row_int, Nglobal_col_int);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetSizes", invoking_file, invoking_line);
                
                error_code = MatSetUp(internal);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetUp", invoking_file, invoking_line);
                
                do_petsc_destroy_ = true;
            }

            
            void Matrix::ZeroEntries(const char* invoking_file, int invoking_line)
            {
                int error_code = MatZeroEntries(Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatZeroEntries", invoking_file, invoking_line);
            }
            
            
            void Matrix::ZeroRows(const std::vector<PetscInt>& row_indexes,
                                  PetscScalar diagonal_value,
                                  const char* invoking_file, int invoking_line)
            {
                int error_code = MatZeroRows(Internal(),
                                             static_cast<PetscInt>(row_indexes.size()),
                                             row_indexes.data(),
                                             diagonal_value,
                                             PETSC_NULL,
                                             PETSC_NULL);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatZeroRows", invoking_file, invoking_line);
            }
            
            
            void Matrix::ZeroRowsColumns(const std::vector<PetscInt>& row_indexes,
                                         PetscScalar diagonal_value,
                                         const char* invoking_file, int invoking_line)
            {
                int error_code = MatZeroRowsColumns(Internal(),
                                                    static_cast<PetscInt>(row_indexes.size()),
                                                    row_indexes.data(),
                                                    diagonal_value,
                                                    PETSC_NULL,
                                                    PETSC_NULL);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatZeroRowsColumns", invoking_file, invoking_line);
            }
            
            
            
            void Matrix::Assembly(const char* invoking_file, int invoking_line)
            {
                const Mat& internal = Internal(); // const is indeed ignored below as Mat is truly a pointer...
                
                int error_code = MatAssemblyBegin(internal, MAT_FINAL_ASSEMBLY);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatAssemblyBegin", invoking_file, invoking_line);
                
                error_code = MatAssemblyEnd(internal, MAT_FINAL_ASSEMBLY);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatAssemblyEnd", invoking_file, invoking_line);
            }
            
            
            bool Matrix::IsAssembled(const char* invoking_file, int invoking_line) const
            {
                const Mat& internal = Internal();
                
                PetscBool value;
                
                int error_code = MatAssembled(internal, &value);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatAssembled", invoking_file, invoking_line);
                
                return (value == PETSC_TRUE);
            }
            
            
            void Matrix::SetValues(const std::vector<PetscInt>& row_indexing,
                                   const std::vector<PetscInt>& column_indexing,
                                   const PetscScalar* values, InsertMode insertOrAppend,
                                   const char* invoking_file, int invoking_line,
                                   ignore_zero_entries do_ignore_zero_entries)
            {
                int error_code;
                
                if (do_ignore_zero_entries == ignore_zero_entries::yes)
                {
                    error_code = MatSetOption(Internal(), MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "MatSetOption - MAT_IGNORE_ZERO_ENTRIES - PETSC_TRUE",
                                                     invoking_file, invoking_line);
                }
                                           
                error_code = MatSetValues(Internal(),
                                              static_cast<PetscInt>(row_indexing.size()),
                                              row_indexing.data(),
                                              static_cast<PetscInt>(column_indexing.size()),
                                              column_indexing.data(),
                                              values,
                                              insertOrAppend);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetValues", invoking_file, invoking_line);
                
                if (do_ignore_zero_entries == ignore_zero_entries::yes)
                {
                    error_code = MatSetOption(Internal(), MAT_IGNORE_ZERO_ENTRIES, PETSC_FALSE);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "MatSetOption - MAT_IGNORE_ZERO_ENTRIES - PETSC_TRUE",
                                                     invoking_file, invoking_line);
                }

            }
            
            
            void Matrix::SetValuesRow(PetscInt row_index,
                                      const PetscScalar* values,
                                      const char* invoking_file, int invoking_line)
            {
                int error_code = MatSetValuesRow(Internal(),
                                                 row_index,
                                                 values);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetValuesRow", invoking_file, invoking_line);
            }

            
            void Matrix::SetValue(PetscInt row_index,
                                  PetscInt column_index,
                                  PetscScalar value, InsertMode insertOrAppend,
                                  const char* invoking_file, int invoking_line)
            {
                int error_code = MatSetValue(Internal(),
                                             row_index,
                                             column_index,
                                             value,
                                             insertOrAppend);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetValue", invoking_file, invoking_line);
            }
            
            
            
            PetscScalar Matrix::GetValue(PetscInt row_index, PetscInt column_index,
                                         const char* invoking_file, int invoking_line) const
            {
                PetscScalar ret;
                
                int error_code = MatGetValues(Internal(),
                                              1, &row_index,
                                              1, & column_index,
                                              &ret);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetValues", invoking_file, invoking_line);
                
                return ret;
            }

            
            
            void Matrix::Copy(const Matrix& source, const char* invoking_file, int invoking_line,
                              const MatStructure& structure)
            {
                int error_code = MatCopy(source.Internal(), Internal(), structure);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCopy", invoking_file, invoking_line);
            }
            
            
            void Matrix::CompleteCopy(const Matrix& source, const char* invoking_file, int invoking_line)
            {
                DuplicateLayout(source, MAT_SHARE_NONZERO_PATTERN, invoking_file, invoking_line);
                Copy(source, invoking_file, invoking_line, SAME_NONZERO_PATTERN);
            }

                     
            
            void Matrix::Scale(PetscScalar a, const char* invoking_file, int invoking_line)
            {
                int error_code = MatScale(Internal(), a);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatScale", invoking_file, invoking_line);
            }
            
            
            
            std::pair<PetscInt, PetscInt> Matrix::GetProcessorWiseSize(const char* invoking_file, int invoking_line) const
            {
                std::pair<PetscInt, PetscInt> ret;
                
                int error_code = MatGetLocalSize(Internal(), &ret.first, &ret.second);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetLocalSize", invoking_file, invoking_line);
                
                return ret;
            }
            
            
            std::pair<PetscInt, PetscInt> Matrix::GetProgramWiseSize(const char* invoking_file, int invoking_line) const
            {
                std::pair<PetscInt, PetscInt> ret;
                
                int error_code = MatGetSize(Internal(), &ret.first, &ret.second);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetSize", invoking_file, invoking_line);
                
                return ret;
                
            }
            
      
            void Matrix::View(const Mpi& mpi, const char* invoking_file, int invoking_line) const
            {
                int error_code = MatView(Internal(), PETSC_VIEWER_STDOUT_(mpi.GetCommunicator()));
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatView", invoking_file, invoking_line);
                
            }
            
            
            void Matrix::View(const Mpi& mpi,
                              const std::string& output_file,
                              const char* invoking_file, int invoking_line,
                              PetscViewerFormat format) const
            {
                Viewer viewer(mpi, output_file, format, invoking_file, invoking_line);

                int error_code = MatView(Internal(), viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatView", invoking_file, invoking_line);
            }
            
            
            void Matrix::ViewBinary(const Mpi& mpi,
                                    const std::string& output_file,
                                    const char* invoking_file, int invoking_line) const
            {
                Viewer viewer(mpi, output_file, FILE_MODE_WRITE, invoking_file, invoking_line);
                
                int error_code = MatView(Internal(), viewer.GetUnderlyingPetscObject());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatView", invoking_file, invoking_line);

            }

            
            void Matrix::GetRow(PetscInt row_index,
                                std::vector<PetscInt>& row_content_position_list,
                                std::vector<PetscScalar>& row_content_value_list,
                                const char* invoking_file, int invoking_line) const
            {
                assert(row_content_position_list.empty());
                assert(row_content_value_list.empty());
                
                PetscInt Nnon_zero_cols;
                const PetscInt* columns;
                const PetscScalar* values;
                
                // Petsc MatGetRow() crashed if row index is not in the interval given below, so I have added
                // this very crude test to return nothing in this case.
                if (row_index < Internal()->rmap->rstart || row_index > Internal()->rmap->rend)
                    return;
                
                int error_code = MatGetRow(Internal(),
                                           row_index,
                                           &Nnon_zero_cols,
                                           &columns,
                                           &values);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetRow", invoking_file, invoking_line);                
                
                // This condition is there because Petsc might not be very consistent and return for instance:
                // - Nnon_zero_cols = 0
                // - columns = 0x0
                // - values to an address in memory (0x0 would be much better...)
                if (Nnon_zero_cols > 0)
                {
                    assert(values != nullptr);
                    assert(values != nullptr);

                    const auto size = static_cast<std::size_t>(Nnon_zero_cols);
                    row_content_position_list.resize(size);
                    row_content_value_list.resize(size);
                
                
                    for (auto i = 0ul; i < size; ++i)
                    {
                        row_content_position_list[i] = columns[i];
                        row_content_value_list[i] = values[i];
                    }
                }

                error_code = MatRestoreRow(Internal(),
                                           row_index,
                                           &Nnon_zero_cols,
                                           &columns,
                                           &values);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatRestoreRow", invoking_file, invoking_line);
            }
            
            
            void Matrix::GetRow(PetscInt row_index,
                                std::vector<std::pair<PetscInt, PetscScalar>>& row_content,
                                const char* invoking_file, int invoking_line) const
            {
                std::vector<PetscInt> row_content_position_list;
                std::vector<PetscScalar> row_content_value_list;
                
                GetRow(row_index,
                       row_content_position_list,
                       row_content_value_list,
                       invoking_file, invoking_line);
                
                const auto size = row_content_position_list.size();
                assert(size == row_content_value_list.size());
                
                row_content.reserve(size);
                
                for (auto i = 0ul; i < size; ++i)
                    row_content.emplace_back(std::make_pair(row_content_position_list[i], row_content_value_list[i]));
            }
            
            
            void Matrix::GetColumnVector(PetscInt column_index,
                                         Vector& column,
                                         const char* invoking_file, int invoking_line) const
            {
                int error_code = MatGetColumnVector(Internal(), column.Internal(), column_index);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetColumnVector", invoking_file, invoking_line);
            }
            
            
            void Matrix::SetFromPetscMat(Mat petsc_matrix)
            {
                assert(petsc_matrix_ == PETSC_NULL && "Should be used only to init a still empty matrix!");
                assert(!do_petsc_destroy_);
                
                petsc_matrix_ = petsc_matrix;
            }
            
            
            double Matrix::Norm(NormType type, const char* invoking_file, int invoking_line) const
            {
                PetscReal norm;
                
                assert(type == NORM_1 || type == NORM_FROBENIUS || type == NORM_INFINITY);
                
                int error_code = MatNorm(Internal(), type, &norm);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatNorm", invoking_file, invoking_line);
                
                return static_cast<double>(norm);
            }
            
            
            void Matrix::GetInfo(MatInfo* infos, const char* invoking_file, int invoking_line)
            {
                int error_code = MatGetInfo(Internal(), MAT_LOCAL, infos);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetInfo", invoking_file, invoking_line);
            }
            
            
            void Matrix::GetOption(MatOption op, PetscBool *flg, const char* invoking_file, int invoking_line)
            {
                int error_code = MatGetOption(Internal(), op, flg);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetOption", invoking_file, invoking_line);
            }
            
            
            void Matrix::SetOption(MatOption op, PetscBool flg, const char* invoking_file, int invoking_line)
            {
                int error_code = MatSetOption(Internal(), op, flg);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatSetOption", invoking_file, invoking_line);
            }

            
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup

