target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Snes.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Enum.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Snes.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Snes.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instantiations/SourceList.cmake)
