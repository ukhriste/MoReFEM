///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Oct 2013 17:33:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

# include <numeric>

#include "Utilities/Containers/Print.hpp"

# include "ThirdParty/Wrappers/Parmetis/Parmetis.hpp"
# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Parmetis/Exceptions/Parmetis.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Parmetis
        {


            std::vector<parmetis_int> PartKway(const std::vector<unsigned int>& processor_partition,
                                               const std::vector<parmetis_int>& iCSR,
                                               const std::vector<parmetis_int>& jCSR,
                                               const Mpi& mpi)
            {
                std::vector<parmetis_int> ret(iCSR.size() - 1, -99);
                
                #ifndef NDEBUG
                // Check at least partially the consistency of input argument: if partition
                // is not on par with iCSR, the code would crash violently without any message!
                const auto rank = mpi.GetRank<unsigned int>();
                assert(rank < processor_partition.size());
                assert(processor_partition[rank] == iCSR.size() - 1
                       && "Inconsistent data: processor-wise iCSR is not the expected size");
                #endif // NDEBUG

                const unsigned int Nproc = mpi.Nprocessor<unsigned int>();

                // From processor partition generate the expected vtxdist vector needed by Parmetis. It is simoply
                // the cumulative number of dofs considered, and the lower and upper boundaries are added
                // (namely 0 and Ndof).
                assert(Nproc == processor_partition.size());

                std::vector<parmetis_int> vtxdist(Nproc + 1, 0);
                {
                    parmetis_int cumulative_sum = 0;

                    for (unsigned int i = 0; i < Nproc; ++i)
                    {
                        cumulative_sum += processor_partition[i];
                        vtxdist[i + 1] = cumulative_sum;
                    }
                }

                parmetis_int wgtFlag = 0; // meaning no weight.
                parmetis_int num_flag = 0; // C - style numbering scheme.
                parmetis_int ncon = 1; // number of weight per vertex.
                parmetis_real ubvec = static_cast<parmetis_real>(1.05); // recommended value (see Parmetis doc)
                std::vector<parmetis_int> option { 1, 100, 100 };
                parmetis_int edgecut; // output parameter we do not need.


                // Repartition of weight for a vertex. All subdomains are the same size in our case.
                std::vector<parmetis_real> tpwgts(static_cast<std::size_t>(Nproc * static_cast<unsigned int>(ncon)), 
                                                 static_cast<parmetis_real>(1.) / static_cast<parmetis_real>(Nproc));

                MPI_Comm comm = mpi.GetCommunicator();
                
                parmetis_int Nproc_parmetis = static_cast<parmetis_int>(Nproc);
                
                if (!jCSR.data())
                    throw Exception("Error in partitioning: at least one processor exhibits no connectivity at all. "
                                    "This case is unfortunately not handled properly yet; try running same code in "
                                    "sequential. In all likelihood, this is due to the fact only unconnected finite "
                                    "elements are present (e.g. with P0/Q0 shape functions.",
                                    __FILE__, __LINE__);
                                    

                int err_code = ParMETIS_V3_PartKway(vtxdist.data(),
                                                    const_cast<parmetis_int*>(iCSR.data()),
                                                    const_cast<parmetis_int*>(jCSR.data()),
                                                    nullptr, nullptr, &wgtFlag, // this line underlines the fact no weight are considered.
                                                    &num_flag, &ncon,
                                                    &Nproc_parmetis, tpwgts.data(),
                                                    &ubvec, option.data(), &edgecut, ret.data(), &comm);
          
                if (err_code != METIS_OK)
                    throw ExceptionNS::Exception(err_code, "ParMETIS_V3_PartKway", __FILE__, __LINE__);


                return ret;
            }


            std::vector<unsigned int> CreateNewPartitioning(const std::vector<unsigned int>& processor_partition,
                                                            const std::vector<parmetis_int>& iCSR,
                                                            const std::vector<parmetis_int>& jCSR,
                                                            const Mpi& mpi)
            {
                auto&& processor_wise_partitioning = PartKway(processor_partition, iCSR, jCSR, mpi);
                
                const unsigned int Nnode = static_cast<unsigned int>(
                    std::accumulate(processor_partition.cbegin(), processor_partition.cend(), 0));
                
                using difference_type = std::vector<parmetis_int>::difference_type;
                
                std::vector<unsigned int> program_wise_partitioning(Nnode, 0);
                
                // We determine in block below the contribution of the local processor to the global partitioning.
                // All other positions are set at 0 (this is important, as MPI_SUM will be used in processor reduction!).
                {
                    auto begin = processor_partition.cbegin();
                    auto end = processor_partition.cbegin() + mpi.GetRank<difference_type>();
                    
                    const unsigned int index_first_non_zero_node =
                        static_cast<unsigned int>(std::accumulate(begin, end, 0));
                    
                    assert(mpi.GetRank<unsigned int>() < processor_partition.size());
                    const unsigned int Nprocessor_wise_node = processor_partition[mpi.GetRank<unsigned int>()];
                    
                    for (unsigned int i = 0; i < Nprocessor_wise_node; ++i)
                        program_wise_partitioning[index_first_non_zero_node + i] = static_cast<unsigned int>(processor_wise_partitioning[i]);
                }
                
                // Now we create the global vector for each of the processor, through a MPI_Allreduce() call.
                
                program_wise_partitioning = mpi.AllReduce(program_wise_partitioning, Wrappers::MpiNS::Op::Sum);
            
                return program_wise_partitioning;
            }



        } // namespace Parmetis
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
