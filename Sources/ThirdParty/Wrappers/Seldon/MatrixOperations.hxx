///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 1 Dec 2014 17:14:34 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_MATRIX_OPERATIONS_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_MATRIX_OPERATIONS_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Seldon
        {


            #ifndef NDEBUG
            template<class MatrixTypeT>
            void AssertBlockValidity(const MatrixTypeT& matrix,
                                     const ::Seldon::Vector<int>& row_indexes,
                                     const ::Seldon::Vector<int>& col_indexes)
            {
                const int Ncol_in_matrix = matrix.GetN();

                AssertBlockRowValidity(matrix, row_indexes);

                {
                    // Each index in col_indexes must belong to [0, Ncol_in_matrix [.
                    const int Ncol_in_block = col_indexes.GetSize();

                    for (int i = 0; i < Ncol_in_block; ++i)
                        assert(col_indexes(i) < Ncol_in_matrix);
                }
            }


            template<class MatrixTypeT>
            void AssertBlockRowValidity(const MatrixTypeT& matrix,
                                        const ::Seldon::Vector<int>& row_indexes)
            {
                const int Nrow_in_matrix = matrix.GetM();

                {
                    // Each index in row_indexes must belong to [0, Nrow_in_matrix [.
                    const int Nrow_in_block = row_indexes.GetSize();

                    for (int i = 0; i < Nrow_in_block; ++i)
                        assert(row_indexes(i) < Nrow_in_matrix);
                }

            }



            template<class VectorTypeT>
            void AssertBlockValidity(const VectorTypeT& vector,
                                     const ::Seldon::Vector<int>& row_indexes)
            {
                const int Nrow_in_vector = vector.GetSize();

                // Each index in row_indexes must belong to [0, Nrow_in_vector [.
                const int Nrow_in_block = row_indexes.GetSize();

                for (int i = 0; i < Nrow_in_block; ++i)
                    assert(row_indexes(i) < Nrow_in_vector);

            }
            #endif // NDEBUG



        } // namespace Seldon


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SELDON_x_MATRIX_OPERATIONS_HXX_
