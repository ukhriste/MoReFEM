///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 9 Sep 2013 09:55:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ThirdPartyGroup
/// \addtogroup ThirdPartyGroup
/// \{

# include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Seldon
        {


            ::Seldon::Vector<int> ContiguousSequence(int first_index, int N)
            {
                ::Seldon::Vector<int> ret(N);

                for (int index = 0; index < N; ++index)
                    ret(index) = first_index + index;

                return ret;
            }


            ::Seldon::Vector<int> ContiguousSequence(unsigned int first_index, unsigned int N)
            {
                assert(first_index <= static_cast<unsigned int>(std::numeric_limits<int>::max()));
                assert(N <= static_cast<unsigned int>(std::numeric_limits<int>::max()));
                
                return ContiguousSequence(static_cast<int>(first_index), static_cast<int>(N));
            }
            
            
        } // namespace Seldon
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


