///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Jun 2014 10:23:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_ELEMENTARY_DATA_STORAGE_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_ELEMENTARY_DATA_STORAGE_HPP_

# include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {



            /*!
             * \brief This CRTP class adds local vector and/or matrix storage to ElementaryData class.
             *
             * \tparam DerivedT A specialization of ElementaryData upon which the CRTP is applied.
             * \tparam IsMatrixOrVectorT Nature of the storage that is added to \a ElementaryDataT.
             * \tparam StorageTypeT Type of the vector or matrix stored.
             *
             * If both vector and matrix behaviour must be added (for elementary_data_type)
             * just inherit from two CRTP classes:
             *
             * \code
             * class elementary_data_type
             * : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
             * public Crtp::ElementaryDataStorage<elementary_data_type, IsMatrixOrVector::vector>,
             * public Crtp::ElementaryDataStorage<elementary_data_type, IsMatrixOrVector::matrix>
             * { ... };
             * \endcode
             */
            template<class DerivedT, IsMatrixOrVector IsMatrixOrVectorT, class StorageTypeT>
            class ElementaryDataStorage;


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            template<class DerivedT, class VectorTypeT>
            class ElementaryDataStorage<DerivedT, IsMatrixOrVector::vector, VectorTypeT>
            {
            protected:


                /// \name Special members.
                ///@{

                //! Constructor.
                explicit ElementaryDataStorage() = default;

                //! Destructor.
                ~ElementaryDataStorage() = default;

                //! Copy constructor.
                ElementaryDataStorage(const ElementaryDataStorage&) = delete;

                //! Move constructor.
                ElementaryDataStorage(ElementaryDataStorage&&) = default;

                //! Affectation.
                ElementaryDataStorage& operator=(const ElementaryDataStorage&) = delete;

                //! Affectation.
                ElementaryDataStorage& operator=(ElementaryDataStorage&&) = delete;

                ///@}

            public:


                //! Constant access to the underlying elementary vector.
                const VectorTypeT& GetVectorResult() const;

                //! Non constant access to the underlying elementary vector.
                VectorTypeT& GetNonCstVectorResult();

            protected:

                //! Allocate the vector.
                void AllocateVector(unsigned int Ndof);


            private:

                //! Underlying elementary vector.
                VectorTypeT vector_;


            };


            template<class DerivedT, class MatrixTypeT>
            class ElementaryDataStorage<DerivedT, IsMatrixOrVector::matrix, MatrixTypeT>
            {
            protected:


                /// \name Special members.
                ///@{

                //! Constructor.
                explicit ElementaryDataStorage() = default;

                //! Destructor.
                ~ElementaryDataStorage() = default;

                //! Copy constructor.
                ElementaryDataStorage(const ElementaryDataStorage&) = delete;

                //! Move constructor.
                ElementaryDataStorage(ElementaryDataStorage&&) = default;

                //! Affectation.
                ElementaryDataStorage& operator=(const ElementaryDataStorage&) = delete;

                //! Affectation.
                ElementaryDataStorage& operator=(ElementaryDataStorage&&) = delete;

                ///@}

            public:

                //! Constant access to the underlying elementary matrix.
                const MatrixTypeT& GetMatrixResult() const;

                //! Non constant access to the underlying elementary matrix.
                MatrixTypeT& GetNonCstMatrixResult();


            protected:

                //! Allocate the matrix.
                void AllocateMatrix(unsigned int Ndof_row, unsigned int Ndof_col);


            private:

                //! Underlying elementary matrix.
                MatrixTypeT matrix_;
            };


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Crtp/ElementaryDataStorage.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_ELEMENTARY_DATA_STORAGE_HPP_
