///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace LocalVariationalOperatorNS
        {
            
            
            SubMatrixForNumberingSubsetPair
            ::SubMatrixForNumberingSubsetPair(const NumberingSubset& row_numbering_subset,
                                              const NumberingSubset& col_numbering_subset)
            : parent(row_numbering_subset, col_numbering_subset)
            { }

          
            
        } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
