///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

//  InvariantHolders.h
//
//  Created by Sebastien Gilles on 3/20/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HPP_

# include "Utilities/Numeric/Numeric.hpp"

# include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"


namespace MoReFEM
{


    namespace InvariantHolderNS
    {


        /*!
         * \brief Content of InvariantHolder.
         *
         * - invariants mean that only the invariants themselves are stored and computed.
         * - invariants_and_first_deriv mean all first derivates are also stored.
         * - invariants_and_first_and_second_deriv adds second derivates to prior content.
         */
        enum class Content
        {
            invariants = 0,
            invariants_and_first_deriv,
            invariants_and_first_and_second_deriv,
        };


    } // namespace InvariantHolderNS


    /*!
     * \brief Class which computes the invariant and its derivatives with respect to Cauchy-Green tensor.
     *
     * This class is entirely optional: you may use directly free functions defined in InvariantComputation file.
     *
     */
    template<unsigned int NinvariantsT>
    class InvariantHolder final
    {

        static_assert(NinvariantsT >= 3, "The number of invariants must be greater or equal to 3.");

    public:

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<InvariantHolder<NinvariantsT>>;

        //! Enum class to define invariants numbering.
        enum class invariants_index : std::size_t
        {
            I1 = 0,
            I2,
            I3,
            I4
        };

        //! Enum class to define invariants first derivative numbering.
        enum class invariants_first_derivative_index : std::size_t
        {
            dI1dC = 0,
            dI2dC,
            dI3dC,
            dI4dC
        };

        //! Enum class to define invariants second derivative numbering. d2I1dCdC = d2I4dCdC = 0.
        enum class invariants_second_derivative_index : std::size_t
        {
            d2I2dCdC = 0,
            d2I3dCdC
        };


    public:

        /// \name Special members.
        ///@{

        //! Constructor.
        explicit InvariantHolder(unsigned int dimension,
                                 InvariantHolderNS::Content content);

        //! Destructor.
        ~InvariantHolder() = default;

        //! Copy constructor.
        InvariantHolder(const InvariantHolder&) = delete;

        //! Move constructor.
        InvariantHolder(InvariantHolder&&) = delete;

        //! Copy affectation.
        InvariantHolder& operator=(const InvariantHolder&) = delete;

        //! Move affectation.
        InvariantHolder& operator=(InvariantHolder&&) = delete;

        ///@}


        /*!
         * \class doxygen_hide_cauchy_green_tensor_as_vector_arg
         *
         * \param[in] cauchy_green_tensor The vector which holds the values of CauchyGreen tensor; expected content is:
         * - (xx, yy, xy) for dimension 2
         * - (xx, yy, zz, xy, yz, xz) for dimension 3
         * It should have been computed by \a UpdateCauchyGreenTensor operator.
         */

        /*!
         * \brief Update the invariants and their derivates with a new Cauchy-Green tensor.
         *
         * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] quad_pt \a QuadraturePoint at which the value is computed.
         */
        void Update(const LocalVector& cauchy_green_tensor,
                    const QuadraturePoint& quad_pt,
                    const GeometricElt& geom_elt);

        /*!
         * \brief Get one of the invariants.
         *
         * \param[in] a_invariants_index The index of the invariant to get (e.g. invariants_index::I1).
         *
         * \return The value of the invariant.
         */
        double GetInvariant(invariants_index a_invariants_index) const noexcept;

        /*!
         * \brief Get one of the first derivative of the invariants against Cauchy-Green tensor.
         *
         * \param[in] a_invariants_first_derivative_index The index of the invariant derivative to get
         * (e.g. invariants_first_derivative_index::dI1dC).
         *
         * \return The value of the invariant derivative.
         */
        const LocalVector& GetFirstDerivativeWrtCauchyGreen(invariants_first_derivative_index a_invariants_first_derivative_index) const noexcept;

        /*!
         * \brief Get one of the first derivative of the invariants against Cauchy-Green tensor.
         *
         * \param[in] a_invariants_second_derivative_index The index of the invariant second derivative to get
         * (e.g. invariants_second_derivative_index::d2I2dCdC).
         *
         * \return The value of the invariant second derivative.
         */
        const LocalMatrix& GetSecondDerivativeWrtCauchyGreen(invariants_second_derivative_index a_invariants_second_derivative_index) const noexcept;

        #ifndef NDEBUG

        /*!
         * \brief In debug mode, indicate invariants should be reset before use.
         */
        void Reset();

        #endif // NDEBUG

        /*!
         * \brief Change the pointer to the fibers of the law to update I4. If i4 not activated set to nullptr.
         *
         * \param[in] fibers The fibers used to define I4.
         */
        void SetFibers(const FiberList<ParameterNS::Type::vector>* fibers);

        /*!
         * \brief To activate I4.
         *
         * \param[in] do_i4_activate A boolean to activate or nor I4.
         */
        void SetI4(bool do_i4_activate);

    private:

        //! Dimension of the mesh.
        unsigned int GetMeshDimension() const noexcept;

        //! Get what is actually stored and computed within the class (see \a InvariantHolderNS::Content for more details).
        InvariantHolderNS::Content GetContent() const noexcept;

        /*!
         * \brief Get all of the second derivative of the invariants against Cauchy-Green tensor.
         *
         * \return An array containing all the second derivative of the invariants.
         */
        std::array<LocalMatrix, 2>& GetNonCstSecondDerivativeWrtCauchyGreen() noexcept;


        /*!
         * \brief Get all of the second derivative of the invariants against Cauchy-Green tensor.
         *
         * \return An array containing all the first derivative of the invariants.
         */
        std::array<LocalVector, NinvariantsT>& GetNonCstFirstDerivativeWrtCauchyGreen() noexcept;


        /*!
         * \brief Get all the invariants.
         *
        * \return An array containing all the invariants.
         */
        std::array<double, NinvariantsT>& GetNonCstInvariant() noexcept;

        //! If I4 is activate or not.
        bool DoI4Activate() const noexcept;

        //! Constant accessors to the fibers in case of I4 is activate.
        const FiberList<ParameterNS::Type::vector>* GetFibers() const noexcept;

    private:

        //! What is actually stored and computed within the class (see \a InvariantHolderNS::Content for more details).
        const InvariantHolderNS::Content content_;

        //! Dimension of the mesh.
        const unsigned int mesh_dimension_;

        //! Array to store all the invariants.
        std::array<double, NinvariantsT> invariants_;

        //! Array to store all the invariants first derivative.
        std::array<LocalVector, NinvariantsT> invariants_first_derivative_;

        /*!
         * \brief Array to store all the invariants second derivative.
         *
         * Only 2 here because d2I1dCdC = d2I4dCdC = 0. See invariants_second_derivative_index.
        */
        std::array<LocalMatrix, 2> invariants_second_derivative_;

        //! Fibers parameter for I4.
        const FiberList<ParameterNS::Type::vector>* fibers_ = nullptr;

        //! To know if I4 is activated or not.
        bool do_i4_activate_ = false;

        #ifndef NDEBUG

        /*!
         * \brief In debug mode, check invariants are correctly set before using them up!
         */
        bool are_invariant_set_ = false;

        #endif // NDEBUG

    };



} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HPP_
