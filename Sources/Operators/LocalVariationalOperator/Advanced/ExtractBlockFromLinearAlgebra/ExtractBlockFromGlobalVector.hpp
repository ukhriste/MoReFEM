///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:41:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_VECTOR_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_VECTOR_HPP_

# include "ThirdParty/Wrappers/Seldon/SubVector.hpp"

# include "Utilities/MatrixOrVector.hpp"

# include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            # ifndef NDEBUG


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            //! See ExtractBlockFromLocalMatrix to get an idea of what it does.
            ::Seldon::SubVector<LocalVector> ExtractBlockFromLocalVector(const Advanced::RefFEltInLocalOperator& ref_felt,
                                                                         unsigned int component,
                                                                         LocalVector& vector);

            ::Seldon::SubVector<LocalVector> ExtractBlockFromLocalVector(const Advanced::RefFEltInLocalOperator& ref_felt,
                                                                         LocalVector& vector);


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            # endif // NDEBUG


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Advanced/ExtractBlockFromLinearAlgebra/ExtractBlockFromGlobalVector.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_VECTOR_HPP_
