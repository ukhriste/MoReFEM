///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:41:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_MATRIX_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_MATRIX_HPP_

# include "ThirdParty/Wrappers/Seldon/SubVector.hpp"
# include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

# include "Utilities/MatrixOrVector.hpp"

# include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            # ifndef NDEBUG


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================



            /*!
             * \brief Extract a block from a matrix; modifications in that block will be reported automatically in the bigger
             * matrix.
             *
             * \attention Actually this should be used for debug purposes, at least for the read-only use; usually it
             * is much more efficient to control tightly your indexes and navigate through the bigger matrix directly.
             *
             *
             * The finite element matrix looks for instance like (for a 3D Stokes problem here):
             *
             *        (vx1 vx2 ... vy1 vy2 ... vz1 vz2 ... p1 p2 ...)
             * (vx1)
             * (vx2)
             *  ...
             * (vy1)
             * (vy2)
             * ...
             * (vz1)
             * (vz2)
             * ...
             * (p1)
             * (p2)
             * ...
             *
             * So it forms underlying blocks for each pair variable/component. The purpose of the current function
             * is to return the index of the first component block (so here velocity would get 0 and pressure would
             * get 3).
             *
             *
             */
            template<class MatrixTypeT>
            ::Seldon::SubMatrix<MatrixTypeT> ExtractBlockFromLocalMatrix(const Advanced::RefFEltInLocalOperator& ref_felt1,
                                                                         unsigned int component1,
                                                                         const Advanced::RefFEltInLocalOperator& ref_felt2,
                                                                         unsigned int component2,
                                                                         MatrixTypeT& matrix);

            template<class MatrixTypeT>
            ::Seldon::SubMatrix<MatrixTypeT> ExtractBlockFromLocalMatrix(const Advanced::RefFEltInLocalOperator& ref_felt1,
                                                                         const Advanced::RefFEltInLocalOperator& ref_felt2,
                                                                         MatrixTypeT& matrix);




            template<class MatrixTypeT>
            ::Seldon::SubMatrix<MatrixTypeT> ExtractBlockFromLocalMatrix(const Advanced::RefFEltInLocalOperator& ref_felt,
                                                                         unsigned int component,
                                                                         MatrixTypeT& matrix);


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================



            # endif // NDEBUG


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Advanced/ExtractBlockFromLinearAlgebra/ExtractBlockFromGlobalMatrix.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_EXTRACT_BLOCK_FROM_LINEAR_ALGEBRA_x_EXTRACT_BLOCK_FROM_GLOBAL_MATRIX_HPP_
