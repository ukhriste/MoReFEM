///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:20:28 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace LocalVariationalOperatorNS
        {
        
        
            void ExtractGradientBasedBlock(const LocalMatrix& full_matrix,
                                           unsigned int row_component,
                                           unsigned int col_component,
                                           LocalMatrix& matrix)
            {
                const auto Ncomponent = matrix.GetM();
                assert(Ncomponent == matrix.GetN());
                assert(static_cast<int>(row_component) < Ncomponent);
                assert(static_cast<int>(col_component) < Ncomponent);
                // >= and not > in the following assert because of the 1D case where this extraction is just a copy.
                // Indeed the two matrices in this case are in fact scalars.
                assert(full_matrix.GetM() >= Ncomponent);
                assert(full_matrix.GetN() >= Ncomponent);
                
                const int first_row_index_in_full_matrix = static_cast<int>(row_component) * Ncomponent;
                const int first_col_index_in_full_matrix = static_cast<int>(col_component) * Ncomponent;
                
                for (int m = 0; m < Ncomponent; ++m)
                {
                    for (int n = 0; n < Ncomponent; ++n)
                    {
                        assert(first_row_index_in_full_matrix + m < full_matrix.GetM());
                        assert(first_col_index_in_full_matrix + n < full_matrix.GetN());
                        
                        matrix(m, n) = full_matrix(first_row_index_in_full_matrix + m,
                                                   first_col_index_in_full_matrix + n);
                    }
                }
            }

    
        } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
