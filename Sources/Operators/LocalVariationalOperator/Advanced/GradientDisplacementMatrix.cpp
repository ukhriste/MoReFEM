///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 May 2016 14:12:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/Advanced/InformationsAtQuadraturePoint.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace OperatorNS
        {
            
            
            namespace // anonymous
            {
                
                
                
                /*!
                 * \brief Perform matrix-vector product on a given subset of the input data.
                 *
                 * \param[in] local_displacement Displacement computed in previous iteration for the current finite element.
                 * \param[in] transposed_dphi Transposed of the gradient shape function matrix.
                 * \param[in] Nnode Number of nodes in the finite element.
                 * \param[in] row Index of the row considered.
                 * \param[in] component Component of displacement considered.
                 */
                double RowProductMatVect(const std::vector<double>& local_displacement,
                                         const LocalMatrix& transposed_dphi,
                                         int Nnode,
                                         int row,
                                         int component);
                
                
            } // namespace anonymous

            
            
            void ComputeGradientDisplacementMatrix(const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt,
                                                   const std::vector<double>& local_displacement,
                                                   LocalMatrix& gradient_matrix)
            {
                const auto mesh_dimension = static_cast<int>(infos_at_quad_pt.GetMeshDimension());
                
                assert(gradient_matrix.GetM() == mesh_dimension);
                assert(gradient_matrix.GetN() == mesh_dimension);
                
                const auto& transposed_dphi = infos_at_quad_pt.GetTransposedGradientFEltPhi();
                const auto Nnode = transposed_dphi.GetN();
                assert(Nnode == static_cast<int>(infos_at_quad_pt.Nnode()));
                
                for (int derivation_component = 0; derivation_component < mesh_dimension; ++derivation_component)
                {
                    for (int component = 0; component < mesh_dimension; ++component)
                    {
                        gradient_matrix(derivation_component, component)
                        = RowProductMatVect(local_displacement,
                                            transposed_dphi,
                                            Nnode,
                                            component,
                                            derivation_component);
                    }
                }
            }
            

            namespace // anonymous
            {
                
                
                double RowProductMatVect(const std::vector<double>& local_displacement,
                                         const LocalMatrix& transposed_dphi,
                                         int Nnode,
                                         int row,
                                         int component)
                {
                    double sum = 0.;
                    
                    //                std::cout << "RowProductMatVect - > " << local_displacement.size() << "\tNnode = " << Nnode << std::endl;
                    
                    for (int i = 0; i < Nnode; ++i)
                    {
                        const auto index = static_cast<std::size_t>(i + component * Nnode);
                        assert(index < local_displacement.size());
                        
                        sum += transposed_dphi(row, i) * local_displacement[index];
                    }
                    return sum;
                };
                
                
            } // namespace anonymous
          
            
        } // namespace OperatorNS
        
        
    } // namespace Advanced
  

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
