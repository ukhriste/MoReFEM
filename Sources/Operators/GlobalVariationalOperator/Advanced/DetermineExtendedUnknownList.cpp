///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Sep 2016 15:09:24 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hpp"
#include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hpp"

namespace MoReFEM
{

        
    namespace Advanced
    {
        
        
        namespace GlobalVariationalOperatorNS
        {
            
            ExtendedUnknown::vector_const_shared_ptr
            DetermineExtendedUnknownList(const FEltSpace& felt_space,
                                         const Unknown::const_shared_ptr unknown_ptr)
            {
                const std::array<Unknown::const_shared_ptr, 1> unknown_ptr_array { { unknown_ptr } };
                                
                return DetermineExtendedUnknownList(felt_space,
                                                    unknown_ptr_array);
            }
           
            
        } // namespace GlobalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup

