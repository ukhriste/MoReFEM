///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 Apr 2015 15:16:37 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_HELPER_HPP_
# define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_HELPER_HPP_

# include <tuple>

# include "Core/LinearAlgebra/GlobalMatrix.hpp"
# include "Core/LinearAlgebra/GlobalVector.hpp"

# include "Operators/LocalVariationalOperator/ElementaryData.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            /*!
             * \class doxygen_hide_linear_algebra_tuple_arg
             *
             * \param[in] linear_algebra_tuple Tuple of global matrices and/or vectors into which the operator is
             * assembled, with their coefficients. Ordering doesn't matter; the linear algebra objects are expected
             * to be properly initialized (e.g. pattern fully determined for a matrix).
             * The tuple is expected to include a mix of \a GlobalMatrixWithCoefficient and \a GlobalVectorWithCoefficient;
             * for internal reasons it is actually a reference to them that is expected. At this point an example
             * is probably required to clarify (it is done here for GlobalVariationalOperator::Assemble() here but
             * any function with this argument expect the same syntax):
             *
             * \code
             * auto& matrix_stiffness = vm.GetNonCstMatrixNewStiffness();
             * auto& vector_stiffness = vm.GetNonCstVectorNewStiffness();
             *
             * GlobalMatrixWithCoefficient mat(matrix_stiffness, 0.5);
             * GlobalVectorWithCoefficient vec(vector_stiffness, 1.);
             *
             * the_operator.Assemble(std::make_tuple(std::ref(mat), std::ref(vec)), ...);
             * \endcode
             */

            /*!
             * \brief Call Petsc's Assembly() function for each item of \a linear_algebra_tuple.
             *
             * \copydoc doxygen_hide_linear_algebra_tuple_arg
             */
            template<class LinearAlgebraTupleT>
            void Assembly(const LinearAlgebraTupleT& linear_algebra_tuple);


            /*!
             * \class doxygen_hide_global_variational_op_helper_common_arg
             *
             * \param[in] local_felt_space Local finite element space being assembled. Remind a local finite element
             * space is the object linked to a geometric element that hold finite elements information.
             * \param[in,out] local_variational_operator Local variational operator in charge of the computation of the local
             * linear algebra. It also holds the results of these computations, hence its presence here.
             * \param[in] global_variational_operator \a GlobalVariationalOperator for which the computation is done.
             */

            /*!
             * \brief Perform the actual assembling for each item of \a linear_algebra_tuple.
             *
             * \copydoc doxygen_hide_global_variational_op_helper_common_arg
             * \copydoc doxygen_hide_linear_algebra_tuple_arg
             */
            template
            <
                class LinearAlgebraTupleT,
                class GlobalVariationalOperatorT,
                class LocalOperatorT
            >
            void InjectInGlobalLinearAlgebra(const GlobalVariationalOperatorT& global_variational_operator,
                                             const LinearAlgebraTupleT& linear_algebra_tuple,
                                             const LocalFEltSpace& local_felt_space,
                                             LocalOperatorT& local_variational_operator);



            /*!
             * \brief Helper class used for metaprogramming iterations in the free functions defined above.
             *
             * The main purpose here is to apply the same operation to each item of \a LinearAlgebraTupleT.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include either \a GlobalMatrixWithCoefficient (for bilinear
             * operators) or \a GlobalVectorWithCoefficient (for linear operators) objects. Some non linear operators
             * may include both types of objects; the ordering doesn't matter.
             * \tparam I Current element of the tuple.
             * \tparam TupleSizeT The result of std::tuple_size<LinearAlgebraTupleT>::value.
             */
            template
            <
                class LinearAlgebraTupleT,
                std::size_t I,
                std::size_t TupleSizeT
            >
            struct Recursivity
            {

                //! Type of the current element; might be either GlobalMatrixWithCoefficient or GlobalVectorWithCoefficient.
                using current_type = typename std::tuple_element<I, LinearAlgebraTupleT>::type;

                // Check here the type of current item is one of those expected.
                static_assert(std::is_same<current_type, GlobalMatrixWithCoefficient&>::value
                              || std::is_same<current_type, GlobalVectorWithCoefficient&>::value
                              , "Tuple is expected to include only one of those types.");


                /*!
                 * \brief Call Assembly() on current element and then apply recursion.
                 */
                static void Assembly(const LinearAlgebraTupleT& linear_algebra_tuple);

                /*!
                 * \brief Report the result of elementary computation into the matrix pointed by current item and
                 * then apply recursion.
                 *
                 * \copydoc doxygen_hide_global_variational_op_helper_common_arg
                 * \copydoc doxygen_hide_linear_algebra_tuple_arg
                 */
                template
                <
                    class GlobalVariationalOperatorT,
                    class LocalOperatorT
                >
                static void InjectInGlobalLinearAlgebra(const GlobalVariationalOperatorT& global_variational_operator,
                                                        const LinearAlgebraTupleT& linear_algebra_tuple,
                                                        const LocalFEltSpace& local_felt_space,
                                                        LocalOperatorT& local_variational_operator);

            private:


                /*!
                 * \brief Internal function called by \a InjectInGlobalLinearAlgebra when current element refers to a
                 * matrix.
                 *
                 * \copydoc doxygen_hide_global_variational_op_helper_common_arg
                 * \param[in] previous_coefficient The elementary matrix has been computed once but during each assembling
                 * the coefficient applied is not the same. Unfortunately Petsc doesn't provide a function that
                 * set the values multiplied by a factor, so I have to multiply the local matrix by a factor.
                 * \param[in] global_matrix_with_coeff Global matrix into which the assembling is done. Associated
                 * coefficient is applied in the process.
                 *
                 * Let's say for instance the operator is assembled in two matrices (this is pseudo code, not actual
                 * MoReFEM call):
                 *
                 * \code
                 * operator.Assemble({ Matrix1, factor1 }, {Matrix2, factor2});
                 * \endcode
                 *
                 * One way to do injection is (still pseudo code):
                 * \code
                 * local_matrix *= factor1;
                 * Inject local_matrix into  Matrix1;
                 * local_matrix /= factor1; // of course with check no 0 here...
                 * local_matrix *= factor2;
                 * Inject local_matrix into  Matrix2;
                 * local_matrix /= factor2; // of course with check no 0 here...
                 * \endcode
                 *
                 * However we see here that on one hand the last division isn't useful, on the other hand two of the
                 * operations could be done in one step.
                 *
                 * The latter case is exactly the point of \a previous_coefficient: when assembling into Matrix2
                 * the program looks the previous matrix being assembled to determine the correct factor. So in
                 * pseudo-code what is done is:
                 *
                 * \code
                 * local_matrix *= factor1;
                 * Inject local_matrix into  Matrix1;
                 * local_matrix *= factor2 / factor1; of course with check no 0 here...
                 * Inject local_matrix into  Matrix2;
                 * \endcode
                 *
                 * If there are both matrices and vectors in \a LinearAlgebraTupleT, it still works whatever the
                 * ordering: when computing a matrix vectors objects are skipped in the determination of the
                 * previous factor.
                 *
                 *
                 */
                template
                <
                    class GlobalVariationalOperatorT,
                    class LocalOperatorT
                >
                static void InjectInGlobalLinearAlgebraImpl(const GlobalVariationalOperatorT& global_variational_operator,
                                                            const LocalFEltSpace& local_felt_space,
                                                            LocalOperatorT& local_variational_operator,
                                                            const GlobalMatrixWithCoefficient& global_matrix_with_coeff,
                                                            double previous_coefficient);

                /*!
                 * \brief Internal function called by \a InjectInGlobalLinearAlgebra when current element refers to a
                 * vector.
                 *
                 * \copydoc doxygen_hide_global_variational_op_helper_common_arg
                 * \param[in] previous_coefficient The elementary vector has been computed once but during each assembling
                 * the coefficient applied is not the same. Unfortunately Petsc doesn't provide a function that
                 * set the values multiplied by a factor, so I have to multiply the local vector by a factor.
                 * \param[in] global_vector_with_coeff Global vector into which the assembling is done. Associated
                 * coefficient is applied in the process.
                 *
                 * See the overload for \a GlobalMatrixWithCoefficient for a much more detailed explanation.
                 */
                template
                <
                    class GlobalVariationalOperatorT,
                    class LocalOperatorT
                >
                static void InjectInGlobalLinearAlgebraImpl(const GlobalVariationalOperatorT& global_variational_operator,
                                                            const LocalFEltSpace& local_felt_space,
                                                            LocalOperatorT& local_variational_operator,
                                                            const GlobalVectorWithCoefficient& global_vector_with_coeff,
                                                            double previous_coefficient);
            };


            /*!
             * \brief Another struct for recursion that works in the opposite direction: the stopping condition
             * is when index is 0.
             *
             *
             * \tparam LinearAlgebraTupleT A tuple that may include either \a GlobalMatrixWithCoefficient (for bilinear
             * operators) or \a GlobalVectorWithCoefficient (for linear operators) objects. Some non linear operators
             * may include both types of objects; the ordering doesn't matter.
             * \tparam I Current element of the tuple.
             */
            template<class LinearAlgebraTupleT, std::size_t I>
            struct ZeroSpecialCase
            {
                //! Type of the current element; might be either GlobalMatrixWithCoefficient or GlobalVectorWithCoefficient.
                using current_type = typename std::tuple_element<I, LinearAlgebraTupleT>::type;


                /*!
                 * \brief This static method computes the coefficient required by
                 * \a Recursivity::InjectInGlobalLinearAlgebraImpl.
                 *
                 * See this function for a detailed explanation.
                 *
                 * \copydoc doxygen_hide_linear_algebra_tuple_arg
                 *
                 * \return Coefficient if the previous liner algebra.
                 */
                template<class CurrentTypeT>
                static double FetchPreviousCoefficient(const LinearAlgebraTupleT& linear_algebra_tuple);


            };




            // ============================
            // Stop points of recursive loops.
            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN


            template<class LinearAlgebraTupleT, std::size_t TupleSizeT>
            struct Recursivity<LinearAlgebraTupleT, TupleSizeT, TupleSizeT>
            {


                static void Assembly(const LinearAlgebraTupleT& linear_algebra_tuple);

                template
                <
                    class GlobalVariationalOperatorT,
                    class LocalOperatorT
                >
                static void InjectInGlobalLinearAlgebra(const GlobalVariationalOperatorT& global_variational_operator,
                                                        const LinearAlgebraTupleT& linear_algebra_tuple,
                                                        const LocalFEltSpace& local_felt_space,
                                                        LocalOperatorT& local_variational_operator);


            };


            template<class LinearAlgebraTupleT>
            struct ZeroSpecialCase<LinearAlgebraTupleT, 0>
            {

                template<class CurrentTypeT>
                static double FetchPreviousCoefficient(const LinearAlgebraTupleT& linear_algebra_tuple);


            };

            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================
            // End of stop points of recursive loops.
            // ============================




        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/GlobalVariationalOperator/Internal/Helper.hxx"


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_HELPER_HPP_
