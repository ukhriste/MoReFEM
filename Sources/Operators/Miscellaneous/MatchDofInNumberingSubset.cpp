///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Jan 2016 10:15:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/Miscellaneous/MatchDofInNumberingSubset.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        
        void MatchDofInNumberingSubsetHelper(const Dof::vector_shared_ptr& dof_list,
                                             const NumberingSubset& source_numbering_subset,
                                             const NumberingSubset& target_numbering_subset,
                                             std::vector<std::vector<unsigned int>>& match);
        
        
        
        
    } // namespace anonymous
    
    
    MatchDofInNumberingSubset::MatchDofInNumberingSubset(const FEltSpace& felt_space,
                                                         const NumberingSubset& source_numbering_subset,
                                                         const NumberingSubset& target_numbering_subset)
    {
        MatchDofInNumberingSubsetHelper(felt_space.GetProcessorWiseDofList(),
                                        source_numbering_subset,
                                        target_numbering_subset,
                                        match_);

        MatchDofInNumberingSubsetHelper(felt_space.GetGhostedDofList(),
                                        source_numbering_subset,
                                        target_numbering_subset,
                                        match_);
    }
    
    
    namespace // anonymous
    {
        
        
        void MatchDofInNumberingSubsetHelper(const Dof::vector_shared_ptr& dof_list,
                                             const NumberingSubset& source_numbering_subset,
                                             const NumberingSubset& target_numbering_subset,
                                             std::vector<std::vector<unsigned int>>& match)
        {
            for (const auto& dof_ptr : dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;
                
                if (dof.IsInNumberingSubset(source_numbering_subset))
                {
                    const auto node_bearer_ptr = dof.GetNodeBearerFromWeakPtr();
                    const auto& node_bearer_content = node_bearer_ptr->GetNodeList();
                    
                    std::vector<unsigned int> target_processor_wise_dof_index_list;
                    
                    unsigned int counter_relevant_node = 0u;
                    
                    // Look in the node bearer for the dofs that belongs to the TARGET numbering subset.
                    for (const auto& node_ptr : node_bearer_content)
                    {
                        if (node_ptr->DoBelongToNumberingSubset(target_numbering_subset))
                        {
                            ++counter_relevant_node;
                            
                            const auto& internal_dof_list = node_ptr->GetDofList();
                            
                            for (const auto& internal_dof_ptr : internal_dof_list)
                                target_processor_wise_dof_index_list.push_back(internal_dof_ptr->GetProcessorWiseOrGhostIndex(target_numbering_subset));
                        }
                    }
                    
                    assert(counter_relevant_node == 1 && "This operator assumes only one unknown in the considered numbering subset!");
                    
                    assert(!target_processor_wise_dof_index_list.empty() && "At the moment this class is restricted to "
                           "the case in which there is a complete match between nodes of both numbering subsets.");
                    
                    match.emplace_back(std::move(target_processor_wise_dof_index_list));
                }
            }
        }
        
        
    } // namespace anonymous
  

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
