///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:06:05 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {


            inline constexpr unsigned int Traits<ParameterNS::Type::scalar>::NvertexPerLine()
            {
                return 6u;
            }


            inline constexpr unsigned int Traits<ParameterNS::Type::vector>::NvertexPerLine()
            {
                return 2u;
            }


            inline constexpr unsigned int Traits<ParameterNS::Type::scalar>::NvaluePerVertex()
            {
                return 1u;
            }


            inline constexpr unsigned int Traits<ParameterNS::Type::vector>::NvaluePerVertex()
            {
                return 3u;
            }



        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_
