///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 10:32:20 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FIBER_LIST_MANAGER_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FIBER_LIST_MANAGER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {


            template<ParameterNS::Type TypeT>
            const std::string& FiberListManager<TypeT>::ClassName()
            {
                static std::string ret(std::string("FiberListManager_") + ::MoReFEM::ParameterNS::Traits<TypeT>::TypeName());
                return ret;
            }


            template<ParameterNS::Type TypeT>
            FiberListManager<TypeT>::FiberListManager(const TimeManager& time_manager)
            : time_manager_(time_manager)
            {
                list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }


            template<ParameterNS::Type TypeT>
            template<class FiberSectionT>
            void FiberListManager<TypeT>::Create(const FiberSectionT& section)
            {
                namespace ipl = Internal::InputParameterListNS;

                decltype(auto) ensight_file = ipl::ExtractPathParameter<typename FiberSectionT::EnsightFile>(section);
                decltype(auto) domain_index = ipl::ExtractParameter<typename FiberSectionT::DomainIndex>(section);
                decltype(auto) felt_space_index = ipl::ExtractParameter<typename FiberSectionT::FEltSpaceIndex>(section);
                decltype(auto) unknown_name = ipl::ExtractParameter<typename FiberSectionT::UnknownName>(section);

                const auto& domain =
                    DomainManager::GetInstance().GetDomain(domain_index, __FILE__, __LINE__);

                decltype(auto) mesh = domain.GetGeometricMeshRegion();

                const auto& god_of_dof = GodOfDofManager::GetInstance().GetGodOfDof(mesh.GetUniqueId());

                const auto& felt_space = god_of_dof.GetFEltSpace(felt_space_index);
                const auto& unknown = UnknownManager::GetInstance().GetUnknown(unknown_name);

                Create(section.GetUniqueId(),
                       ensight_file,
                       domain,
                       felt_space,
                       unknown);
            }


            template<ParameterNS::Type TypeT>
            void FiberListManager<TypeT>::Create(const unsigned int unique_id,
                                                 const std::string& fiber_file,
                                                 const Domain& domain,
                                                 const FEltSpace& felt_space,
                                                 const Unknown& unknown)
            {
                // make_unique is not accepted here: it makes the code yell about private status of the constructor
                // with both clang and gcc.
                fiber_list_type* buf = new fiber_list_type(unique_id,
                                                           fiber_file,
                                                           domain,
                                                           felt_space,
                                                           GetTimeManager(),
                                                           unknown);

                auto&& ptr = typename fiber_list_type::unique_ptr(buf);

                assert(ptr->GetUniqueId() == unique_id);

                auto&& pair = std::make_pair(unique_id, std::move(ptr));

                auto insert_return_value = list_.insert(std::move(pair));

                if (!insert_return_value.second)
                    throw Exception("Two fiber lists objects can't share the same unique identifier! (namely "
                                    + std::to_string(unique_id) + ").", __FILE__, __LINE__);
            }


            template<ParameterNS::Type TypeT>
            const FiberList<TypeT>& FiberListManager<TypeT>::GetFiberList(unsigned int unique_id) const
            {
                auto it = list_.find(unique_id);

                assert(it != list_.cend());
                assert(!(!(it->second)));

                return *(it->second);
            }


            template<ParameterNS::Type TypeT>
            FiberList<TypeT>& FiberListManager<TypeT>::GetNonCstFiberList(unsigned int unique_id)
            {
                return const_cast<FiberList<TypeT>&>(GetFiberList(unique_id));
            }


            template<ParameterNS::Type TypeT>
            const TimeManager& FiberListManager<TypeT>::GetTimeManager() const noexcept
            {
                return time_manager_;
            }


        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FIBER_LIST_MANAGER_HXX_
