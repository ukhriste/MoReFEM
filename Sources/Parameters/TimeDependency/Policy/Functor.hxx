///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FUNCTOR_HXX_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FUNCTOR_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            namespace PolicyNS
            {


                template <class FunctorT>
                Functor<FunctorT>::Functor(FunctorT&& functor)
                : functor_(functor)
                { }


                template <class FunctorT>
                double Functor<FunctorT>::GetCurrentTimeFactor(double time) const
                {
                    return functor_(time);
                }


            } // namespace PolicyNS


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FUNCTOR_HXX_
