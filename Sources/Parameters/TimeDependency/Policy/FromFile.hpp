///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FROM_FILE_HPP_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FROM_FILE_HPP_

# include <memory>
# include <vector>
# include <map>


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            namespace PolicyNS
            {


                /*!
                 * \brief Policy of the time dependency based on a file.
                 *
                 * \copydoc doxygen_hide_param_time_dependancy
                 */
                class FromFile
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = FromFile;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                public:

                    //! Alias to the return type.
                    using return_type = double;

                private:

                    //! Alias to the way each value is stored.
                    using storage_value_type = std::decay_t<return_type>;

                    //! Alias to value holder.
                    using value_holder_type = double;

                    /*!
                     * \brief Alias to the type of the value actually stored.
                     *
                     * First double is the time ie each time step.
                     * Second double is the value of the parameter at the time step considered.
                     */
                    using storage_type = std::vector<std::pair<double, double>>;

                public:

                    /// \name Special members.
                    ///@{

                    /*! Constructor.
                     *
                     * \param[in] file File in which time dependency is read.
                     * The format is two columns: in the first one the time in seconds is read, in the second
                     * the time-dependant value. First column is expected to be given in ascending order.
                     * Currently all time steps are supposed to be found exactly (with
                     * a very small leeway to take into account numerical imprecision...); later on values should
                     * be interpolated if missing.
                     */
                    explicit FromFile(const std::string& file);

                    //! Destructor.
                    ~FromFile() = default;

                    //! Copy constructor.
                    FromFile(const FromFile&) = delete;

                    //! Move constructor.
                    FromFile(FromFile&&) = delete;

                    //! Copy affectation.
                    FromFile& operator=(const FromFile&) = delete;

                    //! Move affectation.
                    FromFile& operator=(FromFile&&) = delete;

                    ///@}

                public:

                    //! Compute the current value at a given time.
                    double GetCurrentTimeFactor(double time) const;

                private:

                    //! Constant accessor to storage.
                    const storage_type& GetStorage() const noexcept;

                    //! Constant accessor to storage.
                    storage_type& GetNonCstStorage() noexcept;

                private:

                    //! Values of the parameter at each time step.
                    storage_type storage_;

                };


            } // namespace PolicyNS


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/TimeDependency/Policy/FromFile.hxx"


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FROM_FILE_HPP_
