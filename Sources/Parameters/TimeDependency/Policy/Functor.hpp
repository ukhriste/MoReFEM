///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FUNCTOR_HPP_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FUNCTOR_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace TimeDependencyNS
        {


            namespace PolicyNS
            {


                /*!
                 * \brief Policy of the time dependency based on a functor.
                 *
                 * \copydoc doxygen_hide_param_time_dependancy
                 *
                 * \tparam FunctorT A function with takes as argument a double (the time in seconds) and returns
                 * the time-related factor (g(t) in the explanation above).
                 *
                 */
                template <class FunctorT>
                class Functor
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = Functor<FunctorT>;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Functor(FunctorT&& functor);

                    //! Destructor.
                    ~Functor() = default;

                    //! Copy constructor.
                    Functor(const Functor&) = delete;

                    //! Move constructor.
                    Functor(Functor&&) = delete;

                    //! Copy affectation.
                    Functor& operator=(const Functor&) = delete;

                    //! Move affectation.
                    Functor& operator=(Functor&&) = delete;

                    ///@}

                public:

                    //! Compute the adequate time factor for \a time.
                    double GetCurrentTimeFactor(double time) const;

                private:

                    /*!
                     * \brief Functor which returns the time dependant factor.
                     *
                     * It takes a double (the time step) and returns the value of the time-dependant factor; a possible
                     * prototype is:
                     * \code
                     * [](double time) -> double
                     * \endcode
                     *
                     */
                    const FunctorT functor_;


                };


            } // namespace PolicyNS


        } // namespace TimeDependencyNS


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/TimeDependency/Policy/Functor.hxx"


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_POLICY_x_FUNCTOR_HPP_
