target_sources(${MOREFEM_PARAM}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/InitStoredQuantity.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ApplyTimeFactor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ApplyTimeFactor.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Dispatcher.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Dispatcher.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/InitStoredQuantity.hpp"
)

