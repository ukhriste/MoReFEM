target_sources(${MOREFEM_PARAM}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Alias.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ParameterAtDof.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ParameterInstance.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ParameterInstance.hxx"
)

