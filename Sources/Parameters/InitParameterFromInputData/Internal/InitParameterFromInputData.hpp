///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 13:41:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
# define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_

# include "Parameters/ParameterType.hpp"

# include "Parameters/Parameter.hpp"
# include "Parameters/Internal/ParameterInstance.hpp"
# include "Parameters/Policy/Constant/Constant.hpp"
# include "Parameters/Policy/OpsFunction/OpsFunction.hpp"
# include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
# include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"

# include "Parameters/Internal/Alias.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            /*!
             * \brief Initialize properly a scalar Parameter from the informations given in the input file.
             *
             * \attention This function is in the Internal namespace and should not be called directly unless you know
             * exactly what you're doing! Usually the namesake function in MoReFEM namespace is what you're looking for.
             *
             * Current function in the Internal namespace has been introduced as there are currently two different cases
             * in which we want to instantiate a scalar parameter:
             * . When it is actually a scalar parameter we want to build.
             *
             * Typically, such a parameter is defined like:
             * \code
             * YoungModulus = {
             * nature = "lua_function",
             * scalar_value = 10.,
             * lua_function = function (x, y, z)
             * return x + y * 10 + z * 100
             *  end
             * }
             * \endcode
             * . When we want to build a vectorial parameter. Under the hood each component is built as a scalar parameter;
             * but nature and scalar_value are given within vectors in Ops file. Lua function can't be given in a vector,
             * hence the asymmetry between some parameters given as template arguments and other as plain arguments.
             *
             *
             * \tparam InputParameterDataT Type of the input parameter file for the considered model instance.
             * \tparam LuaFieldT Input parameter structure from the input file that will be used to build the OpsFunction
             * object. For instance InputParameter::YoungModulus::LuaFunction.
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             * \param[in] name Name of the parameter (for outputs only).
             * \copydoc doxygen_hide_parameter_domain_arg
             * \param[in] nature Nature of the scalar parameter as read in the input file ('constant', 'lua_function', etc..)
             * Ops is supposed to check the given values are legit.
             * \param[in] nature Nature of the parameter (scalar, vectorial or matricial).
             * \param[in] scalar_value Scalar value to use in the case a constant parameter is built. Irrelevant for other
             * natures.
             * \param[in] domain_id_list List of \a Domain unique identifiers in the case of a piecewise constant by
             * domain parameter. Irrelevant for other natures.
             * \param[in] value_per_domain Scalar value to attribute to each domain in the case of a piecewise constant by
             * domain parameter. Must be the same size as \a domain_id_list, as the value for the \a i -th domain is found at
             * the \a i -th position in this container. Irrelevant for other natures.

             * \return Parameter considered.
             */
            template
            <
                class LuaFieldT,
                template<ParameterNS::Type> class TimeDependencyT,
                class StringT,
                class InputParameterDataT
            >
            typename ScalarParameter<TimeDependencyT>::unique_ptr
            InitScalarParameterFromInputData(StringT&& name,
                                             const Domain& domain,
                                             const InputParameterDataT& input_parameter_data,
                                             const std::string& nature,
                                             double scalar_value,
                                             const std::vector<unsigned int>& domain_id_list,
                                             const std::vector<double>& value_per_domain);


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/InitParameterFromInputData/Internal/InitParameterFromInputData.hxx"


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INTERNAL_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
