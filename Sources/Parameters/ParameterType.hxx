///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 12 Oct 2015 14:56:14 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_PARAMETER_TYPE_HXX_
# define MOREFEM_x_PARAMETERS_x_PARAMETER_TYPE_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {



        inline const std::string& Traits<Type::scalar>::TypeName() noexcept
        {
            static std::string ret("scalar");
            return ret;
        }


        inline double Traits<Type::scalar>::AllocateDefaultValue(unsigned int, unsigned int ) noexcept
        {
            return 0.;
        }


        inline const std::string& Traits<Type::vector>::TypeName() noexcept
        {
            static std::string ret("vector");
            return ret;
        }


        inline const std::string& Traits<Type::matrix>::TypeName() noexcept
        {
            static std::string ret("matrix");
            return ret;
        }



    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_PARAMETER_TYPE_HXX_
