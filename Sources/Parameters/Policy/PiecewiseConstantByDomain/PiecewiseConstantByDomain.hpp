///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_

# include <memory>
# include <vector>

# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/UniqueId/UniqueId.hpp"

# include "Geometry/Mesh/GeometricMeshRegion.hpp"
# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Domain/DomainManager.hpp"

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            /*!
             * \brief Parameter policy when the parameter is piecewise constant by domain.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             *
             */
            template<ParameterNS::Type TypeT>
            class PiecewiseConstantByDomain
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = PiecewiseConstantByDomain<TypeT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

            private:

                //! Alias to traits class related to TypeT.
                using traits = Traits<TypeT>;


            public:

                //! Alias to the return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the value actually stored.
                using storage_type = std::unordered_map<unsigned int, std::decay_t<return_type>>;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor for the paramter policy PiecewiseConstantByDomain.
                 *
                 * \copydoc doxygen_hide_parameter_domain_arg
                 * \param[in] key Unique ids of the domains on which the parameter is defined given by the input file.
                 * All the indices must be unique and the domain defined must not intersect.
                 * \param[in] value Value of the parameter in each domain. The number of indices and values must be the same.
                 *
                 * It is expected that the \a Domain defined here are actually subsets of the \a Domain upon which
                 * the \a Parameter is defined.
                 */
                explicit PiecewiseConstantByDomain(const Domain& domain,
                                                   const std::vector<unsigned int>& key,
                                                   const std::vector<std::decay_t<return_type>>& value);

                //! Destructor.
                ~PiecewiseConstantByDomain() = default;

                //! Copy constructor.
                PiecewiseConstantByDomain(const PiecewiseConstantByDomain&) = delete;

                //! Move constructor.
                PiecewiseConstantByDomain(PiecewiseConstantByDomain&&) = delete;

                //! Copy affectation.
                PiecewiseConstantByDomain& operator=(const PiecewiseConstantByDomain&) = delete;

                //! Move affectation.
                PiecewiseConstantByDomain& operator=(PiecewiseConstantByDomain&&) = delete;

                ///@}

            public:

                /*!
                 * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
                 */
                void SetConstantValue(double value);

            protected:

                //! Provided here to make the code compile, but should never be called.
                [[noreturn]] return_type GetConstantValueFromPolicy() const noexcept;

                //! Get the value.
                return_type GetValueFromPolicy(const local_coords_type& local_coords,
                                               const GeometricElt& geom_elt) const;

                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;


            protected:

                //! Whether the parameter varies spatially or not (so always false here).
                constexpr bool IsConstant() const noexcept;

                //! Write the content of the parameter for which policy is used in a stream.
                void WriteFromPolicy(std::ostream& out) const;

            private:

                //! Constant accessor to the attribut value_.
                const storage_type& GetStoredValuesPerDomain() const noexcept;

                //! Accessor to the attribut value_.
                storage_type& GetNonCstStoredValuesPerDomain() noexcept;

                //! Access to the map that links a geometric element to a domain (through their unique ids).
                const std::unordered_map<unsigned int, unsigned int>& GetMapGeomEltDomain() const noexcept;

                //! Non constant access to the map that links a geometric element to a domain (through their unique ids).
                std::unordered_map<unsigned int, unsigned int>& GetNonCstMapGeomEltDomain() noexcept;



            private:

                //! Value of the parameter for each domain (represented by their unique ids).
                storage_type stored_values_per_domain_;

                /*!
                 * \brief Map of geom_elem associated to its domain (must be unique!).
                 *
                 * Key is the unique id of the geometric element.
                 * Value is the unique id of the domain.
                 */
                std::unordered_map<unsigned int, unsigned int> mapping_geom_elt_domain_;


            };


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hxx"


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
