///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Mass.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        Mass::Mass(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                   elementary_data_type&& a_elementary_data)
        : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data))
        {
            assert(unknown_list.size() == 1);
            assert(test_unknown_list.size() == 1);
        }
        
        
        Mass::~Mass() = default;
       
        
        const std::string& Mass::ClassName()
        {
            static std::string name("Mass");
            return name;
        }
        
        
        void Mass::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();
            
            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());
            
            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            
            const int Ncomponent = static_cast<int>(ref_felt.Ncomponent());
            
            assert(Ncomponent == static_cast<int>(test_ref_felt.Ncomponent()));
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();            
            
            const int Nnode_for_unknown = static_cast<int>(ref_felt.Nnode());
            const int Nnode_for_test_unknown = static_cast<int>(test_ref_felt.Nnode());
            
            unsigned int quad_pt_index = 0;
            
            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];
                
                const auto& phi_unknown = infos_at_quad_pt_for_unknown.GetRefFEltPhi();
                const auto& phi_test_unknown = infos_at_quad_pt_for_test_unknown.GetRefFEltPhi();
                
                const double factor = infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetWeight()
                                    * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();
                
                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());
                       
                for (int m = 0; m < Nnode_for_unknown; ++m)
                {
                    const double m_value = factor * phi_unknown(m);
                    
                    for (int n = 0; n < Nnode_for_test_unknown; ++n)
                    {
                        const double value = m_value * phi_test_unknown(n);
                        
                        for (int component = 0; component < Ncomponent; ++component)
                        {
                            const auto shift = Nnode_for_unknown * component;
                            matrix_result(m + shift, n + shift) += value;
                        }
                    }
                }
                
                ++quad_pt_index;
            }
        }

        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
