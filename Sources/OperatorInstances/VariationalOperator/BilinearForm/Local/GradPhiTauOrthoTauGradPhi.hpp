///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 25 Jan 2017 10:21:39 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HPP_

# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

# include "Parameters/Parameter.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"

# include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        /*!
         * \brief Local operator for \a Bidomain.
         */
        class GradPhiTauOrthoTauGradPhi final
        : public BilinearLocalVariationalOperator<LocalMatrix>,
        public Crtp::LocalMatrixStorage<GradPhiTauOrthoTauGradPhi, 12u>,
        public Crtp::LocalVectorStorage<GradPhiTauOrthoTauGradPhi, 3ul>
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = GradPhiTauOrthoTauGradPhi;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Convenient typedef.
            using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! Alias to the parent that provides LocalMatrixStorage.
            using matrix_parent = Crtp::LocalMatrixStorage<self, 12ul>;

            //! Alias to the parent that provides LocalVectorStorage.
            using vector_parent = Crtp::LocalVectorStorage<self, 3ul>;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
             * is due to constraints from genericity; for current operator it is expected to hold exactly
             * two unknowns (the first one vectorial and the second one scalar).
             * \copydoc doxygen_hide_test_unknown_list_param
             * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
             * \param[in] transverse_diffusion_tensor Tmp \todo #9 Complete!
             * \param[in] fiber_diffusion_tensor Tmp \todo #9 Complete!
             * \param[in] fibers Tmp \todo #9 Complete!
             * \param[in] angles Tmp \todo #9 Complete!
             *
             * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
             * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
             */
            explicit GradPhiTauOrthoTauGradPhi(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                               const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                               elementary_data_type&& elementary_data,
                                               const scalar_parameter& transverse_diffusion_tensor,
                                               const scalar_parameter& fiber_diffusion_tensor,
                                               const FiberList<ParameterNS::Type::vector>& fibers,
                                               const FiberList<ParameterNS::Type::scalar>& angles);

            //! Destructor.
            ~GradPhiTauOrthoTauGradPhi();

            //! Copy constructor.
            GradPhiTauOrthoTauGradPhi(const GradPhiTauOrthoTauGradPhi&) = delete;

            //! Move constructor.
            GradPhiTauOrthoTauGradPhi(GradPhiTauOrthoTauGradPhi&&) = delete;

            //! Copy affectation.
            GradPhiTauOrthoTauGradPhi& operator=(const GradPhiTauOrthoTauGradPhi&) = delete;

            //! Move affectation.
            GradPhiTauOrthoTauGradPhi& operator=(GradPhiTauOrthoTauGradPhi&&) = delete;

            ///@}

            //! Compute the elementary vector.
            void ComputeEltArray();

            //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
            void InitLocalComputation() {}

            //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
            void FinalizeLocalComputation() {}


        private:

            /*!
             * \brief Compute contravariant basis.
             *
             * \param[in] infos_at_quad_pt Objects which stores many relevant data for the elementary computation at
             * a given \a QuadraturePoint.
             */
            void ComputeContravariantBasis(const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt);

        private:

            //! Get the first diffusion tensor.
            const scalar_parameter& GetTransverseDiffusionTensor() const noexcept;

            //! Get the third diffusion tensor.
            const scalar_parameter& GetFiberDiffusionTensor() const noexcept;

            //! Get the fiber.
            const FiberList<ParameterNS::Type::vector>& GetFibers() const noexcept;

            //! Get the angles.
            const FiberList<ParameterNS::Type::scalar>& GetAngles() const noexcept;

        private:

            //! \name Material parameters.
            ///@{

            //! First diffusion tensor = sigma_t.
            const scalar_parameter& transverse_diffusion_tensor_;

            //! Second diffusion tensor = sigma_l. (longitudinal)
            const scalar_parameter& fiber_diffusion_tensor_;

            //! Fibers parameter.
            const FiberList<ParameterNS::Type::vector>& fibers_;

            //! Angles parameter.
            const FiberList<ParameterNS::Type::scalar>& angles_;

            ///@}

        private:

            /// \name Useful indexes to fetch the work matrices and vectors.
            ///@{

            enum class LocalMatrixIndex : std::size_t
            {
                block_matrix = 0,
                transposed_dphi_test,
                dphi_sigma,
                dphi_contravariant_metric_tensor,
                tau_sigma,
                tau_ortho_sigma,
                covariant_basis,
                contravariant_basis,
                transposed_covariant_basis,
                covariant_metric_tensor,
                contravariant_metric_tensor,
                reduced_contravariant_metric_tensor
            };


            enum class LocalVectorIndex : std::size_t
            {
                tau_interpolate_ortho = 0,
                tau_covariant_basis,
                tau_ortho_covariant_basis
            };

            ///@}


        };


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauOrthoTauGradPhi.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_ORTHO_TAU_GRAD_PHI_HPP_
