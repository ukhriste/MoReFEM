///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HPP_

# include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

# include "Parameters/Parameter.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"

# include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Local operator for \a SurfacicBidomain.
             */
            class SurfacicBidomain final
            : public BilinearLocalVariationalOperator<LocalMatrix>,
            public Crtp::LocalMatrixStorage<SurfacicBidomain, 18u>,
            public Crtp::LocalVectorStorage<SurfacicBidomain, 3ul>
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = SurfacicBidomain;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Returns the name of the operator.
                static const std::string& ClassName();

                //! Convenient alias.
                using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

                //! Alias to the parent that provides LocalMatrixStorage.
                using matrix_parent = Crtp::LocalMatrixStorage<self, 18ul>;

                //! Alias to the parent that provides LocalVectorStorage.
                using vector_parent = Crtp::LocalVectorStorage<self, 3ul>;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
                 * is due to constraints from genericity; for current operator it is expected to hold exactly
                 * two unknowns (the first one vectorial and the second one scalar).
                 * \copydoc doxygen_hide_test_unknown_list_param
                 * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
                 * \param[in] intracellular_trans_diffusion_tensor Intracellular trans diffusion tensor.
                 * \param[in] extracellular_trans_diffusion_tensor Extracellular trans diffusion tensor.
                 * \param[in] intracellular_fiber_diffusion_tensor Intracellular fiber diffusion tensor.
                 * \param[in] extracellular_fiber_diffusion_tensor Extracellular fiber diffusion tensor.
                 * \param[in] heterogeneous_conductivity_coefficient Heterogeneous conductivity coefficient.
                 * \param[in] fibers Fibers.
                 * \param[in] angles Angles. \todo #9 Should be completed.
                 *
                 * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
                 * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
                 */
                explicit SurfacicBidomain(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                          const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                          elementary_data_type&& elementary_data,
                                          const scalar_parameter& intracellular_trans_diffusion_tensor,
                                          const scalar_parameter& extracellular_trans_diffusion_tensor,
                                          const scalar_parameter& intracellular_fiber_diffusion_tensor,
                                          const scalar_parameter& extracellular_fiber_diffusion_tensor,
                                          const scalar_parameter& heterogeneous_conductivity_coefficient,
                                          const FiberList<ParameterNS::Type::vector>& fibers,
                                          const FiberList<ParameterNS::Type::scalar>& angles);

                //! Destructor.
                ~SurfacicBidomain();

                //! Copy constructor.
                SurfacicBidomain(const SurfacicBidomain&) = delete;

                //! Move constructor.
                SurfacicBidomain(SurfacicBidomain&&) = delete;

                //! Copy affectation.
                SurfacicBidomain& operator=(const SurfacicBidomain&) = delete;

                //! Move affectation.
                SurfacicBidomain& operator=(SurfacicBidomain&&) = delete;

                ///@}

                //! Compute the elementary vector.
                void ComputeEltArray();

                //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
                void InitLocalComputation() {}

                //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
                void FinalizeLocalComputation() {}


            private:

                /*!
                 * \brief Compute contravariant basis.
                 *
                 * \param[in] infos_at_quad_pt Objects which stores many relevant data for the elementary computation at
                 * a given \a QuadraturePoint.
                 */
                void ComputeContravariantBasis(const Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint& infos_at_quad_pt);

            private:

                //! Get the first diffusion tensor.
                const scalar_parameter& GetIntracelluarTransDiffusionTensor() const noexcept;

                //! Get the second diffusion tensor.
                const scalar_parameter& GetExtracelluarTransDiffusionTensor() const noexcept;

                //! Get the thrid diffusion tensor.
                const scalar_parameter& GetIntracelluarFiberDiffusionTensor() const noexcept;

                //! Get the fourth diffusion tensor.
                const scalar_parameter& GetExtracelluarFiberDiffusionTensor() const noexcept;

                //! Get the coefficient for Heterogeneous Conductivity.
                const scalar_parameter& GetHeterogeneousConductivityCoefficient() const noexcept;

                //! Get the fiber.
                const FiberList<ParameterNS::Type::vector>& GetFibers() const noexcept;

                //! Get the angles.
                const FiberList<ParameterNS::Type::scalar>& GetAngles() const noexcept;

            private:

                //! \name Material parameters.
                ///@{

                //! First diffusion tensor = sigma_i_t.
                const scalar_parameter& intracellular_trans_diffusion_tensor_;

                //! Second diffusion tensor = sigma_e_t.
                const scalar_parameter& extracellular_trans_diffusion_tensor_;

                //! Third diffusion tensor = sigma_i_l.
                const scalar_parameter& intracellular_fiber_diffusion_tensor_;

                //! Fourth diffusion tensor = sigma_e_l.
                const scalar_parameter& extracellular_fiber_diffusion_tensor_;

                //! Coefficient for heterogeneous_conductivity.
                const scalar_parameter& heterogeneous_conductivity_coefficient_;

                //! Fibers parameter.
                const FiberList<ParameterNS::Type::vector>& fibers_;

                //! Angles parameter.
                const FiberList<ParameterNS::Type::scalar>& angles_;

                ///@}

            private:

                /// \name Useful indexes to fetch the work matrices and vectors.
                ///@{

                //! Indexes for local matrices.
                enum class LocalMatrixIndex : std::size_t
                {
                    block_matrix1 = 0,
                    block_matrix2,
                    block_matrix3,
                    block_matrix4,
                    transposed_dphi_test,
                    transposed_dpsi_test,
                    dphi_sigma,
                    dpsi_sigma,
                    dphi_contravariant_metric_tensor,
                    dpsi_contravariant_metric_tensor,
                    tau_sigma,
                    tau_ortho_sigma,
                    covariant_basis,
                    contravariant_basis,
                    transposed_covariant_basis,
                    covariant_metric_tensor,
                    contravariant_metric_tensor,
                    reduced_contravariant_metric_tensor
                };


                //! Indexes for local vectors.
                enum class LocalVectorIndex : std::size_t
                {
                    tau_interpolate_ortho = 0,
                    tau_covariant_basis,
                    tau_ortho_covariant_basis
                };

                ///@}


            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_SURFACIC_BIDOMAIN_HPP_
