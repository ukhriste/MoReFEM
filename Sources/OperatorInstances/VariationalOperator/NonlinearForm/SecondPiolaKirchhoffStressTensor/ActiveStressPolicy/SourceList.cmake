target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/AnalyticalPrestress.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/AnalyticalPrestress.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/None.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
