///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HPP_

# include <memory>
# include <vector>

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ViscoelasticityPolicyNS
            {


                /*!
                 * \brief Policy to choose when there is visco-elasticity involved in the operator.
                 */
                template<Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT>
                class Viscoelasticity
                {
                public:

                    //! Alias to the local policy operator.
                    using local_policy = Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::Viscoelasticity<DerivatesWithRespectToT>;

                public:

                    //! Alias to self.
                    using self = Viscoelasticity;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr> ;

                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit Viscoelasticity() = default;

                    //! Destructor.
                    ~Viscoelasticity() = default;

                    //! Copy constructor.
                    Viscoelasticity(const Viscoelasticity&) = delete;

                    //! Move constructor.
                    Viscoelasticity(Viscoelasticity&&) = delete;

                    //! Copy affectation.
                    Viscoelasticity& operator=(const Viscoelasticity&) = delete;

                    //! Move affectation.
                    Viscoelasticity& operator=(Viscoelasticity&&) = delete;

                    ///@}

                private:



                };


            } // namespace ViscoelasticityPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_VISCOELASTICITY_POLICY_x_VISCOELASTICITY_HPP_
