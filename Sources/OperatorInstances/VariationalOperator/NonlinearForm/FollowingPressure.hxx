///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_FOLLOWING_PRESSURE_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_FOLLOWING_PRESSURE_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        FollowingPressure<TimeDependencyT>::FollowingPressure(const FEltSpace& felt_space,
                                                              const Unknown::const_shared_ptr unknown_ptr,
                                                              const Unknown::const_shared_ptr test_unknown_ptr,
                                                              const parameter_type& pressure,
                                                              const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown_ptr,
                 test_unknown_ptr,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 DoComputeProcessorWiseLocal2Global::yes,
                 pressure)
        {
            assert(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
            assert(test_unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        const std::string& FollowingPressure<TimeDependencyT>::ClassName()
        {
            static std::string name("FollowingPressure");
            return name;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        template<class LinearAlgebraTupleT>
        inline void FollowingPressure<TimeDependencyT>
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& input_vector,
                   const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        template<class LocalOperatorTypeT>
        inline void FollowingPressure<TimeDependencyT>
        ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                      LocalOperatorTypeT& local_operator,
                                      const std::tuple<const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  this->GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalDisplacement());
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_FOLLOWING_PRESSURE_HXX_
