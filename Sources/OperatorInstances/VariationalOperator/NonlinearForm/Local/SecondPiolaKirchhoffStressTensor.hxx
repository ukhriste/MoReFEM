///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 May 2014 11:40:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        const std::string& SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>::ClassName()
        {
            static std::string name("SecondPiolaKirchhoffStressTensor");
            return name;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::~SecondPiolaKirchhoffStressTensor() = default;


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SecondPiolaKirchhoffStressTensor(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                           elementary_data_type&& a_elementary_data,
                                           const Solid& solid,
                                           const TimeManager& time_manager,
                                           const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                           input_active_stress_policy_type* input_active_stress_policy)
        : HyperelasticityPolicyT(a_elementary_data.GetMeshDimension(), hyperelastic_law),
        ViscoelasticityPolicyT(a_elementary_data.GetMeshDimension(),
                               a_elementary_data.NdofRow(),
                               a_elementary_data.GetGeomEltDimension(),
                               solid,
                               time_manager),
        ActiveStressPolicyT(a_elementary_data.GetMeshDimension(), a_elementary_data.NnodeRow(), a_elementary_data.NquadraturePoint(), time_manager, input_active_stress_policy),
        parent(unknown_list, test_unknown_list, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent()
        {
            const auto& elementary_data = parent::GetElementaryData();
            const unsigned int Ncomponent = elementary_data.GetGeomEltDimension();
            const unsigned int square_Ncomponent = NumericNS::Square(Ncomponent);

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unknown = static_cast<int>(unknown_ref_felt.Nnode());

            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unknown = static_cast<int>(test_unknown_ref_felt.Nnode());

            const int felt_space_dimension = static_cast<int>(unknown_ref_felt.GetFEltSpaceDimension());

            unsigned int dIndC_size(0u);

            switch(mesh_dimension)
            {
                case 1:
                    dIndC_size = 1u;
                    break;
                case 2:
                    dIndC_size = 3u;
                    break;
                case 3:
                    dIndC_size = 6u;
                    break;
                default:
                    assert(false);
                    break;
            }

            deriv_green_lagrange_ =
                std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(mesh_dimension);

            const auto square_mesh_dimension = NumericNS::Square(mesh_dimension);

            this->matrix_parent::InitLocalMatrixStorage
            ({{
                { square_Ncomponent, square_Ncomponent },  // tangent_matrix
                { square_Ncomponent, square_Ncomponent },  // linear_part
                { Nnode_for_unknown, Ncomponent }, // dPhi_mult_gradient_based_block
                { Nnode_for_unknown, Nnode_for_test_unknown }, // block_contribution
                { felt_space_dimension, Nnode_for_test_unknown }, // transposed dPhi test
                { dIndC_size, dIndC_size }, // d2W,
                { square_mesh_dimension, dIndC_size }, // linear_part_intermediate_matrix
                { mesh_dimension, mesh_dimension }, // gradient-based block
                { mesh_dimension, mesh_dimension } // gradient displacement
            }});


            this->vector_parent::InitLocalVectorStorage
            ({{
                dIndC_size, // dW
                square_mesh_dimension // rhs_part
            }});

            former_local_displacement_.resize(elementary_data.NdofRow());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        inline Unknown::const_shared_ptr SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::GetVectorialUnknownPtr() const noexcept
        {
            const auto& unknown_list = parent::GetExtendedUnknownList();
            assert(unknown_list.size() == 1);
            assert(!(!(unknown_list.front())));
            assert(unknown_list.front()->GetNature() == UnknownNS::Nature::vectorial);
            return unknown_list.front();
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::ComputeEltArray()
        {
            auto& elementary_data = parent::GetNonCstElementaryData();

            const auto& local_displacement = GetFormerLocalDisplacement();

            OperatorNS::Nature what_must_be_built = OperatorNS::Nature::nonlinear; // both matrix and vector must be built.
            // see #533.

            auto& tangent_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
            const auto& rhs_part = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();

            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();

            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            unsigned int quad_pt_index = 0;

            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];

                tangent_matrix.Zero();

                PrepareInternalDataForQuadraturePoint(infos_at_quad_pt_for_unknown,
                                                      geom_elt,
                                                      elementary_data.GetRefFElt(this->GetNthUnknown(0)),
                                                      local_displacement,
                                                      what_must_be_built,
                                                      mesh_dimension);

                ComputeAtQuadraturePoint(infos_at_quad_pt_for_unknown,
                                         infos_at_quad_pt_for_test_unknown,
                                         tangent_matrix,
                                         rhs_part,
                                         elementary_data,
                                         what_must_be_built);

                ++quad_pt_index;
            }

            const auto& active_stress_ptr = static_cast<ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            using dispatcher
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::CorrectRHSWithActiveSchurComplement<ActiveStressPolicyT>;

            dispatcher::Perform(active_stress,
                                elementary_data.GetNonCstVectorResult());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::UpdateActiveStressInternalVariables()
        {
            auto& elementary_data = parent::GetNonCstElementaryData();
            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();

            const auto& active_stress_ptr = static_cast<ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();

                active_stress.UpdateInternalVariables(quad_pt,
                                                      geom_elt,
                                                      infos_at_quad_pt_for_unknown.GetRefFEltPhi(),
                                                      active_stress.GetFiberDeformationAtCurrentTimeIteration().GetValue(quad_pt, geom_elt),
                                                      active_stress.GetFiberDeformationAtPreviousTimeIteration().GetValue(quad_pt, geom_elt));
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::ComputeAtQuadraturePoint(const InformationsAtQuadraturePoint& infos_at_quad_pt_for_unknown,
                                   const InformationsAtQuadraturePoint& infos_at_quad_pt_for_test_unknown,
                                   const LocalMatrix& tangent_matrix,
                                   const LocalVector& rhs_part,
                                   elementary_data_type& elementary_data,
                                   OperatorNS::Nature what_must_be_built) const
        {
            const auto Ncomponent = elementary_data.GetGeomEltDimension();

            const auto& dphi = infos_at_quad_pt_for_unknown.GetGradientFEltPhi();
            const auto& dphi_test = infos_at_quad_pt_for_test_unknown.GetGradientFEltPhi();

            const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();

            const auto weight_meas = quad_pt.GetWeight() * infos_at_quad_pt_for_unknown.GetAbsoluteValueJacobianDeterminant();

            const auto& unknown_ref_felt = elementary_data.GetRefFElt(this->GetNthUnknown(0));
            const int Nnode_for_unknown = static_cast<int>(unknown_ref_felt.Nnode());

            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(this->GetNthTestUnknown(0));
            const int Nnode_for_test_unknown = static_cast<int>(test_unknown_ref_felt.Nnode());

            if (what_must_be_built != OperatorNS::Nature::linear)
            {
                auto& gradient_based_block = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
                auto& transposed_dphi_test = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi_test)>();
                Wrappers::Seldon::Transpose(dphi_test, transposed_dphi_test);

                // Matrix related calculation.
                auto& matrix_result = elementary_data.GetNonCstMatrixResult();

                // LocalMatrix dPhi_mult_gradient_based_block(dPhi.GetM(), static_cast<int>(Ncomponent));
                auto& dPhi_mult_gradient_based_block =
                    this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dPhi_mult_gradient_based_block)>();

                auto& block_contribution =
                    this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

                for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                {
                    const auto row_first_index = static_cast<int>(unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component));

                    for (unsigned int col_component = 0u; col_component < Ncomponent; ++col_component)
                    {
                        const auto col_first_index = static_cast<int>(test_unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component));

                        Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(tangent_matrix,
                                                                                        row_component,
                                                                                        col_component,
                                                                                        gradient_based_block);

                        Seldon::Mlt(1., dphi,
                                    gradient_based_block,
                                    dPhi_mult_gradient_based_block);

                        Seldon::Mlt(weight_meas,
                                    dPhi_mult_gradient_based_block,
                                    transposed_dphi_test,
                                    block_contribution);

                        for (int row_node = 0; row_node < Nnode_for_unknown; ++row_node)
                        {
                            for (int col_node = 0u; col_node < Nnode_for_test_unknown; ++col_node)
                                matrix_result(row_first_index + row_node, col_first_index + col_node)
                                += block_contribution(row_node, col_node);
                        }
                    }
                }
            }

            const auto int_Ncomponent = static_cast<int>(Ncomponent);

            if (what_must_be_built != OperatorNS::Nature::bilinear)
            {
                // Vector related calculation.
                auto& vector_result = elementary_data.GetNonCstVectorResult();

                for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                {
                    const auto dof_first_index = static_cast<int>(unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component));
                    const auto component_first_index = static_cast<int>(row_component * Ncomponent);

                    // Compute the new contribution to vector_result here.
                    // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
                    for (int row_node = 0; row_node < Nnode_for_unknown; ++row_node)
                    {
                        double value = 0.;

                        for (int col = 0; col < int_Ncomponent; ++col)
                           value += dphi(row_node, col) * rhs_part(col + component_first_index);

                        vector_result(dof_first_index + row_node) += value * weight_meas;
                    }
                }
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<unsigned int DimensionT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::ComputeWDerivates(const InformationsAtQuadraturePoint& infos_at_quad_pt_for_unknown,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalMatrix& gradient_displacement,
                            const LocalMatrix& De,
                            const LocalMatrix& transposed_De,
                            LocalVector& dW,
                            LocalMatrix& d2W)
        {
            static_cast<void>(gradient_displacement);

            const auto& hyperelasticity_ptr = static_cast<HyperelasticityPolicyT*>(this);
            auto& hyperelasticity = *hyperelasticity_ptr;

            decltype(auto) cauchy_green_tensor = GetCauchyGreenTensor();
            decltype(auto) quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();
            decltype(auto) cauchy_green_tensor_value = cauchy_green_tensor.GetValue(quad_pt, geom_elt);

            using dispatcher
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyT>;

            dispatcher::Perform(quad_pt,
                                geom_elt,
                                cauchy_green_tensor_value,
                                hyperelasticity,
                                dW,
                                d2W);

            const auto& active_stress_ptr = static_cast<ActiveStressPolicyT*>(this);
            auto& active_stress = *active_stress_ptr;

            using dispatcher2
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeWDerivatesActiveStress<DimensionT, ActiveStressPolicyT>;

            dispatcher2::Perform(infos_at_quad_pt_for_unknown,
                                 geom_elt,
                                 ref_felt,
                                 cauchy_green_tensor_value,
                                 transposed_De,
                                 active_stress,
                                 dW,
                                 d2W);

            const auto& viscoelasticity_ptr = static_cast<ViscoelasticityPolicyT*>(this);
            auto& viscoelasticity = *viscoelasticity_ptr;

            using dispatcher3
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyT>;

            dispatcher3::Perform(infos_at_quad_pt_for_unknown,
                                 geom_elt,
                                 De,
                                 transposed_De,
                                 viscoelasticity,
                                 dW,
                                 d2W);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::PrepareInternalDataForQuadraturePoint(const InformationsAtQuadraturePoint& infos_at_quad_pt_for_unknown,
                                                const GeometricElt& geom_elt,
                                                const Advanced::RefFEltInLocalOperator& ref_felt,
                                                const std::vector<double>& local_displacement,
                                                OperatorNS::Nature what_must_be_built,
                                                const unsigned int mesh_dimension)
        {
            switch(mesh_dimension)
            {
                case 1:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<1>(infos_at_quad_pt_for_unknown,
                                                                               geom_elt,
                                                                               ref_felt,
                                                                               local_displacement,
                                                                               what_must_be_built);
                    break;
                }
                case 2:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<2>(infos_at_quad_pt_for_unknown,
                                                                               geom_elt,
                                                                               ref_felt,
                                                                               local_displacement,
                                                                               what_must_be_built);
                    break;
                }
                case 3:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<3>(infos_at_quad_pt_for_unknown,
                                                                               geom_elt,
                                                                               ref_felt,
                                                                               local_displacement,
                                                                               what_must_be_built);
                    break;
                }
                default:
                    assert(false);
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        inline const std::vector<double>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::GetFormerLocalDisplacement() const noexcept
        {
            return former_local_displacement_;
        }



        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::GetNonCstDerivativeGreenLagrange() noexcept
        {
            assert(!(!deriv_green_lagrange_));
            return *deriv_green_lagrange_;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        inline std::vector<double>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::GetNonCstFormerLocalDisplacement() noexcept
        {
            return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        template<unsigned int DimensionT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::PrepareInternalDataForQuadraturePointForDimension(const InformationsAtQuadraturePoint& infos_at_quad_pt_for_unknown,
                                                            const GeometricElt& geom_elt,
                                                            const Advanced::RefFEltInLocalOperator& ref_felt,
                                                            const std::vector<double>& local_displacement,
                                                            OperatorNS::Nature what_must_be_built)
        {
            auto& gradient_displacement = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_displacement)>();

            Advanced::OperatorNS::ComputeGradientDisplacementMatrix(infos_at_quad_pt_for_unknown,
                                                                    local_displacement,
                                                                    gradient_displacement);

            // ========================================================================================================
            // Compute first the invariants for current quadrature point.
            // For this purpose, Cauchy-Green tensor and De matrix (see P22 on Philippe's note) must be computed first.
            // ========================================================================================================

            auto& derivative_green_lagrange = GetNonCstDerivativeGreenLagrange();
            const auto& De = derivative_green_lagrange.Update(gradient_displacement);
            const auto& transposed_De = derivative_green_lagrange.GetTransposed();

            // ===================================================================================
            // Then compute the derivates of W, required to build both bilinear and linear terms.
            // ===================================================================================

            auto& dW = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dW)>();
            auto& d2W = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W)>();

            // Put to zero as in each ComputeWDerivates of the policies once should add the contributions and use Add.
            dW.Zero();
            d2W.Zero();

            ComputeWDerivates<DimensionT>(infos_at_quad_pt_for_unknown,
                                          geom_elt,
                                          ref_felt,
                                          gradient_displacement,
                                          De,
                                          transposed_De,
                                          dW,
                                          d2W);

            # ifdef MOREFEM_CHECK_NAN_AND_INF
            Wrappers::Seldon::ThrowIfNanInside(d2W, __FILE__, __LINE__);
            # endif // MOREFEM_CHECK_NAN_AND_INF

            // ===================================================================================
            // Finally build the terms that are actually required.
            // ===================================================================================

            // Linear term.
            switch(what_must_be_built)
            {
                case OperatorNS::Nature::linear:
                case OperatorNS::Nature::nonlinear:
                {
                    auto& rhs_part = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();
                    Seldon::Mlt(transposed_De, dW, rhs_part);
                    break;
                }
                case OperatorNS::Nature::bilinear:
                    break;
            }

            // Bilinear terms. There are in fact both: one for linear part and another for non-linear one.
            switch(what_must_be_built)
            {
                case OperatorNS::Nature::bilinear:
                case OperatorNS::Nature::nonlinear:
                {
                    auto& tangent_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
                    auto& linear_part_intermediate_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part_intermediate_matrix)>();
                    auto& linear_part = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part)>();

                    Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
                    ::ComputeLinearPart(De,
                                       transposed_De,
                                       d2W,
                                       linear_part_intermediate_matrix,
                                       // < internal quantity; better design
                                       // would be to encapsulate it in
                                       // an Internal class but I have no time
                                       // to do this minor fix now.
                                       linear_part);

                    Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ComputeNonLinearPart<DimensionT>(dW, tangent_matrix); // tangent_matrix not complete here; linear_part
                    // must be added for that (done immediately after this function call...)

                    const auto& viscoelasticity_ptr = static_cast<ViscoelasticityPolicyT*>(this);
                    auto& viscoelasticity = *viscoelasticity_ptr;

                    Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
                    ::AddTangentMatrixViscoelasticity<ViscoelasticityPolicyT>
                    ::Perform(tangent_matrix, viscoelasticity);

                    // Complete the tangent matrix with the linear part!
                    Seldon::Add(1., linear_part, tangent_matrix);
                    break;
                }
                case OperatorNS::Nature::linear:
                    break;
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::SetCauchyGreenTensor(const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>* param)
        {
            assert(cauchy_green_tensor_ == nullptr && "Should be called only once.");
            cauchy_green_tensor_ = param;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class ActiveStressPolicyT>
        inline const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, ActiveStressPolicyT>
        ::GetCauchyGreenTensor() const noexcept
        {
            assert(!(!cauchy_green_tensor_));
            return *cauchy_green_tensor_;
        }


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
