///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 20 Jan 2016 11:01:34 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        namespace SecondPiolaKirchhoffStressTensorNS
        {
            
            
            namespace HyperelasticityPolicyNS
            {
                
                
                None::None(unsigned int mesh_dimension,
                           const law_type* hyperelastic_law)
                {
                    static_cast<void>(mesh_dimension);
                    static_cast<void>(hyperelastic_law);
                }
                
                
            } // namespace HyperelasticityPolicyNS
            
            
        } // namespace SecondPiolaKirchhoffStressTensorNS
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
