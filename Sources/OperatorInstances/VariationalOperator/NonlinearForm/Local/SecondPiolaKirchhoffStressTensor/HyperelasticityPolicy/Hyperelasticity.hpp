///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 13 Jan 2016 11:18:38 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HPP_

# include <memory>
# include <vector>

# include "Utilities/Containers/EnumClass.hpp"

# include "ThirdParty/Wrappers/Seldon/SeldonFunctions.hpp"
# include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

# include "Parameters/ParameterAtQuadraturePoint.hpp"
# include "ParameterInstances/Compound/Solid/Solid.hpp"

# include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                namespace HyperelasticityPolicyNS
                {


                    /*!
                     * \brief Policy to use when hyperelasticity is involved in \a SecondPiolaKirchhoffStressTensor operator.
                     *
                     * \tparam HyperelasticLawT Hyperelastic law considered.
                     */
                    template <class HyperelasticLawT>
                    class Hyperelasticity
                    {

                    public:

                        //! \copydoc doxygen_hide_alias_self
                        using self = Hyperelasticity<HyperelasticLawT>;

                        //! Alias to unique pointer.
                        using unique_ptr = std::unique_ptr<self>;

                        //! Alias to vector of unique pointers.
                        using vector_unique_ptr = std::vector<unique_ptr>;

                        //! Type of the elementary matrix.
                        using matrix_type = LocalMatrix;

                        //! Type of the elementary vector.
                        using vector_type = LocalVector;

                        //! Alias to undelrying law.
                        using law_type = HyperelasticLawT;

                    public:

                        /// \name Special members.
                        ///@{

                        /*!
                         * \brief Constructor.
                         *
                         * \param[in] mesh_dimension Dimension of the mesh.
                         * \param[in] hyperelasticity_law Hyperelastic law considered, which must be defined elsewhere.
                         */
                        explicit Hyperelasticity(unsigned int mesh_dimension,
                                                 const HyperelasticLawT* const hyperelasticity_law);

                        //! Destructor.
                        ~Hyperelasticity() = default;

                        //! Copy constructor.
                        Hyperelasticity(const Hyperelasticity&) = delete;

                        //! Move constructor.
                        Hyperelasticity(Hyperelasticity&&) = delete;

                        //! Copy affectation.
                        Hyperelasticity& operator=(const Hyperelasticity&) = delete;

                        //! Move affectation.
                        Hyperelasticity& operator=(Hyperelasticity&&) = delete;

                        ///@}


                        //! Alias to invariant manager.
                        using invariant_holder_type = InvariantHolder<HyperelasticLawT::Ninvariants()>;

                        //! Alias to Cauchy Green tensor.
                        using cauchy_green_tensor_type = ParameterAtQuadraturePoint<ParameterNS::Type::vector>;

                        /*!
                         * \class doxygen_hide_second_piola_compute_W_deriv_hyperelasticity
                         *
                         * \brief Compute the hyperelasticity and its derivatives.
                         *
                         * \copydoc doxygen_hide_dW_d2W_derivates_arg
                         * \param[in] quad_pt \a QuadraturePoint considered.
                         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                         * \param[in] cauchy_green_tensor_value Value oif the CauchyGreen tensor at the current
                         * \a QuadraturePoint.
                         */

                        //! \copydoc doxygen_hide_second_piola_compute_W_deriv_hyperelasticity
                        void ComputeWDerivates(const QuadraturePoint& quad_pt,
                                               const GeometricElt& geom_elt,
                                               const LocalVector& cauchy_green_tensor_value,
                                               LocalVector& dW,
                                               LocalMatrix& d2W);

                    private:

                        //! Access to the invariant manager.
                        const invariant_holder_type& GetInvariantHolder() const noexcept;

                        //! Non constant access to the invariant manager.
                        invariant_holder_type& GetNonCstInvariantHolder() noexcept;

                        //! Helper matrix to store outer product.
                        LocalMatrix& GetNonCstWorkMatrixOuterProduct() noexcept;

                        //! Underlying hyperelasticity law.
                        const HyperelasticLawT& GetHyperelasticLaw() const noexcept;


                    private:

                        //! Invariant manager.
                        typename invariant_holder_type::unique_ptr invariant_holder_ = nullptr;

                    private:

                        //! Helper matrix to store outer product.
                        LocalMatrix work_matrix_outer_product_;

                        //! Underlying hyperelasticity law.
                        const HyperelasticLawT* const hyperelastic_law_;

                    };


                } // namespace HyperelasticityPolicyNS


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HPP_
