///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_NONE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_NONE_HPP_


# include <memory>
# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/InputNone.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class TimeManager;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace ActiveStressPolicyNS
            {


                /*!
                 * \brief Policy to use when there are no active stress involved in the
                 * \a SecondPiolaKirchhoffStressTensor operator.
                 */
                class None
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = None;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                    //! Alias to the type of the input.
                    using input_active_stress_policy_type =
                        ::MoReFEM::Advanced::LocalVariationalOperatorNS::ActiveStressPolicyNS::InputNone;

                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor: all arguments are dummy as this policy does zilch.
                    explicit None(const unsigned int mesh_dimension,
                                  const unsigned int Nnode,
                                  const unsigned Nquad_point,
                                  const TimeManager& time_manager,
                                  input_active_stress_policy_type* input_active_stress_policy);

                    //! Destructor.
                    ~None() = default;

                    //! Copy constructor.
                    None(const None&) = delete;

                    //! Move constructor.
                    None(None&&) = delete;

                    //! Copy affectation.
                    None& operator=(const None&) = delete;

                    //! Move affectation.
                    None& operator=(None&&) = delete;

                    ///@}

                };


            } // namespace ActiveStressPolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_ACTIVE_STRESS_POLICY_x_NONE_HPP_
