target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ReactionLaw.hpp"
)

include(${CMAKE_CURRENT_LIST_DIR}/Instantiations/SourceList.cmake)
