///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 12:18:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_FROM_VERTEX_MATCHING_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_FROM_VERTEX_MATCHING_HPP_

# include <memory>
# include <vector>

# include "Core/LinearAlgebra/GlobalMatrix.hpp"

# include "Geometry/Interpolator/VertexMatching.hpp"

# include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp" // \todo #762 Not very clean...


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class NumberingSubset;


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            class DofProgramWiseIndexListPerVertexCoordIndexList;


        } // namespace FEltSpaceNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace NonConformInterpolatorNS
    {

        /*!
         * \brief Enum class to say whether matrix pattern should be stored.
         *
         * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
         */
        enum class store_matrix_pattern { yes, no };


        //! Convenient alias to pairing.
        using pairing_type = Advanced::ConformInterpolatorNS::pairing_type;


        /*!
         * \brief Class that matches the dofs on vertices from two different meshes.
         *
         * \attention This class is a tad unwieldy to use, and currently assumes that:
         *
         * . Finite element spaces/numbering subset couples from both source and target feature the same amounts
         * of unknowns sort in the same way. In other words, (velocity_solid, pressure_solid) for source and
         * (pressure_fluid, velocity_fluid) won't be handled for the time being.
         * . Finite elements on interface must be P1.
         *
         * \internal There is room for improvement here should the construction of this interpolator proves to be a
         * bottleneck (I don't think so, but still possible): the reduction to processor-wise dofs intervenes rather
         * lately in the process and so there are some unused computation.
         */
        class FromVertexMatching
        {

        public:

            //! \copydoc doxygen_hide_alias_self
            using self = FromVertexMatching;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to unique pointer to const object.
            using const_unique_ptr = std::unique_ptr<const self>;


        public:

            /// \name Special members.
            ///@{


            /*!
             * \class doxygen_hide_from_vertex_matching_constructor_arg
             *
             * \param[in] source_index Unique id of the source InitVertexMatchingInterpolator in the input parameter file.
             * \param[in] target_index Unique id of the target InitVertexMatchingInterpolator in the input parameter file.
             * \param[in] do_store_matrix_pattern Whether matrix pattern should be stored.
             * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
             */


            /*! Constructor.
             *
             * \copydoc doxygen_hide_input_parameter_data_arg
             * \copydetails doxygen_hide_from_vertex_matching_constructor_arg
                         */
            template<class InputParameterDataT>
            explicit FromVertexMatching(const InputParameterDataT& input_parameter_data,
                                        unsigned int source_index,
                                        unsigned int target_index,
                                        store_matrix_pattern do_store_matrix_pattern = store_matrix_pattern::no);

            //! Destructor.
            ~FromVertexMatching() = default;

            //! Copy constructor.
            FromVertexMatching(const FromVertexMatching&) = delete;

            //! Move constructor.
            FromVertexMatching(FromVertexMatching&&) = delete;

            //! Copy affectation.
            FromVertexMatching& operator=(const FromVertexMatching&) = delete;

            //! Move affectation.
            FromVertexMatching& operator=(FromVertexMatching&&) = delete;

            ///@}

            //! Access to interpolation matrix.
            const GlobalMatrix& GetInterpolationMatrix() const noexcept;

            /*!
             * \brief Pattern of the matrix.
             *
             * \attention Should be called only if the matrix pattern is stored in the class (see constructor dedicated
             * argument).
             *
             * \return Pattern of the matrix.
             */
            const Wrappers::Petsc::MatrixPattern& GetMatrixPattern() const noexcept;

        private:

            /*!
             * \brief Construct the object. Should not be called outside of constructor.
             *
             * \param[in] vertex_matching Object which holds information about the \a geometric matching between both
             * meshes.
             * \copydetails doxygen_hide_from_vertex_matching_constructor_arg
             */
            void Construct(const MeshNS::InterpolationNS::VertexMatching& vertex_matching,
                           unsigned int source_index,
                           unsigned int target_index,
                           store_matrix_pattern do_store_matrix_pattern);

        private:

            //! Global matrix used for the interpolation.
            GlobalMatrix::unique_ptr interpolation_matrix_ = nullptr;

            /*!
             * \brief Pattern of the matrix.
             *
             * \note It is stored only
             */
            Wrappers::Petsc::MatrixPattern::const_unique_ptr matrix_pattern_ = nullptr;

            # ifndef NDEBUG
            //! Whether matrix pattern is kept within the class or not. If 'no', GetMatrixPattern() shouldn't be called.
            const store_matrix_pattern do_store_matrix_pattern_;
            # endif // NDEBUG

        };


    } // namespace NonConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/NonConformInterpolator/FromVertexMatching/FromVertexMatching.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_FROM_VERTEX_MATCHING_HPP_
