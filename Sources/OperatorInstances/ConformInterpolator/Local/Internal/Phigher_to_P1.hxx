///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 15:21:05 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_PHIGHER_xTO_x_P1_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_PHIGHER_xTO_x_P1_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


            namespace Local
            {


            } // namespace Local


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_INTERNAL_x_PHIGHER_xTO_x_P1_HXX_
