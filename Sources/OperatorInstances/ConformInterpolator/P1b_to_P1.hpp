///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 15:21:05 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1B_xTO_x_P1_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1B_xTO_x_P1_HPP_

# include "OperatorInstances/ConformInterpolator/Local/P1b_to_P1.hpp"
# include "OperatorInstances/ConformInterpolator/Internal/Phigher_to_P1.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        //! Alias that defines P1b -> P1 interpolator.
        using P1b_to_P1 = Internal::ConformInterpolatorNS::Phigher_to_P1<Local::P1b_to_P1>;



    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1B_xTO_x_P1_HPP_
