///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:37:55 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
# define MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_


namespace MoReFEM
{


    template<class InputParameterDataT>
    TimeManager::TimeManager(const InputParameterDataT& input_parameter_data)
    {
        namespace IPL = Utilities::InputParameterListNS;
        using TimeManager = InputParameter::TimeManager;

        time_ = IPL::Extract<TimeManager::TimeInit>::Value(input_parameter_data);
    }


    inline unsigned int TimeManager::NtimeModified() const
    {
        return Ntime_modified_;
    }


    inline double TimeManager::GetTime() const
    {
        return time_;
    }


    inline void TimeManager::SetStaticOrDynamic(StaticOrDynamic value)
    {
        static_or_dynamic_ = value;
    }


    inline StaticOrDynamic TimeManager::GetStaticOrDynamic() const
    {
        return static_or_dynamic_;
    }


    inline double& TimeManager::GetNonCstTime()
    {
        return time_;
    }


    inline void TimeManager::SetTime(double time)
    {
        time_ = time;
    }


    inline void TimeManager::IncrementNtimeModified()
    {
        SetStaticOrDynamic(StaticOrDynamic::dynamic_);
        ++Ntime_modified_;
    }


    inline double TimeManager::GetInverseTimeStep() const noexcept
    {
        const double time_step = GetTimeStep();
        assert(time_step > 0.);
        return 1. / time_step;
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
