///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 5 Jun 2015 17:24:41 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#include "Core/TimeManager/Policy/ConstantTimeStep.hpp"


namespace MoReFEM
{
    
    
    namespace TimeManagerNS
    {
        
        
        namespace Policy
        {

        
        } // namespace Policy
        
        
    } // namespace TimeManagerNS
    

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
