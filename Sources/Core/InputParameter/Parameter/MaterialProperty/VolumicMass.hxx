///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Aug 2015 12:18:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace MaterialProperty
        {


            template<class EnclosingTypeT>
            const std::string& VolumicMass<EnclosingTypeT>::GetName()
            {
                static std::string ret("VolumicMass");
                return ret;
            }


        } // namespace MaterialProperty


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HXX_
