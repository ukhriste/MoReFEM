///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HXX_

namespace MoReFEM
{


    namespace InputParameter
    {


        template<unsigned int IndexT>
        const std::string& Diffusion::Tensor<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("Tensor", IndexT);
            return ret;
        }


        template<unsigned int IndexT>
        const std::string& Diffusion::Tensor<IndexT>::Scalar::Constraint()
        {
            return Utilities::EmptyString();
        }


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HXX_
