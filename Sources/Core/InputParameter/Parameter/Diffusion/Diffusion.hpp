///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:45:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_

#include "Utilities/String/EmptyString.hpp"

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"
# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        //! \copydoc doxygen_hide_core_input_parameter_list_section
        struct Diffusion : public Crtp::Section<Diffusion, NoEnclosingSection>
        {


            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = Diffusion;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
            template<unsigned int IndexT>
            struct Tensor : public Crtp::Section<Tensor<IndexT>, Diffusion>
            {


                //! Convenient alias.
                using self = Tensor<IndexT>;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Diffusion>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the diffusion tensor (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                {
                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();
                };


                /*!
                 * \brief Function that determines diffusion tensor value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };



                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };



                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;

            }; // struct Tensor


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct Density : public Crtp::Section<Density, Diffusion>
            {


                //! Convenient alias.
                using self = Density;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Diffusion>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();



                /*!
                 * \brief Choose how is described the diffusion density (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, Density, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, Density, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines diffusion density value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, Density, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, Density, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, Density, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };



                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;


            }; // struct Density



            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct TransfertCoefficient : public Crtp::Section<TransfertCoefficient, Diffusion>
            {

                //! Convenient alias.
                using self = TransfertCoefficient;

                //! Friendship to section parent.
                using parent = Crtp::Section<self, Diffusion>;


                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                /*!
                 * \brief Return the name of the section in the input parameter.
                 *
                 */
                static const std::string& GetName();


                /*!
                 * \brief Choose how is described the transfert coefficient (through a scalar, a function, etc...)
                 */
                struct Nature : public Crtp::InputParameter<Nature, TransfertCoefficient, Impl::Nature::storage_type>,
                                public Impl::Nature
                { };



                /*!
                 * \brief Scalar value. Irrelevant if nature is not scalar.
                 */
                struct Scalar : public Crtp::InputParameter<Scalar, TransfertCoefficient, Impl::Scalar::storage_type>,
                                public Impl::Scalar
                { };


                /*!
                 * \brief Function that determines transfert coefficient value. Irrelevant if nature is not lua_function.
                 */
                struct LuaFunction : public Crtp::InputParameter<LuaFunction, TransfertCoefficient, Impl::LuaFunction::storage_type>,
                                     public Impl::LuaFunction
                { };


                /*!
                 * \brief Piecewise Constant domain index.
                 */
                struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, TransfertCoefficient, Impl::PiecewiseConstantByDomainId::storage_type>,
                                                     public Impl::PiecewiseConstantByDomainId
                { };


                /*!
                 * \brief Piecewise Constant value by domain.
                 */
                struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, TransfertCoefficient, Impl::PiecewiseConstantByDomainValue::storage_type>,
                                                        public Impl::PiecewiseConstantByDomainValue
                { };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    Nature,
                    Scalar,
                    LuaFunction,
                    PiecewiseConstantByDomainId,
                    PiecewiseConstantByDomainValue
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;


            }; // struct TransfertCoefficient


        }; // struct Diffusion


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputParameter/Parameter/Diffusion/Diffusion.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_
