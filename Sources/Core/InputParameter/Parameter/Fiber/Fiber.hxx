///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 12 Oct 2015 12:00:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_FIBER_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_FIBER_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        template
        <
            unsigned int IndexT,
            ParameterNS::Type TypeT
        >
        constexpr unsigned int Fiber<IndexT, TypeT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


        template
        <
            unsigned int IndexT,
            ParameterNS::Type TypeT
        >
        const std::string& Fiber<IndexT, TypeT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName(std::string("Fiber_") + ParameterNS::Traits<TypeT>::TypeName() + "_",
                                                                  IndexT);
            return ret;
        };



    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_FIBER_HXX_
