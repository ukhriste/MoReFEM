///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        template<unsigned int IndexT>
        const std::string& ScalarTransientSource<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("TransientSource", IndexT);
            return ret;
        };


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_SOURCE_x_SCALAR_TRANSIENT_SOURCE_HXX_
