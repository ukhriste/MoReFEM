target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/SpatialFunction.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/SpatialFunction.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/SpatialFunction.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Heart/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Source/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/AnalyticalPrestress/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Solid/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Diffusion/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Fluid/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/MaterialProperty/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ViscoelasticBoundaryCondition/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ElectricalActivation/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Fiber/SourceList.cmake)
