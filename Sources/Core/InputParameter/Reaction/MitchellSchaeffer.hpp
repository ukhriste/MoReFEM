///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"
# include "Core/InputParameter/Parameter/Impl/ParameterUsualDescription.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace ReactionNS
        {


            //! \copydoc doxygen_hide_core_input_parameter_list_section
            struct MitchellSchaeffer : public Crtp::Section<MitchellSchaeffer, NoEnclosingSection>
            {



                //! Return the name of the section in the input parameter.
                static const std::string& GetName();

                //! Convenient alias.
                using self = MitchellSchaeffer;


                //! Friendship to section parent.
                using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                friend parent;
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN


                //! Coefficient Tau_in in MitchellSchaeffer.
                struct TauIn : public Crtp::InputParameter<TauIn, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient Tau_out in MitchellSchaeffer.
                struct TauOut : public Crtp::InputParameter<TauOut, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient Tau_open in MitchellSchaeffer.
                struct TauOpen : public Crtp::InputParameter<TauOpen, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! \copydoc doxygen_hide_core_input_parameter_list_section
                struct TauClose : public Crtp::Section<TauClose, MitchellSchaeffer>
                {


                    //! Convenient alias.
                    using self = TauClose;

                    //! Friendship to section parent.
                    using parent = Crtp::Section<self, MitchellSchaeffer>;

                    //! \cond IGNORE_BLOCK_IN_DOXYGEN
                    friend parent;
                    //! \endcond IGNORE_BLOCK_IN_DOXYGEN

                    //! Return the name of the section in the input parameter.
                    static const std::string& GetName();



                    /*!
                     * \brief Choose how is described the diffusion tensor (through a scalar, a function, etc...)
                     */
                    struct Nature : public Crtp::InputParameter<Nature, self, Impl::Nature::storage_type>,
                    public Impl::Nature
                    { };



                    /*!
                     * \brief Scalar value. Irrelevant if nature is not scalar.
                     */
                    struct Scalar : public Crtp::InputParameter<Scalar, self, Impl::Scalar::storage_type>,
                    public Impl::Scalar
                    {
                        /*!
                         * \return Constraint to fulfill.
                         *
                         * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                         *
                         * An age should be greater than 0 and less than, say, 150. It is possible
                         * to check it with a logical expression (written in Lua). The expression
                         * should be written with 'v' being the variable to be checked.
                         * \a constraint = "v >= 0 and v < 150"
                         *
                         * It is possible to check whether a variable is in a set of acceptable
                         * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                         * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                         *
                         * If a vector is retrieved, the constraint must be satisfied on every
                         * element of the vector.
                         */
                        static const std::string& Constraint();
                    };


                    /*!
                     * \brief Function that determines diffusion tensor value. Irrelevant if nature is not lua_function.
                     */
                    struct LuaFunction : public Crtp::InputParameter<LuaFunction, self, Impl::LuaFunction::storage_type>,
                    public Impl::LuaFunction
                    { };



                    /*!
                     * \brief Piecewise Constant domain index.
                     */
                    struct PiecewiseConstantByDomainId : public Crtp::InputParameter<PiecewiseConstantByDomainId, self, Impl::PiecewiseConstantByDomainId::storage_type>,
                    public Impl::PiecewiseConstantByDomainId
                    { };


                    /*!
                     * \brief Piecewise Constant value by domain.
                     */
                    struct PiecewiseConstantByDomainValue : public Crtp::InputParameter<PiecewiseConstantByDomainValue, self, Impl::PiecewiseConstantByDomainValue::storage_type>,
                    public Impl::PiecewiseConstantByDomainValue
                    { };



                    //! Alias to the tuple of structs.
                    using section_content_type = std::tuple
                    <
                        Nature,
                        Scalar,
                        LuaFunction,
                        PiecewiseConstantByDomainId,
                        PiecewiseConstantByDomainValue
                    >;


                private:

                    //! Content of the section. // \todo #635 Should be private with accessor.
                    section_content_type section_content_;

                }; // struct TauClose


                //! Coefficient U_gate in MitchellSchaeffer.
                struct PotentialGate : public Crtp::InputParameter<PotentialGate, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient U_min in MitchellSchaeffer.
                struct PotentialMin : public Crtp::InputParameter<PotentialMin, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Coefficient U_max in MitchellSchaeffer.
                struct PotentialMax : public Crtp::InputParameter<PotentialMax, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Reaction Coefficient in FitzHughNagumo.
                struct ReactionCoefficient : public Crtp::InputParameter<ReactionCoefficient, self, double>
                {


                    //! Name of the input parameter in Lua input file.
                    static const std::string& NameInFile();

                    //! Description of the input parameter.
                    static const std::string& Description();

                    /*!
                     * \return Constraint to fulfill.
                     *
                     * Might be left empty; if not the format to respect is the Ops one. Hereafter some text from Ops example file:
                     *
                     * An age should be greater than 0 and less than, say, 150. It is possible
                     * to check it with a logical expression (written in Lua). The expression
                     * should be written with 'v' being the variable to be checked.
                     * \a constraint = "v >= 0 and v < 150"
                     *
                     * It is possible to check whether a variable is in a set of acceptable
                     * value. This is performed with 'ops_in' (a Lua function defined by Ops).
                     * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
                     *
                     * If a vector is retrieved, the constraint must be satisfied on every
                     * element of the vector.
                     */
                    static const std::string& Constraint();

                    /*!
                     * \return Default value.
                     *
                     * This is intended to be used only when the class is used to create a default file; never when no value has been given
                     * in the input parameter file (doing so is too much error prone...)
                     *
                     * This is given as a string; if no default value return an empty string. The value must be Ops-formatted.
                     */
                    static const std::string& DefaultValue();
                };


                //! Alias to the tuple of structs.
                using section_content_type = std::tuple
                <
                    TauIn,
                    TauOut,
                    TauOpen,
                    TauClose,
                    PotentialGate,
                    PotentialMin,
                    PotentialMax
                >;


            private:

                //! Content of the section.
                section_content_type section_content_;




            }; // struct MitchellSchaeffer


        } // namespace ReactionNS


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Reaction/MitchellSchaeffer.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_
