///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 11:47:43 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_F_ELT_SPACE_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_F_ELT_SPACE_HPP_

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/FElt/Impl/FEltSpace.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputParameter::FEltSpace should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            struct FEltSpace
            { };


        } // namespace BaseNS



        //! \copydoc doxygen_hide_core_input_parameter_list_section_with_index
        template<unsigned int IndexT>
        struct FEltSpace
        : public BaseNS::FEltSpace,
        public Crtp::Section<FEltSpace<IndexT>, NoEnclosingSection>
        {

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'FEltSpace' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;


            //! Convenient alias.
            using self = FEltSpace<IndexT>;



            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Indicates the god of dof that possess the finite element space.
             *
             * God of dof index is the same as mesh one.
             */
            struct GodOfDofIndex : public Crtp::InputParameter<GodOfDofIndex, self, unsigned int>,
                                   public Impl::FEltSpaceNS::GodOfDofIndex
            { };


            //! Indicates the domain upon which the finite element space is defined. This domain must be uni-dimensional.
            struct DomainIndex : public Crtp::InputParameter<DomainIndex, self, unsigned int>,
                                 public Impl::FEltSpaceNS::DomainIndex
            { };


            //! Indicates which unknowns are defined on the finite element space.
            struct UnknownList : public Crtp::InputParameter<UnknownList, self, std::vector<std::string>>,
                                 public Impl::FEltSpaceNS::UnknownList
            { };


            //! Indicates for each unknowns the shape function to use.
            struct ShapeFunctionList : public Crtp::InputParameter<ShapeFunctionList, self, std::vector<std::string>>,
                                       public Impl::FEltSpaceNS::ShapeFunctionList
            { };


            //! Indicates the numbering subset to use for each unknown.
            struct NumberingSubsetList: public Crtp::InputParameter<NumberingSubsetList, self, std::vector<unsigned int>>,
                                        public Impl::FEltSpaceNS::NumberingSubsetList
            { };



            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                GodOfDofIndex,
                DomainIndex,
                UnknownList,
                ShapeFunctionList,
                NumberingSubsetList
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;


        }; //struct FEltSpace


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/FElt/FEltSpace.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_F_ELT_x_F_ELT_SPACE_HPP_
