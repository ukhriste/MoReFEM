///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Oct 2016 14:15:02 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_ADVANCED_x_SET_FROM_INPUT_PARAMETER_DATA_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_ADVANCED_x_SET_FROM_INPUT_PARAMETER_DATA_HPP_

# include "Core/InputParameterData/InputParameterList.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        /// \addtogroup CoreGroup
        ///@{


        /*!
         * \brief Create all the instances read from the input parameter data of the type held by \a ManagerT.
         *
         * \code
         * SetFromInputParameterData<DomainManager>(input_parameter_data, domain_manager);
         * \endcode
         *
         * iterates through the entries of \a input_parameter_data and create all the domains encountered doing so.
         *
         * \copydoc doxygen_hide_input_parameter_data_arg
         * \copydoc doxygen_hide_cplusplus_variadic_args
         * These variadic arguments are here transmitted to the Create() static method arguments, not to \a ManagerT
         * constructor.
         * \param[in,out] manager The (singleton) manager which content will be set through this function. The singleton must
         * be created beforehand: this was not the case before but could lead to issues for singletons that takes
         * constructor arguments.
         */
        template
        <
            class ManagerT,
            class InputParameterDataT,
            typename... Args
        >
        void SetFromInputParameterData(const InputParameterDataT& input_parameter_data,
                                       ManagerT& manager,
                                       Args&&... args);


        ///@} // \addtogroup CoreGroup


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameterData/Advanced/SetFromInputParameterData.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_DATA_x_ADVANCED_x_SET_FROM_INPUT_PARAMETER_DATA_HPP_
