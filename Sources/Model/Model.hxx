///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 30 Oct 2013 17:29:19 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ModelGroup
/// \addtogroup ModelGroup
/// \{

#ifndef MOREFEM_x_MODEL_x_MODEL_HXX_
# define MOREFEM_x_MODEL_x_MODEL_HXX_


namespace MoReFEM
{


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    template<class MoReFEMDataT>
    Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::Model(const MoReFEMDataT& morefem_data,
                                                                 create_domain_list_for_coords a_create_domain_list_for_coords)
    : Crtp::CrtpMpi<Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>>(morefem_data.GetMpi()),
    output_directory_(morefem_data.GetResultDirectory())
    {
        if (a_create_domain_list_for_coords == create_domain_list_for_coords::yes)
            Coords::SetCreateDomainListForCoords();

        // current date and time system
        time_t now = std::time(0);
        char* date_time = ctime(&now);

        namespace IPL = Utilities::InputParameterListNS;
        using TimeManager = InputParameter::TimeManager;
        using Result = InputParameter::Result;
        
        decltype(auto) input_parameter_data = morefem_data.GetInputParameterList();

        decltype(auto) time_evolution_policy =
            IPL::Extract<TimeManager::TimeEvolutionPolicy>::Value(input_parameter_data);

        // \todo #574 Add new time evolution policy... To make the code cleaner we would use a factory with policy registering,
        // but as there won't be that much policies it's not that huge a problem to do it manually and edit the if here
        // whenever a new one is added.
        if (time_evolution_policy == "constant_time_step")
            time_manager_ = std::make_unique<TimeManagerInstance<TimeManagerNS::Policy::ConstantTimeStep>>(input_parameter_data);
        else if (time_evolution_policy == "variable_time_step")
            time_manager_ = std::make_unique<TimeManagerInstance<TimeManagerNS::Policy::VariableTimeStep>>(input_parameter_data);
        else
        {
            assert(false && "Unknown time evolution policy!");
        }

        display_value_ = IPL::Extract<Result::DisplayValue>::Value(input_parameter_data);

        const auto& mpi = this->GetMpi();

        if (mpi.IsRootProcessor())
        {
            {
                std::string target(output_directory_);
                target += "/input_parameter_file.lua";

                FilesystemNS::File::Copy(input_parameter_data.GetInputFile(),
                                         target,
                                         FilesystemNS::File::fail_if_already_exist::no,
                                         FilesystemNS::File::autocopy::yes,
                                         __FILE__, __LINE__);
            }

            // Specify in output directory how many processors are involved.

            {
                std::string target(output_directory_);
                target += "/mpi.hhdata";

                std::ofstream out;
                FilesystemNS::File::Create(out, target, __FILE__, __LINE__);

                out << "Nprocessor: " << mpi.template Nprocessor<int>() << std::endl;
            }

            // Specify the name of the model. It might be useful as same Lua file might be used with different models
            // (e.g. different time scheme or hyperelastic law for hyperelastic model).
            {
                std::string target(output_directory_);
                target += "/model_name.data";

                std::ofstream out;
                FilesystemNS::File::Create(out, target, __FILE__, __LINE__);

                out << DerivedT::ClassName() << std::endl;
            }
        }


        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n================================================================\n",
                                                      mpi, __FILE__, __LINE__);

        std::ostringstream oconv;
        oconv << "MoReFEM ";
        oconv << 8 * sizeof(void*);
        oconv << " bits ";
        Wrappers::Petsc::PrintMessageOnFirstProcessor(oconv.str().c_str(),
                                                      mpi, __FILE__, __LINE__);

        if (mpi.template Nprocessor<int>() == 1)
            Wrappers::Petsc::PrintMessageOnFirstProcessor("on 1 processor \n",
                                                          mpi, __FILE__, __LINE__);
        else
            Wrappers::Petsc::PrintMessageOnFirstProcessor("on %d processors \n",
                                                          mpi, __FILE__, __LINE__, mpi.template Nprocessor<int>());

        Wrappers::Petsc::PrintMessageOnFirstProcessor("%s=================================================================\n",
                                                      mpi, __FILE__, __LINE__, date_time);
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::~Model()
    {
        const auto& mpi = this->GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\nIf no exception, all results have been printed in %s.\n",
                                                      mpi, __FILE__, __LINE__, GetOutputDirectory().c_str());

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n==============================================================\n",
                                                      mpi, __FILE__, __LINE__);
        Wrappers::Petsc::PrintMessageOnFirstProcessor("MoReFEM %s ended (if exception thrown it will appear afterwards).\n",
                                                      mpi, __FILE__, __LINE__, DerivedT::ClassName().c_str());

        // current date and time system
        time_t now = time(0);
        char* date_time = ctime(&now);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("%s==============================================================\n",
                                                      mpi, __FILE__, __LINE__, date_time);

        #ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
        if (mpi.template Nprocessor<int>() > 1 && mpi.IsRootProcessor())
            ::MoReFEM::Internal::Wrappers::Petsc::CheckUpdateGhostManager::GetInstance().Print();
        #endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    bool Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::HasFinished()
    {
        auto& time_manager = GetNonCstTimeManager();

        if (time_manager.HasFinished())
            return true;

        if (static_cast<const DerivedT&>(*this).SupplHasFinishedConditions())
            return true;

        return false;
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    template<class MoReFEMDataT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>
    ::Initialize(const MoReFEMDataT& morefem_data,
                 create_output_dir do_create_output_dir)
    {
        const auto& mpi = this->GetMpi();
        
        decltype(auto) input_parameter_data = morefem_data.GetInputParameterList();
        
        decltype(auto) output_directory = GetOutputDirectory();
        const bool is_root_processor = mpi.IsRootProcessor();

        // Init all the relevant objects found in the input parameter file.
        // Ordering is important here: do not modify it lightly!

        {
            auto& manager = Internal::MeshNS::GeometricMeshRegionManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }

        auto mesh_directory_storage = CreateMeshDataDirectory(do_create_output_dir);

        if (is_root_processor)
            Internal::MeshNS::WriteInterfaceListForEachMesh(mesh_directory_storage);

        mpi.Barrier();

        {
            auto& manager = DomainManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }


        {
            auto& manager = Advanced::LightweightDomainListManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }

        if (Coords::GetCreateDomainListForCoords() == create_domain_list_for_coords::yes)
            CreateDomainListForCoords();

        // Advanced::SetFromInputParameterData<Internal::PseudoNormalsManager>(input_parameter_data); #938 not activated for the moment.
        {
            auto& manager = UnknownManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }

        {
            auto& manager = DirichletBoundaryConditionManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }


        // Write the list of unknowns.
        // #289 We assume here there is one model, as the target file gets a hardcoded name...
        if (is_root_processor)
            WriteUnknownList(output_directory);

        {
            auto& manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }

        {
            auto& manager = GodOfDofManager::CreateOrGetInstance();
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager, mpi);
        }


        Internal::ModelNS::InitGodOfDof(input_parameter_data,
                                        DoConsiderProcessorWiseLocal2GlobalT,
                                        std::move(mesh_directory_storage));

        // As FiberListManager gets a constructor with arguments, call it explicitly there.
        decltype(auto) time_manager = GetTimeManager();

        {
            auto& manager = Internal::FiberNS::FiberListManager<ParameterNS::Type::scalar>::CreateOrGetInstance(time_manager);
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }

        {
            auto& manager = Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::CreateOrGetInstance(time_manager);
            Advanced::SetFromInputParameterData<>(input_parameter_data, manager);
        }


        static_cast<DerivedT&>(*this).SupplInitialize(morefem_data);

        if (do_clear_god_of_dof_temporary_data_after_initialize_)
            ClearGodOfDofTemporaryData();

        ClearAllBoundaryConditionInitialValueList();
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::InitializeStep()
    {
        // Update time for current time step.
        UpdateTime();

        // Print time information
        if (do_print_new_time_iteration_banner_)
        {
            if ((this->GetTimeManager().NtimeModified() % GetDisplayValue()) == 0)
            {
                PrintNewTimeIterationBanner();
            }
        }

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplInitializeStep();
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::FinalizeStep()
    {
        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplFinalizeStep();
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::Finalize()
    {
        const auto& mpi = this->GetMpi();

        decltype(auto) time_keep = TimeKeep::GetInstance();

        std::string time_end = time_keep.TimeElapsedSinceBeginning();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n",
                                                      mpi, __FILE__, __LINE__);
        Wrappers::Petsc::PrintMessageOnFirstProcessor("Time of execution : %s.\n",
                                                      mpi, __FILE__, __LINE__,
                                                      time_end.c_str());
        Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n",
                                                      mpi, __FILE__, __LINE__);

        // Destroy manually some singletons that would blow up at the end of the program: due to a dependancy
        // their content must be destroyed before the call to Mpi::Finalize(), which occurs before the natural end of
        // singletons.
        Internal::FiberNS::FiberListManager<ParameterNS::Type::scalar>::GetInstance().Destroy();
        Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance().Destroy();

        time_keep.PrintEndProgram();

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplFinalize();
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    template<class MoReFEMDataT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::Run(const MoReFEMDataT& morefem_data,
                                                                    create_output_dir do_create_output_dir)
    {
        auto& crtp_helper = static_cast<DerivedT&>(*this);

        crtp_helper.Initialize(morefem_data, do_create_output_dir);

        while (!crtp_helper.HasFinished())
        {
            crtp_helper.InitializeStep();
            crtp_helper.Forward();
            crtp_helper.FinalizeStep();
        }

        crtp_helper.Finalize();
    }


    ////////////////////////
    // ACCESSORS          //
    ////////////////////////



    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    inline const GeometricMeshRegion& Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>
    ::GetGeometricMeshRegion(unsigned int mesh_index) const
    {
        return Internal::MeshNS::GeometricMeshRegionManager::GetInstance().GetMesh(mesh_index);
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    inline GeometricMeshRegion& Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>
    ::GetNonCstGeometricMeshRegion(unsigned int mesh_index) const
    {
        return Internal::MeshNS::GeometricMeshRegionManager::GetInstance().GetNonCstMesh(mesh_index);
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    const GodOfDof& Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::GetGodOfDof(unsigned int unique_id) const
    {
        return GodOfDofManager::GetInstance().GetGodOfDof(unique_id);
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    inline TimeManager& Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::GetNonCstTimeManager()
    {
        assert(!(!time_manager_));
        return *time_manager_;
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    inline const TimeManager& Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::GetTimeManager() const
    {
        assert(!(!time_manager_));
        return *time_manager_;
    }



    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    const std::string& Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::GetOutputDirectory() const
    {
        assert(!output_directory_.empty());
        return output_directory_;
    }



    ////////////////////////
    // PRIVATE METHODS    //
    ////////////////////////



    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::UpdateTime()
    {
        time_manager_->IncrementTime();
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::PrintNewTimeIterationBanner() const
    {
        const auto& mpi = this->GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n---------------------------------------------------\n",
                                                      mpi, __FILE__, __LINE__);
        Wrappers::Petsc::PrintMessageOnFirstProcessor("%s Iteration: %d, Time: %f -> %f \n",
                                                      mpi, __FILE__, __LINE__,
                                                      DerivedT::ClassName().c_str(),
                                                      time_manager_->NtimeModified(),
                                                      time_manager_->GetTime() - time_manager_->GetTimeStep(),
                                                      time_manager_->GetTime());
        Wrappers::Petsc::PrintMessageOnFirstProcessor("---------------------------------------------------\n",
                                                      mpi, __FILE__, __LINE__);
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    unsigned int Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::GetDisplayValue() const
    {
        return display_value_;
    }


    template
    <
        class DerivedT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    std::map<unsigned int, std::string> Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>
    ::CreateMeshDataDirectory(create_output_dir do_create_output_dir) const
    {

        const auto& mpi = this->GetMpi();

        std::map<unsigned int, std::string> ret;

        decltype(auto) mesh_manager = Internal::MeshNS::GeometricMeshRegionManager::GetInstance();

        decltype(auto) mesh_id_list = mesh_manager.GetUniqueIdList();
        decltype(auto) output_dir = GetOutputDirectory();

        for (const auto mesh_id : mesh_id_list)
            ret.insert({ mesh_id, output_dir + "/Mesh_" + std::to_string(mesh_id)});

        if (do_create_output_dir == create_output_dir::yes && mpi.IsRootProcessor())
        {
            for (const auto& pair : ret)
            {
                const auto& mesh_directory = pair.second;

                if (FilesystemNS::Folder::DoExist(mesh_directory))
                    FilesystemNS::Folder::Remove(mesh_directory, __FILE__, __LINE__);

                FilesystemNS::Folder::Create(mesh_directory, __FILE__, __LINE__);
            }
        }

        mpi.Barrier();

        return ret;
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::SetClearGodOfDofTemporaryDataToFalse()
    {
        do_clear_god_of_dof_temporary_data_after_initialize_ = false;
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    inline void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::SetDoPrintNewTimeIterationBanner(bool do_print) noexcept
    {
        do_print_new_time_iteration_banner_ = do_print;
    }


    template<class DerivedT, DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT>
    inline void Model<DerivedT, DoConsiderProcessorWiseLocal2GlobalT>::CreateDomainListForCoords()
    {
        {
            auto& domain_manager = DomainManager::CreateOrGetInstance();

            const auto& domain_list = domain_manager.GetDomainList();

            for (const auto& domain_and_id : domain_list)
            {
                const auto& domain_ptr = domain_and_id.second;

                assert(!(!domain_ptr));

                const auto& domain = *domain_ptr;

                const auto domain_unique_id = domain_and_id.first;

                const auto& mesh = domain.GetGeometricMeshRegion();

                const auto& geometric_elt_list = mesh.GetGeometricEltList();

                for (const auto& geometric_elt_ptr : geometric_elt_list)
                {
                    assert(!(!geometric_elt_ptr));

                    const auto& geom_elt = *geometric_elt_ptr;

                    if (domain.IsGeometricEltInside(geom_elt))
                    {
                        auto& coords_list_elem = geom_elt.GetCoordsList();

                        for (auto& coords_ptr : coords_list_elem)
                        {
                            assert(!(!coords_ptr));

                            auto& coords = *coords_ptr;

                            coords.AddDomainContainingCoords(domain_unique_id);
                        }
                    }
                }
            }
        }
    }


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


#endif // MOREFEM_x_MODEL_x_MODEL_HXX_
