///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 13:35:14 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_DOF_SOURCE_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_DOF_SOURCE_HPP_

# include <memory>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GlobalVector;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace VariationalFormulationNS
    {


        namespace DofSourcePolicyNS
        {


            /*!
             * \brief Policy to use when there is a source expressed at the dofs.
             *
             * It is the case for instance in FSI model, where the source is actually the residual obtained from
             * a previous variational formulation.
             */
            class DofSource
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = DofSource;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] dof_source Vector containing the source values at the dofs. It is expected that this
                 * vector keeps existing (it should therefore be a data attribute of the calling Model) and that its
                 * dimension fits the expected one (namely the same as the rhs and solution of the variational
                 * formulation that uses up this policy).
                 * \param[in] factor Factor by which the stored vector is multiplied when it is added to the rhs.
                 */
                explicit DofSource(double factor, const GlobalVector& dof_source);

            protected:

                //! Destructor.
                ~DofSource() = default;

                //! Copy constructor.
                DofSource(const DofSource&) = delete;

                //! Move constructor.
                DofSource(DofSource&&) = delete;

                //! Copy affectation.
                DofSource& operator=(const DofSource&) = delete;

                //! Move affectation.
                DofSource& operator=(DofSource&&) = delete;

                ///@}

                //! Add the contribution of dof_source_ to the rhs.
                void AddToRhs(GlobalVector& rhs);

            private:


                /*!
                 * \brief Source expressed at the dofs.
                 *
                 */
                const GlobalVector& dof_source_;

                //! Factor by which the stored vector is multiplied when it is added to the rhs.
                const double factor_;


            };


        } // namespace DofSourcePolicyNS


    } // namespace VariationalFormulationNS


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/DofSourcePolicy/DofSource.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_DOF_SOURCE_POLICY_x_DOF_SOURCE_HPP_
