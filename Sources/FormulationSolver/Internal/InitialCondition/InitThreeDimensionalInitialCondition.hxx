///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            template<class InitialConditionT, class InputParameterDataT>
            InitialCondition<ParameterNS::Type::vector>::unique_ptr
            InitThreeDimensionalInitialCondition(const GeometricMeshRegion& geometric_mesh_region,
                                 const InputParameterDataT& input_parameter_data)
            {
                namespace IPL = Utilities::InputParameterListNS;

                using nature_type = typename InitialConditionT::Nature;

                decltype(auto) nature_list =
                IPL::Extract<nature_type>::Value(input_parameter_data);

                if (nature_list.size() != 3)
                    throw Exception("Exactly three items were expected for " + nature_type::enclosing_section::GetFullName() + "::"
                                    + nature_type::NameInFile(),
                                    __FILE__, __LINE__);

                {
                    auto it = std::find(nature_list.cbegin(), nature_list.cend(), "ignore");

                    if (it != nature_list.cend())
                    {
                        if (std::count(nature_list.cbegin(), nature_list.cend(), "ignore") != 3)
                            throw Exception("Error for " + nature_type::enclosing_section::GetFullName() + "::" + nature_type::NameInFile() + ": if "
                                            "one of the item is 'ignore' the other ones should be 'ignore' as well.",
                                            __FILE__, __LINE__);

                        return nullptr;
                    }
                }

                using scalar_type = typename InitialConditionT::Scalar;

                decltype(auto) component_value_list =
                IPL::Extract<scalar_type>::Value(input_parameter_data);

                if (component_value_list.size() != 3)
                    throw Exception("Exactly three items were expected for " + scalar_type::enclosing_section::GetFullName() + "::"
                                    + scalar_type::NameInFile(),
                                    __FILE__, __LINE__);

                auto&& component_x = InitScalarInitialCondition<typename InitialConditionT::LuaFunctionX>(geometric_mesh_region,
                                                                                                          input_parameter_data,
                                                                                                          nature_list[0],
                                                                                                          component_value_list[0]);

                auto&& component_y = InitScalarInitialCondition<typename InitialConditionT::LuaFunctionY>(geometric_mesh_region,
                                                                                                          input_parameter_data,
                                                                                                          nature_list[1],
                                                                                                          component_value_list[1]);

                auto&& component_z = InitScalarInitialCondition<typename InitialConditionT::LuaFunctionZ>(geometric_mesh_region,
                                                                                                          input_parameter_data,
                                                                                                          nature_list[2],
                                                                                                          component_value_list[2]);

                return std::make_unique<ThreeDimensionalInitialCondition>(std::move(component_x),
                                                                          std::move(component_y),
                                                                          std::move(component_z));


            }


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_THREE_DIMENSIONAL_INITIAL_CONDITION_HXX_
