///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_OPS_FUNCTION_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_OPS_FUNCTION_HPP_

# include <memory>
# include <vector>


# include "Utilities/InputParameterList/OpsFunction.hpp"
# include "Utilities/InputParameterList/OpsFunction.hpp"

# include "Core/InputParameter/Parameter/SpatialFunction.hpp"

# include "Geometry/Coords/Coords.hpp"

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            namespace Policy
            {


                /*!
                 * \brief Parameter policy when the parameter gets a value given by an analytic function provided in the
                 * input parameter file.
                 *
                 * \tparam TypeT  Type of the parameter (real, vector, matrix).
                 */
                template<ParameterNS::Type TypeT, class SpatialFunction>
                class OpsFunction
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = OpsFunction<TypeT, SpatialFunction>;

                private:

                    //! Alias to traits class related to TypeT.
                    using traits = ::MoReFEM::ParameterNS::Traits<TypeT>;

                public:

                    //! Alias to return type.
                    using return_type = typename traits::return_type;

                    //! Alias to the storage of the Ops function.
                    using storage_type = const typename SpatialFunction::return_type&;


                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    explicit OpsFunction(const GeometricMeshRegion& geometric_mesh_region,
                                         storage_type lua_function);

                    //! Destructor.
                    ~OpsFunction() = default;

                    //! Copy constructor.
                    OpsFunction(const OpsFunction&) = delete;

                    //! Move constructor.
                    OpsFunction(OpsFunction&&) = delete;

                    //! Copy affectation.
                    OpsFunction& operator=(const OpsFunction&) = delete;

                    //! Move affectation.
                    OpsFunction& operator=(OpsFunction&&) = delete;

                    ///@}

                protected:

                    /*!
                     * \brief Provided here to make the code compile, but should never be called
                     *
                     * (Constant value should not be given through a function: it is less efficient and the constness
                     * is not appearant in the code...).
                     *
                     * \return Spatially constant value.
                     */
                    [[noreturn]] return_type GetConstantValueFromPolicy() const;

                    //! Return the value for \a coords.
                    return_type GetValueFromPolicy(const SpatialPoint& coords) const;


                protected:

                    /*!
                     * \brief Whether the parameter varies spatially or not.
                     *
                     * \return False: for OpsFunction it is always assumed to be false. If it is indeed constant,
                     * rather use the dedicated \a Constant policy.
                     */
                    bool IsConstant() const noexcept;


                private:

                    //! Store a reference to the ops function (which is owned by the InputParameterList class).
                    storage_type lua_function_;


                };


            } // namespace Policy


        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/InitialCondition/Policy/OpsFunction.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_OPS_FUNCTION_HPP_
