///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_ENSIGHT_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_ENSIGHT_HPP_

# include <memory>
# include <vector>

# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                /*!
                 * \brief Specialization of Informations struct that provides generic informations about Ensight format.
                 */
                template<>
                struct Informations<::MoReFEM::MeshNS::Format::Ensight>
                {

                    //! Name of the format.
                    static const std::string& Name();

                    //! Extension of the Ensight files.
                    static const std::string& Extension();

                };



                namespace Ensight
                {


                    /*!
                     * \brief Read an Ensight 6 geometric file.
                     *
                     * It reads Ensight 6 format file; there are absolutely no guarantee it is able to read any valid
                     * Ensight file (on the contrary, I'm positive it does not). However it is able to read the Ensight
                     * files likely to be given as input in MoReFEM.
                     *
                     * An Ensight case file is also required to read it with Ensight client.
                     *
                     * \copydoc doxygen_hide_geometric_mesh_region_constructor_3_bis
                     * \copydoc doxygen_hide_geometric_mesh_region_constructor_4
                     * \copydoc doxygen_hide_space_unit_arg
                     * \param[out] dimension Dimension of the mesh, as read in the input file.
                     * \param[in] mesh_id Unique identifier of the \a GeometricMeshRegion for the construction of which
                     * present function is called.
                     *
                     */
                    void ReadFile(unsigned int mesh_id,
                                  const std::string& mesh_file,
                                  double space_unit,
                                  unsigned int& dimension,
                                  GeometricElt::vector_shared_ptr& unsort_element_list,
                                  Coords::vector_unique_ptr& coords_list,
                                  MeshLabel::vector_const_shared_ptr& mesh_label_list);


                    /*!
                     * \brief Write a mesh in Ensight format.
                     *
                     * \copydoc doxygen_hide_geometry_format_write_common_arg
                     */
                    void WriteFile(const GeometricMeshRegion& mesh,
                                   const std::string& mesh_file);




                } // namespace Ensight


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/Format/Ensight.hxx"
# include "Geometry/Mesh/Internal/Format/Dispatch/Ensight.hpp"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_ENSIGHT_HPP_
