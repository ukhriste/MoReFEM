///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include <sstream>
#include <cassert>

#include "Geometry/Mesh/Internal/Format/Exceptions/Medit.hpp"


namespace // anonymous
{
    
    
    // Only declarations are provided here; definitions are at the end of this file
    
    std::string fileInformation(const std::string& medit_filename);
    std::string UnableToOpenMsg(const std::string& medit_filename, int mesh_version, const std::string& action);
    std::string InvalidExtensionMsg(const std::string& medit_filename, const std::string& action);
    std::string InvalidDimensionMsg(const std::string& medit_filename, int dimension);
    std::string InvalidCoordIndexMsg(const std::string& medit_filename, unsigned int index, unsigned int Ncoord);
    std::string InvalidPathMsg(const std::string& medit_filename, const std::string& action);
    
    
} // namespace anonymous



namespace MoReFEM
{
    
    
    namespace ExceptionNS
    {
        
        
        namespace Format
        {
            
            
            namespace Medit
            {
                
                
                UnableToOpen::~UnableToOpen() = default;
                
                
                UnableToOpen::UnableToOpen(const std::string& medit_filename, int mesh_version,
                                           const std::string& action,
                                           const char* invoking_file, int invoking_line)
                : Exception(UnableToOpenMsg(medit_filename, mesh_version, action), invoking_file, invoking_line)
                { }
                
                
                InvalidExtension::~InvalidExtension() = default;
                
                
                InvalidExtension::InvalidExtension(const std::string& medit_filename, const std::string& action,
                                                const char* invoking_file, int invoking_line)
                : Exception(InvalidExtensionMsg(medit_filename, action), invoking_file, invoking_line)
                { }
                
                
                InvalidPath::~InvalidPath() = default;
                
                
                InvalidPath::InvalidPath(const std::string& medit_filename, const std::string& action,
                                         const char* invoking_file, int invoking_line)
                : Exception(InvalidPathMsg(medit_filename, action), invoking_file, invoking_line)
                { }
                
                
                
                InvalidDimension::~InvalidDimension() = default;
                
                
                InvalidDimension::InvalidDimension(const std::string& medit_filename, int dimension,
                                                   const char* invoking_file, int invoking_line)
                : Exception(InvalidDimensionMsg(medit_filename, dimension), invoking_file, invoking_line)
                { }
                
                
                InvalidCoordIndex::~InvalidCoordIndex() = default;
                
                
                InvalidCoordIndex::InvalidCoordIndex(const std::string& medit_filename,
                                                     unsigned int index, unsigned int Ncoord,
                                                     const char* invoking_file, int invoking_line)
                : Exception(InvalidCoordIndexMsg(medit_filename, index, Ncoord), invoking_file, invoking_line)
                { }
                
                
                
                
                
            } // namespace Medit
            
            
        } // namespace Format
        
        
    } // namespace ExceptionNS
    
    
} // namespace MoReFEM



// Definitions are provided here; declarations were provided at the beginning of this file
namespace // anonymous
{
    
    
    
    std::string fileInformation(const std::string& medit_filename)
    {
        std::ostringstream oconv;
        oconv << "Error in Medit file ";
        oconv << medit_filename << ": ";
        return oconv.str();
    }
    
    
    
    /*!
     * \brief This function writes on \a stream a warning when mesh version is 3 and architecture is not 64 bits
     *
     * \tparam SizeOfPointerT Must be sizeof(void*)
     */
    template<int SizeOfPointerT>
    inline void WarnInvalidArchitectureHelper(std::ostream& stream)
    {
        assert(SizeOfPointerT == sizeof(void*));
        stream << "Mesh version 3 expects 64 bits architecture, whereas size of a pointer on your system is "
        << SizeOfPointerT << ", which matches a " << SizeOfPointerT * 8 << " bits architecture.";
        
        if (SizeOfPointerT > 8)
            stream << "\nPlease provide a specialization for your type of architecture for "
            "warnInvalidArchitectureHelper() template function";
    }
    
    
    template<>
    inline void WarnInvalidArchitectureHelper<8>(std::ostream& stream)
    {
        assert(8 == sizeof(void*));
        (void) stream;
        // Do nothing
    }
    
    
    void WarnInvalidArchitecture(std::ostream& stream)
    {
        WarnInvalidArchitectureHelper<sizeof(void*)>(stream);
    }
    
    
    
    
    
    std::string UnableToOpenMsg(const std::string& medit_filename, int mesh_version, const std::string& action)
    {
        std::ostringstream oconv;
        oconv << fileInformation(medit_filename);
        oconv << "Unable to " << action << " the file. Medit API is unfortunately not very specific, however "
        "invalid file extension and invalid path should have already been ruled out.";
        
        if (mesh_version == 3)
            WarnInvalidArchitecture(oconv);
        
        return oconv.str();
    }
    
    
    std::string InvalidExtensionMsg(const std::string& medit_filename, const std::string& action)
    {
        std::ostringstream oconv;
        oconv << fileInformation(medit_filename);
        oconv << "Unable to " << action
        << " the file: file extension is not valid (mesh or meshb extension are expected).";
        
        return oconv.str();
    }
    
    
    std::string InvalidPathMsg(const std::string& medit_filename, const std::string& action)
    {
        std::ostringstream oconv;
        oconv << fileInformation(medit_filename);
        oconv << "Unable to " << action << " the file: path is invalid.";
        
        return oconv.str();
    }
    
    
    
    std::string InvalidDimensionMsg(const std::string& medit_filename, int dimension)
    {
        std::ostringstream oconv;
        oconv << fileInformation(medit_filename);
        oconv << "Dimension read is incorrect: 2D or 3D mesh was expected and dimension read is " << dimension;
        
        return oconv.str();
    }
    
    
    std::string InvalidCoordIndexMsg(const std::string& medit_filename, unsigned int index, unsigned int Ncoord)
    {
        std::ostringstream oconv;
        oconv << fileInformation(medit_filename);
        oconv << "One of the indexes read for a Coords is " << index << " whereas the expected values are "
        "between 1 and the total number of coords in the mesh (" << Ncoord << " here).";
        
        return oconv.str();
    }
    
    
    
    
} // namespace anonymous


/// @} // addtogroup GeometryGroup
