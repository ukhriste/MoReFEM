///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HPP_

# include <memory>
# include <vector>

# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                /*!
                 * \brief Specialization of Informations struct that provides generic informations about VTK_PolygonalData format.
                 */
                template<>
                struct Informations<::MoReFEM::MeshNS::Format::VTK_PolygonalData>
                {

                    //! Name of the format.
                    static const std::string& Name();

                    //! Extension of the VTK_PolygonalData files.
                    static const std::string& Extension();

                };



                namespace VTK_PolygonalData
                {


                    /*!
                     * \brief Read a VTK_PolygonalData geometric file.
                     *
                     * It reads VTK_PolygonalData format file; there are absolutely no guarantee it is able to read any valid
                     * VTK_PolygonalData file (on the contrary, I'm positive it does not). However it is able to read the VTK_PolygonalData
                     * files likely to be given as input in MoReFEM.
                     *
                     * An VTK_PolygonalData case file is also required to read it with VTK_PolygonalData client.
                     *
                     * \copydoc doxygen_hide_geometric_mesh_region_constructor_3_bis
                     * \copydoc doxygen_hide_geometric_mesh_region_constructor_4
                     * \copydoc doxygen_hide_space_unit_arg
                     * \param[out] dimension Dimension of the mesh, as read in the input file.
                     * \param[in] mesh_id Unique identifier of the \a GeometricMeshRegion for the construction of which
                     * present function is called.
                     *
                     */
                    [[noreturn]] void ReadFile(unsigned int mesh_id,
                                                 const std::string& mesh_file,
                                                 double space_unit,
                                                 unsigned int& dimension,
                                                 GeometricElt::vector_shared_ptr& unsort_element_list,
                                                 Coords::vector_unique_ptr& coords_list,
                                                 MeshLabel::vector_const_shared_ptr& mesh_label_list);


                    /*!
                     * \brief Write a mesh in VTK_PolygonalData format.
                     *
                     * \copydoc doxygen_hide_geometry_format_write_common_arg
                     */
                    void WriteFile(const GeometricMeshRegion& mesh,
                                   const std::string& mesh_file);




                } // namespace VTK_PolygonalData


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hxx"
//# include "Geometry/Mesh/Internal/Format/Dispatch/VTK_PolygonalData.hpp"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HPP_
