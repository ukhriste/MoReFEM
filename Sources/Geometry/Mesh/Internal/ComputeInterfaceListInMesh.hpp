///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 14:59:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HPP_

# include <vector>

# include "Geometry/Interfaces/Instances/Vertex.hpp"
# include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
# include "Geometry/Interfaces/Instances/OrientedFace.hpp"
# include "Geometry/Interfaces/Instances/Volume.hpp"


namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricMeshRegion;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace MeshNS
        {


            /*!
             * \brief Compute the list of all interfaces to consider at the level of a \a GeometricMeshRegion.
             *
             * Natively there are no such list, as GeometricElt are in charge of storing pointers to their references,
             * but it's not a big deal to compute it.
             *
             * \attention The list of interfaces hence computed does not split interfaces between processor-wise and
             * ghosted; so there is no guarantee there are actually a NodeBearer associated to the interface.
             */
            class ComputeInterfaceListInMesh final
            {
            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit ComputeInterfaceListInMesh(const GeometricMeshRegion& geometric_mesh_region);

                //! Destructor.
                ~ComputeInterfaceListInMesh() = default;

                //! Forbid copy.
                ComputeInterfaceListInMesh(const ComputeInterfaceListInMesh&) = delete;

                //! Forbid move.
                ComputeInterfaceListInMesh(ComputeInterfaceListInMesh&&) = delete;

                //! Forbid affectation.
                ComputeInterfaceListInMesh& operator=(const ComputeInterfaceListInMesh&) = delete;

                //! Forbid affectation.
                ComputeInterfaceListInMesh& operator=(ComputeInterfaceListInMesh&&) = delete;

                ///@}

            public:

                //! Print the list of interfaces in an output file (the whole purpose when this class was designed...).
                void Print(std::ostream& out) const;

            public:

                //! List of all Vertices at mesh level.
                const Vertex::vector_shared_ptr& GetVertexList() const;

                //! List of all Edges at mesh level.
                const OrientedEdge::vector_shared_ptr& GetEdgeList() const;

                //! List of all Faces at mesh level.
                const OrientedFace::vector_shared_ptr& GetFaceList() const;

                //! List of all Volumes at mesh level.
                const Volume::vector_shared_ptr& GetVolumeList() const;


            private:

                //! List of all Vertices at mesh level.
                Vertex::vector_shared_ptr vertex_list_;

                //! List of all Edges at mesh level.
                OrientedEdge::vector_shared_ptr edge_list_;

                //! List of all Faces at mesh level.
                OrientedFace::vector_shared_ptr face_list_;

                //! List of all Volumes at mesh level.
                Volume::vector_shared_ptr volume_list_;
            };


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COMPUTE_INTERFACE_LIST_IN_MESH_HPP_
