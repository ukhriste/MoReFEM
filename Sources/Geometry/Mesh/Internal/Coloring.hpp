///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HPP_

# include <memory>
# include <vector>
# include <unordered_map>

# include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ColoringNS
        {

            //! Convenient alias.
            using connecticity_helper_type = std::unordered_map
            <
                GeometricElt::shared_ptr,
                GeometricElt::vector_shared_ptr,
                std::hash<GeometricElt::shared_ptr>,
                Utilities::PointerComparison::Equal<GeometricElt::shared_ptr>
            >;


            /*!
             * \brief Compute the connectivity of the geometric elements within the \a geometric_mesh_region.
             *
             * \param[in] geometric_mesh_region The geometric mesh region for which the connectivity is computed.
             * \param[in] dimension Consider only the geometric elements of this dimension.
             *
             * \return Key is each geometric element, value the list of contiguous geometric elements. Self connexion
             * is rejected here.
             */
            connecticity_helper_type
            ComputeConnectivity(const GeometricMeshRegion& geometric_mesh_region,
                                unsigned int dimension);


            //! A function to print the results of the connectivity, very useful in debug mode.
            void PrintConnectivity(const connecticity_helper_type& connectivity);


            /*!
             * \brief Compute for each vertex the list of \a GeometricElt of a given dimension to which it belongs to.
             *
             * \param[in] geometric_mesh_region \a GeometricMeshRegion in which the computation is performed.
             * \param[in] dimension Only geometric elements of this dimension are considered.
             *
             * \return Key is the \a Vertex, value the list of \a GeometricElt to which the \a Vertex belongs to.
             */
            std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>
            ComputeGeometricElementForEachVertex(const GeometricMeshRegion& geometric_mesh_region,
                                                 const unsigned int dimension);



        } // namespace ColoringNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/Coloring.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HPP_
