///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_MESH_REGION_MANAGER_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_MESH_REGION_MANAGER_HPP_

# include <set>

# include "Utilities/Singleton/Singleton.hpp"

# include "Core/InputParameter/Geometry/Mesh.hpp"

# include "Geometry/Mesh/GeometricMeshRegion.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            /*!
             * \brief This class is used to create and retrieve GeometricMeshRegion objects.
             *
             * GeometricMeshRegion objects get private constructor and can only be created through this class. In addition
             * to their creation, this class keeps their address, so it's possible from instance to retrieve a
             * GeometricMeshRegion object given its unique id (which is the one that appears in the input parameter file).
             *
             */
            class GeometricMeshRegionManager
            : public Utilities::Singleton<GeometricMeshRegionManager>
            {

            public:

                //! Alias to storage.
                using storage_type = std::unordered_map<unsigned int, GeometricMeshRegion::const_unique_ptr>;


                //! Name of the class (required for some Singleton-related errors).
                static const std::string& ClassName();

                //! Base type of Mesh as input parameter (requested to identify domains in the input parameter data).
                using input_parameter_type = InputParameter::BaseNS::Mesh;

                /*!
                 * \brief Create a new GeometricMeshRegion object from the data of the input parameter file.
                 *
                 */
                template<class MeshSectionT>
                void Create(const MeshSectionT& section);

                //! Fetch the geometric mesh region object associated with \a unique_id unique identifier.
                const GeometricMeshRegion& GetMesh(unsigned int unique_id) const;

                //! Fetch the geometric mesh region object associated with \a unique_id unique identifier.
                GeometricMeshRegion& GetNonCstMesh(unsigned int unique_id);

                //! Fetch the geometric mesh region object associated with \a UniqueIdT unique identifier.
                template<unsigned int UniqueIdT>
                const GeometricMeshRegion& GetMesh() const;

                /*!
                 * \brief Create a brand new GeometricMeshRegion and returns a reference to it.
                 *
                 * \copydoc doxygen_hide_geometric_mesh_region_constructor_5
                 * \copydoc doxygen_hide_geometric_mesh_region_constructor_1
                 * \copydoc doxygen_hide_geometric_mesh_region_constructor_2
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test executables; however in full-fledged
                 * model instances you should not use this constructor at all: the other one above calls it with the correct
                 * data from the input parameter file.
                 */
                void Create(const unsigned int unique_id,
                            const std::string& mesh_file,
                            unsigned dimension,
                            ::MoReFEM::MeshNS::Format format,
                            const double space_unit,
                            GeometricMeshRegion::BuildEdge do_build_edge = GeometricMeshRegion::BuildEdge::no,
                            GeometricMeshRegion::BuildFace do_build_face = GeometricMeshRegion::BuildFace::no,
                            GeometricMeshRegion::BuildVolume do_build_volume = GeometricMeshRegion::BuildVolume::no,
                            GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals = GeometricMeshRegion::BuildPseudoNormals::no);


                /*!
                 * \brief Create a brand new GeometricMeshRegion and returns a reference to it.
                 *
                 * \copydoc doxygen_hide_space_unit_arg
                 * \param[in] dimension Dimension of the mesh.
                 * \copydoc doxygen_hide_geometric_mesh_region_constructor_2
                 * \copydoc doxygen_hide_geometric_mesh_region_constructor_3
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test executables; however in full-fledged
                 * model instances you should not use this constructor at all: the other one above calls it with the correct
                 * data from the input parameter file.
                 */
                void Create(unsigned int dimension,
                            double space_unit,
                            GeometricElt::vector_shared_ptr&& unsort_element_list,
                            Coords::vector_unique_ptr&& coords_list,
                            MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                            GeometricMeshRegion::BuildEdge do_build_edge,
                            GeometricMeshRegion::BuildFace do_build_face,
                            GeometricMeshRegion::BuildVolume do_build_volume,
                            GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals = GeometricMeshRegion::BuildPseudoNormals::no);


                /*!
                 * \brief Constant accessor to the store the geometric mesh region objects.
                 *
                 * Key is the unique id of each mesh.
                 * Value is the actual mesh.
                 *
                 * \return All meshes in the form of pairs (unique_id, pointer to mesh object).
                 */
                const storage_type& GetStorage() const noexcept;

                /*!
                 * \brief Yields a unique id currently not used.
                 *
                 * \return Unique id not currently used.
                 */
                unsigned int GenerateUniqueId();

                //! Constant accessor to the list of unique ids already in use.
                const std::set<unsigned int>& GetUniqueIdList() const noexcept;


            private:

                /*!
                 * \brief Non constant accessor to the store the geometric mesh region objects.
                 *
                 * Key is the unique id of each mesh.
                 * Value is the actual mesh.
                 *
                 * \return All meshes in the form of pairs (unique_id, pointer to mesh object).
                 */
                storage_type& GetNonCstStorage() noexcept;


                //! Non constant accessor to the list of unique ids already in use.
                std::set<unsigned int>& GetNonCstUniqueIdList() noexcept;


                //! Enum class to determine whether the unique id is already known in \a unique_id_list_ or not.
                enum class is_unique_id_known { no, yes };


                /*!
                 * \brief Helper method to Create() in charge of adding new mesh to storage.
                 *
                 * \param[in] mesh Pointer to the mesh to be introduced. This pointer will be mutated into
                 * a unique_ptr in the storage and must not therefore be deleted.
                 */
                template<is_unique_id_known IsUniqueIdKnownT>
                void InsertMesh(const GeometricMeshRegion* const mesh);


            private:


                //! \name Singleton requirements.
                ///@{
                //! Constructor.
                GeometricMeshRegionManager();

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<GeometricMeshRegionManager>;
                ///@}


            private:

                /*!
                 * \brief Store the geometric mesh region objects.
                 *
                 * Key is the unique id of each mesh.
                 * Value is the actual mesh.
                 */
                storage_type storage_;

                /*!
                 * \brief List of unique ids already in use.
                 *
                 * In most cases it's redundant with keys of \a storage_; however in some functions in which Create()
                 * method is to be called there might be temporarily several more items here.
                 */
                std::set<unsigned int> unique_id_list_;


            };


            /*!
             * \brief Write the interface list for each mesh.
             *
             * Should be called only on root processor.
             * \param[in] mesh_output_directory_storage Key is the unique id of the meshes, value the path to the
             * associated output directory, which should already exist.
             */
            void WriteInterfaceListForEachMesh(const std::map<unsigned int, std::string>& mesh_output_directory_storage);


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_GEOMETRIC_MESH_REGION_MANAGER_HPP_
