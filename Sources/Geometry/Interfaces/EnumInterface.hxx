///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Oct 2014 15:22:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HXX_


namespace MoReFEM
{


    namespace InterfaceNS
    {


        template<class TopologyT, Nature NatureT>
        constexpr unsigned int Ninterface<TopologyT, NatureT>::Value()
        {
            return NatureT == Nature::vertex ? TopologyT::Nvertex
            : (NatureT == Nature::edge ? TopologyT::Nedge
            : (NatureT == Nature::face ? TopologyT::Nface
               : TopologyT::Nvolume));

            // Note: I personnally like better the more verbosy form below, but gcc can't cope with it (whereas
            // to my knowledge it's perfectly acceptable C++ 14).
//            switch (NatureT)
//            {
//                case Nature::vertex:
//                    return TopologyT::Nvertex;
//                case Nature::edge:
//                    return TopologyT::Nedge;
//                case Nature::face:
//                    return TopologyT::Nface;
//                case Nature::volume:
//                    return TopologyT::Nvolume;
//            }
//
//            assert(false);
//            return NumericNS::UninitializedIndex<unsigned int>();
        }


    } // namespace InterfaceNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HXX_
