target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumInterface.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Interface.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumInterface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/EnumInterface.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Interface.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Interface.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/LocalInterface/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instances/SourceList.cmake)
