///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 09:23:48 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HXX_


namespace MoReFEM
{



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VERTEX_HXX_
