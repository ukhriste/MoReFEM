///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Sep 2015 10:47:20 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_LOCAL_INTERFACE_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_LOCAL_INTERFACE_HPP_

# include <memory>
# include <vector>
# include <cassert>

# include "Geometry/Interfaces/EnumInterface.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            //! Whether the interface is an interior interface or not.
            enum IsInterior { no, yes };


            /*!
             * \brief Geometric interface considered at the local level ('local' in the finite element meaning).
             */
            class LocalInterface
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = LocalInterface;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] vertex_index_list List of vertices that delimits the interface, given by their index.
                 * The index here are the ones used in the description of the topology; so for instance for a triangle
                 * it is among {0, 1, 2} where the position of each of those is given in topologic description.
                 * \param[in] nature Nature of the interface (vertex, edge, face or volume).
                 * \param[in] is_interior Whether the interface is an interior interface or not.
                 */
                template<class VertexIndexListTypeT>
                explicit LocalInterface(const VertexIndexListTypeT& vertex_index_list,
                                        InterfaceNS::Nature nature,
                                        IsInterior is_interior);


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // ============================

                /*!
                 * \brief 'Overload' of the constructor when VertexIndexListTypeT is std::false_type, e.g. volume for a
                 * segment.
                 *
                 * This constructor should never be called but is required to make the code compile.
                 *
                 */
                [[noreturn]] LocalInterface(std::false_type, InterfaceNS::Nature nature, IsInterior is_interior);

                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


                //! Destructor.
                ~LocalInterface() = default;

                //! Copy constructor.
                LocalInterface(const LocalInterface&) = delete;

                //! Move constructor.
                LocalInterface(LocalInterface&&) = default;

                //! Copy affectation.
                LocalInterface& operator=(const LocalInterface&) = delete;

                //! Move affectation.
                LocalInterface& operator=(LocalInterface&&) = default;

                ///@}

                /*!
                 * \brief Get the list of vertices that delimits the interface.
                 *
                 * This list can't be required for an interior interface, which is a very special case.
                 *
                 * \return List of vertices that delimits the interface.
                 */
                const std::vector<unsigned int>& GetVertexIndexList() const noexcept;

                //! Get the nature of the interface.
                const InterfaceNS::Nature& GetNature() const noexcept;


            private:

                //! Constant accessor to the whether the interface is an interior interface or not.
                IsInterior GetIsInterior() const noexcept;

            private:

                /*!
                 * \brief List of vertices that delimits the interface, given by their index.
                 *
                 * The index here are the ones used in the description of the topology; so for instance for a triangle
                 * it is amon {0, 1, 2} where the position of each of those is given in topologic description.
                 */
                std::vector<unsigned int> vertex_index_list_;

                //! Nature of the interface.
                InterfaceNS::Nature nature_;

                //! Whether the interface is an interior interface or not.
                const IsInterior is_interior_;

            };


        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/LocalInterface/LocalInterface.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_LOCAL_INTERFACE_HPP_
