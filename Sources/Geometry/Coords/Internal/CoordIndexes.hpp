///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 14:28:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HPP_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HPP_

# include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            /*!
             * \brief Inner object in Coords in charge of storing relevant indexes.
             *
             * We in fact need three kind of indexes for a given Coord:
             *
             * - The index as read in the original mesh file. No assumption is made at all on this one: it might be
             * contiguous or not, and might start at whichever value it wants. It's kept only to be able to generate
             * back the mesh in file format; it is rather impractical otherwise.
             * - A contiguous index that gives away the position of the Coord object within GeometricMeshRegion object.
             * There are two flavors for such an index:
             *   . The original one, computed before any processor-wise reduction and that gives the position in the initial
             *     list.
             *   . One recomputed after processor-wise reduction, that gives away the position in the updated list.
             */
            class CoordIndexes
            {


            public:

                //! \copydoc doxygen_hide_alias_self
                using self = CoordIndexes;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit CoordIndexes() = default;

                //! Destructor.
                ~CoordIndexes() = default;

                //! Copy constructor.
                CoordIndexes(const CoordIndexes&) = default;

                //! Move constructor.
                CoordIndexes(CoordIndexes&&) = default;

                //! Copy affectation.
                CoordIndexes& operator=(const CoordIndexes&) = default;

                //! Move affectation.
                CoordIndexes& operator=(CoordIndexes&&) = default;

                ///@}

            public:

                //! Index given in the mesh file.
                unsigned int GetFileIndex() const noexcept;

                //! Position in GeometricMeshRegion::coords_list_ before processor-wise reduction.
                unsigned int GetPositionInOriginalCoordList() const noexcept;

                //! Position in GeometricMeshRegion::coords_list_ after processor-wise reduction.
                unsigned int GetPositionInReducedCoordList() const noexcept;


                //! Set index given in the mesh file.
                void SetFileIndex(unsigned int value) noexcept;

                //! Set position in GeometricMeshRegion::coords_list_ before processor-wise reduction.
                void SetPositionInOriginalCoordList(unsigned int value) noexcept;

                //! Set position in GeometricMeshRegion::coords_list_ after processor-wise reduction.
                void SetPositionInReducedCoordList(unsigned int value) noexcept;



            private:

                //! Index given in the mesh file.
                unsigned int file_index_ = NumericNS::UninitializedIndex<unsigned int>();

                //! Position in GeometricMeshRegion::coords_list_ before processor-wise reduction.
                unsigned int position_in_original_coord_list_ = NumericNS::UninitializedIndex<unsigned int>();

                /*!
                 * \brief Position in GeometricMeshRegion::coords_list_ after processor-wise reduction.
                 *
                 * Note: before reduction or in sequential case, the convention is to set the value below with the same
                 * value as position_in_original_coord_list_.
                 */
                unsigned int position_in_reduced_coord_list_ = NumericNS::UninitializedIndex<unsigned int>();


                # ifndef NDEBUG
                //! To check SetPositionInReducedCoordList() is called at most once.
                bool was_set_position_in_reduced_coord_list_already_called_ = false;
                # endif // NDEBUG


            };


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Coords/Internal/CoordIndexes.hxx"


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HPP_
