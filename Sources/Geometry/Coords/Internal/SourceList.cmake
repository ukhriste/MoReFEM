target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CoordIndexes.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CoordIndexes.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/CoordIndexes.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Factory.hxx"
)

