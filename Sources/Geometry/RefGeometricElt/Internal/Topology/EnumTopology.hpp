///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 16 Jun 2015 13:29:52 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_ENUM_TOPOLOGY_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_ENUM_TOPOLOGY_HPP_


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace TopologyNS
        {


            /*!
             * \brief Identifier of the topology.
             */
            enum class Type
            {
                point,
                segment,
                triangle,
                tetrahedron,
                quadrangle,
                hexahedron
            };



        } // namespace TopologyNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


namespace std
{


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     *
     * The name of the underlying topology is written in lower case, for instance "triangle".
     *
     */
    std::ostream& operator<<(std::ostream& stream, const MoReFEM::RefGeomEltNS::TopologyNS::Type rhs);


} // namespace std


/// @} // addtogroup GeometryGroup



#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_TOPOLOGY_x_ENUM_TOPOLOGY_HPP_
