///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON20_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON20_HPP_

# include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
# include "Geometry/RefGeometricElt/Instances/Hexahedron/ShapeFunction/Hexahedron20.hpp"
# include "Geometry/RefGeometricElt/Instances/Hexahedron/Format/Hexahedron20.hpp"
    // < absolutely required to let MoReFEM know the format actually supported!

# include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
# include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace Traits
        {


            /*!
             * \brief Traits class that holds the static functions related to shape functions, interface and topology.
             *
             * It can't be instantiated directly: its purpose is either to provide directly a data through
             * static function:
             *
             * \code
             * constexpr auto Nshape_function = Hexahedron20::NshapeFunction();
             * \endcode
             *
             * or to be a template parameter to MoReFEM::RefGeomEltNS::Hexahedron20 class (current class is in an
             * additional layer of namespace):
             *
             * \code
             * MoReFEM::RefGeomEltNS::Traits::Hexahedron20
             * \endcode
             *
             */
            class Hexahedron20 final
            : public ::MoReFEM::Internal::RefGeomEltNS::RefGeomEltImpl
            <
                Hexahedron20,
                ShapeFunctionNS::Hexahedron20,
                TopologyNS::Hexahedron
            >
            {


            protected:

                //! Convenient alias.
                using self = Hexahedron20;

                /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
                ///@{

                //! Constructor.
                Hexahedron20() = default;

                //! Destructor.
                ~Hexahedron20() = default;

                //! Copy constructor.
                Hexahedron20(const Hexahedron20&) = delete;

                //! Move constructor.
                Hexahedron20(Hexahedron20&&) = delete;

                //! operator=.
                Hexahedron20& operator=(const Hexahedron20&) = delete;

                //! operator=.
                self& operator=(self&&) = delete;

                ///@}


            public:

                /*!
                 * \brief Name associated to the RefGeomElt.
                 *
                 * \return Name that is guaranteed to be unique (throught the GeometricEltFactory) and can
                 * therefore also act as an identifier.
                 */
                static const std::string& ClassName();

                //! Number of Coords required to describe fully a GeometricElt of this type.
                enum : unsigned int { Ncoords = 20u };


                /*!
                 * \brief Enum associated to the RefGeomElt.
                 *
                 * \return Enum value guaranteed to be unique (throught the GeometricEltFactory); its
                 * raison d'être is that many operations are much faster on an enumeration than on a string.
                 */
                static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
                {
                    return MoReFEM::Advanced::GeometricEltEnum::Hexahedron20;
                }


            private:

                // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!


            };



        } // namespace Traits


        /*!
         * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Hexahedron20.
         *
         * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
         * store in one dynamic container all the kinds of GeometricElt present in a mesh.
         *
         * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
         * can be included in:
         *
         * \code
         * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
         * \endcode
         *
         */
        class Hexahedron20 final
        : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Hexahedron20>
        {
        public:

            //! Convenient alias.
            using self = Hexahedron20;

            //! Constructor.
            Hexahedron20() = default;

            //! Destructor.
            ~Hexahedron20();

            //! Copy constructor.
            Hexahedron20(const Hexahedron20&) = delete;

            //! Move constructor.
            Hexahedron20(Hexahedron20&&) = delete;

            //! operator=.
            Hexahedron20& operator=(const Hexahedron20&) = delete;

            //! operator=.
            self& operator=(self&&) = delete;


        private:

            // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...


        };


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON20_HPP_
