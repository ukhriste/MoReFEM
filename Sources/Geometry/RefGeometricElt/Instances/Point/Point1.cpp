///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/RefGeometricElt/Instances/Point/Point1.hpp"


namespace MoReFEM
{
    
    
    namespace RefGeomEltNS
    {
        
        
        Point1::~Point1() = default;
        
        
        namespace Traits
        {
            
            
            const std::string& Point1::ClassName()
            {
                static std::string ret("Point1");
                return ret;
            }
            
            
        } // namespace Traits
        
        
    } // namespace RefGeomEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
