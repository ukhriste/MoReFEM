///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Mar 2014 09:01:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle9.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

namespace MoReFEM
{
    
    
    namespace RefGeomEltNS
    {
        
        
        namespace ShapeFunctionNS
        {
            
            
            const std::array<ShapeFunctionType, 9>& Quadrangle9::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 9> ret
                {
                    {
                        [](const auto& local_coords) { return 0.25 * (1. - local_coords.r()) * (1. - local_coords.s()) * local_coords.r() * local_coords.s(); },
                        [](const auto& local_coords) { return -0.25 * (1. + local_coords.r()) * (1. - local_coords.s()) * local_coords.r() * local_coords.s(); },
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.r()) * (1. + local_coords.s()) * local_coords.r() * local_coords.s(); },
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.r()) * (1. + local_coords.s()) * local_coords.r() * local_coords.s(); },
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.r() * local_coords.r()) * (1. - local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()) * (1. - local_coords.s() * local_coords.s()) * local_coords.r(); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r() * local_coords.r()) * (1. + local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.r()) * (1. - local_coords.s() * local_coords.s()) * local_coords.r(); },
                        [](const auto& local_coords) { return (1. - local_coords.r()) * (1. + local_coords.r()) * (1. - local_coords.s()) * (1. + local_coords.s()); }
                    }
                };
                
                return ret;
            };
            
            
            const std::array<ShapeFunctionType, 18>& Quadrangle9::FirstDerivateShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 18> ret
                {
                    {
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.s()) * local_coords.r() * local_coords.s() + 0.25 * (1. - local_coords.r()) * (1. - local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.r()) * local_coords.r() * local_coords.s() + 0.25 * (1. - local_coords.r()) * (1. - local_coords.s()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.s()) * local_coords.r() * local_coords.s() - 0.25 * (1. + local_coords.r()) * (1. - local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.r()) * local_coords.r() * local_coords.s() - 0.25 * (1. + local_coords.r()) * (1. - local_coords.s()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.s()) * local_coords.r() * local_coords.s() + 0.25 * (1. + local_coords.r()) * (1. + local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.r()) * local_coords.r() * local_coords.s() + 0.25 * (1. + local_coords.r()) * (1. + local_coords.s()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.s()) * local_coords.r() * local_coords.s() - 0.25 * (1. - local_coords.r()) * (1. + local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.r()) * local_coords.r() * local_coords.s() - 0.25 * (1. - local_coords.r()) * (1. + local_coords.s()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return local_coords.r() * local_coords.s() * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r() * local_coords.r()) * (local_coords.s() - (1. - local_coords.s())); },
                        
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()) * (1. - local_coords.s() * local_coords.s()); },
                        [](const auto& local_coords) { return -local_coords.r() * local_coords.s() * (1. + local_coords.r()); },
                        
                        [](const auto& local_coords) { return -local_coords.r() * local_coords.s() * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.r() * local_coords.r()) * (local_coords.s() + (1. + local_coords.s())); },
                        
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.r()) * (1. - local_coords.s() * local_coords.s()); },
                        [](const auto& local_coords) { return local_coords.r() * local_coords.s() * (1. - local_coords.r()); },
                        
                        [](const auto& local_coords) { return -2. * local_coords.r() * (1. - local_coords.s()) * (1. + local_coords.s()); }, 
                        [](const auto& local_coords) { return -2. * local_coords.s() * (1. - local_coords.r()) * (1. + local_coords.r()); }
                    }
                };
                
                return ret;
            };
            
            
            
            const std::array<ShapeFunctionType, 36>& Quadrangle9::SecondDerivateShapeFunctionList()
            {
                
                static std::array<ShapeFunctionType, 36> ret
                {
                    {
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.s()) * local_coords.s() - 0.25 * (1. - local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return -0.25 * local_coords.r() * (-2. * local_coords.s() + 1.) ; },
                        [](const auto& local_coords) { return -0.25 * (1. - 2. * local_coords.r()) * (local_coords.s() - (1. - local_coords.s())); },
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.r()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return -0.25 * (1. - local_coords.s()) * local_coords.s() - 0.25 * (1. - local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return -0.25 * local_coords.r() * (-2. * local_coords.s() + 1.); },
                        [](const auto& local_coords) { return 0.25 * (1. + 2. * local_coords.r()) * (local_coords.s() - (1. - local_coords.s())); },
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.s()) * local_coords.s() + 0.25 * (1. + local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return 0.25 * local_coords.r() * (2. * local_coords.s() + 1.); },
                        [](const auto& local_coords) { return 0.25 * (1. + 2. * local_coords.r()) * (local_coords.s() + (1. + local_coords.s())); },
                        [](const auto& local_coords) { return 0.5 * (1. + local_coords.r()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return 0.25 * (1. + local_coords.s()) * local_coords.s() + 0.25 * (1. + local_coords.s()) * local_coords.s(); },
                        [](const auto& local_coords) { return 0.25 * local_coords.r() * (2. * local_coords.s() + 1.); },
                        [](const auto& local_coords) { return -0.25 * (1. - 2. * local_coords.r()) * (local_coords.s() + (1. + local_coords.s())); },
                        [](const auto& local_coords) { return -0.5 * (1. - local_coords.r()) * local_coords.r(); },
                        
                        [](const auto& local_coords) { return local_coords.s() * (1. - local_coords.s()); },
                        [](const auto& local_coords) { return local_coords.r() * (1. - 2. * local_coords.s()); },
                        [](const auto& local_coords) { return - local_coords.r() * (local_coords.s() - (1. - local_coords.s())); },
                        [](const auto& local_coords) { return 1. - local_coords.r() * local_coords.r(); },
                        
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.s() * local_coords.s()); },
                        [](const auto& local_coords) { return -local_coords.s() * (1. + local_coords.r()); },
                        [](const auto& local_coords) { return -local_coords.s() * (1. + 2. * local_coords.r()); },
                        [](const auto& local_coords) { return -local_coords.r() * (1. + local_coords.r()); },
                        
                        [](const auto& local_coords) { return -local_coords.s() * (1. + local_coords.s()); },
                        [](const auto& local_coords) { return -local_coords.r() * (1. + 2. * local_coords.s()); },
                        [](const auto& local_coords) { return - local_coords.r() * (local_coords.s() + (1. + local_coords.s())); },
                        [](const auto& local_coords) { return 1. - local_coords.r() * local_coords.r(); },
                        
                        [](const auto& local_coords) { return 0.5 * (1. - local_coords.s() * local_coords.s()); },
                        [](const auto& local_coords) { return local_coords.s() * (1. - local_coords.r()); },
                        [](const auto& local_coords) { return local_coords.s() * (1. - 2. * local_coords.r()); },
                        [](const auto& local_coords) { return local_coords.r() * (1. - local_coords.r()); },
                        
                        [](const auto& local_coords) { return -2. * (1. - local_coords.s()) * (1. + local_coords.s()); }, 
                        [](const auto& local_coords) { return 4. * local_coords.r() * local_coords.s(); }, 
                        [](const auto& local_coords) { return 4. * local_coords.r() * local_coords.s(); }, 
                        [](const auto& local_coords) { return -2. * (1. - local_coords.r()) * (1. + local_coords.r()); }
                    }
                };
                
                return ret;
            };

            
            
            
            
            
            
            
        } //  namespace ShapeFunctionNS
        
        
    } // namespace RefGeomEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
