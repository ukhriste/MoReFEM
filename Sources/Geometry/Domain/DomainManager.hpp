///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HPP_
# define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HPP_

# include "Utilities/Singleton/Singleton.hpp"
# include "Utilities/Containers/Tuple.hpp"

# include "Core/InputParameter/Geometry/Domain.hpp"

# include "Geometry/Domain/Domain.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Advanced
    {


        class LightweightDomainList;


    } // namespace Advanced


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief This class is used to create and retrieve Domain objects.
     *
     * Domain objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * Domain object given its unique id (which is the one that appears in the input parameter file).
     *
     */
    class DomainManager
    : public Utilities::Singleton<DomainManager>
    {

    private:

        //! Convenient alias to avoid repeating the type.
        using storage_type = std::unordered_map<unsigned int, Domain::const_unique_ptr>;

    public:

        //! Base type of Domain as input parameter (requested to identify domains in the input parameter data).
        using input_parameter_type = InputParameter::BaseNS::Domain;

        //! Name of the class (required for some Singleton-related errors).
        static const std::string& ClassName();

        /*!
         * \brief Create a new Domain object from the data of the input parameter file.
         *
         * \param[in] section Section in an input parameter file that describes content of this \a Domain.
         */
        template<class DomainSectionT>
        void Create(const DomainSectionT& section);

        /*!
         * \brief Fetch the domain object associated with \a unique_id unique identifier.
         *
         * If the unique_id is invalid, an exception is thrown.
         *
         * \param[in] unique_id Unique identifier of the sought \a Domain.
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \return Domain which GetUniqueId() method yield \a unique_id.
         */
        const Domain& GetDomain(unsigned int unique_id, const char* invoking_file, int invoking_line) const;

        /*!
         * \brief Fetch the domain object associated with \a unique_id unique identifier.
         *
         * If the unique_id is invalid, an exception is thrown.
         *
         * \param[in] unique_id Unique identifier of the sought \a Domain.
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * \return Non constant reference to the domain which GetUniqueId() method yield \a unique_id.
         */
        Domain& GetNonCstDomain(unsigned int unique_id, const char* invoking_file, int invoking_line);

        //! Friendship to a class that needs access to \a CreateLightweightDomain method.
        friend class Advanced::LightweightDomainList;

        //! Constant accessor to the domain list.
        const storage_type& GetDomainList() const noexcept;

    private:

        /*!
         * \brief Create a brand new Domain.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique. It is in the input parameter file
         * the figure that is in the block name, e.g. 1 for Domain1 = { .... }.
         * \param[in] mesh_index_list There might be here one index, that indicates in which mesh the domain is defined.
         * If the domain is not limited to one mesh, leave it empty.
         * \param[in] dimension_list List of dimensions to consider. If empty, no restriction on dimension.
         * \param[in] mesh_label_list List of mesh labels to consider. If empty, no restriction on it. This argument
         * must mandatorily be empty if \a mesh_index is empty: a mesh label is closely related to one given mesh.
         * \param[in] geometric_type_list List of geometric element types to consider in the domain. List of elements
         * available is given by Advanced::GeometricEltFactory::GetNameList(); most if not all of them should been
         * displayed in the comment in the input parameter file.
         *
         */
        void Create(unsigned int unique_id,
                    const std::vector<unsigned int>& mesh_index_list,
                    const std::vector<unsigned int>& dimension_list,
                    const std::vector<unsigned int>& mesh_label_list,
                    const std::vector<std::string>& geometric_type_list);



        /*!
         * \brief Create a brand new lightweight \a Domain.
         *
         * Such a \a Domain, more restricted than a full-fledged one, should be created only by a \a LightweightDomainList
         * object.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique.
         * \param[in] mesh_index Index of the mesh onto which \a Domain must be created.
         * \param[in] mesh_label_list List of mesh labels to consider.
         *
         */
        void CreateLightweightDomain(unsigned int unique_id,
                                     unsigned int mesh_index,
                                     const std::vector<unsigned int>& mesh_label_list);



    private:

        //! \name Singleton requirements.
        ///@{
        //! Constructor.
        DomainManager();

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<DomainManager>;
        ///@}


    private:

        //! Store the domain objects by their unique identifier.
        storage_type list_;

    };


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Domain/DomainManager.hxx"


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HPP_
