///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Aug 2014 14:43:07 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HPP_
# define MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HPP_

# include <memory>
# include <vector>

# include "Geometry/Domain/MeshLabel.hpp"
# include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class RefGeomElt;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace DomainNS
        {


            /*!
             * \brief Returns whether the \a geometric_elt belongs to the geometric mesh region identified by
             * \a geometric_mesh_region_identifier.
             *
             * \param[in] geometric_elt Geometric element upon which the test is performed.
             * \param[in] geometric_mesh_region_identifier Identifier of the geometric mesh region tested.
             *
             * \return Whether \a geometric_elt belongs to the GeometricMEshRegion which id is
             * geometric_mesh_region_identifier.
             */
            bool IsObjectInGeometricMeshRegion(const GeometricElt& geometric_elt,
                                               unsigned int geometric_mesh_region_identifier);

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            /*!
             * \brief A function to make the code compile.
             *
             * It should never be called in runtime!
             */
            [[noreturn]] bool IsObjectInGeometricMeshRegion(const RefGeomElt& , unsigned int);
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Returns whether \a geometric_element's MeshLabel is in \a mesh_label_list.
             *
             * \param[in] geometric_elt Geometric element upon which the test is performed.
             * \param[in] mesh_label_list Mesh labels in the domain.
             *
             * \return Whether \a geometric_elt belongs to any of the \a MeshLabel cited in \a mesh_label_list.
             */
            bool IsMeshLabelInList(const GeometricElt& geometric_elt,
                                   const MeshLabel::vector_const_shared_ptr& mesh_label_list);


            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            /*!
             * \brief A function to make the code compile.
             *
             * It should never be called in runtime!
             */
            [[noreturn]] bool IsMeshLabelInList(const RefGeomElt&,
                                                const MeshLabel::vector_const_shared_ptr& );
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        } // namespace DomainNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Domain/Internal/DomainHelper.hxx"


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_INTERNAL_x_DOMAIN_HELPER_HPP_
