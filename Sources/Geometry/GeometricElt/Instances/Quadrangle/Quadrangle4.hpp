///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE4_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE4_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Quadrangle/Quadrangle4.hpp"


namespace MoReFEM
{



    /*!
     * \brief A Quadrangle4 geometric element read in a mesh.
     *
     * We are not considering here a generic Quadrangle4 object (that's the role of MoReFEM::RefGeomEltNS::Quadrangle4), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Quadrangle4 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle4>
    {
    public:


        //! Minimal constructor: only the GeometricMeshRegion is provided.
        explicit Quadrangle4(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Quadrangle4(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::istream& stream);

        //! Constructor from vector of coords.
        explicit Quadrangle4(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Quadrangle4() override;

        //! Copy constructor.
        Quadrangle4(const Quadrangle4&) = default;

        //! Move constructor.
        Quadrangle4(Quadrangle4&&) = default;

        //! Copy affectation.
        Quadrangle4& operator=(const Quadrangle4&) = default;

        //! Move affectation.
        Quadrangle4& operator=(Quadrangle4&&) = default;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Quadrangle4& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE4_HPP_
