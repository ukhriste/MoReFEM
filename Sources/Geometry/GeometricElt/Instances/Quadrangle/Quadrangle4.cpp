///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Geometry/GeometricElt/Instances/Quadrangle/Quadrangle4.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"


namespace MoReFEM
{
    
    
    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {
        
        
        auto CreateEnsight(unsigned int mesh_unique_id,
                           const Coords::vector_unique_ptr& mesh_coords_list,
                           std::istream& in)
        {
            return std::make_unique<Quadrangle4>(mesh_unique_id, mesh_coords_list, in);
        }
        
        
        auto CreateMinimal(unsigned int mesh_unique_id)
        {
            return std::make_unique<Quadrangle4>(mesh_unique_id);
        }
        
        
        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
        Advanced::GeometricEltFactory::CreateOrGetInstance().RegisterGeometricElt<RefGeomEltNS::Quadrangle4>(CreateMinimal,
                                                                                                      CreateEnsight);

        
    } // anonymous namespace
    
    
    Quadrangle4::Quadrangle4(unsigned int mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle4>(mesh_unique_id)
    { }
    
    
    Quadrangle4::Quadrangle4(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::istream& stream)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle4>(mesh_unique_id, mesh_coords_list, stream)
    { }
    
    
    Quadrangle4::Quadrangle4(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::vector<unsigned int>&& coords)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle4>(mesh_unique_id, mesh_coords_list, std::move(coords))
    { }
    

    Quadrangle4::~Quadrangle4() = default;

      
    const RefGeomElt& Quadrangle4::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }
    
    
    const RefGeomEltNS::Quadrangle4& Quadrangle4::StaticRefGeomElt()
    {
        static RefGeomEltNS::Quadrangle4 ret;
        return ret;
    }
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup

