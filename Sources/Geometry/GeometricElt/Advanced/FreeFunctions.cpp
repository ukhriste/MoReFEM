///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 23 Apr 2016 11:48:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "ThirdParty/Wrappers/Seldon/MatrixOperations.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace GeomEltNS
        {
    
            
            void ComputeBarycenter(const GeometricElt& geometric_elt, SpatialPoint& point)
            {
                point.Reset();
                
                const auto& coords_list = geometric_elt.GetCoordsList();
                const unsigned int dimension = geometric_elt.GetDimension();
                
                const double inv_Ncoords = 1. / static_cast<double>(coords_list.size());
                
                for (unsigned int i = 0u; i < dimension; ++i)
                {
                    double& current_barycenter_coord = point.GetNonCstValue(i);
                    
                    for (const auto& coords_ptr : coords_list)
                    {
                        assert(!(!coords_ptr));
                        current_barycenter_coord += inv_Ncoords * coords_ptr->operator[](i);
                    }
                }
            }

            
            void Local2Global(const GeometricElt& geometric_elt,
                              const LocalCoords& local_coords,
                              SpatialPoint& out)
            {
                #ifndef NDEBUG
                const unsigned int Ncomponent = local_coords.GetDimension();
                assert(geometric_elt.GetDimension() == Ncomponent);
                assert(Ncomponent <= 3u);
                #endif // NDEBUG
                
                out.Reset();
                
                const auto& local_coords_list = geometric_elt.GetCoordsList();
                const unsigned int Ncoords = geometric_elt.Ncoords();
                assert(static_cast<std::size_t>(Ncoords) == local_coords_list.size());
                
                for (unsigned int i = 0u; i < Ncoords; ++i)
                {
                    auto local_coords_in_geom_elt_ptr = local_coords_list[i];
                    assert(!(!local_coords_in_geom_elt_ptr));
                    const auto& local_coords_in_geom_elt = *local_coords_in_geom_elt_ptr;
                    
                    const double shape_fct = geometric_elt.ShapeFunction(i, local_coords);
                    
                    for (unsigned int j = 0u; j < 3u; ++j)
                        out.GetNonCstValue(j) += shape_fct * local_coords_in_geom_elt[j];
                }
            }
            
            
            LocalCoords Global2local(const GeometricElt& geometric_elt,
                                     const Coords& coords,
                                     const unsigned int mesh_dimension)
            {
                static_cast<void>(mesh_dimension);
                
                //General Newton routine for the nonlinear case
                assert(geometric_elt.GetDimension() == mesh_dimension
                       && "This routine works only if the mesh as the same dimension as the elements.");
                
                const unsigned int dimension = geometric_elt.GetDimension();
                
                //Initial point of the routine: barycenter of the reference element.
                const auto& ref_geom_elt = geometric_elt.GetRefGeomElt();
                
                LocalCoords current_guess = ref_geom_elt.GetBarycenter();
                
                unsigned int iter = 0u;
                unsigned int Niter_max = 100u;
                double error = std::numeric_limits<double>::max();
                
                double det;
                
                Coords::unique_ptr buf_ptr = Internal::CoordsNS::Factory::Origin();
                Coords::unique_ptr coords_from_guess_ptr = Internal::CoordsNS::Factory::Origin();
                
                auto& buf = *buf_ptr;
                auto& coords_from_guess = *coords_from_guess_ptr;
                
                ComputeJacobian compute_jacobian_helper(mesh_dimension);
 
                while ((!NumericNS::IsZero(error)) && (++iter < Niter_max))
                {
                    Local2Global(geometric_elt, current_guess, buf);
                    
                    decltype(auto) jacobian = compute_jacobian_helper.Compute(geometric_elt, current_guess);
                    
                    LocalMatrix inv_jacobian = Wrappers::Seldon::ComputeInverseSquareMatrix(jacobian, det);
                    
                    error = 0.;
                    
                    for (unsigned int i = 0u; i < dimension; ++i)
                    {
                        const int int_i = static_cast<int>(i);
                        double tmp = 0.;
                        
                        for (unsigned int j = 0u; j < dimension; ++j)
                            tmp += inv_jacobian(int_i, static_cast<int>(j)) * (coords[j] - buf[j]);
                        
                        error += NumericNS::Square(tmp);
                        current_guess.GetNonCstValue(i) += tmp;
                    }
                    
                    error = std::sqrt(error);
                }
                
                if (iter == Niter_max)
                    throw ExceptionNS::GeometricElt::Global2LocalNoConvergence(__FILE__, __LINE__);
                
                {
                    Local2Global(geometric_elt, current_guess, coords_from_guess);
                    
                    if (!NumericNS::IsZero(Distance(coords, coords_from_guess)))
                        throw Exception("Convergence toward a bad solution!", __FILE__, __LINE__);
                }

                
                return current_guess;
            }
            
    
        } // namespace GeomEltNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
