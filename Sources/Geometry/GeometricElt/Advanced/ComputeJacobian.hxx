///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Jun 2016 10:13:09 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HXX_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace GeomEltNS
        {


            inline LocalMatrix& ComputeJacobian::GetNonCstJacobian() noexcept
            {
                return jacobian_;
            }


            inline std::vector<double>& ComputeJacobian::GetNonCstFirstDerivateShapeFunction() noexcept
            {
                return first_derivate_shape_function_;
            }


        } // namespace GeomEltNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_COMPUTE_JACOBIAN_HXX_
