///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_HPP_


# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace GeometricElt
        {


            //! Generic class for GeometricElt exceptions.
            class Exception : public MoReFEM::Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! Copy constructor.
                Exception(const Exception&) = default;

                //! Move constructor.
                Exception(Exception&&) = default;

                //! Copy affectation.
                Exception& operator=(const Exception&) = default;

                //! Move affectation.
                Exception& operator=(Exception&&) = default;



            };


            /*!
             * \brief Thrown when a geometric element is not supported by the chosen format.
             *
             * For instance some geometric elements are recognized by Ensight and not by Medit (and vice-versa).
             * So for instance if you read an Ensight mesh with a Quadrangle8 and then attempts to write the mesh
             * as a Medit one, this exception will be thrown as Medit can't cope with it.
             */
            class FormatNotSupported final : public Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] geometric_elt_identifier String that identifies the kind of geometric elementconsidered (eg 'Triangle3')
                 * \param[in] format Name of the format considered  ('Ensight' or 'Medit' currently)
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit FormatNotSupported(const std::string& geometric_elt_identifier,
                                            const std::string& format,
                                            const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~FormatNotSupported();

                //! Copy constructor.
                FormatNotSupported(const FormatNotSupported&) = default;

                //! Move constructor.
                FormatNotSupported(FormatNotSupported&&) = default;

                //! Copy affectation.
                FormatNotSupported& operator=(const FormatNotSupported&) = default;

                //! Move affectation.
                FormatNotSupported& operator=(FormatNotSupported&&) = default;

            };



            /*!
             * \brief Thrown when edges or faces are requested whereas they weren't properly built.
             *
             * They are not built automatically, so user must have called GeometricMeshRegion::BuildEdgeList() or
             * GeometricMeshRegion::BuildFaceList() beforehand.
             */
            class InterfaceTypeNotBuilt final : public Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] edge_or_face "edge" or "face"
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InterfaceTypeNotBuilt(const std::string& edge_or_face,
                                               const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~InterfaceTypeNotBuilt();

                //! Copy constructor.
                InterfaceTypeNotBuilt(const InterfaceTypeNotBuilt&) = default;

                //! Move constructor.
                InterfaceTypeNotBuilt(InterfaceTypeNotBuilt&&) = default;

                //! Copy affectation.
                InterfaceTypeNotBuilt& operator=(const InterfaceTypeNotBuilt&) = default;

                //! Move affectation.
                InterfaceTypeNotBuilt& operator=(InterfaceTypeNotBuilt&&) = default;

            };



            /*!
             * \brief Thrown when you attempt to build an interface type while not having built first the type just below.
             *
             * For instance in order to build faces you must ahve already built edges.
             */
            class InvalidInterfaceBuildOrder final : public Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] attempted_type Type which build was required by the developer.
                 * \param[in] missing_type Type that ought to be already built to build \a attempted_type.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit InvalidInterfaceBuildOrder(std::string&& attempted_type,
                                                    std::string&& missing_type,
                                                    const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~InvalidInterfaceBuildOrder();

                //! Copy constructor.
                InvalidInterfaceBuildOrder(const InvalidInterfaceBuildOrder&) = default;

                //! Move constructor.
                InvalidInterfaceBuildOrder(InvalidInterfaceBuildOrder&&) = default;

                //! Copy affectation.
                InvalidInterfaceBuildOrder& operator=(const InvalidInterfaceBuildOrder&) = default;

                //! Move affectation.
                InvalidInterfaceBuildOrder& operator=(InvalidInterfaceBuildOrder&&) = default;

            };


            /*!
             * \brief Thrown when no convergence was reached in Global2Local.
             *
             */
            class Global2LocalNoConvergence : public Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Global2LocalNoConvergence(const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Global2LocalNoConvergence();

                //! Copy constructor.
                Global2LocalNoConvergence(const Global2LocalNoConvergence&) = default;

                //! Move constructor.
                Global2LocalNoConvergence(Global2LocalNoConvergence&&) = default;

                //! Copy affectation.
                Global2LocalNoConvergence& operator=(const Global2LocalNoConvergence&) = default;

                //! Move affectation.
                Global2LocalNoConvergence& operator=(Global2LocalNoConvergence&&) = default;

            };





        } // namespace GeometricElt


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup



#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_EXCEPTIONS_x_GEOMETRIC_ELT_HPP_
