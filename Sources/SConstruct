import os
import copy
import subprocess
import platform
import imp
import SCons

import six

custom = imp.load_source('custom_scons_functions', '../SCons/custom_scons_functions.py')

#---------------------------------------------------------------------------------------
# Read command line expected arguments.
#---------------------------------------------------------------------------------------

AddOption('--config_file',
          dest='configuration_file',
          type='string',
          nargs=1,
          action='store',
          metavar='DIR',
          help='Path to the configuration file to use.')
          
AddOption('--aliases',
          dest='aliases',
          action='store_true',
          metavar='DIR',
          help='Print the list of available aliases.')
          
aliases = GetOption('aliases')       

#---------------------------------------------------------------------------------------
# Read the content of the configuration file.
#---------------------------------------------------------------------------------------
configuration_file = GetOption('configuration_file')

if not configuration_file:
    print("[ERROR] No configuration file given. You should specify in your command line --config_file=*path to your configuration file*.")
    Exit(1)


config_file_module = imp.load_source('configuration_file', '../SCons/configuration_file.py')
vars = config_file_module.ReadConfigurationFile(configuration_file)

#---------------------------------------------------------------------------------------
# Define environment settings (compilation flags, path to libraries and so forth...)
#---------------------------------------------------------------------------------------
env = custom.DefineEnvironment(vars)


#---------------------------------------------------------------------------------------
# Generate the help when -h is given to command line.
#---------------------------------------------------------------------------------------
unknown = vars.UnknownVariables()
if unknown:
    print("[ERROR] Unknown variables in command line:", unknown.keys())
    Exit(1)

Help(vars.GenerateHelpText(env))


#-----------------------------------------------------------------------------------------------------
# List of core MoReFEM libraries to build; a folder with the same name is expected to exist.
#-----------------------------------------------------------------------------------------------------
# 'ModelInstances' and 'ThirdParty' are left out on purpose:
# - ModelInstances is dedicated to build executables, and is managed later. It will probably move somewhere else in the near future.
# - ThirdParty is in fact compiled within Utilities: both are entangled and it would be tricky to separate them.
# Beware: ordering is important: a library below depends on all the ones on its left.
libraries = (\
            'Utilities', \
            'Core', \
            'Geometry', \
            'FiniteElement', \
            'Parameters', \
            'Operators', \
            'ParameterInstances', \
            'OperatorInstances', \
            'FormulationSolver')

# List of all libraries, as strings without path. For instance ['libmorefem_utilities.dylib', 'libmorefem_core.dylib'].
morefem_libs = []

env["LINK_LIBRARIES_LIST"] = copy.copy(env['THIRD_PARTY_LIBRARIES'])
 
if platform == 'Linux':
    env["LINK_LIBRARIES_LIST"].extend(link_flags)


# Iterate through the SConscript of all the libraries to build.
libs_for_alias = []

for library in libraries:

    # SConscript will create the library.
    variant_dir=os.path.join(env['CURRENT_BUILD_INTERMEDIATE_DIR'], library)    
    
    scons_library_object = SConscript(os.path.join(library, 'SConscript'), 
                                      exports=['env'],
                                      variant_dir=variant_dir, 
                                      duplicate=0)
                                      
    libname = custom.PrepareTargetName(scons_library_object)
    
    # Install here the library in the final target folder.
    full_path_target = os.path.join(env["TARGET_VARIANT_DIR"], libname)
    
    env.Command(full_path_target, scons_library_object, custom.InstallStaticLibrary)
    libs_for_alias.append(full_path_target)
    
    # Update the list of known libraries so far.
    morefem_libs.append(libname)
    env["LINK_LIBRARIES_LIST"].insert(0, libname)
    
    
env.Alias('libraries', libs_for_alias)

aggregated_morefem = os.path.join(env["TARGET_VARIANT_DIR"], "libMoReFEM.a")
env.Alias('aggregated_morefem', aggregated_morefem)


## Aggregate all the basic libraries into a single one, easier to link with in other projects.
env.Command(aggregated_morefem, libs_for_alias, custom.AggregateLibraries)

executable_list = []
exec_library_list = []

#---------------------------------------------------------
# Create an executable and post processing library for post-processing executable(s).
#---------------------------------------------------------

(post_processing_exec, post_processing_lib) = SConscript('PostProcessing/SConscript',
                                                         exports=(['env']),
                                                         variant_dir=os.path.join(env['CURRENT_BUILD_INTERMEDIATE_DIR'], 'PostProcessing'),
                                                         duplicate=0)
                                                         
assert(len(post_processing_lib) == 1)
libname = custom.PrepareTargetName(post_processing_lib[0])
full_path_target = os.path.join(env["TARGET_VARIANT_DIR"], libname)
env.Command(full_path_target, post_processing_lib, custom.InstallStaticLibrary)
env.Alias('post_processing_library', full_path_target)


executable_list.extend(post_processing_exec)


#---------------------------------------------------------
# Create test tools library.
#---------------------------------------------------------

test_tools_lib = SConscript('Test/Tools/SConscript',
                            exports=(['env']),
                            variant_dir=os.path.join(env['CURRENT_BUILD_INTERMEDIATE_DIR'], 'TestTools'),
                            duplicate=0)

assert(len(test_tools_lib) == 1)
libname = custom.PrepareTargetName(test_tools_lib[0])
full_path_target = os.path.join(env["TARGET_VARIANT_DIR"], libname)
env.Command(full_path_target, test_tools_lib, custom.InstallStaticLibrary)
env.Alias('test_tools_library', full_path_target)


#---------------------------------------------------
# Now create the executables (and potentially their libraries) for all the models.
# It should be noted that ModelInstances SConscript calls each model's sconscript; when you add a new
# model this is the file you have to modify (whereas you are not supposed to modify SConstruct itself
# when writing a new model).
#---------------------------------------------------
(model_exec_list, model_library_list) = SConscript('ModelInstances/SConscript',
                                                  exports=(['env', 'post_processing_lib']),
                                                  variant_dir=os.path.join(env['CURRENT_BUILD_INTERMEDIATE_DIR'], 'ModelInstances'),
                                                  duplicate=0)
                                                  
executable_list.extend(model_exec_list)
exec_library_list.extend(model_library_list)                                                  
                                                  
list_test_exec = SConscript('Test/SConscript',
                            exports=(['env', 'post_processing_lib', 'test_tools_lib']),
                            variant_dir=os.path.join(env['CURRENT_BUILD_INTERMEDIATE_DIR'], 'Tests'),
                            duplicate=0)

executable_list.extend(list_test_exec)





#---------------------------------------------------------
# Install each executable and library in the final build folder.
#---------------------------------------------------------

for executable in executable_list:
    executable_name = custom.PrepareTargetName(executable, "-{0}".format(env["LIBRARY_TYPE"]))
    env.Command(os.path.join(env["TARGET_VARIANT_DIR"], executable_name), executable, custom.InstallExecutable)

for library in exec_library_list:
    libname = custom.PrepareTargetName(library)
    
    full_path_target = os.path.join(env["TARGET_VARIANT_DIR"], libname)
    env.Command(full_path_target, library, custom.InstallStaticLibrary)



#---------------------------------------------------------
# Make sure a target has been chosen in command line.
# It might be strange to call this at the very end of the file, but if we did it at the same time we build the
# help message we wouldn't have the proper list of possible targets.
#---------------------------------------------------------
if not COMMAND_LINE_TARGETS:
    print('Warning: no target was actually given, and as a result nothing was built. Possible targets are (more than one might be specified):')
    
    if six.PY2:
        aliases = SCons.Node.Alias.default_ans.keys()
    else:
        aliases = list(SCons.Node.Alias.default_ans)
    
    aliases.sort()
    
    for x in aliases:
        print('\t{0}'.format(x))


