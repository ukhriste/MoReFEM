/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_HEAT_MODEL_HXX_
# define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_HEAT_MODEL_HXX_


namespace MoReFEM
{


    namespace HeatNS
    {


        inline const std::string& HeatModel::ClassName()
        {
            static std::string name("HeatModel");
            return name;
        }


        inline const HeatVariationalFormulation& HeatModel::GetVariationalFormulation() const
        {
            assert(!(!variational_formulation_));
            return *variational_formulation_;
        }


        inline HeatVariationalFormulation& HeatModel::GetNonCstVariationalFormulation()
        {
            return const_cast<HeatVariationalFormulation&>(GetVariationalFormulation());
        }


        inline bool HeatModel::SupplHasFinishedConditions() const
        {
            const HeatVariationalFormulation& formulation = GetVariationalFormulation();

            // In the static case no loop time so HasFinished is set at true to avoid the while loop
            if (formulation.GetStaticOrDynamic() == HeatVariationalFormulation::StaticOrDynamic::static_case)
                return true;
            else
                return false; // ie no additional condition
        }


        inline void HeatModel::SupplInitializeStep()
        { }


    } // namespace HeatNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_HEAT_MODEL_HXX_
