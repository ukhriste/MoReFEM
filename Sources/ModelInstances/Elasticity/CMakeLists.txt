add_library(MoReFEM4Elasticity_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Elasticity_lib
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/ElasticityModel.cpp" / 
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulationElasticity.cpp"
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/ElasticityModel.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/ElasticityModel.hxx" /
		"${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp" /
 		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulationElasticity.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulationElasticity.hxx"
)

target_link_libraries(MoReFEM4Elasticity_lib
                      ${MOREFEM_MODEL}
                      ${ALL_LOAD_FLAG})


add_executable(MoReFEM4Elasticity ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4Elasticity
                      MoReFEM4Elasticity_lib)
                      
morefem_install(MoReFEM4Elasticity MoReFEM4Elasticity_lib)                      


add_executable(MoReFEM4ElasticityEnsightOutput ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
target_link_libraries(MoReFEM4ElasticityEnsightOutput
                      MoReFEM4Elasticity_lib
                      MoReFEMPostProcessing_lib)
                      
morefem_install(MoReFEM4ElasticityEnsightOutput)
