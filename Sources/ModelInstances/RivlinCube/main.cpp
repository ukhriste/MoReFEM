/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "ModelInstances/RivlinCube/Model.hpp"
#include "ModelInstances/RivlinCube/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = RivlinCubeNS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        const auto& input_parameter_data = morefem_data.GetInputParameterList();
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            RivlinCubeNS::Model model(morefem_data);
            model.Run(morefem_data);
            
            input_parameter_data.PrintUnused(std::cout);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;

}

