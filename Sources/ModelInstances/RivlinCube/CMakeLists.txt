add_library(MoReFEM4RivlinCube_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4RivlinCube_lib
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/Model.cpp"
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp"
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/InputParameterList.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp"
        "${CMAKE_CURRENT_LIST_DIR}/Model.hxx"
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp"
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx"        
)

target_link_libraries(MoReFEM4RivlinCube_lib
                      ${MOREFEM_MODEL}
                      ${ALL_LOAD_FLAG})


add_executable(MoReFEM4RivlinCube ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4RivlinCube
                      MoReFEM4RivlinCube_lib)
                  
morefem_install(MoReFEM4RivlinCube MoReFEM4RivlinCube_lib)                   

add_executable(MoReFEM4RivlinCubeEnsightOutput ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
target_link_libraries(MoReFEM4RivlinCubeEnsightOutput
                      MoReFEM4RivlinCube_lib
                      MoReFEMPostProcessing_lib)
                      
morefem_install(MoReFEM4RivlinCubeEnsightOutput)
