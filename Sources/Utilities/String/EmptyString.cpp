///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Mar 2015 09:26:19 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include "Utilities/String/EmptyString.hpp"


namespace MoReFEM
{
    
    
    namespace Utilities
    {
        
        
        const std::string& EmptyString()
        {
            static std::string ret;
            return ret;
        }
        
     
        
    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
