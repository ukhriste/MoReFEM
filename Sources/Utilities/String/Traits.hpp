///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Jan 2017 22:38:09 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_STRING_x_TRAITS_HPP_
# define MOREFEM_x_UTILITIES_x_STRING_x_TRAITS_HPP_


# include <string>


namespace MoReFEM
{


    namespace Utilities
    {


        namespace String
        {


            //! Convenient enum class for IsString.
            enum class CharAllowed { yes, no };


            /*!
             * \brief Traits class which tells whether T is a string class.
             *
             * Currently recognized string classes are:
             * - When std::decay_t<T> is either char* or const char*.
             * - When std::decay_t<T> is std::string.
             * - When std::decay_t<T> is a char in the case \a IsCharAllowedT is CharAllowed::yes.
             *
             * \tparam IsCharAllowedT Whether a char should be recognized as a string or not.
             *
             * This was most inspired by http://stackoverflow.com/questions/8097534/type-trait-for-strings
             * with few tweaks of my own.
             */
            template
            <
                class T,
                CharAllowed IsCharAllowedT = CharAllowed::yes
            >
            struct IsString
            : public std::integral_constant
            <
                bool,
                std::is_same<char*, std::decay_t<T>>::value
            || std::is_same<const char*, std::decay_t<T>>::value
            || std::is_same<std::string, std::decay_t<T>>::value
            || (IsCharAllowedT == CharAllowed::yes && std::is_same<char, std::decay_t<T>>::value)
            >
            { };

            static_assert(!IsString<int>::value);
            static_assert(IsString<char*>::value);
            static_assert(IsString<const char*>::value);
            static_assert(IsString<char* const >::value);
            static_assert(IsString<const char* const >::value);
            static_assert(IsString<char (&)[5] >::value);
            static_assert(IsString<const char (&)[5] >::value);
            static_assert(IsString<std::string>::value);
            static_assert(IsString<const std::string>::value);
            static_assert(IsString<const std::string&>::value);


        } // namespace String


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_STRING_x_TRAITS_HPP_
