///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Oct 2013 16:57:38 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <sstream>
#include "Utilities/Exceptions/Factory.hpp"


namespace // anonymous
{
    
    
    // Forward declarations here; definitions are at the end of the file
    std::string UnableToRegisterMsg(const std::string& object_name, const std::string& factory_content);
    
    
    std::string UnregisteredNameMsg(const std::string& object_name, const std::string& factory_content);
    
    
} // namespace anonymous



namespace MoReFEM
{


    namespace ExceptionNS
    {
        
        
        namespace Factory
        {
            
            
            Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }
            
            
            Exception::~Exception() = default;
            
            
            UnableToRegister::UnableToRegister(const std::string& object_name,
                                               const std::string& factory_content,
                                               const char* invoking_file, int invoking_line)
            : Exception(UnableToRegisterMsg(object_name, factory_content), invoking_file, invoking_line)
            { }
            
            
            UnableToRegister::~UnableToRegister() = default;
            
            
            UnregisteredName::~UnregisteredName() = default;
            
            
            UnregisteredName::UnregisteredName(const std::string& object_name, const std::string& factory_content,
                                               const char* invoking_file, int invoking_line)
            : Exception(UnregisteredNameMsg(object_name, factory_content), invoking_file, invoking_line)
            { }
            
            
            
        } // namespace FactoryNS
        
        
    } // namespace ExceptionNS


} // namespace MoReFEM



namespace // anonymous
{
    
    
    // Definitions of functions defined at the beginning of the file
    std::string UnableToRegisterMsg(const std::string& object_name, const std::string& factory_content)
    {
        std::ostringstream oconv;
        oconv << "Unable to register " << factory_content << " named " << object_name << "; in all likelihood either another "
        << factory_content << " was already registered with that name or the pointer function passed to create the "
        << factory_content << " was invalid.";
        return oconv.str();
    }
    
    
    
    std::string UnregisteredNameMsg(const std::string& object_name, const std::string& factory_content)
    {
        std::ostringstream oconv;
        oconv << "Trying to create " << factory_content << " which identifier is '"  << object_name << "'; no such "
        "identifier has been registered in the " << factory_content << " factory.";
        
        return oconv.str();
    }
    
    
} // namespace anonymous


/// @} // addtogroup UtilitiesGroup










