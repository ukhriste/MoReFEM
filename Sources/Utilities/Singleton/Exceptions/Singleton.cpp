///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 17:41:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <sstream>
#include "Utilities/Singleton/Exceptions/Singleton.hpp"




namespace // anonymous
{
    // Forward declarations here; definitions are at the end of the file

    std::string DeadReferenceMsg(const std::string& name);


    std::string NotYetCreatedMsg(const std::string& name);


} // namespace anonymous



namespace MoReFEM
{


    namespace Utilities
    {


        namespace ExceptionNS
        {


            namespace Singleton
            {

                

                DeadReference::DeadReference(const std::string& name, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(DeadReferenceMsg(name), invoking_file, invoking_line)
                { }

                
                DeadReference::~DeadReference() = default;


                NotYetCreated::~NotYetCreated() = default;


                NotYetCreated::NotYetCreated(const std::string& name, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(NotYetCreatedMsg(name), invoking_file, invoking_line)
                { }




            } // namespace Singleton


        } // namespace ExceptionNS


    } // namespace Utilities


} // namespace MoReFEM





namespace // anonymous
{
    // Definitions of functions defined at the beginning of the file


    std::string DeadReferenceMsg(const std::string& name)
    {
        return "Dead reference found: singleton " + name + " was invoked after its demise...";
    }



    std::string NotYetCreatedMsg(const std::string& name)
    {
        return "Singleton was invoked while underlying object (" + name + ") not yet created. Singleton<T>::Instance() "
                         "creates the object if not yet existing, while Singleton<T>::GetInstance() you called "
                         "expects the object to already have been created. The subtlety exists so that the user "
                         "is not forced to create a default constructor, which could be error prone.";

    }


} // namespace anonymous


/// @} // addtogroup UtilitiesGroup





