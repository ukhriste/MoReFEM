///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 17:41:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_SINGLETON_x_EXCEPTIONS_x_SINGLETON_HPP_
# define MOREFEM_x_UTILITIES_x_SINGLETON_x_EXCEPTIONS_x_SINGLETON_HPP_



# include "Utilities/Exceptions/Exception.hpp"



namespace MoReFEM
{


    namespace Utilities
    {


        namespace ExceptionNS
        {


            namespace Singleton
            {


                //! This exception is thrown if a call is made after singleton destruction.
                class DeadReference final : public MoReFEM::Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] name Name of the \a Singleton for which the exception was thrown.
                     * \copydoc doxygen_hide_invoking_file_and_line
                     */
                    explicit DeadReference(const std::string& name, const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    DeadReference(const DeadReference&) = default;

                    //! Move constructor.
                    DeadReference(DeadReference&&) = default;

                    //! Copy affectation.
                    DeadReference& operator=(const DeadReference&) = default;

                    //! Move affectation.
                    DeadReference& operator=(DeadReference&&) = default;


                    //! Destructor
                    virtual ~DeadReference();


                };


                /*!
                 * This exception is thrown when Singleton<T>::GetInstance() is called while singleton has not
                 * yet been created.
                 */
                class NotYetCreated final : public MoReFEM::Exception
                {
                public:

                    /*!
                     * \brief Constructor
                     *
                     * \param[in] name Name of the \a Singleton for which the exception was thrown.
                     * \copydoc doxygen_hide_invoking_file_and_line
                     */
                    explicit NotYetCreated(const std::string& name, const char* invoking_file, int invoking_line);

                    //! Copy constructor.
                    NotYetCreated(const NotYetCreated&) = default;

                    //! Move constructor.
                    NotYetCreated(NotYetCreated&&) = default;

                    //! Copy affectation.
                    NotYetCreated& operator=(const NotYetCreated&) = default;

                    //! Move affectation.
                    NotYetCreated& operator=(NotYetCreated&&) = default;

                    //! Destructor
                    virtual ~NotYetCreated();


                };


            } // namespace Singleton


        } // namespace ExceptionNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_SINGLETON_x_EXCEPTIONS_x_SINGLETON_HPP_
