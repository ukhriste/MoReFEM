///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Aug 2013 10:53:29 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_CRTP_x_INPUT_PARAMETER_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_CRTP_x_INPUT_PARAMETER_HXX_



namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            namespace Crtp
            {


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                const std::string& InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>::GetIdentifier()
                {
                    static std::string ret =
                        std::is_same<EnclosingSectionT, NoEnclosingSection>()
                        ? DerivedT::NameInFile()
                        : EnclosingSectionT::GetFullName() + "." + DerivedT::NameInFile();
                    return ret;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                void InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>::SetValue(ops_type value)
                {
                    value_ = Utilities::StaticCast<ops_type, return_type>::Perform(std::move(value));
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                constexpr Nature InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>::GetNature() noexcept
                {
                    return Nature::parameter;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline typename Utilities::ConstRefOrValue<ReturnTypeT>::type
                InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>::GetTheValue() const
                {
                    return value_;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline void InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>::SetAsUsed() const noexcept
                {
                    is_used_ = true;
                }


                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                inline bool InputParameter<DerivedT, EnclosingSectionT, ReturnTypeT>::IsUsed() const noexcept
                {
                    return is_used_;
                }



            } // namespace Crtp


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_CRTP_x_INPUT_PARAMETER_HXX_
