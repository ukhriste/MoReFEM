///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_SUBTUPLE_x_SUBTUPLE_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_SUBTUPLE_x_SUBTUPLE_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            /*!
             * \brief Reach recursively in \a TupleT the types that were given in \a SubTupleT.
             *
             * \tparam SubTupleT Tuple which all element types are looked at in \a TupleT.
             * \tparam Index Index of element currently considered in \a SubTupleT.
             * \tparam Length Size of \a SubTupleT.
             * \tparam TupleT The tuple which has been submitted to InputParameterList::Base. \a SubTupleT
             * is assumed to include types that are represented in \a TupleT
             *
             * \internal <b><tt>[internal]</tt></b> Kept here at the moment; this code was written a while ago when I was less familiar
             * with metaprogramming and might need some work if really needed.
             *
             */
            template<class SubTupleT, std::size_t Index, std::size_t Length, class TupleT>
            struct ThroughSubtuple
            {


                /*!
                 * \brief Recursively ensure that all input parameters considered are vector of the same length.
                 *
                 * \param[in] tuple Tuple nested in InputParameterList::Base.
                 * \param[in] length Expected size of the vectors considered, fixed in the very first element in
                 * the recursion.
                 */

                static void EnsureSameLength(const TupleT& tuple, std::size_t length);


            private:

                //! Type if the \a Index -th element of \a SubTupleT.
                using EltSubTupleType = typename std::tuple_element<Index,SubTupleT>::type;

                //! Index of \a EltSubTupleType in \a TupleT.
                enum { index_in_tuple = Utilities::Tuple::IndexOf<EltSubTupleType, TupleT>::value };


            };



            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Begin recursion.
            template<class SubTupleT, std::size_t Length, class TupleT>
            struct ThroughSubtuple<SubTupleT, 0, Length, TupleT>
            {

                static void EnsureSameLength(const TupleT& tuple, std::size_t& length);


            private:

                using EltSubTupleType = typename std::tuple_element<0,SubTupleT>::type;

                enum { index_in_Tuple = Utilities::Tuple::IndexOf<EltSubTupleType, TupleT>::value };


            };

            // End recursion.
            template<class SubTupleT, std::size_t Length, class TupleT>
            struct ThroughSubtuple<SubTupleT, Length, Length, TupleT>
            {


                static void EnsureSameLength(const TupleT&, std::size_t);


            };


            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Internal/Subtuple/Subtuple.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_SUBTUPLE_x_SUBTUPLE_HPP_
