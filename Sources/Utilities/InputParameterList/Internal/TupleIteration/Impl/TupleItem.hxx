///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Impl
            {



                template<class TupleEltTypeT>
                template<class T>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const std::string& fullname,
                                                     ExtendedOps* ops,
                                                     Utilities::Type2Type<T>)
                {
                    assert(ops != nullptr);
                    element.SetValue(ops->Get<typename TupleEltTypeT::ops_type>(fullname, TupleEltTypeT::Constraint()));
                }


                template<class TupleEltTypeT>
                template<class T, typename ...Args>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const std::string& fullname,
                                                     ExtendedOps* ops,
                                                     Utilities::Type2Type<Utilities::InputParameterListNS::OpsFunction<T(Args...)>>)
                {
                    assert(ops != nullptr);
                    Utilities::InputParameterListNS::OpsFunction<T(Args...)> ops_function(fullname, ops);
                    element.SetValue(std::move(ops_function));

                }


                template<class TupleEltTypeT>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const std::string& fullname,
                                                     ExtendedOps* ops)
                {
                    assert(ops != nullptr);
                    Utilities::Type2Type<typename TupleEltTypeT::return_type> t;
                    Set(element, fullname, ops, t);
                }


            } // namespace Impl


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_
