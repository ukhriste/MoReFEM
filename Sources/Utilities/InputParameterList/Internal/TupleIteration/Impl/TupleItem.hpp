///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HPP_


# include "Utilities/InputParameterList/ExtendedOps.hpp"
# include "Utilities/InputParameterList/OpsFunction.hpp"
# include "Utilities/InputParameterList/Definitions.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Traits/Traits.hpp"
# include "Utilities/String/String.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Impl
            {


                //! Convenient alias to avoid repeating endlessly namespaces.
                using ExtendedOps = Utilities::InputParameterListNS::ExtendedOps;



                /*!
                 * \brief Helper class to set the value of an InputParameter.
                 *
                 * \tparam TupleEltTypeT Current element of the input parameter tuple which value is set.
                 */
                template<class TupleEltTypeT>
                struct ElementType
                {


                    /*!
                     * \class doxygen_hide_input_parameter_list_element_type_args
                     *
                     * \param[in] element Current tuple element which value is set.
                     * \param[in] fullname Name of the key in Ops, with its possible sections included. E.g.
                     * "domain4.unique_id".
                     * \param[in] ops All the values read from the input parameter
                     * file are stored within this Ops object. Not constant due to Ops interface.
                     */

                    /*!
                     * \brief Set the value of \a element from the values read by Ops in the input parameter file.
                     *
                     * \copydoc doxygen_hide_input_parameter_list_element_type_args
                     */
                    static void Set(TupleEltTypeT& element,
                                    const std::string& fullname,
                                    ExtendedOps* ops);


                private:

                    /*!
                     * \brief Internal case to read any value but the OpsFunction ones.
                     *
                     * \copydoc doxygen_hide_input_parameter_list_element_type_args
                     */
                    template<class T>
                    static void Set(TupleEltTypeT& element,
                                    const std::string& fullname,
                                    ExtendedOps* ops,
                                    Utilities::Type2Type<T>);


                    /*!
                     * \brief Internal case to read OpsFunction values (which is a thin wrapper over a Lua function).
                     *
                     * \copydoc doxygen_hide_input_parameter_list_element_type_args
                     */
                    template<class T, typename ...Args>
                    static void Set(TupleEltTypeT& element,
                                    const std::string& fullname,
                                    ExtendedOps* ops,
                                    Utilities::Type2Type<Utilities::InputParameterListNS::OpsFunction<T(Args...)>>);




                };


            } // namespace Impl


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/TupleItem.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HPP_
