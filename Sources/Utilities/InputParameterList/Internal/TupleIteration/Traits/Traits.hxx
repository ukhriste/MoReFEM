///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Traits
            {


                template<class return_type>
                std::string Format<return_type>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "VALUE";
                };




                template<class T>
                std::string Format<std::vector<T>>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "{VALUE1, VALUE2, ...}";
                };



                template<class T>
                std::string Format<Utilities::InputParameterListNS::OpsFunction<T>>
                ::Print(const std::string& indent_comment)
                {
                    std::ostringstream oconv;

                    oconv << "Function in Lua language, for instance:\n "
                    << indent_comment << "function(arg1, arg2, arg3)\n"
                    << indent_comment << "return arg1 + arg2 - arg3\n"
                    << indent_comment << "end\n"
                    << indent_comment << "sin, cos and tan require a 'math.' preffix.\n"
                    << indent_comment << "If you do not wish to provide one, put anything you want (e.g. 'none'): "
                    "the content is not interpreted by Ops until an actual use of the underlying function.";
                    ;

                    return oconv.str();
                };


            } // namespace Traits


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HXX_
