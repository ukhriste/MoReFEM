///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputParameterList/OpsFunction.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/OpsType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Traits
            {


                /*!
                 * \brief Traits class for InputParameter.
                 *
                 * See ops_type comment for more details.
                 */
                template<class ReturnTypeT>
                struct InputParameter
                {
                    //! Yields the closest type of \a ReturnTypeT compatible with Ops.
                    using ops_type = typename Impl::ops_type<ReturnTypeT>::type;
                };


                /*!
                 * \brief Class specializations used in CreateDefaultInputFile() to write the expected
                 * format for a given input parameter.
                 */
                template<class return_type>
                struct Format
                {
                    //! Print the expected format associated to \a return_type.
                    static std::string Print(const std::string& indent_comment);
                };


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                // Specializations.
                // ============================


                template<>
                struct Format<std::string>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<class T>
                struct Format<std::vector<T>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<>
                struct Format<std::vector<std::string>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<class T>
                struct Format<Utilities::InputParameterListNS::OpsFunction<T>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<>
                struct Format<bool>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                template<>
                struct Format<std::vector<bool>>
                {
                    static std::string Print(const std::string& indent_comment);
                };


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            } // namespace Traits


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputParameterList/Internal/TupleIteration/Traits/Traits.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HPP_
