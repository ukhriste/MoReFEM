///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HPP_


# include <unordered_set>
# include <list>
# include <bitset>
# include <tuple>
# include <vector>
# include <map>

# include "Utilities/Containers/Tuple.hpp"
# include "Utilities/String/String.hpp"
# include "Utilities/InputParameterList/Exceptions/InputParameterList.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/StaticIf.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/PrepareDefaultEntry.hpp"

# include "Core/InputParameter/Crtp/Section.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            //! Convenient alias to avoid repeating endlessly namespaces.
            using Nature = Utilities::InputParameterListNS::Nature;

            //! Convenient alias to avoid repeating endlessly namespaces.
            using ExtendedOps = Utilities::InputParameterListNS::ExtendedOps;



            /*!
             * \brief The struct in charge of depth management.
             *
             * The tuple upon which TupleIteration is expected to work may contain twio kind of items:
             * - End parameter, which values are read fron the input file and stored.
             * - Sections, that in turn may contain other sections and/or end parameters.
             *
             * The purpose of current class is to handle each of these cases. If this is a section, another TupleIteration
             * is called to deal with tuple related to this section. Once it's done, work upon the first tuple
             * is resumed.
             *
             * \tparam TupleEltTypeT SectionOrParameter should be called only by TupleIteration template class;
             * each of this call is on one of its tuple element, which type is given by current template parameter.
             * \tparam NatureT Whether the current tuple element is a end parameter or a section.
             * \tparam InputParameterTypeT Used only when a given type is sought within the tuple; otherwise
             * just take the same as \a TupleEltTypeT. Only the Find() and ExtractValue() static methods are defined in
             * the case \a TupleEltTypeT and \a InputParameterTypeT differ.
             *
             * \internal <b><tt>[internal]</tt></b> Due to the circularity involved here, this class must be in the same
             * file as TupleIteration template class. Definition of specializations occurs after the declaration of
             * TupleIteration.
             */
            template<class TupleEltTypeT, Nature NatureT, class InputParameterTypeT = TupleEltTypeT>
            struct SectionOrParameter;



            /*!
             * \brief An helper class used to perform some operations recursively for all elements of \a TupleT
             *
             * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input parameters.
             * \tparam Index Index of the current element considered in the tuple.
             * \tparam Length Number of elements in the tuple. Recursion ends when Index == Length.
             */
            template<class TupleT, unsigned int Index, unsigned int Length>
            struct TupleIteration
            {


                /*!
                 * \brief Extract the value of the \a Index -th element in the \a TupleT tuple.
                 *
                 * \tparam InputParameterT Type of the input paramater considered.
                 *
                 * \param[in] tuple Tuple from which the input parameter is extracted.
                 * \param[out] parameter_if_found Value of the parameter if it is found. If not, an exception is
                 * thrown.
                 */
                template<class InputParameterT>
                static void ExtractValue(const TupleT& tuple,
                                         const InputParameterT*& parameter_if_found);



                /*!
                 * \brief Static method which looks for \a InputParameterT within \a TupleT.
                 *
                 * \tparam InputParameterT Input parameter that is sought in the tuple. It can't be a section!
                 *
                 * \return True when current element of the tuple (at \a Index position) is \a InputParameterT.
                 *
                 */
                template<class InputParameterT>
                static constexpr bool Find();



                /*!
                 * \brief Set the content of the tuple.
                 *
                 * \param[in] ops Ops object that gives the requested informations.
                 * \param[in,out] tuple The tuple into which the information is stored.
                 */
                static void Fill(ExtendedOps* ops, TupleT& tuple);


                /*!
                 * \brief Prepare default entry for a given input parameter.
                 *
                 */
                static void PrepareDefaultEntries(std::vector<std::pair<std::string, std::string>>& parameter_block_per_identifier);

                /*!
                 * \brief Print the list of keys found in the tuple.
                 *
                 * \param[in,out] out Stream onto which the result is printed.
                 */
                static void PrintKeys(std::ostream& out);


                /*!
                 * \brief Collect statistics about usage of input parameters.
                 *
                 * \param[in] tuple Tuple considered.
                 * \param[in,out] identifier_list List of all identifiers met, whether they are used or not.
                 * Depth is taken into account here, so this list might be bigger than tuple size if sections are
                 * involved in the tuple considered.
                 * \param[in,out] usage_statistics This vector is the same size as \a identifier_list and is read
                 * alongside it; it tells whether the parameter was used or not.
                 */
                static void IsUsed(const TupleT& tuple,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);


                /*!
                 * \brief Helper that check current key has not yet been used.
                 *
                 * \param[in,out] keys List of keys found so far. New key will be added if the exception wasn't
                 * triggered first.
                 */
                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& keys);

                /*!
                 * \brief Helper recursive function that looks which tuple match a key found in the input file.
                 *
                 * \param[in] section Section read in the input file.
                 * \param[in] variable Name of the variable read in the input file.
                 *
                 * \return Whether \a variable is in the tuple or not.
                 */
                static bool DoMatchIdentifier(const std::string& section, const std::string& variable);


                /*!
                 * \brief Do something only if current item consider in the tuple is a section.
                 *
                 * \param[in] tuple Tuple considered.
                 * \param[in] action Functor applied to the section.
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 */
                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleT& tuple,
                                         ActionT action,
                                         Args&&... args);


            private:

                //! Convenient alias.
                using tuple_elt_type = typename std::tuple_element<Index, TupleT>::type;

                //! Whether the current element is a parameter or a section.
                static constexpr Nature GetNature() noexcept;

                //! Alias to the next element in the tuple.
                using next_item = TupleIteration<TupleT, Index + 1, Length>;

                //! Convenient alias.
                using section_or_parameter = SectionOrParameter<tuple_elt_type, GetNature()>;

            };


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // Stopping condition of TupleIteration.
            // ============================


            template<class TupleT, unsigned int Length>
            struct TupleIteration<TupleT, Length, Length>
            {

                template<class InputParameterT>
                static void ExtractValue(const TupleT& ,
                                         const InputParameterT*& );


                template<class InputParameterT>
                static constexpr bool Find();



                static void Fill(ExtendedOps* , TupleT& );


                static void PrepareDefaultEntries(std::vector<std::pair<std::string, std::string>>& parameter_block_per_identifier);


                // \todo  TMP
                static void PrintKeys(std::ostream& out);

                static void IsUsed(const TupleT& tuple,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);

                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>&);


                static bool DoMatchIdentifier(const std::string&, const std::string&);

                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleT& tuple,
                                         ActionT action,
                                         Args&&... args);



            };


            // ============================
            // Stopping condition of TupleIteration.
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // SectionOrParameter was hacked into MoReFEM to introduce section management but is pointlessly
            // complicated; it should be refactored at some point to make it clearer and also allow a more direct
            // handle on sections (you can't manipulate directly a section in MoReFEM models).
            // I don't therefore take the time required to redocument all of this.
            // ============================


            template<class TupleEltTypeT, class InputParameterT>
            struct SectionOrParameter<TupleEltTypeT, Nature::section, InputParameterT>
            {

                static void ExtractValue(const TupleEltTypeT& item,
                                         const InputParameterT*& parameter_if_found);


                static constexpr bool Find();

                using section_tuple = typename TupleEltTypeT::section_content_type;

                using tuple_iteration = TupleIteration<section_tuple, 0, std::tuple_size<section_tuple>::value>;

            };


            template<class TupleEltTypeT, class InputParameterT>
            struct SectionOrParameter<TupleEltTypeT, Nature::parameter, InputParameterT>
            {

                static void ExtractValue(const TupleEltTypeT& item,
                                         const InputParameterT*& parameter_if_found);

                static constexpr bool Find();

            };


            //! Specialization of \a SectionOrParameter for sections.
            template<class TupleEltTypeT>
            struct SectionOrParameter<TupleEltTypeT, Nature::section, TupleEltTypeT>
            {



                static constexpr bool Find();


                static void ExtractValue(const TupleEltTypeT& item,
                                         const TupleEltTypeT*& parameter_if_found);


                static void Fill(ExtendedOps* ops, TupleEltTypeT& item);


                static void PrintKeys(std::ostream& out);


                static void IsUsed(const TupleEltTypeT& item,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);

                /*!
                 * \brief Helper that check current key has not yet been used.
                 *
                 * \param[in,out] end_parameter_keys List of keys found so far for end parameters (sections keys are
                 * not considered). New key will be added if the exception wasn't triggered first.
                 *
                 */
                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& end_parameter_keys);



                using section_tuple = typename TupleEltTypeT::section_content_type;

                static bool DoMatchIdentifier(const std::string& section,
                                              const std::string& variable);

                static void PrepareDefaultEntries(std::vector<std::pair<std::string, std::string>>& parameter_block_per_identifier);

                using tuple_iteration = TupleIteration<section_tuple, 0, std::tuple_size<section_tuple>::value>;

                /*!
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 */
                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleEltTypeT& item,
                                         ActionT action,
                                         Args&&... args);

            };


            //! Specialization of \a SectionOrParameter for parameters.
            template<class TupleEltTypeT>
            struct SectionOrParameter<TupleEltTypeT, Nature::parameter, TupleEltTypeT>
            {
                static constexpr bool Find();

                static void ExtractValue(const TupleEltTypeT& item,
                                         const TupleEltTypeT*& parameter_if_found);

                static void Fill(ExtendedOps* ops, TupleEltTypeT& item);

                static void PrintKeys(std::ostream& out);


                static void IsUsed(const TupleEltTypeT& item,
                                   std::vector<std::string>& identifier_list,
                                   std::vector<bool>& usage_statistics);

                /*!
                 * \brief Helper that check current key has not yet been used.
                 *
                 * \param[in,out] end_parameter_keys List of keys found so far for end parameters (sections keys are
                 * not considered). New key will be added if the exception wasn't triggered first.
                 *
                 */
                static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& end_parameter_keys);

                static bool DoMatchIdentifier(const std::string& section,
                                              const std::string& variable);

                static void PrepareDefaultEntries(std::vector<std::pair<std::string, std::string>>& parameter_block_per_identifier);

                /*!
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 */
                template<class SectionTypeT, class ActionT, typename... Args>
                static void ActIfSection(const TupleEltTypeT& item,
                                         ActionT action,
                                         Args&&... args);


            };


            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            /*!
             * \brief Function that will recursively fill the values read in the input file for each input parameter.
             *
             *
             * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input parameters.
             *
             * \param[in] ops Ops object.
             * \param[in,out] tuple Should be the data attribute tuple_ nested in Base class.
             */
            template<class TupleT>
            void FillTuple(ExtendedOps* ops, TupleT& tuple);


            /*!
             * \brief Prepare the entries to put in the default input file.
             *
             * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input parameters.
             *
             * \param[in,out] out Stream onto which entries are written.
             */
            template<class TupleT>
            void PrepareDefaultEntries(std::ostream& out);



        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Internal/TupleIteration/TupleIteration.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_TUPLE_ITERATION_HPP_
