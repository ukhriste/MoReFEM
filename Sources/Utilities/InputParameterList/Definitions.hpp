///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Aug 2015 10:41:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_DEFINITIONS_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_DEFINITIONS_HPP_


# include <string>


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            /*!
             * \brief Placeholder class to use as one of the template parameter for sections and end parameters at root level.
             *
             * Should be used as Crtp::Section or Crtp::InputParameter template argument.
             */
            struct NoEnclosingSection
            {


                //! Returns empty string.
                static const std::string& GetName();

                //! Returns empty string.
                static const std::string& GetFullName();


            };


            //! Enum to tell whether current item of input parameter list is a section or an input parameter.
            enum class Nature
            {
                section = 0,
                parameter
            };


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_DEFINITIONS_HPP_
