///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Apr 2014 11:08:30 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_ENUM_CLASS_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_ENUM_CLASS_HPP_


# include <type_traits>


namespace MoReFEM
{


    /*!
     * \brief Return the underlying integer value behind an enum class member.
     */
    template<typename EnumT>
    constexpr auto EnumUnderlyingType(EnumT enumerator) noexcept
    {
        return static_cast<std::underlying_type_t<EnumT>>(enumerator);
    }


    namespace Utilities
    {


        namespace EnumClass
        {




            /*!
             * \brief Helper class to be able to use enum class in metaprogramming.
             *
             * Enum classes are tricky to use in metaprogramming, as arithmetic can't be used on them: you can't
             * directly call the next instance such as this example from MoReFEM:
             * \code
             * template
             * <
             *     MoReFEM::Advanced::GeometricEltEnum,
             *     Internal::MeshNS::FormatNS::Type N,
             *     Internal::MeshNS::FormatNS::Type EndT
             * >
             * struct IsAtLeastOneFormatSupported
             * {
             * constexpr operator bool() const
             * {
             * return Format::Support<N, IdentifierT>()
             * || IsAtLeastOneFormatSupported<N + 1, EndT>(); // N + 1 doesn't compile!
             * }
             *
             * };
             * \endcode
             *
             * The new class bypass this limitation, with the replacement call hereafter:
             *
             * \code
             * constexpr operator bool() const
             * {
             * return Format::Support<N, IdentifierT>()
             * IsAtLeastOneFormatSupported<Utilities::EnumClass::Iterator<Internal::MeshNS::FormatNS::Type, N>::Increment(), EndT>();
             * }
             * \endcode
             *
             * or to make it prettier with an alias
             *
             * \code
             * using CurrentType = Utilities::EnumClass::Iterator<Internal::MeshNS::FormatNS::Type, N>;
             *
             * constexpr operator bool() const
             * {
             * return Format::Support<N, IdentifierT>()
             * || IsAtLeastOneFormatSupported<CurrentType::Increment(), EndT>();
             * }
             * \endcode
             *
             * \tparam EnumT Must be an enum (but in this case N + 1 above was fine as well) or an enum class. It is expected
             * its internal layout is:
             * \code
             * enum class T { Begin, *MyFirstElt* = Begin, ..., End };
             * \endcode
             * \tparam CurrentValueT Current value of the enum.
             */

            template<class EnumT, EnumT CurrentValueT>
            struct Iterator
            {

                //! Returns the value immediately after \a CurrentValueT.
                static constexpr EnumT Increment()
                {
                    static_assert(CurrentValueT >= EnumT::Begin, "The input type is invalid!");
                    static_assert(CurrentValueT < EnumT::End,
                                  "Incrementing this Internal::MeshNS::FormatNS::Type object would make it go out of bounds!");
                    return static_cast<EnumT>(EnumUnderlyingType(CurrentValueT) + 1);
                }


                //! Returns the value immediately before \a CurrentValueT.
                static constexpr EnumT Decrement()
                {
                    static_assert(CurrentValueT > EnumT::Begin, "Could not decrement when starting at Begin or below!");
                    static_assert(CurrentValueT <= EnumT::End,
                                  "Decrementing an Iterator outside of boundaries is meaningless...");
                    return static_cast<EnumT>(EnumUnderlyingType(CurrentValueT) - 1);
                }


            };



        } // namespace EnumClass


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_ENUM_CLASS_HPP_
