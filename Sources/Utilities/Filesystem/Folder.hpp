///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 3 Oct 2013 14:58:15 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FOLDER_HPP_
# define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FOLDER_HPP_

# include <string>


namespace MoReFEM
{


    namespace FilesystemNS
    {


        namespace Folder
        {


            /*!
             * \brief Tells whether a folder exists or not.
             *
             * \param[in] folder Name of the folder under investigation.
             *
             * \return True if \a folder exists.
             */
            bool DoExist(const std::string& folder);


            /*!
             * \brief Create a folder.
             *
             * This function currently creates it if possible, creating in the process intermediate directory
             * that might not exist.
             *
             * For instance:
             *
             \code
             Create("/tmp/foo/bar", __FILE__, __LINE__);
             \endcode
             * will create directory foo if it doesn't exist yet.
             *
             * An exception is thrown if the operation failed.
             *
             * \param[in] folder Name of the folder to be created.
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            void Create(const std::string& folder, const char* invoking_file, int invoking_line);


            /*!
             * \brief Delete a folder.
             *
             * An exception is thrown if the operation failed or if it doesn't exist.
             *
             * \param[in] folder Name of the folder to be deleted.
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            void Remove(const std::string& folder, const char* invoking_file, int invoking_line);


        } // namespace Folder


    } // namespace FilesystemNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FOLDER_HPP_
